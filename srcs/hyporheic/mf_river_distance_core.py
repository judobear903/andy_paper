"""
計算伏流水高度關聯的地下水區域
流量 6 ~ 48 cms
河道寬度 10 - 50 m
slope=0.005
manning_n = 0.013
底泥 1 m

地下水部分
地表至地下 50 m
模擬範圍 寬 10 m
        長 5100 m   (與河川垂直)
        100 公尺作為河川模擬用
網格尺寸 10 m x 10 m
地下水深 5-20 m
    最遠處採定水頭
    初始條件為
含水層   K 0.5 m/day ~ 50 m/day
河川底泥 K 0.5 m/day ~ 50 m/day
Sy 0.05 ~ 0.03

變因
1. 地下水深 
2. 河川平均流量
3. 地下水 K
4. 河川底泥 K

Conductance = K / L * A
A 為河長河寬
L 為底泥厚度

ibound 最右側, const. head
"""

import os
import sys
import numpy as np
from scipy import stats
import pandas as pd
import flopy
import math
import matplotlib.pyplot as plt
from typing import List, Dict, Tuple
import matplotlib.dates as mdates
from multiprocessing import current_process
import datetime
from scipy import signal
import pickle
import time

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jutility as jut
import plt_parameters  # noqa: F401
import ts_process_fft_v2 as tsfft
import time_phrase
import file_utility as fut
import parallel_framework
import cmap_define
import polar_coord

sys.path.append(
    os.path.join("..", "..", "mf_process", "srcs", "mf_utility")
)
# import mf_utility
import mf_execute
import mf_utility

import open_channel
import mf_params as mfp
import mf_river_utility as mfu


def plot_mf_cell(mf, fig_fname: str):
    _fig, ax = plt.subplots(1, figsize=(60, 3))
    plt.rcParams.update({"font.size": 54})
    riv_stage = mf.get_package("RIV").stress_period_data.array[
        "stage"
    ][1, 0, :, :]
    riv_stage = np.where(riv_stage == riv_stage[1, 1], 1, np.NaN)

    mapview = flopy.plot.PlotMapView(model=mf)
    mapview.plot_ibound(
        alpha=0.7,
        color_noflow="darkgray",
        color_ch="darkgoldenrod",
    )  # ibound
    # mapview.plot_bc("RIV", color="royalblue", alpha=1)
    mapview.plot_array(riv_stage)
    mapview.plot_grid(linewidth=1)  # grid

    plt.tight_layout()
    jut.save_fig(fig_fname)
    plt.rcParams.update({"font.size": 12})


def mf_simulation_framework(
    flags,
    results,
    mf_params,
    df_variable,
    pickle_fname,
    **kwargs,
):
    """
    1. 逐段平行計算 MF 模型
    2. 繪製完成案例的成果
    """
    root_logger = kwargs.get("root_logger", None)

    with open(
        os.path.join(
            kwargs["base_ws"], "mf_case_size_count.csv"
        ),
        "w",
    ) as f:
        f.write("{},{}\n".format(datetime.datetime.now(), 0))
        if os.path.exists(
            os.path.join(kwargs["base_ws"], "mf_case.csv")
        ):
            num_lines = sum(
                1
                for _ in open(
                    os.path.join(
                        kwargs["base_ws"], "mf_case.csv"
                    )
                )
            )
            f.write(
                "{},{}\n".format(
                    datetime.datetime.now(),
                    num_lines - 1,
                )
            )
    plot_case_size_count(df_variable, **kwargs)

    loop_size_outter = kwargs["process_size"] * 16
    loop_count = 0
    for i in range(0, len(flags), loop_size_outter):
        # 分段進行平行運算
        flags2 = []
        for j in range(i, min(i + loop_size_outter, len(flags))):
            # if flags[j][0] not in results:
            # flags[j][0] not in results 用來判斷過往有計算
            flags2.append(
                flags[j] + [flags[j][0] not in results]
            )
            assert len(flags2[-1]) == 9, flags2[-1]
            j2 = j - i
            assert len(flags2) == j2 + 1
            # flags[-2] 為 kwargs

            for my_item in ["log_parallel"]:
                try:
                    flags2[j2][-2][my_item] = kwargs[my_item]
                except IndexError as e:
                    raise IndexError(
                        "{}: {} / {}".format(
                            type(flags2[j2]), len(flags2[j2]), j2
                        )
                    ) from e

        # flags2 = flags[i: min(i + loop_size_outter, len(flags))]
        if len(flags2) > 0:
            message = (
                "MF loop: {}. from {} to {} / size={}".format(
                    loop_count,
                    i,
                    min(i + loop_size_outter, len(flags)),
                    loop_size_outter,
                )
            )
            if root_logger is not None:
                root_logger.info(message)
                root_logger.debug("  --> MODFLOW simulation")
            mat = parallel_framework.parallel_process_framework(
                mf_river_corr_shell,
                flags2,
                log_parallel=kwargs["log_parallel"],
                **{
                    key: elem
                    for key, elem in kwargs.items()
                    if key in ["root_logger", "process_ratio"]
                },
            )

            ###############################################################
            # 完成 flags2 的計算
            # 繪製 完成案例的成果
            # plot_case_size_count(**kwargs)
            flags3 = []
            log_refresh = False
            for i, elem in enumerate(flags2):
                index = elem[0]  # index
                if mat[i] is not None:
                    log_refresh = True
                    # mf_river_corr_shell 先檢查是否完成計算,
                    # 若完成計算, 不重複計算, 直接回傳 None
                    # 如果完成計算, 但是模型有收斂問題, 導致可用模擬水位數量不足, 直接回傳 None
                    results[index] = {
                        "IC": elem[1],
                        "Kh": elem[2],
                        "Sy": elem[3],
                        "RIV_Kh": elem[4],
                        "qflow_range": elem[5],
                        "riv_width": elem[6],
                        "df_strt": mat[i][0],
                        "df_corrcoef": mat[i][1],
                        "df_scipy_signal": mat[i][2],
                        "df_amplitude_ratio": mat[i][3],
                        "mf": mat[i][-1],
                    }
                    assert "mf" in results[index]

                    ###############################################################
                    # 繪製模型架構
                    mf_cell_fname = os.path.join(
                        kwargs["base_ws"],
                        "mf_cell_structure",
                    )
                    if not os.path.exists(
                        "{}.png".format(mf_cell_fname)
                    ):
                        assert (
                            "mf" in results[index]
                        ), "{}: 'mf' not in {}".format(
                            index, results[index].keys()
                        )
                        if root_logger is not None:
                            root_logger.debug(
                                "  --> MODFLOW structure creation"
                            )
                        plot_mf_cell(
                            results[index]["mf"], mf_cell_fname
                        )
                if index in results:
                    flags3.append(
                        [
                            results,
                            index,
                            mf_params,
                            {
                                "root_logger": root_logger,
                                "log_plot_others": False,
                                "base_ws": kwargs["base_ws"],
                            },
                        ]
                    )
            if root_logger is not None:
                root_logger.debug("  --> MODFLOW post analysis")
            if kwargs.get("log_single_case_plot", False):
                parallel_framework.parallel_process_framework(
                    plot_mf_post_shell,
                    flags3,
                )
            if log_refresh:
                if root_logger is not None:
                    root_logger.debug(
                        "  --> Export pickle: {} / size={}".format(
                            pickle_fname, len(results)
                        )
                    )
                # 備份 pickle 檔案, 再 dump 資料
                pickle_backup_fname = pickle_fname.replace(
                    ".pickle", "_bak.pickle"
                )
                os.system(
                    "cp -rf {} {}".format(
                        pickle_fname, pickle_backup_fname
                    )
                )
                dt1 = datetime.datetime.now()
                with open(pickle_fname, "wb") as f:
                    pickle.dump(results, f)
                dt2 = datetime.datetime.now()
                if root_logger is not None:
                    root_logger.debug(
                        "  Pickle dumping 耗時: {}".format(
                            dt2 - dt1
                        )
                    )
            loop_count += 1

        ###############################################################
        # 刪除 MF 目錄
        if not mf_params["log_mf_undelete"]:
            for i in range(len(mat)):
                if mat[i] is not None:
                    try:
                        fut.remove_file(mat[i][-2])  # workspace
                    except FileNotFoundError:
                        pass
    pickle_backup_fname = pickle_fname.replace(
        ".pickle", "_bak.pickle"
    )
    try:
        fut.remove_file(pickle_backup_fname)
    except FileNotFoundError:
        pass
    return results


# 隨機產生
# 變因：地下水深, Kh, Sy, 底泥 Kh, 流量, 河寬
def variable_generate(
    r, vrange: Tuple, loglog: bool = False
) -> float:
    assert isinstance(vrange, tuple)
    assert len(vrange) == 2
    vari = (vrange[1] - vrange[0]) * r + vrange[0]

    if loglog:
        # log scale
        vrange2 = np.log10(vrange)
        vari = np.power(
            10, (vrange2[1] - vrange2[0]) * r + vrange2[0]
        )
    return vari


def mf_case_setup_preprocess(mf_params, df_variable, **_kwargs):
    ###############################################################
    # 建立豐枯水期的流量變化
    ###############################################################
    tlist, df_qflow_ratio = determine_qflow_variation()

    # 建立模型基本設定
    log_parallel = True
    (
        base_ws,
        Lx,
        Ly,
        nrow,
        ncol,
        nlay,
        delr,
        delc,
        ztop,
        zbot,
        ibound,
    ) = mfp.mf_predefine(mf_params)
    kwargs = {
        "dis_size": (nlay, nrow, ncol),
        "cell_length": (delr, delc),
        "zparams": (
            ztop,
            zbot,
            np.linspace(ztop, zbot, nlay + 1),
        ),
        "ibound": ibound,
        "base_ws": base_ws,
        "log_parallel": log_parallel,
        "rbot": mf_params["RIV"]["rbot"],
        "tlist": tlist,
        "df_qflow_ratio": df_qflow_ratio,
        **_kwargs,
    }

    os.makedirs(base_ws, exist_ok=True)
    # 紀錄所有case執行進度
    if not os.path.exists(os.path.join(base_ws, "mf_case.csv")):
        with open(
            os.path.join(base_ws, "mf_case.csv"), "w"
        ) as f:
            f.write("time, index, index2\n")
    if "root_logger" in kwargs:
        kwargs["root_logger"].debug("Create workspace folder")

    if mf_params["case_index"][0] == "all":
        mf_params["case_index"] = df_variable.index
    else:
        mf_params["case_index"] = [
            int(elem) for elem in mf_params["case_index"]
        ]

    flags = []
    for index in mf_params["case_index"]:
        flag_sub = (
            [
                index,
            ]
            + [
                df_variable.loc[index, col]
                for col in df_variable.columns
            ]
            + [
                kwargs,
            ]
        )
        flags.append(flag_sub)
        assert len(flags[-1]) == 8, "{}: {} / {} / {}".format(
            index,
            len(flags[-1]),
            flags[-1],
            df_variable,
        )

    pickle_fname = os.path.join(base_ws, "mf_corrcoef.pickle")
    results = {}
    if os.path.exists(pickle_fname):
        pickle_backup_fname = pickle_fname.replace(
            ".pickle", "_bak.pickle"
        )
        if _kwargs.get("root_logger", None) is not None:
            _kwargs.get("root_logger", None).debug(
                "Load pickle file: {}".format(pickle_fname)
            )
        try:
            with open(pickle_fname, "rb") as f:
                results = pickle.load(f)
        except EOFError:
            # 取回備份的檔案
            if _kwargs.get("root_logger", None) is not None:
                _kwargs.get("root_logger", None).debug(
                    "  --> EOFError: Replaced by the previous pickle file: {}".format(
                        pickle_backup_fname
                    )
                )
            os.system(
                "cp -rf {} {}".format(
                    pickle_backup_fname, pickle_fname
                )
            )
            with open(pickle_fname, "rb") as f:
                results = pickle.load(f)

    try:
        fut.remove_file(
            os.path.join(
                kwargs["base_ws"], "mf_case_size_count.csv"
            )
        )
    except FileNotFoundError:
        pass
    return flags, results, kwargs, pickle_fname


def determine_qflow_variation():
    ###############################################################
    # 建立豐枯水期的流量變化
    ###############################################################
    tlist = np.array(
        [
            t
            * time_phrase.timeflag2timedelta(
                mfp.mf_params["DIS"]["tdelta"]
            )
            for t in range(mfp.mf_params["DIS"]["tsize"] + 1)
        ]
    )

    rad = []
    for t in tlist:
        rad.append(
            (
                t
                / time_phrase.timeflag2timedelta(
                    mfp.mf_params["DIS"]["tdelta"]
                )
                / (36 / 2)
                - 1
            )
            * math.pi
        )
        rad[-1] = rad[-1] % (2 * math.pi)
    df_qflow_ratio = pd.DataFrame(
        np.ones(mfp.mf_params["DIS"]["tsize"] + 1)
        + 0.9 * np.cos(rad),
        columns=["qr"],
        index=tlist,
    )
    return tlist, df_qflow_ratio


###############################################################
def plot_mf_post_shell(flag):
    return plot_mf_post(*tuple(flag[:-1]), **flag[-1])


def plot_mf_post(results, index, mf_params, **kwargs):
    root_logger = kwargs.get("root_logger", None)
    # 計算完成案例的成果繪圖
    result_keys = list(results.keys())
    for key, plot_elem in mfu.plot_elems.items():

        xlim = [-50, 5050]
        ylim = plot_elem[4]
        case_size = 80

        kwargs_plot = {
            "xlim": xlim,
            "ylim": ylim,
            "plot_elem": plot_elem,
            "case_size": case_size,
        }

        def plot_line_all(index, ax, **kwargs2):
            if kwargs.get("log_plot_others", True):
                for i in range(case_size):
                    if i != index:
                        try:
                            mfu.plot_line_single(
                                ax,
                                mf_result=results[
                                    result_keys[i]
                                ],
                                color="k",
                                linestyle="-",
                                linewidth=0.5,
                                alpha=0.4,
                                log_plot_criteria=False,
                                log_annotate_message=False,
                                root_logger=root_logger,
                                **kwargs_plot,
                                **{
                                    key: elem
                                    for key, elem in kwargs2.items()
                                    if key not in ["color"]
                                },
                            )
                        except IndexError:
                            pass

            # 主要的案例
            mfu.plot_line_single(
                ax,
                index,
                mf_result=results[result_keys[index]],
                log_plot_criteria=kwargs2.get(
                    "log_plot_criteria", True
                ),
                log_annotate_message=kwargs2.get(
                    "log_annotate_message", True
                ),
                alpha=0.7,
                **kwargs_plot,
                **{
                    key: elem
                    for key, elem in kwargs2.items()
                    if key
                    not in [
                        "log_plot_criteria",
                        "log_annotate_message",
                    ]
                },
            )
            return index

        # 繪製靜態圖
        if kwargs.get("log_plot_compare", False):
            # 繪製不同案例的比較圖
            colors = cmap_define.bmh_colors
            fig_fname = os.path.join(
                kwargs["base_ws"],
                "pics",
                key,
                "mf_{}_compare".format(key),
            )

            if not os.path.exists(
                "{}.{}".format(fig_fname, plot_elem[6])
            ):
                if root_logger is not None:
                    message = "  --> {} / {}".format(
                        key, os.path.basename(fig_fname)
                    )
                    root_logger.debug(message)

                result_keys = list(results.keys())
                if plot_elem[0] in [
                    "df_corrcoef",
                    "df_amplitude_ratio",
                    "df_scipy_signal",
                ]:
                    plt.style.use("bmh")
                    _fig, ax = plt.subplots(1, figsize=(9, 8))
                    for i, key in enumerate(result_keys):
                        i = plot_line_all(
                            i,
                            ax,
                            color=colors[i % len(colors)],
                            log_plot_criteria=False,
                            log_annotate_message=True,
                            **kwargs,
                        )
                    ax.set_xlim(xlim)
                    ax.set_ylim(ylim)
                    plt.tight_layout()
                    jut.save_fig(fig_fname)
                elif plot_elem[0] in ["df_strt"]:
                    # 繪製水位動畫
                    if index < 4:
                        mfu.plot_strt_animation_compare(
                            results,
                            "{}.{}".format(
                                fig_fname, plot_elem[6]
                            ),
                        )

                    xlim = [-50, 5050]
                    ylim = mfu.determine_ylim(results)
                    for tindex in [0, 6, 12, 18, 24, 30, 36]:
                        fig_fname = "{}_{}".format(
                            fig_fname,
                            str(tindex).rjust(4, "0"),
                        )
                        if not os.path.exists(
                            "{}.png".format(fig_fname)
                        ):
                            plt.style.use("bmh")
                            _fig, ax = plt.subplots(
                                1, figsize=(9, 8)
                            )
                            mfu.plot_strt(
                                tindex,
                                xlim,
                                ylim,
                                results,
                                ax,
                                **kwargs,
                            )
                            plt.tight_layout()
                            jut.save_fig(fig_fname)
        else:
            # 針對特定案例, 對應的 index 為主要重點
            # 以紅粗線呈現, 其餘案例則以細黑線
            fig_fname = os.path.join(
                kwargs["base_ws"],
                "pics",
                key,
                "mf_{}_{}".format(key, str(index).rjust(4, "0")),
            )

            if not os.path.exists(
                "{}.{}".format(fig_fname, plot_elem[6])
            ):

                if plot_elem[0] in [
                    "df_corrcoef",
                    "df_amplitude_ratio",
                    "df_scipy_signal",
                ]:
                    if root_logger is not None:
                        message = "  --> {} / {}".format(
                            key,
                            "{}.{}".format(
                                fig_fname, plot_elem[6]
                            ),
                        )
                        root_logger.debug(message)
                    plt.style.use("bmh")
                    _fig, ax = plt.subplots(1, figsize=(9, 8))
                    index = plot_line_all(
                        index,
                        ax,
                        color="firebrick",
                        **kwargs,
                    )
                    ax.set_xlim(xlim)
                    ax.set_ylim(ylim)
                    plt.tight_layout()
                    jut.save_fig(fig_fname)
                elif plot_elem[0] in ["df_strt"]:
                    if index < 5:
                        if root_logger is not None:
                            message = "  --> {} / {}".format(
                                key,
                                "{}.{}".format(
                                    fig_fname, plot_elem[6]
                                ),
                            )
                            root_logger.debug(message)
                        # 繪製水位動畫
                        mfu.plot_strt_animation(
                            results,
                            result_keys[index],
                            "{}.{}".format(
                                fig_fname, plot_elem[6]
                            ),
                        )


def determine_amplitude_ratio(
    elem: Dict, **kwargs
) -> pd.DataFrame:
    """
    計算隨著距離的地下水 cross correlation
    """
    df_strt = elem["df_strt"]
    df_strt2 = df_strt.copy()
    df_strt2.index = [
        datetime.datetime(2020, 1, 1, 0, 0, 0) + index
        for index in df_strt.index
    ]
    tri_h_list = elem["riv"]
    df_riv = pd.DataFrame(tri_h_list, columns=["h"])

    mat = []
    column1 = df_strt.columns[10]
    # 河水位
    # Tstr 為數據間隔
    fft1 = tsfft.TSP_fft(df_riv).get_fft(
        col_index=0, Tstr=mfp.mf_params["DIS"]["tdelta"]
    )
    # 取出 1.9 ~ 2.1 cycle/day 區間的最大值
    amp1 = np.mean(
        fft1.get_yf()[
            fft1.find_xf_index(
                kwargs["amp_range"][0]
            ) : fft1.find_xf_index(kwargs["amp_range"][1])
        ]
    )
    mat = []
    ncol = kwargs["ncol"]
    xloc = mfu.determine_xloc(
        ncol, mfp.mf_params["DIS"]["cell_size"][0]
    ) - 100 * np.ones(ncol)
    for i in range(11, df_strt.shape[1] - 1):
        fft2 = tsfft.TSP_fft(
            df_strt2.loc[:, [df_strt.columns[i]]]
        ).get_fft(
            col_index=0, Tstr=mfp.mf_params["DIS"]["tdelta"]
        )
        amp2 = np.mean(
            fft2.get_yf()[
                fft2.find_xf_index(
                    kwargs["amp_range"][0]
                ) : fft2.find_xf_index(kwargs["amp_range"][1])
            ]
        )
        mat.append(
            [xloc[i], amp1, amp2, np.round(amp2 / amp1, 4)]
        )

    df_amp_ratio = pd.DataFrame(
        mat, columns=["dist", "amp1", "amp2", "amp_ratio"]
    ).set_index("dist")
    return df_amp_ratio


def determine_scipy_signal(elem: Dict, **kwargs) -> pd.DataFrame:
    """
    以 scipy.signal 來計算 cross correlation
    """
    df_strt = elem["df_strt"]
    tri_h_list = elem["riv"]
    df_riv = pd.DataFrame(tri_h_list, columns=["h"])
    mat = []
    vlist1 = np.array(df_riv.iloc[:, 0].values)  # river
    mat = []
    ncol = kwargs["ncol"]
    xloc = mfu.determine_xloc(
        ncol, mfp.mf_params["DIS"]["cell_size"][0]
    ) - 100 * np.ones(ncol)
    for i in range(11, df_strt.shape[1] - 1):
        vlist2 = np.array(
            df_strt.loc[:, [df_strt.columns[i]]].values
        ).reshape((-1))
        corr = signal.correlate(
            vlist1 - np.mean(vlist1),
            vlist2 - np.mean(vlist2),
            method="fft",
        )
        lags = signal.correlation_lags(len(vlist1), len(vlist2))
        corr /= np.max(corr)

        arg_lags_positive = np.argwhere(lags >= 0)
        corr2 = corr[arg_lags_positive[0][0] :]
        lags2 = lags[arg_lags_positive[0][0] :]

        max_corr = np.nanmax(corr2)
        arg = np.argwhere(corr2 == max_corr)

        if len(arg) > 0:
            try:
                mat.append(
                    [
                        xloc[i],
                        max_corr,
                        lags2[arg[0][0]],
                    ]
                )
            except IndexError as e:
                raise IndexError(
                    "{} / {} ({} / {})".format(
                        len(lags2), arg, max_corr, corr2
                    )
                ) from e
    df_scipy_signal = pd.DataFrame(
        mat, columns=["dist", "corr", "lags"]
    ).set_index("dist")
    return df_scipy_signal


def mf_river_corr_core(*arg, **kwargs):
    """
    呼叫 MF
    kwargs = {
        "dis_size": (nlay, nrow, ncol),
        "cell_length": (delr, delc),
        "zparams": (ztop, zbot, botm),
        "ibound": ibound,
        "base_ws": base_ws,
        "log_parallel": log_parallel,
        "process_size": process_size,
    }

    """
    assert len(arg) == 8
    root_logger = kwargs.get("root_logger", None)
    (
        index,
        strt_longend,
        Kh,
        Sy,
        RIV_Kh,
        qflow_range,
        riv_width,
        log_new_mf,
    ) = arg
    df_qflow_ratio = kwargs["df_qflow_ratio"]

    df_strt = pd.DataFrame([])
    df_corrcoef = pd.DataFrame([])
    df_scipy_signal = pd.DataFrame([])
    df_amplitude_ratio = pd.DataFrame([])
    tri_h_list = []
    workspace = None
    mf = None
    if log_new_mf:
        (nlay, nrow, ncol) = kwargs["dis_size"]
        (delr, delc) = kwargs["cell_length"]
        (ztop, _zbot, botm) = kwargs["zparams"]
        tlist = kwargs["tlist"]

        index2 = index % kwargs["process_size"]
        if kwargs["log_parallel"]:
            # 如果為平行
            p = current_process()
            index2 = (p._identity[0] - 1) % kwargs[
                "process_size"
            ]

        message_index = "(case_id={}, {}".format(index, index2)
        if kwargs["log_parallel"]:
            message_index += " / parallel_id={}".format(
                current_process()._identity[0]
            )
        message_index += ")"
        if root_logger is not None:
            root_logger.debug(
                "MF case: {}".format(message_index)
            )

        base_ws = kwargs["base_ws"]
        workspace = os.path.join(base_ws, str(index))
        name = "tutorial01_mf"
        mf = flopy.modflow.Modflow(
            name, exe_name="mf2005", model_ws=workspace
        )

        # MF 單位採用 m/hr
        # DIS
        steady = [False for _ in range(tlist.shape[0] - 1)]
        _dis = flopy.modflow.ModflowDis(
            mf,
            nlay,
            nrow,
            ncol,
            delr=delr,
            delc=delc,
            top=ztop,
            botm=botm[1:],
            nper=tlist.shape[0] - 1,
            perlen=10,  # 10 day, 旬
            steady=steady,
        )

        # BAS
        # strt = IC * np.ones((nlay, nrow, ncol))  # 初始水位
        strt = np.zeros((nlay, nrow, ncol))
        for k in range(nlay):
            for j in range(nrow):
                strt[k, j, :] = np.linspace(
                    0, strt_longend, ncol
                )

        _bas = flopy.modflow.ModflowBas(
            mf,
            ibound=kwargs["ibound"],
            strt=strt,
            hnoflo=-999.0,
            stoper=0.001,
        )

        # LPF
        _lpf = flopy.modflow.ModflowLpf(
            mf,
            hk=Kh,
            vka=1.0,
            sy=Sy,
            ss=1e-4,
            laytyp=1,
            ipakcb=53,
        )
        _pcg = flopy.modflow.ModflowPcg(mf)

        # RIV
        stress_period_data = {}
        for t in range(1, df_qflow_ratio.shape[0]):
            OC = open_channel.open_channel(
                channel_width=riv_width,
                slope=mfp.mf_params["RIV"]["slope"],
                manning_n=mfp.mf_params["RIV"]["manning_n"],
            )
            # normal_h = OC.determine_normal_depth(
            #    df_qflow_ratio.loc[t, "qr"] * qflow_range
            # )  # cms
            """
            運算概念說明
            1. 以三角形渠道，下方角度為60度, math.pi / 3
            2. 以三角形渠道搭配曼寧式計算對應的水深與濕潤寬度
                高流量的水深與寬度變大
            3. 寬度部分, 輸入Conductance
            水深部分, 輸入stage


            河寬為 riv_width
            渠道深度為 10 m

            以此計算頂部角度
            """
            length = math.sqrt(
                math.pow(riv_width / 2, 2) + math.pow(10, 2)
            )
            rad_top = polar_coord.sin_cos2rad(
                riv_width / 2 / length, 10 / length
            )
            tri_h = OC.determine_triangle_depth(
                df_qflow_ratio.loc[df_qflow_ratio.index[t], "qr"]
                * qflow_range,
                rad_top,
            )  # cms
            tri_width = OC.determine_triangle_width(
                tri_h, math.pi / 3
            )
            tri_h_list.append(tri_h)

            # 整個河面積 100 * 10 = 1000
            # riv_cell_num 為河川網格數量
            #   河川網格配置在前 100 m, 以此算出數量
            riv_cell_num = int(
                100 / mfp.mf_params["DIS"]["cell_size"][0]
            )
            river_area = (
                mfp.mf_params["DIS"]["cell_size"][0] * tri_width
            )
            cond = RIV_Kh * (river_area / riv_cell_num)
            stress_period_data[t] = [
                [
                    0,
                    row,
                    col,
                    tri_h,
                    cond,
                    kwargs["rbot"],
                ]
                for row in range(1, nrow - 1)
                for col in range(riv_cell_num)
            ]

        _riv = flopy.modflow.ModflowRiv(
            mf,
            stress_period_data=stress_period_data,
            ipakcb=110,
        )

        stress_period_data = {}
        for kper in range(tlist.shape[0] - 1):
            for kstp in range(1):
                stress_period_data[(kper, kstp)] = [
                    "save head",
                    "save drawdown",
                    "save budget",
                    "print head",
                    "print budget",
                ]
        _oc = flopy.modflow.ModflowOc(
            mf,
            stress_period_data=stress_period_data,
            extension=["oc", "hds", "cbc"],
            unitnumber=[14, 51, 53],
            compact=True,
        )
        # Write the model input files
        # if root_logger is not None:
        #    root_logger.debug("{}: MF writing & running".format(message_index))
        mf.write_input()

        # MF running
        mf_execute.run_modflow_multiple_ways(
            mf,
            modelws=workspace,
            exec="mf_owhm_2_1",
            nam_fname=name,
        )

        # if root_logger is not None:
        #    root_logger.debug("{}: MF post analysis".format(message_index))
        # Post Analysis
        hds_fname = os.path.join(
            workspace, "{}.hds".format(name)
        )

        strt: List = []
        try:
            strt = mf_utility.read_hds(
                hds_fname, "ALL", mask_val=-999.0
            )
        except OSError:
            time.sleep(5)
            strt = mf_utility.read_hds(
                hds_fname, "ALL", mask_val=-999.0
            )
        except ValueError:
            return None

        view_point = []
        for i in range(ncol):
            view_point.append((0, 1, i))
        mat = []
        for t, elem in enumerate(strt):
            mat_sub = [
                elem[vp[0], vp[1], vp[2]] for vp in view_point
            ]
            mat.append([t] + mat_sub)

        df_strt = pd.DataFrame(
            mat, columns=["time"] + [vp[2] for vp in view_point]
        ).set_index("time")
        try:
            df_strt.index = df_qflow_ratio.index[1:]
        except ValueError:
            # ValueError: Length mismatch: Expected axis has 49 elements, new values have 512 elements
            # 可用的地下水模擬數量不足
            return None

        # 計算水位的相關性變化
        mat = []
        # mask = df_strt.index > datetime.timedelta(days=360)
        column1 = df_strt.columns[10]
        dlist1 = np.array(tri_h_list)
        xloc = mfu.determine_xloc(
            ncol, mfp.mf_params["DIS"]["cell_size"][0]
        ) - 100 * np.ones(ncol)
        for i in range(11, ncol - 1):
            mat.append(
                [
                    xloc[i],  # 網格中心
                    stats.pearsonr(
                        dlist1,
                        np.array(
                            df_strt.loc[
                                :, df_strt.columns[i]
                            ].values
                        ),
                    ).statistic,
                ]
            )
        df_corrcoef = pd.DataFrame(
            mat, columns=["dist", "corr"]
        ).set_index("dist")

        kwargs2 = {
            "ncol": ncol,
            "amp_range": [1.0 / 380, 1.0 / 340],  # cycle/day,
        }
        df_scipy_signal = determine_scipy_signal(
            {"df_strt": df_strt, "riv": np.array(tri_h_list)},
            **kwargs2,
        )
        df_amplitude_ratio = determine_amplitude_ratio(
            {"df_strt": df_strt, "riv": np.array(tri_h_list)},
            **kwargs2,
        )

        with open(
            os.path.join(kwargs["base_ws"], "mf_case.csv"), "a"
        ) as f:
            message = "{}, {}, {}".format(
                datetime.datetime.now(),
                index,
                index2,
            )
            if kwargs["log_parallel"]:
                message += ", {}".format(
                    current_process()._identity[0],
                )
            f.write(
                "{}\n".format(
                    message,
                )
            )
        if index % kwargs["process_size"] == 0:
            # 只在 pid == 0 處理
            with open(
                os.path.join(
                    kwargs["base_ws"], "mf_case_size_count.csv"
                ),
                "a",
            ) as f2:

                num_lines = sum(
                    1
                    for _ in open(
                        os.path.join(
                            kwargs["base_ws"], "mf_case.csv"
                        )
                    )
                )
                f2.write(
                    "{},{}\n".format(
                        datetime.datetime.now(),
                        num_lines - 1,
                    )
                )
    else:
        # 重複模型, 不需要重新計算
        return None

    return (
        df_strt,
        df_corrcoef,
        df_scipy_signal,
        df_amplitude_ratio,
        workspace,
        mf,
    )


def mf_river_corr_shell(flag: List):
    """
    呼叫 MF 計算河流對地下水系統的影響程度與距離
    """
    return mf_river_corr_core(
        *tuple(flag[:-2] + [flag[-1]]), **flag[-2]
    )


def plot_case_size_count(df_variable, **kwargs):
    """
    繪製 case size 資訊
    """
    df_case_size_count = pd.read_csv(
        os.path.join(
            kwargs["base_ws"], "mf_case_size_count.csv"
        ),
        header=None,
    )
    df_case_size_count.columns = ["datetime", "csize"]
    df_case_size_count = df_case_size_count.set_index("datetime")
    df_case_size_count.loc[:, "pratio"] = (
        df_case_size_count.loc[:, "csize"]
        / df_variable.shape[0]
        * 100
    )
    df_case_size_count.index = pd.to_datetime(
        df_case_size_count.index
    )
    plt.style.use("bmh")
    _fig, axs = plt.subplots(1, figsize=(12, 9), sharex=True)

    df_case_size_count["csize"].plot(ax=axs, grid=True)
    axs.set_ylabel("完成案例數")
    axs.set_title("MF 完成度")

    axs.set_xlabel("時間")
    axs.set_xticklabels(
        axs.get_xticklabels(), rotation=45, ha="right"
    )
    myFmt = mdates.DateFormatter("%H:%M:%S")
    axs.xaxis.set_major_formatter(myFmt)
    jut.save_fig(
        os.path.join(kwargs["base_ws"], "mf_case_size_count")
    )
