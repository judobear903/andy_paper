"""
計算伏流水高度關聯的地下水區域
流量 6 ~ 48 cms
河道寬度 10 - 50 m
slope=0.005
manning_n = 0.013
底泥 1 m

地下水部分
地表至地下 50 m
模擬範圍 寬 10 m
        長 5100 m   (與河川垂直)
        100 公尺作為河川模擬用
網格尺寸 10 m x 10 m
地下水深 5-20 m
    最遠處採定水頭
    初始條件為
含水層   K 0.5 m/day ~ 50 m/day
河川底泥 K 0.5 m/day ~ 50 m/day
Sy 0.05 ~ 0.03

變因
1. 地下水深 
2. 河川平均流量
3. 地下水 K
4. 河川底泥 K

Conductance = K / L * A
A 為河長河寬
L 為底泥厚度

ibound 最右側, const. head
"""

import os
import sys
import numpy as np
import pandas as pd
from cpuinfo import get_cpu_info

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import plt_parameters  # noqa: F401
import jlib_logging

sys.path.append(
    os.path.join("..", "..", "mf_process", "srcs", "mf_utility")
)
# import mf_utility
import mf_params as mfp
import mf_river_distance_core as mrdc

# 平行數量
cpu_item = get_cpu_info()
processor_ratio = 0.3
process_size = max(
    int(float(cpu_item["count"]) * processor_ratio),
    1,
)


if __name__ == "__main__":
    program_name = "hyporheic_mf"
    root_logger = jlib_logging.logger_setup(
        program_name,
        filename=os.path.join(
            "logging",
            program_name,
        ),
        log_append=False,
    )

    ###############################################################
    # 數據參數
    mf_params = mfp.mf_params
    mf_params["export_path"] = sys.argv[1]
    mf_params["case_index"] = sys.argv[2].split(",")
    mf_params["log_mf_undelete"] = bool(
        np.any(
            [argv == "log_mf_undelete" for argv in sys.argv[1:]]
        )
    )

    ###############################################################
    ###############################################################
    ###############################################################
    # 隨機合成參數
    mat = []
    df_variable_fname = os.path.join(
        mf_params["export_path"], "df_variable.csv"
    )
    if not os.path.exists(df_variable_fname):
        for i in range(mfp.mf_params["test_size"]):
            r = np.random.uniform(0, 1, 6)
            mat_sub = [
                mrdc.variable_generate(
                    r[0], tuple(mf_params["BAS"]["IC"])
                ),  # 地下水深
                mrdc.variable_generate(
                    r[1],
                    tuple(
                        mf_params["LPF"]["Kh"],
                    ),
                    loglog=True,
                ),
                mrdc.variable_generate(
                    r[2], tuple(mf_params["LPF"]["Sy"])
                ),
                mrdc.variable_generate(
                    r[3],
                    tuple(mf_params["RIV"]["Kh"]),
                    loglog=True,
                ),
                mrdc.variable_generate(
                    r[4], tuple(mf_params["RIV"]["qflow_range"])
                ),  # 流量
                mrdc.variable_generate(
                    r[5], tuple(mf_params["RIV"]["riv_width"])
                ),  # 河寬
            ]
            mat.append(mat_sub)

        df_variable = pd.DataFrame(
            mat,
            columns=[
                "IC",
                "Kh",
                "Sy",
                "RIV_Kh",
                "qflow_range",
                "riv_width",
            ],
        )
        try:
            df_variable.to_csv(df_variable_fname)
        except OSError:
            os.makedirs(os.path.dirname(df_variable_fname))
            df_variable.to_csv(df_variable_fname)
    else:
        df_variable = pd.read_csv(df_variable_fname)
        df_variable = df_variable.set_index("Unnamed: 0")
    print(df_variable)
    ###############################################################
    ###############################################################
    ###############################################################

    # 建立模型基本設定, 前處理
    flags, results, kwargs, pickle_fname = (
        mrdc.mf_case_setup_preprocess(
            mf_params,
            df_variable,
            root_logger=root_logger,
            process_ratio=processor_ratio,
            process_size=process_size,
        )
    )

    # 逐段平行計算 MF 模型
    # 繪製必要的後處理結果
    results = mrdc.mf_simulation_framework(
        flags,
        results,
        mf_params,
        df_variable,
        pickle_fname,
        log_single_case_plot=True,
        **kwargs,
    )
