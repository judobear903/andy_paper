"""
Open Channel
計算 Normal Depth
"""

from typing import Union
from pydantic import BaseModel
import numpy as np
from scipy.optimize import root_scalar
import math


class open_channel(BaseModel):
    """
    Open Channel 計算 Normal flow
    """

    channel_width: Union[float, int]  # 渠道寬
    slope: float  # 水面坡度
    manning_n: float  # Manning's n

    def determine_R_tri(
        self, h: Union[int, float], theta: float
    ) -> float:
        """
        三角形渠道的濕週
        theta 為三角形的底部角度
        """
        return 2 * h / np.cos(theta / 2)

    def determine_A_tri(
        self, h: Union[int, float], theta: float
    ) -> float:
        """
        三角形渠道的通水面積
        theta 為三角形的底部角度
        """
        return np.power(h, 2) * np.tan(theta / 2)

    def determine_triangle_depth(
        self, Q: Union[float, int], theta
    ) -> float:
        """
        計算三角形渠道中, 以流量計算水深
        theta 為三角函數
        """
        theta_half = theta / 2
        h = np.power(
            Q
            * self.manning_n
            / 1.486
            * np.power(np.cos(theta_half) / 2, 2.0 / 3)
            / np.tan(theta_half)
            / np.power(self.slope, 0.5),
            3.0 / 8,
        )
        return h

    def determine_triangle_width(
        self, h: Union[float, int], theta
    ) -> Union[float, int]:
        theta_half = theta / 2
        return 2 * h * np.tan(theta_half)

    def determine_R(self, h: Union[int, float]) -> float:
        """
        計算濕週
        """
        return h / (1 + 2 * h / self.channel_width)

    def determine_normal_depth(
        self, Q: Union[float, int]
    ) -> float:
        """
        輸入流量, 計算對應的 normal depth
        Q 單位 cms
        """
        h1 = np.power(
            (self.manning_n * Q)
            / (self.channel_width * np.power(self.slope, 0.5)),
            0.6,
        )

        def normal_depth_calc(h) -> float:
            """
            h = 0.8316(1 + 2h/3)^(2/5)
            --> error = (0.8316(1 + 2h/3)^(2/5) - h)^2
            """
            error = (
                h1
                * np.power(1 + 2 * h / self.channel_width, 0.4)
                - h
            )
            return error

        def normal_depth_calc_derive(h) -> float:
            """ """
            error_derive = (
                h1
                * 0.8
                / self.channel_width
                * np.power(1 + 2 * h / self.channel_width, -0.6)
                - 1
            )
            return error_derive

        def normal_depth_calc_derive_twice(h) -> float:
            """ """
            error_derive = (
                h1
                * (-12 / 5)
                / self.channel_width
                * np.power(
                    1 + 2 * h / self.channel_width, -8.0 / 5
                )
            )
            return error_derive

        vlim = [10, 100]
        try:
            sol = root_scalar(
                normal_depth_calc,
                bracket=vlim,
                x0=np.random.uniform(*tuple(vlim)),
                fprime=normal_depth_calc_derive,  # 一階微分
                fprime2=normal_depth_calc_derive_twice,  # 二階微分
                method="halley",
            )
            assert not math.isnan(sol.root)
        except AssertionError:
            return self.determine_normal_depth(Q)

        return sol.root
