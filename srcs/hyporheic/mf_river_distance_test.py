"""
相較於 mf_river_distance_main.py 隨機合成參數
mf_river_distance_test.py 則手動設定參數
"""

import os
import sys
import pandas as pd
import numpy as np
from cpuinfo import get_cpu_info

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import plt_parameters  # noqa: F401
import jlib_logging

sys.path.append(
    os.path.join("..", "..", "mf_process", "srcs", "mf_utility")
)
# import mf_utility
import mf_params as mfp
import mf_river_distance_core as mrdc

# 平行數量
cpu_item = get_cpu_info()
processor_ratio = 0.3
process_size = max(
    int(float(cpu_item["count"]) * processor_ratio),
    1,
)


if __name__ == "__main__":
    program_name = "hyporheic_mf"
    root_logger = jlib_logging.logger_setup(
        program_name,
        filename=os.path.join(
            "logging",
            program_name,
        ),
        log_append=False,
    )

    ###############################################################
    # 數據參數
    mf_params = mfp.mf_params
    mf_params["export_path"] = sys.argv[1]
    mf_params["case_index"] = sys.argv[2].split(",")
    mf_params["log_mf_undelete"] = bool(
        np.any(
            [argv == "log_mf_undelete" for argv in sys.argv[1:]]
        )
    )

    ###############################################################
    ###############################################################
    ###############################################################
    # 隨機合成參數
    mat = []
    """
        mat = [
            [-5, 5, 0.2, 10, 20, 50],
            [-5, 25, 0.2, 10, 20, 50],
            [-5, 50, 0.2, 10, 20, 50],
        ]

        df_variable = pd.DataFrame(
            mat,
            columns=[
                "IC",
                "Kh",
                "Sy",
                "RIV_Kh",
                "qflow_range",
                "riv_width",
            ],
        )
    """
    df_variable = pd.read_csv(sys.argv[3])
    df_variable = df_variable.set_index(df_variable.columns[0])
    df_variable.index = range(df_variable.shape[0])
    ###############################################################
    ###############################################################
    ###############################################################

    # 建立模型基本設定, 前處理
    flags, results, kwargs, pickle_fname = (
        mrdc.mf_case_setup_preprocess(
            mf_params,
            df_variable,
            root_logger=root_logger,
            process_ratio=processor_ratio,
            process_size=process_size,
        )
    )

    # 逐段平行計算 MF 模型
    # 繪製必要的後處理結果
    results = mrdc.mf_simulation_framework(
        flags,
        results,
        mf_params,
        df_variable,
        pickle_fname,
        log_single_case_plot=False,
        **kwargs,
    )
    assert len(results) == df_variable.shape[0]

    mrdc.plot_mf_post(
        results,
        -1,
        mf_params,
        root_logger=root_logger,
        log_plot_others=False,
        log_plot_compare=True,  # 不同案例不同色階
        base_ws=kwargs["base_ws"],
    )
