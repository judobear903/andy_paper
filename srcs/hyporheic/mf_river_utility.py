"""
繪製各案例的關聯性數據 & 水位動畫 
"""

import sys
import os
from typing import Optional, Dict, List
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy import interpolate
import numpy as np
import pandas as pd
import math


sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import cmap_define

import mf_params as mfp

plot_elems = {
    "corrcoef_compare": [
        "df_corrcoef",
        "相關係數",
        "corr",
        1,
        [-0.05, 1.05],
        True,
        "png",
    ],
    "amp_ratio_compare": [
        "df_amplitude_ratio",
        "Amplitude ratio",
        "amp_ratio",
        1,
        [0, None],
        False,
        "png",
    ],
    "scipy_signal": [
        "df_scipy_signal",
        "Correlation",
        "corr",
        1,
        [None, None],
        False,
        "png",
    ],
    "scipy_signal_lags": [
        "df_scipy_signal",
        r"Lags $days$",
        "lags",
        1.0 / 24,
        [None, None],
        False,
        "png",
    ],
    "strt": [
        "df_strt",
        r"Head $m$",
        "head",
        1,
        [None, None],
        False,
        "gif",
    ],
}


def determine_xloc(ncol, deltax) -> pd.DataFrame:
    xloc = np.array(
        [(i + 1) * deltax - deltax / 2 for i in range(ncol)]
    )
    return xloc


def annotate_variable(ax, count, **kwargs):
    """
    輸出 MF 案例參數
    """
    mf_result = kwargs["mf_result"]

    xlocs = [0.15, 0.92]
    message = r""
    if kwargs.get("log_annotate_message", False):
        for j, elem2 in enumerate(
            [
                ["IC", r"$IC$", r"$m$", 1],
                ["Kh", r"$K_h$", r"$m/day$", 0],
                ["Sy", r"$S_y$", "", 2],
                ["RIV_Kh", r"河川底質 $K_{s}$", r"$m/day$", 2],
                ["qflow_range", r"$\bar{Q}$", r"$cms$", 0],
                ["riv_width", r"河寬", r"$m$", 0],
            ]
        ):
            if message != r"":
                if j % 3 == 0:
                    message += "\n"
                else:
                    message += ", "
            message += r"{}$=${} {}".format(
                elem2[1],
                r"${}$".format(
                    round(mf_result[elem2[0]], elem2[3])
                ),
                elem2[2],
            )

        ax.annotate(
            message,
            xy=(xlocs[0], xlocs[1] - 0.08 * count),
            xytext=(xlocs[0], xlocs[1] - 0.08 * count),
            xycoords="axes fraction",
            fontsize=14,
            color=kwargs.get("color", "black"),
        )
    return ax, message


def plot_line_single(ax, count, **kwargs):
    xlim = kwargs.get("xlim", [None, None])
    ylim = kwargs.get("ylim", [None, None])
    mf_result = kwargs["mf_result"]
    plot_elem = kwargs.get("plot_elem", None)

    # 畫圖, line
    mf_result[plot_elem[0]].loc[:, plot_elem[2]].plot(
        ax=ax,
        grid=True,
        **{
            key_plot: elem_plot
            for key_plot, elem_plot in kwargs.items()
            if key_plot
            in ["color", "linestyle", "linewidth", "alpha"]
        },
    )
    # 輸出案例參數
    ax, message = annotate_variable(
        ax,
        count,
        mf_result=mf_result,
        **{
            key: elem
            for key, elem in kwargs.items()
            if key not in ["mf_result"]
        },
    )

    def query_distance(
        r_criteria: float,
    ) -> Optional[float]:
        """
        依據相關係數的門檻, 評估對應的距離
        """
        df = mf_result[plot_elem[0]]
        # 內差
        x = []
        y = []
        for i, index in enumerate(df.index):
            try:
                # 只保留逐漸向下的部分, 再次抬升部分則刪除
                if (
                    df.loc[index, plot_elem[2]]
                    > df.loc[df.index[i + 1], plot_elem[2]]
                ) and (df.loc[index, plot_elem[2]] >= 0):
                    x.append(df.loc[index, plot_elem[2]])
                    y.append(index)
                else:
                    break
            except IndexError:
                pass
        f = interpolate.interp1d(x, y)
        value = None
        try:
            value = f(r_criteria)
        except ValueError:
            if (
                np.min(df.loc[:, plot_elem[2]].values)
                > r_criteria
            ):
                value = df.index[-1]
            else:
                value = df.index[0]
        return value

    if kwargs.get("log_plot_criteria", False):
        if plot_elem[5]:
            # 是否繪製 r=0.3, 0.5, 0.7,0.9 的箭頭與虛線
            for elem3 in [
                [0.7, (1750, 0.75)],
                [0.6, (1750, 0.65)],
                [0.5, (1750, 0.55)],
                [0.4, (1750, 0.45)],
                [0.3, (1750, 0.35)],
            ]:
                # 箭頭
                message = r"$r={}$".format(round(elem3[0], 2))
                loc = (query_distance(elem3[0]), elem3[0])
                if loc[0] is not None:
                    ax.annotate(
                        message,
                        xy=loc,
                        xytext=elem3[1],
                        xycoords="data",
                        fontsize=16,
                        arrowprops=dict(facecolor="black"),
                    )
                    ax.hlines(
                        elem3[0],
                        xlim[0],
                        xlim[1],
                        linestyles="--",
                        colors="darkblue",
                        alpha=0.6,
                        linewidth=1.0,
                    )

    ax.set_ylabel(plot_elem[1])
    ax.set_xlabel(r"距離 $m$")
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_title("河川對地下水關聯性分析")
    plt.tight_layout()


def plot_strt_animation(mf_results, key, gif_fname: str):
    """
    水位動畫繪製
    """
    df_strt = mf_results[key]["df_strt"]
    df_strt2 = df_strt.iloc[1:, 10:]
    xloc = determine_xloc(
        df_strt2.shape[1], mfp.mf_params["DIS"]["cell_size"][0]
    )
    ylim = [
        math.floor(np.min(np.min(df_strt2.values))) - 1,
        math.ceil(np.max(np.max(df_strt2.values))) + 1,
    ]
    xlim = [-50, 5050]

    plt.style.use("bmh")
    _fig, ax = plt.subplots(1, figsize=(9, 8))

    def plot_strt(tindex, ax, **kwargs):
        ax.plot(xloc, df_strt2.iloc[tindex, :].values)
        ax.set_ylim(ylim)
        ax.set_xlim(xlim)
        ax.set_xlabel(r"距離 $m$")
        ax.set_ylabel(r"地下水位 $m$")
        ax.set_title(
            r"地下水位變化 $T = {}\; day$".format(tindex * 10)
        )

        # 輸出案例參數
        ax, _message = annotate_variable(
            ax,
            0,
            mf_result=mf_results[key],
            **{
                key: elem
                for key, elem in kwargs.items()
                if key not in ["mf_result"]
            },
        )
        for elem in [
            ["河\n川", (0.02, 0.02)],
            ["遠\n離\n河\n川", (0.96, 0.02)],
        ]:
            ax.annotate(
                elem[0],
                xy=elem[1],
                xytext=elem[1],
                xycoords="axes fraction",
                fontsize=16,
            )
        plt.tight_layout()

    def run(tindex, **kwargs):
        ax.clear()  # 清空圖表
        return plot_strt(tindex, ax, **kwargs)

    def init(**kwargs):
        return plot_strt(0, ax, **kwargs)

    ani = animation.FuncAnimation(
        _fig,
        run,
        init_func=init,
        frames=108,
        interval=10,
        repeat=True,
    )
    fps = 2.0
    try:
        ani.save(
            gif_fname,
            fps=fps,
        )
    except FileNotFoundError:
        os.makedirs(os.path.dirname(gif_fname))
        ani.save(
            gif_fname,
            fps=fps,
        )


def plot_strt(tindex: int, xlim, ylim, mf_results, ax, **kwargs):
    assert isinstance(tindex, int)
    count = 0
    for key, mf_result in mf_results.items():
        df_strt = mf_result["df_strt"]
        df_strt2 = df_strt.iloc[1:, 10:]
        xloc = determine_xloc(
            df_strt2.shape[1],
            mfp.mf_params["DIS"]["cell_size"][0],
        )
        color = cmap_define.bmh_colors[
            count % len(cmap_define.bmh_colors)
        ]
        ax.plot(
            xloc,
            df_strt2.iloc[tindex, :].values,
            color=color,
            alpha=0.7,
        )
        # 輸出案例參數
        ax, message = annotate_variable(
            ax,
            count,
            mf_result=mf_result,
            log_annotate_message=True,
            color=color,
            **kwargs,
        )
        count += 1
    assert len(mf_results) == count

    ax.set_ylim(ylim)
    ax.set_xlim(xlim)
    ax.set_xlabel(r"距離 $m$")
    ax.set_ylabel(r"地下水位 $m$")
    ax.set_title(
        r"地下水位變化 $T = {}\; day$".format(tindex * 10)
    )

    for elem in [
        ["河\n川", (0.02, 0.02)],
        ["遠\n離\n河\n川", (0.96, 0.02)],
    ]:
        ax.annotate(
            elem[0],
            xy=elem[1],
            xytext=elem[1],
            xycoords="axes fraction",
            fontsize=16,
        )
    plt.tight_layout()


def determine_ylim(mf_results: Dict) -> List:
    """
    決定 ylim
    """
    ylim = [
        math.floor(
            np.min(
                [
                    mf_result["df_strt"].min()
                    for _key, mf_result in mf_results.items()
                ]
            )
        ),
        math.ceil(
            np.max(
                [
                    mf_result["df_strt"].max()
                    for _key, mf_result in mf_results.items()
                ]
            )
        ),
    ]
    return ylim


def plot_strt_animation_compare(mf_results, gif_fname: str):
    """
    水位動畫繪製

    比較多組案例的水位變化

    """
    xlim = [-50, 5050]
    ylim = determine_ylim(mf_results)

    plt.style.use("bmh")
    _fig, ax = plt.subplots(1, figsize=(9, 8))

    def run(tindex, **kwargs):
        ax.clear()  # 清空圖表
        return plot_strt(
            tindex, xlim, ylim, mf_results, ax, **kwargs
        )

    def init(**kwargs):
        return plot_strt(0, xlim, ylim, mf_results, ax, **kwargs)

    ani = animation.FuncAnimation(
        _fig,
        run,
        init_func=init,
        frames=108,
        interval=10,
        repeat=True,
    )
    fps = 2.0
    try:
        ani.save(
            gif_fname,
            fps=fps,
        )
    except FileNotFoundError:
        os.makedirs(os.path.dirname(gif_fname))
        ani.save(
            gif_fname,
            fps=fps,
        )
