"""
計算伏流水高度關聯的地下水區域
後處理分析
"""

import os
import sys
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pickle

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jutility as jut
import plt_parameters  # noqa: F401
import jlib_logging

import mf_params as mfp
import mf_river_utility as mfu

# Python implementation of amplitude-unbiased, phase-based correlation technique
# https://github.com/adienakhmad/phasecorr/tree/master
# sys.path.append(os.path.join("..", "srcs"))
# from phasecorr.phasecorr import xcorr, acorr


if __name__ == "__main__":
    program_name = "hyporheic_mf_post"
    root_logger = jlib_logging.logger_setup(
        program_name,
        filename=os.path.join(
            "logging",
            program_name,
        ),
        log_append=False,
    )

    ###############################################################
    mf_params = mfp.mf_params
    mf_params["export_path"] = sys.argv[1]

    ###############################################################
    # 建立基流量+豪雨事件的流量變化
    ###############################################################
    (
        base_ws,
        Lx,
        Ly,
        nrow,
        ncol,
        nlay,
        delr,
        delc,
        ztop,
        zbot,
        ibound,
    ) = mfp.mf_predefine(mf_params)
    pickle_fname = os.path.join(base_ws, "mf_corrcoef.pickle")
    assert os.path.exists(pickle_fname)
    if os.path.exists(pickle_fname):
        # 完成 amplitude ratio, signal 等計算,
        # 直接 load data
        with open(pickle_fname, "rb") as f:
            results = pickle.load(f)

    for key, plot_elem in mfu.plot_elems.items():
        message = " --> Plotting: {}".format(key)
        root_logger.debug(message)
        keys = list(results.keys())

        count = 0
        xlim = [-50, 5050]
        ylim = plot_elem[4]
        r_criteria = 0.5
        case_size = 80

        kwargs_plot = {
            "xlim": xlim,
            "ylim": ylim,
            "plot_elem": plot_elem,
            "case_size": case_size,
        }

        def plot_line_all(count, ax, **kwargs):
            for i in range(case_size):
                if i != count:
                    mfu.plot_line_single(
                        ax,
                        i,
                        mf_result=results[keys[i]],
                        color="k",
                        linestyle="-",
                        linewidth=0.5,
                        alpha=0.4,
                        log_plot_criteria=False,
                        root_logger=root_logger,
                        **kwargs_plot,
                        **kwargs,
                    )

            # 主要的案例
            mfu.plot_line_single(
                ax,
                count,
                mf_result=results[keys[count]],
                color="firebrick",
                log_plot_criteria=True,
                root_logger=root_logger,
                **kwargs_plot,
                **kwargs,
            )

        # 繪製動畫
        gif_fname = os.path.join(
            base_ws,
            "pics",
            "mf_{}_anaimation_{}.gif".format(
                "corrcoef_compare", key
            ),
        )
        if not os.path.exists(gif_fname):
            root_logger.debug(
                "Creating {}".format(os.path.basename(gif_fname))
            )
            _fig, ax = plt.subplots(1, figsize=(12, 9))

            def run(count, **kwargs):
                ax.clear()
                return plot_line_all(count, ax, **kwargs)

            def init(**kwargs):
                return plot_line_all(0, ax, **kwargs)

            ani = animation.FuncAnimation(
                _fig,
                run,
                init_func=init,
                frames=case_size * 3,
                interval=10,
                repeat=True,
            )
            ani.save(
                gif_fname,
                fps=0.6,
            )

        # 繪製靜態圖
        for count in range(case_size * 10):
            fig_fname = os.path.join(
                base_ws,
                "pics",
                key,
                "mf_{}_{}".format(key, str(count).rjust(4, "0")),
            )

            if not os.path.exists("{}.png".format(fig_fname)):
                root_logger.debug(
                    "Plotting {}.png".format(
                        os.path.basename(fig_fname)
                    )
                )
                plt.style.use("bmh")
                _fig, ax = plt.subplots(1, figsize=(9, 8))
                plot_line_all(count, ax)
                ax.set_xlim(xlim)
                ax.set_ylim(ylim)
                plt.tight_layout()
                jut.save_fig(fig_fname)

    """
    sys.exit()

    # dis_size = elem["df_corrcoef"].shape[0]
    bin_size = 21
    bins = np.arange(0, 1, 1.0 / bin_size)
    heatmap_result = np.zeros((bin_size - 1, dis_size))
    cell_delta = (
        elem["df_corrcoef"].index[1]
        - elem["df_corrcoef"].index[0]
    )
    for i in range(dis_size):
        mat = np.array(
            [
                elem["df_corrcoef"].loc[
                    elem["df_corrcoef"].index[i], "corr"
                ]
                for _key2, elem in results.items()
            ]
        )

        hist, _bin_edge = np.histogram(
            mat,
            bins=bins,
            density=True,
        )
        try:
            heatmap_result[:, i] = hist
        except ValueError as e:
            raise ValueError(
                "{} / {} / {}".format(
                    heatmap_result.shape, bins.shape, hist.shape
                )
            ) from e
    _fig, ax = plt.subplots(1, figsize=(12, 9))
    extent = [
        np.min(elem["df_corrcoef"].index) - cell_delta / 2,
        np.max(elem["df_corrcoef"].index) + cell_delta / 2,
        0,
        1,
    ]
    im = ax.imshow(
        heatmap_result,
        vmin=0,
        vmax=30,
        # extent=extent,
    )
    bin_flip = np.flip(bins, 0)  # 翻轉
    bin_center = [
        (bin_flip[i] + bin_flip[i + 1]) / 2
        for i in range(bins.shape[0] - 1)
    ]

    cbar = _fig.colorbar(im, fraction=0.046, pad=0.04)
    cbar.ax.set_ylabel(r"Density $\%$")
    plt.tight_layout()
    jut.save_fig(os.path.join(base_ws, "mf_heatmap"))
    """
