import sys
from typing import Dict, Tuple
import math
import numpy as np
import pandas as pd

# m/day,
# 時間單位為 day
# 模擬 1440 days
mf_params = {
    "test_size": 5000,
    "DIS": {
        "dis_range": [10100, 125, -100],
        "cell_size": [25, 25],
        "tsize": 512,
        "tdelta": "10d",
    },
    "BAS": {
        "IC": [10, -30],
    },
    "LPF": {
        "Kh": [0.5, 100],
        "Sy": [0.05, 0.3],
    },
    "RIV": {
        "Kh": [0.5, 100],
        "riv_width": [10, 200],
        "qflow_range": [2, 100],
        "mf_riv_width": 100,
        "rbot": 1,
        "slope": 0.001,
        "manning_n": 0.13,
    },
}


def mf_predefine(mf_params: Dict) -> Tuple:
    """
    預先定義 mf 相關資訊
    """
    base_ws = mf_params["export_path"]
    Lx = mf_params["DIS"]["dis_range"][0]
    Ly = mf_params["DIS"]["dis_range"][1]
    nrow = int(math.floor(Ly / mf_params["DIS"]["cell_size"][1]))
    ncol = int(math.floor(Lx / mf_params["DIS"]["cell_size"][0]))
    nlay = 1
    delr = mf_params["DIS"]["cell_size"][0]
    delc = mf_params["DIS"]["cell_size"][1]
    # 含水層頂部
    ztop = np.zeros((nrow, ncol))  # 河道高程
    df = pd.DataFrame(
        np.cumsum(np.ones(ncol) * delc) - delc / 2, columns=["x"]
    )
    # 坡度 10/1000, 千分之10
    df.loc[:, "z"] = 0.01 * (df.loc[:, "x"].values - 100) + 5
    df.loc[:, "z"] = np.where(
        df.loc[:, "x"] < 100,
        0,
        df.loc[:, "z"].values,
    )
    df.loc[:, "z"] = np.where(
        df.loc[:, "z"] < 30,
        df.loc[:, "z"].values,
        30,
    )

    for j in range(ztop.shape[0]):
        ztop[j, :] = df.loc[:, "z"].values
    # 含水層底部
    zbot = mf_params["DIS"]["dis_range"][2] * np.ones(
        (nrow, ncol)
    )
    ibound = np.ones((nlay, nrow, ncol), dtype=np.int32)
    ibound[:, 0, :] = 0
    ibound[:, -1, :] = 0

    ibound[:, :, -5:] = -1  # 最右側 Const. Head
    return (
        base_ws,
        Lx,
        Ly,
        nrow,
        ncol,
        nlay,
        delr,
        delc,
        ztop,
        zbot,
        ibound,
    )
