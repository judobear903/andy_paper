"""
緊密互動距離的回歸模型
"""

import os
import sys
import pickle
import pandas as pd
import numpy as np
from scipy import interpolate
import torch
import torch.nn as nn
from torch.utils.data import TensorDataset, DataLoader
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm
import datetime
import matplotlib.pyplot as plt
from typing import Dict
import flopy

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jlib_logging
import mf_params as mfp
import jutility as jut
import file_utility as fut

dtype = torch.float32  # 模型 dtype


def determine_device_name():
    device_name = "cpu"
    if torch.cuda.is_available():
        device_name = "cuda"
    elif torch.backends.mps.is_available():
        device_name = "mps"
    return device_name


device_name = "cuda" if torch.cuda.is_available() else "cpu"


# float64 --> float32
def data_transform(data):
    assert isinstance(data, np.ndarray), type(data)
    try:
        data = torch.from_numpy(data)
    except TypeError as e:
        raise TypeError(data.dtype) from e
    data = data.to(dtype)
    data = data.to(device_name)
    return data


def model_create(layer_node_size, **kwargs):
    """
    建立 ANN model
    權重隨機初始化
    """
    device_name = kwargs["device"]
    # Construct our model by instantiating the class defined above
    model = nn_river_distance(layer_node_size=layer_node_size)
    model = model.to(device=device_name)
    return model


def data_loader_create(
    df, batch_size, param_describe, columns_in, columns_out
):
    for i, col in enumerate(columns_in + columns_out):
        df.loc[:, col] = (
            df.loc[:, col] - param_describe["mean"][i]
        ) / param_describe["std"][i]

    for col in df.columns:
        # 轉換格式
        # --> np.float32
        df = df.astype({col: np.float32})
    X_data = np.array(df.loc[:, columns_in])
    Y_data = np.array(df.loc[:, columns_out])
    x = data_transform(X_data)
    y = data_transform(Y_data)
    my_dataset = TensorDataset(x, y)
    my_loader = DataLoader(
        my_dataset,
        batch_size=batch_size,
        shuffle=True,
    )
    return my_loader


def train_one_epoch(
    training_loader, epoch_index, tb_writer, optimizer, loss_fn
):
    running_loss = 0.0
    last_loss = 0.0
    # Here, we use enumerate(training_loader) instead of
    # iter(training_loader) so that we can track the batch
    # index and do some intra-epoch reporting
    for i, data in enumerate(training_loader):
        # Every data instance is an input + label pair
        inputs, labels = data

        # Zero your gradients for every batch!
        optimizer.zero_grad()

        # Make predictions for this batch
        outputs = model(inputs)

        # Compute the loss and its gradients
        loss = loss_fn(outputs, labels)
        loss.backward()

        # Adjust learning weights
        optimizer.step()

        # Gather data and report
        running_loss += loss.item()
        if i % 1000 == 999:
            last_loss = running_loss / 1000  # loss per batch
            # print(
            #    "  batch {} loss: {}".format(
            #        i + 1, last_loss
            #    )
            # )
            tb_x = epoch_index * len(training_loader) + i + 1
            tb_writer.add_scalar("Loss/train", last_loss, tb_x)
            # running_loss = 0.0
    return last_loss


def training_process(
    model,
    model_info,
    model_path,
    train_loader,
    validation_loader,
    device_name,
    num_epochs,
    loss_fn,
    **kwargs,
):
    """
    訓練模型
    更新模型參數
    """
    print("# {}".format(os.path.basename(model_path)))
    base_index = kwargs.get("base_index", 0)
    base_ws = kwargs.get("base_ws", "")
    # 搜尋演算法: 隨機梯度下降 (SGD)、Adam
    # optimizer = torch.optim.Adam(
    #    model.parameters(), lr=1e-6
    # )
    optimizer = torch.optim.SGD(
        model.parameters(), lr=0.001, momentum=0.9
    )
    # device = device_name
    # convergence_time = None
    # last_iter = 0

    """
    1.使用 tqdm 庫顯示一個進度條，方便監控訓練過程。
    """
    epoch_number = 0
    mat_train = []
    mat_valid = []
    loss_fn_mse = torch.nn.MSELoss(reduction="sum")
    for epoch_index in tqdm(
        range(num_epochs),
        desc="Training Epochs",
    ):
        # Make sure gradient tracking is on, and do a pass over the data
        model.train(True)
        # 單次訓練
        avg_loss = train_one_epoch(
            train_loader, epoch_index, writer, optimizer, loss_fn
        )
        # Disable gradient computation and reduce memory consumption.
        # Set the model to evaluation mode, disabling dropout and
        # using population statistics for batch normalization.
        model.eval()
        avg_loss = calc_loss(
            model, train_loader, loss_fn_mse
        )  # training
        avg_vloss = calc_loss(
            model, validation_loader, loss_fn_mse
        )  # validation
        for j in range(len(avg_loss)):
            mat_train.append(
                [
                    epoch_index + base_index,
                    avg_loss[j],
                ]
            )
        for j in range(len(avg_vloss)):
            mat_valid.append(
                [
                    epoch_index + base_index,
                    avg_vloss[j],
                ]
            )
        epoch_number += 1

    model_info = loss_create(
        mat_train, model_info, "loss_process_train"
    )
    model_info = loss_create(
        mat_valid, model_info, "loss_process_valid"
    )
    model_info["best_loss"] = np.min(
        model_info["loss_process_train"]
    )
    for flag in [
        "loss_process_train",
        "loss_process_valid",
        "best_loss",
    ]:
        root_logger.debug(
            "{}: {}".format(flag, model_info[flag])
        )

    # 模型權重儲存
    # model_info["model"] = model.state_dict()
    # 跟前期比較
    model, model_info, log_replace = load_compare_model(
        model, model_info, model_path
    )

    if log_replace:
        # 更新模型, 輸出
        try:
            torch.save(model.state_dict(), model_path)
            with open(
                model_path.replace(".weight", ".pickle"), "wb"
            ) as f:
                pickle.dump(model_info, f)
        except (FileNotFoundError, RuntimeError):
            os.makedirs(
                os.path.dirname(model_path),
                exist_ok=True,
            )
            torch.save(model.state_dict(), model_path)
            with open(
                model_path.replace(".weight", ".pickle"), "wb"
            ) as f:
                pickle.dump(model_info, f)

    sepline = (
        os.path.basename(model_path)
        .replace(".pickle", "")
        .replace("model_", "")
        .split("_")
    )
    batch_size = sepline[-1]
    nnp = sepline[-2]

    # Plot
    plot_model_result(
        model_info,
        os.path.join(
            base_ws,
            "pics",
            "epoch_process_{}_{}".format(nnp, batch_size),
        ),
    )

    return model, model_info, log_replace, nnp, batch_size


class nn_river_distance(nn.Module):
    """
    計算緊密互動距離的回歸模型
    """

    def __init__(self, **ann_setup):
        super(nn_river_distance, self).__init__()
        model_list = []
        self.layer_size = len(ann_setup["layer_node_size"])
        for i in range(self.layer_size - 1):
            lin = nn.Linear(
                ann_setup["layer_node_size"][i],
                ann_setup["layer_node_size"][i + 1],
            )

            model_list.append(lin)
        self.linears = nn.ModuleList(
            model_list
        )  # an iterable of modules to add

    def forward(self, x):
        for i in range(self.layer_size - 1):
            x = self.linears[i](x)
        return x


def loss_create(mat, model_info, flag: str) -> Dict:
    """
    Create Loss records, and append
    """
    df_loss = pd.DataFrame(
        mat, columns=["index", "loss"]
    ).set_index("index")
    for col in df_loss.columns:
        df_loss = df_loss.astype({col: np.float32})
    try:
        model_info[flag] = pd.concat(
            [model_info[flag], df_loss], axis=0
        )
    except KeyError:
        model_info[flag] = df_loss
    model_info[flag] = model_info[flag].sort_index()
    return model_info


def calc_loss(model, data_loader, loss_fn, log_eval=True):
    """ """
    results = []
    with torch.no_grad():
        for _, data in enumerate(data_loader):
            inputs, labels = data
            outputs = model(inputs)
            loss = loss_fn(outputs, labels)
            results.append(loss.to("cpu") / outputs.shape[0])
    return results


def load_nn_model(model_info):
    if model_info["model_type"] == "nn_river_distance":
        model = nn_river_distance(
            layer_node_size=model_info["layer_node_size"],
            device=device_name,
        )
        model.load_state_dict = model_info["model"]
    return model


def load_compare_model(model, model_info, model_path: str):
    """
    1. 檔案如果存在, 則比較是否有較佳
        如果新的 model 更好, 則取代
    2. 反之, 直接寫出
    """
    # 延續舊的結果
    log_replace = True
    try:
        with open(model_path, "rb") as f:
            model_info_previous = pickle.load(f)

            assert (
                model_info_previous["model_type"]
                == model_info["model_type"]
            )
            if (
                model_info_previous["model_type"]
                == "nn_river_distance"
            ):
                model_previous = load_nn_model(
                    model_info_previous
                )
            try:
                if (
                    model_info_previous["best_loss"]
                    < model_info["best_loss"]
                ):
                    # 如果前期較佳, 則維持前期資訊
                    model = model_previous
                    model_info = model_info_previous
                    log_replace = False
            except KeyError:
                # 如果前期結果, 不存在 best_loss
                pass
    except FileNotFoundError:
        pass
    return model, model_info, log_replace


def plot_model_result(model_info, fig_fname: str):
    """
    繪製模型結果
    """
    ###################################################################
    plt.style.use("bmh")
    _fig, ax = plt.subplots(1, figsize=(12, 9))
    for flag in [
        "loss_process_train",
        "loss_process_valid",
    ]:
        plt.plot(
            model_info[flag].index,
            model_info[flag].loc[:, model_info[flag].columns[0]],
            marker="o",
            markersize=3,
            linestyle="none",
            alpha=0.6,
        )

    ax.legend(["Training", "Validation"], shadow=True)
    ax.set_xlabel("Iteration")
    ax.set_ylabel(r"Loss Function ($MSE$)")
    plt.tight_layout()
    jut.save_fig(fig_fname)


def data_split(
    df_results,
    param_describe,
    columns_in,
    columns_out,
    batch_size,
):
    # 數據切割
    df = df_results[df_results["rval"] <= 0.6]
    train_loader = data_loader_create(
        df, batch_size, param_describe, columns_in, columns_out
    )
    df = df_results[
        np.logical_and(
            df_results["rval"] > 0.6,
            df_results["rval"] <= 0.8,
        )
    ]
    validation_loader = data_loader_create(
        df, batch_size, param_describe, columns_in, columns_out
    )
    df = df_results[df_results["rval"] > 0.8]
    testing_loader = data_loader_create(
        df, batch_size, param_describe, columns_in, columns_out
    )
    return train_loader, validation_loader, testing_loader


if __name__ == "__main__":
    dt1 = datetime.datetime.now()
    program_name = "hyporheic_mf_regression"
    root_logger = jlib_logging.logger_setup(
        program_name,
        filename=os.path.join(
            "logging",
            program_name,
        ),
        log_append=True,
    )
    device_name = (
        determine_device_name()
    )  # 決定可以使用的 device_name
    root_logger.info(
        "# Running process '{}' with {} mode".format(
            program_name,
            device_name,
        )
    )

    ###############################################################
    mf_params = mfp.mf_params
    mf_params["export_path"] = sys.argv[1]
    calibration_type = sys.argv[2]
    assert calibration_type in ["training", "re-train"]

    ###############################################################
    # 建立基流量+豪雨事件的流量變化
    ###############################################################

    (
        base_ws,
        Lx,
        Ly,
        nrow,
        ncol,
        nlay,
        delr,
        delc,
        ztop,
        zbot,
        ibound,
    ) = mfp.mf_predefine(mf_params)
    pickle_fname2 = os.path.join(base_ws, "mf_corrcoef.pickle")
    root_logger.debug(
        "  Pickle file loading: {}".format(
            pickle_fname2,
        )
    )
    results = {}
    if os.path.exists(pickle_fname2):
        # 完成 amplitude ratio, signal 等計算,
        # 直接 load data
        with open(pickle_fname2, "rb") as f:
            results = pickle.load(f)

    ########################################################################
    # 數據預備
    columns_in = [
        "IC",
        "Kh",
        "Sy",
        "RIV_Kh",
        "qflow_range",
        "riv_width",
    ]
    columns_out = [0.9, 0.7, 0.5, 0.3]
    labels_in = [
        "地下水位",
        r"含水層 $K_h$",
        r"含水層 $S_y$",
        r"河川底質 $K_{s}$",
        "河川平均流量",
        "河川流量",
        "河川濕潤區寬度",
    ]
    labels_out = [r"$r=0.9$", r"$r=0.7$", r"$r=0.5$", r"$r=0.3$"]
    mat = []
    mat2 = []
    root_logger.debug("  Transforming dict to pd.DataFrame")
    mf_data_fname = os.path.join(base_ws, "mf_data.csv")

    if not os.path.exists(mf_data_fname):
        for key, elem in results.items():
            df = elem["df_corrcoef"]
            # 內差
            f = interpolate.interp1d(
                df.loc[:, "corr"].values,
                df.index,
            )

            try:
                mat.append(
                    [elem[col] for col in columns_in]
                    + [f(r) for r in columns_out]
                )
            except ValueError:
                pass
        df_results = pd.DataFrame(
            mat, columns=columns_in + columns_out
        )
        # 數據切割 60% training, 20% validation, 20% testing
        df_results.loc[:, "rval"] = np.random.uniform(
            0, 1, df_results.shape[0]
        )
        root_logger.debug(
            "MF 數值試驗數據: {}".format(df_results)
        )
        df_results.to_csv(mf_data_fname)
    else:
        df_results = pd.read_csv(mf_data_fname)
        df_results = df_results.rename(
            columns={
                "0.3": 0.3,
                "0.5": 0.5,
                "0.7": 0.7,
                "0.9": 0.9,
            }
        )
        df_results.set_index(df_results.columns[0])

    ########################################################################
    # 數據預備
    root_logger.debug("# Normalize parameter create")
    try:
        param_describe = {
            "mean": np.array(
                [
                    df_results[col].mean()
                    for col in columns_in + columns_out
                ]
            ),
            "std": np.array(
                [
                    df_results[col].std()
                    for col in columns_in + columns_out
                ]
            ),
        }
        picke_describe = os.path.join(
            mf_params["export_path"], "data_describe.pickle"
        )
        with open(picke_describe, "wb") as f:
            pickle.dump(param_describe, f)  # 寫入檔案
    except KeyError as e:
        raise KeyError(
            "{} / {}".format(
                df_results.columns,
                [
                    "{} / {}".format(col, type(col))
                    for col in columns_in + columns_out
                ],
            )
        ) from e
    # 數據切割
    best_vloss = 1_000_000.0
    batch_size = 2048
    train_loader, validation_loader, testing_loader = data_split(
        df_results,
        param_describe,
        columns_in,
        columns_out,
        batch_size,
    )

    """
    1.將已轉換好的數據打包成 PyTorch 的 TensorDataset, 並利用 DataLoader 來迭代數據集
    2.shuffle=True 打亂數據 ，訓練過程中可以幫助模型更好地泛化。
    """
    # loss_fn = torch.nn.MSELoss(reduction="sum")
    loss_fn = torch.nn.CrossEntropyLoss()
    model_params = {
        "nn_river_distance": [
            [30],
            [15, 15],
            [20, 20],
            [15, 15, 15],
            [20, 20, 20],
            [30, 30, 30],
            [40, 40, 40],
        ]
    }

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    writer = SummaryWriter(
        "runs/fashion_trainer_{}".format(timestamp)
    )
    model_info: Dict = {}
    if calibration_type in ["training"]:
        num_epochs = 500
        for key, elem in model_params.items():
            for nnp in elem:
                root_logger.debug("{}: {}".format(key, nnp))
                model_path = os.path.join(
                    base_ws,
                    "models",
                    "model_{}_{}_{}.weight".format(
                        key, nnp, batch_size
                    ),
                )

                model_info = {
                    "model_type": key,
                    "model": None,
                    "layer_node_size": -1,
                    "loss_process_train": pd.DataFrame([]),
                    "loss_process_validation": pd.DataFrame([]),
                    "best_loss": best_vloss,
                }
                base_index = 0
                # 權重不存在, 才要重新計算
                layer_node_size = (
                    [len(columns_in)] + nnp + [len(columns_out)]
                )
                model = model_create(
                    layer_node_size, device=device_name
                )
                model_info["layer_node_size"] = layer_node_size

                # 訓練模型
                # 更新模型參數
                # 繪圖
                (
                    model,
                    model_info,
                    log_replace,
                    nnp,
                    batch_size,
                ) = training_process(
                    model,
                    model_info,
                    model_path,
                    train_loader,
                    validation_loader,
                    device_name,
                    num_epochs,
                    loss_fn,
                    base_index=base_index,
                    base_ws=base_ws,
                )
    elif calibration_type in ["re-train"]:
        num_epochs = 3000
        model_flist = fut.filter_search_result(
            fut.search_files_in_dir(
                os.path.join(base_ws, "models"),
                regular_flags="weight",
            ),
            regular_flags="model",
        )
        all_model = {}
        all_model_info = {}
        for model_path in model_flist:
            model_info = {
                # "model_type": key,
                "model": None,
                "layer_node_size": -1,
                "loss_process_train": pd.DataFrame([]),
                "loss_process_validation": pd.DataFrame([]),
                "best_loss": best_vloss,
            }

            # 延續舊的結果
            with open(
                model_path.replace(".weight", ".pickle"), "rb"
            ) as f:
                all_model_info[model_path] = pickle.load(f)
                if (
                    all_model_info[model_path]["model_type"]
                    == "nn_river_distance"
                ):
                    # re-create model & load weight data
                    all_model[model_path] = nn_river_distance(
                        layer_node_size=all_model_info[
                            model_path
                        ]["layer_node_size"],
                        device=device_name,  # 將模型轉換到 device
                    )
                    all_model[model_path].load_state_dict(
                        torch.load(model_path), weights_only=True
                    )

        # 挑選最佳的模型
        mat = []
        for model_path, model_info in all_model_info.items():
            mat.append([model_path, model_info["best_loss"]])
        df_all = pd.DataFrame(
            mat, columns=["model_path", "best_loss"]
        )
        df_all = df_all.sort_values(
            by=["best_loss"], ascending=True
        )  # 上升, 由小至上

        for i in range(3):
            # 挑出最佳的三個模型
            index = df_all.index
            model_path = df_all.loc[index, "model_path"]
            model = all_model[model_path]
            # 訓練模型
            # 更新模型參數
            # 繪圖
            model, model_info, log_replace, nnp, batch_size = (
                training_process(
                    model,
                    model_info,
                    model_path,
                    train_loader,
                    validation_loader,
                    device_name,
                    num_epochs,
                    loss_fn,
                    base_index=np.max(
                        model["loss_process_train"]
                    )
                    + 1,
                    base_ws=base_ws,
                )
            )
