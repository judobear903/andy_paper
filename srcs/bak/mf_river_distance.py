"""
評估河川與地下水高度關聯的距離
"""

import sys
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import math

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jutility as jut
import plt_parameters  # noqa: F401

import open_channel

if __name__ == "__main__":
    ###############################################################
    # 測試 Open Cannel Normal Depth 計算
    ###############################################################
    OC = open_channel.open_channel(
        channel_width=3, slope=0.005, manning_n=0.013
    )
    normal_h = OC.determine_normal_depth(12)  # cms

    qlist = np.linspace(1, 50, 10)
    blist = np.linspace(10, 50, 10)
    mat = []
    for q in qlist:
        for b in blist:
            OC = open_channel.open_channel(
                channel_width=b, slope=0.005, manning_n=0.013
            )
            normal_h = OC.determine_normal_depth(q)  # cms

            mat_sub = [
                q,
                b,
                normal_h,
            ]
            mat.append(mat_sub)

    df_river = pd.DataFrame(mat, columns=["q", "b", "h"])

    # ax = plt.figure().add_subplot(projection='3d')
    fig = plt.figure(figsize=(12, 9))
    ax = fig.add_subplot(111, projection="3d")
    qq, bb = np.meshgrid(
        np.linspace(1, 50, 50), np.linspace(10, 50, 50)
    )
    qq2 = qq.ravel()
    bb2 = bb.ravel()

    reg = LinearRegression().fit(
        np.array(df_river.loc[:, ["q", "b"]].values),
        np.array(df_river.loc[:, ["h"]].values),
    )
    hh2 = reg.predict(
        np.array([[qq2[i], bb2[i]] for i in range(qq2.shape[0])])
    )
    hh = hh2.reshape(qq.shape)
    ax.plot_surface(
        qq,
        bb,
        hh,
        edgecolor="royalblue",
        lw=0.5,
        rstride=8,
        cstride=8,
        alpha=0.3,
    )

    ax.set_xlabel(r"q $cms$")
    ax.set_ylabel(r"b $m$")
    ax.set_zlabel(r"h $m$")
    # ax.view_init(-140, 75)
    plt.tight_layout()
    jut.save_fig("normal_depth")
    for var in ["q", "b", "h"]:
        print(var, df_river[var].min(), df_river[var].max())

    _fig, ax = plt.subplots(figsize=(12, 9))
    df_river.plot.scatter(x="q", y="h", ax=ax)
    ax.set_xlabel(r"q $cms$")
    ax.set_ylabel(r"h $m$")
    plt.tight_layout()
    jut.save_fig("normal_depth2")

    ###############################################################
    ###############################################################
    q = 12  # cms, 基流量
    # 測試流量動態變遷
    mat = []
    tlist = np.linspace(0, 720, 721)
    # qlist = np.sin(tlist / 24 * math.pi * 2)
    qlist = np.ones(tlist.shape[0]) * q
    r1 = np.random.uniform(0, 1, tlist.shape[0])
    r2 = np.random.uniform(6, 36, tlist.shape[0])
    mat = []
    for i in range(tlist.shape[0]):
        if r1[i] > 0.975:
            log_add = len(mat) == 0
            if not log_add:
                if i > mat[-1][1]:
                    log_add = True

            if log_add:
                mat.append([i, i + int(math.ceil(r2[i])), r2[i]])

    for elem in mat:
        tlist_sub = np.arange(elem[1] - elem[0])
        qlist_sub = np.sin(tlist_sub / elem[2] * math.pi)
        qlist_sub *= q * np.random.uniform(0.5, 4)
        try:
            qlist[
                elem[0] : min(elem[1], qlist.shape[0])
            ] += qlist_sub
        except ValueError:
            qlist[
                elem[0] : min(elem[1], qlist.shape[0])
            ] += qlist_sub[
                : min(elem[1], qlist.shape[0]) - elem[0]
            ]

    fig, ax = plt.subplots(2, 1, figsize=(12, 9), sharex=True)
    legend = []
    for b in [5, 10, 20]:
        mat2 = []
        for i in range(qlist.shape[0]):
            OC = open_channel.open_channel(
                channel_width=b, slope=0.005, manning_n=0.013
            )
            normal_h = OC.determine_normal_depth(qlist[i])  # cms
            mat2.append(
                [
                    tlist[i],
                    qlist[i],
                    normal_h,
                ]
            )
        df_river_temporal = pd.DataFrame(
            mat2, columns=["t", "q", "h"]
        ).set_index("t")
        df_river_temporal = df_river_temporal.sort_index()
        df_river_temporal.loc[:, ["q"]].plot(ax=ax[0], grid=True)
        ax[0].set_ylabel(r"q $cms$")
        df_river_temporal.loc[:, ["h"]].plot(ax=ax[1], grid=True)
        ax[1].set_ylabel(r"h $m$")
        legend.append(r"b={} $m$".format(round(b, 2)))

    for i in range(2):
        ax[i].legend(legend, shadow=True)
    plt.tight_layout()
    jut.save_fig("normal_depth_temporal")
    # print (df_river_temporal)
