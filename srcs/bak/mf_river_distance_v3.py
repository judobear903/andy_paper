"""
計算伏流水高度關聯的地下水區域
流量 6 ~ 48 cms
河道寬度 10 - 50 m
slope=0.005
manning_n = 0.013
底泥 1 m

地下水部分
地表至地下 50 m
模擬範圍 寬 30 m
        長 5100 m   (與河川垂直)
        100 公尺作為河川模擬用
網格尺寸 10 m x 10 m
地下水深 5-20 m
    最遠處採定水頭
    初始條件為
含水層   K 0.5 m/day ~ 50 m/day
河川底泥 K 0.5 m/day ~ 50 m/day
Sy 0.05 ~ 0.03

變因
1. 地下水深 
2. 河川平均流量
3. 地下水 K
4. 河川底泥 K

Conductance = K / L * A
A 為河長河寬
L 為底泥厚度

ibound 最右側, const. head
"""

import os
import sys
import numpy as np
import pandas as pd
import flopy
import math
import matplotlib.pyplot as plt
from typing import Tuple, List
import matplotlib.pyplot as plt

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jutility as jut
import plt_parameters  # noqa: F401
import parallel_framework

sys.path.append(
    os.path.join("..", "..", "mf_process", "srcs", "mf_utility")
)
# import mf_utility
import mf_execute
import mf_utility

import open_channel
import mf_params as mfp

if __name__ == "__main__":
    ###############################################################
    # 建立基流量+豪雨事件的流量變化
    ###############################################################
    tlist = np.linspace(0, 720, 721)  # 創時間點
    r1 = np.random.uniform(0, 1, tlist.shape[0])  # 建立流量
    # 降雨延時 6 hrs ~ 36
    qlist = np.ones(tlist.shape[0])  # 基流量
    r2 = np.random.uniform(6, 36, tlist.shape[0])  # 建立延時
    mat = []  # 建立空集合
    for i in range(tlist.shape[0]):
        if r1[i] > 0.975:  # 尋找流量變化時間點
            log_add = len(mat) == 0  # 增加判斷指令
            if not log_add:  # 避免出現擾動時間點重疊
                if i > mat[-1][1]:
                    log_add = True

            if log_add:
                mat.append(
                    [i, i + int(math.ceil(r2[i])), r2[i]]
                )  # 建立擾動時間點

    for elem in mat:
        tlist_sub = np.arange(elem[1] - elem[0])  # 擾動時間差
        qlist_sub = np.sin(tlist_sub / elem[2] * math.pi)
        qlist_sub *= np.random.uniform(0.5, 4)  # 權重
        try:
            qlist[
                elem[0] : min(elem[1], qlist.shape[0])
            ] += qlist_sub
        except ValueError:
            qlist[
                elem[0] : min(elem[1], qlist.shape[0])
            ] += qlist_sub[
                : min(elem[1], qlist.shape[0]) - elem[0]
            ]
    df_qflow_ratio = pd.DataFrame(qlist, columns=["qr"])
    df_qflow_ratio.index = tlist  # 時間

    ###############################################################
    fig, ax = plt.subplots(1, figsize=(12, 9), sharex=True)
    df_qflow_ratio.plot(ax=ax, grid=True)
    ax.set_xlabel("時間")
    ax.set_ylabel("Ratio")
    plt.tight_layout()
    jut.save_fig("normal_depth_temporal2")
    ###############################################################
    mf_params = mfp.mf_params
    mf_params["export_path"] = sys.argv[1]

    # 隨機產生
    # 變因：地下水深, Kh, Sy, 底泥 Kh, 流量, 河寬
    def variable_generate(
        r, vrange: Tuple, loglog: bool = False
    ) -> float:
        assert isinstance(vrange, tuple)
        assert len(vrange) == 2
        vari = (vrange[1] - vrange[0]) * r + vrange[0]

        if loglog:
            # log scale
            vrange2 = np.log10(vrange)
            vari = np.power(
                10, (vrange2[1] - vrange2[0]) * r + vrange2[0]
            )
        return vari

    mat = []
    for i in range(100):
        r = np.random.uniform(0, 1, 6)
        mat_sub = [
            variable_generate(
                r[0], tuple(mf_params["BAS"]["IC"])
            ),  # 地下水深
            variable_generate(
                r[1],
                tuple(
                    mf_params["LPF"]["Kh"],
                ),
                loglog=True,
            ),
            variable_generate(
                r[2], tuple(mf_params["LPF"]["Sy"])
            ),
            variable_generate(
                r[3], tuple(mf_params["RIV"]["Kh"]), loglog=True
            ),
            variable_generate(
                r[4], tuple(mf_params["RIV"]["qflow_range"])
            ),  # 流量
            variable_generate(
                r[5], tuple(mf_params["RIV"]["riv_width"])
            ),  # 河寬
        ]
        mat.append(mat_sub)
    df_variable = pd.DataFrame(
        mat,
        columns=[
            "IC",
            "Kh",
            "Sy",
            "RIV_Kh",
            "qflow_range",
            "riv_width",
        ],
    )
    print(df_variable)

    (
        base_ws,
        Lx,
        Ly,
        nrow,
        ncol,
        nlay,
        delr,
        delc,
        ztop,
        zbot,
        ibound,
    ) = mfp.mf_predefine(mf_params)
    for index in df_variable.index:
        workspace = os.path.join(base_ws, str(index))
        name = "tutorial01_mf"
        mf = flopy.modflow.Modflow(
            name, exe_name="mf2005", model_ws=workspace
        )

        # MF 單位採用 m/hr
        # DIS
        delv = (ztop - zbot) / nlay
        botm = np.linspace(ztop, zbot, nlay + 1)
        dis = flopy.modflow.ModflowDis(
            mf,
            nlay,
            nrow,
            ncol,
            delr=delr,
            delc=delc,
            top=ztop,
            botm=botm[1:],
            nper=tlist.shape[0] - 1,
            perlen=1,  # 1 hr
            steady=False,
        )

        # BAS
        strt = -df_variable.loc[index, "IC"] * np.ones(
            (nlay, nrow, ncol)
        )  # 初始水位
        bas = flopy.modflow.ModflowBas(
            mf,
            ibound=ibound,  # 邊界
            strt=strt,  # 初始水位
            hnoflo=-999.0,  # 非活動區域
            stoper=0.001,  # 收斂標準值
        )

        # LPF
        lpf = flopy.modflow.ModflowLpf(
            mf,
            hk=df_variable.loc[index, "Kh"],
            vka=1.0,
            sy=df_variable.loc[index, "Sy"],
            ss=1e-4,
            laytyp=1,
            ipakcb=53,
        )
        pcg = flopy.modflow.ModflowPcg(mf)

        # RIV
        stress_period_data = {}
        for t in range(1, df_qflow_ratio.shape[0]):
            OC = open_channel.open_channel(
                channel_width=df_variable.loc[
                    index, "riv_width"
                ],
                slope=0.005,
                manning_n=0.013,
            )
            normal_h = OC.determine_normal_depth(
                df_qflow_ratio.loc[t, "qr"]
            )  # cms

            # 整個河面積 100 * 10 = 1000 m
            # 等校分配到分成 10 個網格
            stress_period_data[t] = [
                [
                    0,
                    row,
                    col,
                    normal_h,
                    df_variable.loc[index, "RIV_Kh"]
                    / mf_params["RIV"]["rbot"]
                    * df_variable.loc[index, "river_width"]
                    / 10,
                    mf_params["RIV"]["rbot"],
                ]
                for row in range(nrow)
                for col in range(10)
            ]
        riv = flopy.modflow.ModflowRiv(
            mf, stress_period_data=stress_period_data
        )

        stress_period_data = {}
        for kper in range(tlist.shape[0] - 1):
            for kstp in range(1):
                stress_period_data[(kper, kstp)] = [
                    "save head",
                    "save drawdown",
                    "save budget",
                    "print head",
                    "print budget",
                ]
        oc = flopy.modflow.ModflowOc(
            mf,
            stress_period_data=stress_period_data,
            extension=["oc", "hds", "cbc"],
            unitnumber=[14, 51, 53],
            compact=True,
        )
        # Write the model input files
        print(
            "Write MF model",
            os.path.join(workspace, "{}.nam".format(name)),
        )
        mf.write_input()
        # MF running
        print("Execute MF model")
        mf_execute.run_modflow_multiple_ways(
            mf,
            modelws=workspace,
            exec="mf_owhm_2_1",
            nam_fname=name,
        )

        # Post Analysis
        hds_fname = os.path.join(
            workspace, "{}.hds".format(name)
        )

        strt: List = mf_utility.read_hds(
            hds_fname, "ALL", mask_val=-999.0
        )

        plt.style.use("bmh")
        fig, ax = plt.subplots(1, figsize=(12, 9), sharex=True)
        legend = []
        for t in range(len(strt)):
            if t < 10:
                ax.plot(
                    np.arange(strt[t].shape[2]) * delc,
                    strt[t][0, 0, :],
                    label=str(t),
                )
                legend.append(str(t))
        ax.legend(legend, shadow=True)
        plt.tight_layout()
        jut.save_fig("strt")

        """
        print(type(cbc_list))
        print(len(cbc_list))
        print(type(cbc_list[0]))
        print(cbc_list[0][0].shape, cbc_list[0][1].shape)
        """

        # sys.exit()
