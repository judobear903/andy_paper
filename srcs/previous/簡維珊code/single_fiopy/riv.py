# -*- coding: utf-8 -*-
"""
Created on Sat Feb 11 02:04:20 2023

@author: 維珊
"""

# -*- coding: utf-8 -*-
import numpy as np
import flopy
import random
import pandas as pd
import flopy.utils.binaryfile as bf
import datetime
import matplotlib.pyplot as plt

# ________________準備河川資料________________
starttime = datetime.datetime.now()
RIV_cond = []
stage = []
thick = []
cond_idx = []
diff = random.uniform(0.01, 0.1)  # 河水位等差
for i in range(1):
    b = []
    stage.append(b)
    a = []
    RIV_cond.append(a)
    c = random.uniform(1, 100)
    cond_idx.append(c)

    # Conductance
    for j in range(1000):
        for k in range(1000):
            if len(a) > 999:
                break
            a.append(c)

    # 河水位 (訓練ann用、水位list之間無連續關係性)

    for j in range(1000):
        stage_idx = random.uniform(45.5, 50)
        stage_idx_1 = random.uniform(45.5, 50)
        # stage_idx=46
        # stage_idx_1=48
        for k in range(random.randrange(1, 30)):
            if len(b) > 999:
                break
            b.append(stage_idx)
        for k in range(random.randrange(1, 30)):
            if len(b) > 999:
                break
            b.append(stage_idx_1)
            # b.sort()
        # stage.append(b)
"""
#河水位(做要接成一條河川的單一河川網格的、有diff)               
    for j in range(1000):
        if i == 0:
            stage_idx = random.uniform(45.5, 50)
            stage_idx_1 = random.uniform(45.5, 50)
            for k in range(random.randrange(1, 30)):
                if len(b) > 99:
                    break
                b.append(stage_idx)
            for k in range(random.randrange(1, 30)):
                if len(b) > 99:
                    break
                b.append(stage_idx_1)
        else:
            if j < len(stage[i-1]):
                stage_idx = stage[i-1][j] + diff
            else:
                stage_idx = random.uniform(45.5, 50)
            if len(b) > 99:
                break
            b.append(stage_idx)            
"""


pd.DataFrame(stage).to_csv(
    "D:/paper_data/原始資料/RIV隨機/test/stage.csv"
)
# well = pd.DataFrame(well)
# well.to_csv('D:/論文資料/ANN_gwater/input/4test-confined-p/pump.csv')

# _______________建立modflow基本設置_____________
# create flopy model
modelname = "4000m_mf"
mf = flopy.modflow.Modflow(modelname, exe_name="mf2005")

# model information
Lx = 4000.0
Ly = 4000.0
ztop = 30.0
zbot = 0.0
nlay = 1
nrow = 400
ncol = 400
delr = Lx / ncol
delc = Ly / nrow
delv = (ztop - zbot) / nlay
botm = np.linspace(ztop, zbot, nlay + 1)
sy = 0.1
# ss = 0.01
# 變成unconfined(confined laytyp=0)
laytyp = 0

# basic package(constant head) 四周定水頭
ibound = np.ones((nlay, nrow, ncol), dtype=np.int32)
ibound[:, :, 0] = -1
ibound[:, :, -1] = -1
ibound[:, 0, :] = -1
ibound[:, -1, :] = -1
strt = 40 * np.ones(
    (nlay, nrow, ncol), dtype=np.float32
)  # 初始水位

nper = 1000
perlen = np.ones(1000)
nstp = np.ones(1000)
steady = []
for i in range(1000):
    if i == 0:
        steady.append(False)
    else:
        steady.append(False)
# create flopy objects that do not change with time
dis = flopy.modflow.ModflowDis(
    mf,
    nlay,
    nrow,
    ncol,
    delr=delr,
    delc=delc,
    top=ztop,
    botm=botm[1:],
    nper=nper,
    perlen=perlen,
    nstp=nstp,
    itmuni=4,
    steady=steady,
)
bas = flopy.modflow.ModflowBas(mf, ibound=ibound, strt=strt)
pcg = flopy.modflow.ModflowPcg(mf)
# _______________建立操作迴圈 _____________________
ss_idx = []
hk_idx = []
T_idx = []
S_idx = []

rbot = 45
for i in range(len(stage)):
    stress_period_data = {}
    bb = stage[i]

    # 河川設置
    cc = RIV_cond[i]
    # bb = stage[i]
    for k in range(len(bb)):
        stress_period_data[k] = [
            0,
            nrow / 2,
            ncol / 2,
            bb[k],
            cc[k],
            rbot,
        ]

    riv = flopy.modflow.ModflowRiv(
        mf, stress_period_data=stress_period_data
    )
    T = random.randrange(30, 1440)
    T_idx.append(T)
    hk = T / delv
    S = random.uniform(0.0015, 0.03)
    S_idx.append(S)
    ss = S / delv

    # 基本設置
    while True:
        vka = hk
        #        sy = random.uniform(0.1,0.3)
        #        lpf = flopy.modflow.ModflowLpf(mf, hk=hk, vka=vka, sy=sy, ss=ss, laytyp=laytyp, ipakcb=53)
        lpf = flopy.modflow.ModflowLpf(
            mf, hk=hk, vka=vka, sy=sy, ss=ss, ipakcb=53
        )
        ss_idx.append(ss)
        hk_idx.append(hk)
        # output設置
        stress_period_data = {}
        for kper in range(nper):
            for kstp in range(int(nstp[kper])):
                stress_period_data[(kper, kstp)] = [
                    "save head",
                    "save drawdown",
                    "save budget",
                    "print head",
                    "print budget",
                ]
        oc = flopy.modflow.ModflowOc(
            mf,
            stress_period_data=stress_period_data,
            compact=True,
        )

        # RUNNING THE MODEL
        mf.write_input()
        success, buff = mf.run_model()
        if not success:
            raise Exception(
                "MODFLOW did not terminate normally."
            )

        # ___________________結果後處理 ___________________________
        # 取head資料
        headobj = bf.HeadFile(modelname + ".hds")
        times = headobj.get_times()
        cbb = bf.CellBudgetFile(modelname + ".cbc")

        # plot head - time
        idx = []
        #        for q in range(int(ncol / 2)):
        idx.append((0, int(nrow / 2), int(ncol / 2)))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 5))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 10))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 15))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 20))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 25))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 30))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 35))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 40))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 45))
        ts = []
        # for m in range(int(ncol/2)):
        for m in range(10):
            head = headobj.get_ts(idx[m])
            ts.append(head[:, 1])
            one = ts[0]
        if min(one) < 0:
            print("false")
            #            continue
            break
        else:
            df = pd.DataFrame(ts)
            df.to_csv("test.csv")
            break

    # 轉置成神經可用的input
    d = pd.read_csv("test.csv")
    d = d.values
    d = np.delete(d, 0, axis=1)
    e = d
    data1 = list(map(list, zip(*e)))
    data1 = pd.DataFrame(data1)
    data1 = data1.values
    data1 = np.insert(data1, 0, bb, axis=1)  # 河水位
    b = T * np.ones((1000, 1))  # T值
    a = S * np.ones((1000, 1))  # S值
    con = cond_idx[i] * np.ones((1000, 1))  # C值
    data1 = np.column_stack((data1, b))
    data1 = np.column_stack((data1, a))
    data1 = np.column_stack((data1, con))
    data1 = pd.DataFrame(data1)
    data1.to_csv(
        "D:/paper_data/原始資料/RIV隨機/test/riv_"
        + str(i + 1)
        + ".csv"
    )  # ann training


#   data.to_csv('D:/論文資料/ANN_gwater/input/4test-unconfined-25/un4case'+str(i+92)+'.csv')


stage1 = map(list, zip(*stage))
endtime = datetime.datetime.now()
print(endtime - starttime)
