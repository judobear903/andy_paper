# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 15:02:59 2023

@author: 維珊
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Aug 18 15:49:58 2022

@author: 維珊
"""
# -*- coding: utf-8 -*-
import numpy as np
import flopy
import random
import pandas as pd
import flopy.utils.binaryfile as bf
import datetime
import matplotlib.pyplot as plt

# ________________準備RCH資料________________
starttime = datetime.datetime.now()

rechrate = []


for i in range(1):
    rch = []
    rechrate.append(rch)

    for i in range(1000):
        rch_idx = random.uniform(0, 0.008)
        rch_idx_1 = random.uniform(0, 0.008)
        for k in range(random.randrange(1, 5)):
            # rch_idx = random.uniform(0,0.008)
            if len(rch) > 999:
                break
            rch.append(rch_idx)
        for k in range(random.randrange(0, 30)):
            if len(rch) > 999:
                break
            rch.append(0)
        for k in range(random.randrange(1, 5)):
            # rch_idx_1 = random.uniform(0,0.008)
            if len(rch) > 999:
                break
            rch.append(rch_idx_1)


# well = pd.DataFrame(well)
# well.to_csv('D:/論文資料/ANN_gwater/input/4test-confined-p/pump.csv')


# _______________建立modflow基本設置_____________
# create flopy model
modelname = "4000m_mf"
mf = flopy.modflow.Modflow(modelname, exe_name="mf2005")

# model information
Lx = 4000.0
Ly = 4000.0
ztop = 30.0
zbot = 0.0
nlay = 1
nrow = 400
ncol = 400
delr = Lx / ncol
delc = Ly / nrow
delv = (ztop - zbot) / nlay
botm = np.linspace(ztop, zbot, nlay + 1)
sy = 0.1
# ss = 0.01
# 變成unconfined(confined laytyp=0)
laytyp = 0

# basic package(constant head) 四周定水頭
ibound = np.ones((nlay, nrow, ncol), dtype=np.int32)
ibound[:, :, 0] = -1
ibound[:, :, -1] = -1
ibound[:, 0, :] = -1
ibound[:, -1, :] = -1
strt = 40 * np.ones(
    (nlay, nrow, ncol), dtype=np.float32
)  # 初始水位

nper = 1000
perlen = np.ones(1000)
nstp = np.ones(1000)
steady = []
for i in range(1000):
    if i == 0:
        steady.append(False)
    else:
        steady.append(False)
# create flopy objects that do not change with time
dis = flopy.modflow.ModflowDis(
    mf,
    nlay,
    nrow,
    ncol,
    delr=delr,
    delc=delc,
    top=ztop,
    botm=botm[1:],
    nper=nper,
    perlen=perlen,
    nstp=nstp,
    itmuni=4,
    steady=steady,
)
bas = flopy.modflow.ModflowBas(mf, ibound=ibound, strt=strt)
pcg = flopy.modflow.ModflowPcg(mf)


# _______________建立操作迴圈 _____________________
ss_idx = []
hk_idx = []
S_idx = []
T_idx = []

for i in range(len(rechrate)):
    rech_spd = {}
    rr = rechrate[i]

    # RCH設置
    for k in range(len(rr)):
        rech_spd[k] = np.zeros([nrow, ncol])
        rech_spd[k][200, 200] = rr[k]
    rec = flopy.modflow.ModflowRch(mf, nrchop=3, rech=rech_spd)

    # 基本設置
    while True:
        T = random.uniform(30, 1440)
        T_idx.append(T)
        hk = T / delv
        S = random.uniform(0.0015, 0.03)
        S_idx.append(S)
        ss = S / delv

        vka = hk
        # ss =random.uniform(0.00005,0.001)
        #        sy = random.uniform(0.1,0.3)
        #        lpf = flopy.modflow.ModflowLpf(mf, hk=hk, vka=vka, sy=sy, ss=ss, laytyp=laytyp, ipakcb=53)
        lpf = flopy.modflow.ModflowLpf(
            mf, hk=hk, vka=vka, sy=sy, ss=ss, ipakcb=53
        )
        ss_idx.append(ss)
        hk_idx.append(hk)
        # output設置
        stress_period_data = {}
        for kper in range(nper):
            for kstp in range(int(nstp[kper])):
                stress_period_data[(kper, kstp)] = [
                    "save head",
                    "save drawdown",
                    "save budget",
                    "print head",
                    "print budget",
                ]
        oc = flopy.modflow.ModflowOc(
            mf,
            stress_period_data=stress_period_data,
            compact=True,
        )

        # RUNNING THE MODEL
        mf.write_input()
        success, buff = mf.run_model()
        if not success:
            raise Exception(
                "MODFLOW did not terminate normally."
            )

        # ___________________結果後處理 ___________________________
        # 取head資料
        headobj = bf.HeadFile(modelname + ".hds")
        times = headobj.get_times()
        cbb = bf.CellBudgetFile(modelname + ".cbc")

        # plot head - time
        idx = []
        #        for q in range(int(ncol / 2)):
        idx.append((0, int(nrow / 2), int(ncol / 2)))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 5))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 10))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 15))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 20))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 25))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 30))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 35))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 40))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 45))
        ts = []
        # for m in range(int(ncol/2)):
        for m in range(10):
            head = headobj.get_ts(idx[m])
            ts.append(head[:, 1])
            one = ts[0]
        if min(one) < 0:
            print("false")
            #            continue
            break
        else:
            df = pd.DataFrame(ts)
            df.to_csv("test.csv")
            break
    initial = 40 * np.ones((10, 20))
    # 轉置成神經可用的input
    d = pd.read_csv("test.csv")
    d = d.values
    d = np.delete(d, 0, axis=1)
    d = d - initial
    data = list(map(list, zip(*d)))
    data = pd.DataFrame(data)
    data = data.values
    # data = np.insert(data, 0, rr, axis=1)#補注量
    b = T * np.ones((1000, 1))
    # print(f'b={b}')
    a = S * np.ones((1000, 1))
    #    c = 4* np.ones((1000,1))
    # data = np.column_stack((data,b))
    # data = np.column_stack((data,a))
    data = pd.DataFrame(data)
    data.to_csv(
        "D:/paper_data/測試/RCH隨機/multi-test/rch_"
        + str(i + 1)
        + ".csv"
    )
#    data.to_csv('D:/論文資料/ANN_gwater/input/4test-unconfined-25/un4case'+str(i+92)+'.csv')

r1 = map(list, zip(*rechrate))
pd.DataFrame(r1).to_csv(
    "D:/paper_data/測試/RCH隨機/multi-test/flux2.csv"
)
pd.DataFrame(ss_idx).to_csv(
    "D:/paper_data/測試/RCH隨機/multi-test/ss2.csv"
)
pd.DataFrame(hk_idx).to_csv(
    "D:/paper_data/測試/RCH隨機/multi-test/hk2.csv"
)

endtime = datetime.datetime.now()
print(endtime - starttime)
