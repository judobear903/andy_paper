# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 13:41:46 2022

@author: 維珊
"""

# -*- coding: utf-8 -*-
import numpy as np
import flopy
import random
import pandas as pd
import flopy.utils.binaryfile as bf

# 目的________做多井案例(出golden data)
# 1. 輸入河川網格位置(rol、col)、觀測井位置(rol、col)、參數(para.csv)、注意侷限/非侷限
# 2. 建立各抽水井抽水方案
# 3. 輸出水井位置/水位結果
# ________________________

# 補注網格位置
rch = pd.read_csv("rech1-1.csv")
rch = rch.values
# 觀測井位置
t = pd.read_csv("target1-1.csv")
t = t.values

# 參數
para = pd.read_csv("para-1.csv")
para = para.values
para = np.delete(para, 0, axis=1)

# create flopy model
modelname = "4000m_mf"
mf = flopy.modflow.Modflow(modelname, exe_name="mf2005")

# model information
Lx = float(para[0])  # 4000.0
Ly = float(para[1])
ztop = 30.0
zbot = 0.0
nlay = 1
nrow = int(para[2])  # 400
ncol = int(para[3])  # 400
delr = Lx / ncol
delc = Ly / nrow
delv = (ztop - zbot) / nlay
botm = np.linspace(ztop, zbot, nlay + 1)
k1 = float(para[4])
hk = k1 * np.ones((nlay, nrow, ncol), dtype=np.int32)
# hk[:,:,501:1000] = para[5]


# 注意侷限/非侷限
laytyp = 0

# 定義初始水位
if laytyp == 1:
    waterlevel = 25
else:
    waterlevel = 40


vka = hk
ss = float(para[6])  # 0.000545461
ss = ss * np.ones((nlay, nrow, ncol), dtype=np.int32)
# ss[:,501:1000,:] = para[7]
sy = para[8]  # 0.01
sy = sy * np.ones((nlay, nrow, ncol), dtype=np.int32)
# sy[:,501:1000,:] = para[9]


# basic package(constant head) 四周定水頭
ibound = np.ones((nlay, nrow, ncol), dtype=np.int32)
ibound[:, :, 0] = -1
ibound[:, :, -1] = -1
ibound[:, 0, :] = -1
ibound[:, -1, :] = -1
strt = waterlevel * np.ones((nlay, nrow, ncol), dtype=np.float32)

# stress periods
nper = int(para[10])  # 500
perlen = np.ones(nper)
nstp = np.ones(nper)
steady = []
for i in range(nper):
    if i == 0:
        steady.append(False)
    else:
        steady.append(False)

# create flopy objects that do not change with time
dis = flopy.modflow.ModflowDis(
    mf,
    nlay,
    nrow,
    ncol,
    delr=delr,
    delc=delc,
    top=ztop,
    botm=botm[1:],
    nper=nper,
    perlen=perlen,
    nstp=nstp,
    itmuni=4,
    steady=steady,
)

bas = flopy.modflow.ModflowBas(mf, ibound=ibound, strt=strt)
# lpf = flopy.modflow.ModflowLpf(mf, hk=hk, vka=vka, sy=sy, ss=ss, ipakcb=53)
lpf = flopy.modflow.ModflowLpf(
    mf, hk=hk, vka=vka, sy=sy, ss=ss, laytyp=laytyp, ipakcb=53
)
pcg = flopy.modflow.ModflowPcg(mf)

############ Transient Rechage Package ##########

"""
#輸入已知補注深度
rechrate = pd.read_csv("D:/paper_data/測試/flux.csv")
rechrate = rechrate.values   
rechrate = np.delete(rechrate,[0],axis=1)


        

for i in range(len(rechrate)): 
    rech_spd = {}
    rr = rechrate[i]
    
#RCH設置
    for k in range(len(rr)):
        rech_spd[k]=np.zeros([nrow,ncol])
        rech_spd[k][200,200] = rr[k]
        rech_spd[k][200,201] = rr[k]
        rech_spd[k][201,201] = rr[k]
        rech_spd[k][201,200] = rr[k]
    rec = flopy.modflow.ModflowRch(mf, nrchop=3, rech=rech_spd)
"""
# 讀取 rechrate csv 檔
rechrate = pd.read_csv("D:/paper_data/multi測試/flux.csv")
rechrate = rechrate.values
rechrate = np.delete(rechrate, [0], axis=1)

# 創建 rech_spd 字典
rech_spd = {}
for i in range(len(rechrate)):
    rr = rechrate[i]

    # 將特定位置的值設置到 rech_spd 字典中
    positions = rch
    for k in range(len(rr)):
        if k not in rech_spd:
            rech_spd[k] = np.zeros([nrow, ncol])
        for pos in positions:
            rech_spd[k][pos[0], pos[1]] = rr[k]

    # 使用 rech_spd 字典設置 MODFLOW 模型中的 RCH
    # for k, v in rech_spd.items():
    rec = flopy.modflow.ModflowRch(mf, nrchop=3, rech=rech_spd)

############ Outpur Control #################
stress_period_data = {}
for kper in range(nper):
    for kstp in range(int(nstp[kper])):
        stress_period_data[(kper, kstp)] = [
            "save head",
            "save drawdown",
            "save budget",
            "print head",
            "print budget",
        ]
oc = flopy.modflow.ModflowOc(
    mf, stress_period_data=stress_period_data, compact=True
)

############ RUNNING THE MODEL ################
# write the model input files
mf.write_input()
# run the model
success, buff = mf.run_model()
if not success:
    raise Exception("MODFLOW did not terminate normally.")

########### 結果後處理 ################
# 取head資料
headobj = bf.HeadFile(modelname + ".hds")
times = headobj.get_times()
cbb = bf.CellBudgetFile(modelname + ".cbc")

############### plot head - time ######################3
import matplotlib.pyplot as plt

# col/row(補注網格位置，用x,y表示)
rcht = []
for i in range(len(rch)):
    rcht.append(tuple(rch[i] * 10))

# col/row
tar = []
for i in range(len(t)):
    tar.append(tuple(t[i]))

# 畫補注網格及觀測井位置圖
fig = plt.figure(figsize=(15, 15))
ax = fig.add_subplot()
pmv = flopy.plot.PlotMapView(model=mf, layer=0, ax=ax)
qm = pmv.plot_ibound()
lc = pmv.plot_grid()
# qm = pmv.plot_bc(name='rec')


for i in range(len(rch)):
    (l1,) = ax.plot(
        rcht[i][1],
        rcht[i][0],
        lw=0,
        marker="s",
        color="green",
        markersize=15,
        markeredgewidth=1.5,
        markeredgecolor="blue",
        zorder=9,
    )  # , label='well')
    ax.text(
        rcht[i][1] + 18,
        rcht[i][0] - 18,
        "r" + str(i + 1),
        size=30,
        zorder=12,
    )

for i in range(len(t)):
    (l2,) = ax.plot(
        tar[i][1],
        tar[i][0],
        lw=0,
        marker="o",
        color="red",
        markersize=15,
        markeredgewidth=0.5,
        markeredgecolor="black",
        zorder=9,
    )
    ax.text(
        tar[i][1] + 18,
        tar[i][0] - 18,
        "o" + str(i + 1),
        size=30,
        zorder=12,
    )
"""    
    if i ==1:
        l2,=ax.plot(tar[i][1],tar[i][0],lw=0,marker="o",color='red',
            markersize=15,markeredgewidth=0.5,
            markeredgecolor="black",zorder=9)
        ax.text(tar[i][1]+18, tar[i][0] - 18, 'o'+str(i+1), size=30, zorder=12)
    elif i ==4:
        l2,=ax.plot(tar[i][1],tar[i][0],lw=0,marker="o",color='red',
            markersize=15,markeredgewidth=0.5,
            markeredgecolor="black",zorder=9)
        ax.text(tar[i][1]+18, tar[i][0] - 18, 'o'+str(i+1), size=30, zorder=12)
    elif i ==7:
        l2,=ax.plot(tar[i][1],tar[i][0],lw=0,marker="o",color='red',
            markersize=15,markeredgewidth=0.5,
            markeredgecolor="black",zorder=9)
        ax.text(tar[i][1]+18, tar[i][0] - 18, 'o'+str(i+1), size=30, zorder=12)
    elif i ==9:
        l2,=ax.plot(tar[i][1],tar[i][0],lw=0,marker="o",color='red',
            markersize=15,markeredgewidth=0.5,
            markeredgecolor="black",zorder=9)
        ax.text(tar[i][1]+18, tar[i][0] - 18, 'o'+str(i+1), size=30, zorder=12)
    else:
        continue
"""

plt.legend(
    handles=[l1, l2],
    labels=["rch grid", "observation well"],
    loc="upper left",
)

# fig2 = plt.figure(figsize=(15, 15))
# plt.imshow(rech_spd[0])
# 輸出水位資料

# row/col
idx = []
for q in range(len(rch)):
    idx.append(
        (0, int(rch[q][0]), int(rch[q][1]))
    )  # 補注網格位置水位
for q in range(len(t)):
    idx.append(
        (0, int(t[q][0] / 10), int(t[q][1] / 10))
    )  # 觀測井位置水位


ts = []
for m in range(len(idx)):
    head = headobj.get_ts(idx[m])
    ts.append(head[:, 1])
    if min(ts[m]) < 0:
        print("false!")
        break
    else:
        df = pd.DataFrame(ts)
        df.to_csv("test_rch.csv")
