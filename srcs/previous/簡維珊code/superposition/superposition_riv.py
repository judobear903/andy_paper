# -*- coding: utf-8 -*-
import math
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error


# 線性疊加[實際值 和 (單井類神經+疊加) 比較]________________________

# 匯入點的位置
river = pd.read_csv("D:/paper_data/multi測試/river1-1.csv")
river = river.values
target = pd.read_csv("D:/paper_data/multi測試/target1-1.csv")
target = target.values


# 算各自距離(ex:抽水井1對觀測井1、2、3各自的距離，抽水井2對觀測井1、2、3各自的距離...，存成一個list)
r = []
for i in range(len(target)):
    for j in range(len(river)):
        a = (
            math.sqrt(
                (target[i, 0] - river[j, 0]) ** 2
                + (target[i, 1] - river[j, 1]) ** 2
            ),
        )
        r.append(a)
r = np.array(r)


def split_list(init_list, list_len):
    list_of_groups = zip(*(iter(init_list),) * list_len)
    end_list = [list(i) for i in list_of_groups]
    count = len(init_list) % list_len
    (
        end_list.append(init_list[-count:])
        if count != 0
        else end_list
    )
    return end_list


# distance[0]:target1和river1~6間距離(distance[0]為觀測井1跟抽水井1~6各自的距離，ex:distance[1]可能就是觀測井2對抽水井1~6各自的距離)
distance = split_list(r, len(river))

# 計算疊加水位(各個單井類神經預測水位做疊加)
one = np.ones((100, 1))
intial = 40 * one
zero = 0 * one
D2 = []
for i in range(len(target)):
    D1 = []
    for j in range(len(river)):
        data = pd.read_csv(
            "D:/paper_data/multi測試/riv_" + str(j + 1) + ".csv"
        )  # 讀的檔案是單抽水井ANN模式預測出來的水位
        data = data.values
        data = np.delete(data, [0], axis=1)
        # 大於450的用0
        if distance[i][j] > 450:
            d1 = zero[:, 0]
            D1.append(d1)
        # 距離/10 可以被5整除(代表是整數。直接可以用)
        elif (distance[i][j] / 10) % 5 == 0:
            c = int((distance[i][j] / 10) / 5)  # -1
            d1 = data[:, c]
            D1.append(d1)
        # 其他不是整數也沒超過450的用計算的(線性內插)
        else:
            d = int(distance[i][j] / 50)  # -1
            d1 = (
                data[:, d + 1]
                - (
                    (data[:, d + 1] - data[:, d])
                    * ((d + 1) * 50 - distance[i][j])
                )
                / 50
            )
            D1.append(d1)
    D2.append(D1)

head2 = []
t = []
h = intial[0, 0]
for k in range(len(D2)):
    t.append(sum(D2[k]))
    head0 = intial[:, 0] + t
    head2.append(head0)

# 輸入實際值(3.產生的結果)
actual = pd.read_csv("D:/paper_data/multi測試/test.csv")
actual = actual.values
actual = np.delete(actual, [0], axis=1)
"""
xmax =[]
xmin =[]
for i in range(len(target)): 
    xmax.append(actual[5:, i+len(river)+1].max())
    xmin.append(actual[5:, i+len(river)+1].min())
"""


def normalization(data):
    _range = np.max(data) - np.min(data)
    return (data - np.min(data)) / _range


def nor(data, head):
    _min = np.min(data)
    _range = np.max(data) - np.min(data)
    return (head - _min) / _range


"""
#計算疊加水位(參數檢定用的)(如果圖只要畫檢定前的那留上面那段這段槓掉，如果圖要檢定前後的話，這個也留著)
one = np.ones((500,1))
intial = 25 * one
zero = 0 * one
D_21 = []
for i in range(len(target)):
    D1 = []    
    for j in range(len(river)):
        #輸入讀取檔案的路徑
        data = pd.read_csv('D:/論文資料/ANN_gwater/input/un_case5-1/un_case5_real_w'+str(j+1)+'.csv')#讀的檔案是參數檢定後的檔案
        data =data.values    
        data = np.delete(data,[0,1,12,13],axis=1) 
    #大於450的用0
        if distance[i][j] > 450:
            d1 = zero[:,0]
            D1.append(d1)   
    #距離/10 可以被5整除(代表是整數。直接可以用)
        elif (distance[i][j]/10) % 5 ==0:
            c = int((distance[i][j]/10)/5)#-1
            d1 = intial[:,0] -data[:,c]
            D1.append(d1)
    #其他不是整數也沒超過450的用計算的(線性內插)
        else:
            d = int(distance[i][j]/50)#-1
            d1 = intial[:,0] -(data[:,d+1]-((data[:,d+1]-data[:,d])*((d+1)*50 - distance[i][j])) / 50 )
            D1.append(d1)
    D_21.append(D1)
    
Head2 = []
t = []
h = intial[0,0] 
for k in range(len(D_21)):
    t.append(sum(D_21[k]))
    head0 = intial[:,0] - t
    Head2.append(head0)

#相關性計算
H21 =[]
for i in range(len(target)):
    H21.append(mean_squared_error(actual[3:,i+len(river)+1], Head2[9][i,3:].T))
h21_1 =[]
for i in range(len(target)):
    h21_1.append(sum(((Head2[9][i,3:].T)-(np.mean(actual[2:,i+len(river)+1])))**2)/(len(Head2[0][0])))
Hreal=[]
for i in range(len(H21)):
    Hreal.append(H21[i]/h21_1[i])
"""
# 畫圖(水位時序比較)
for i in range(len(target)):
    # actual1 = normalization(actual[5:,i+len(river)+1])
    plt.figure(figsize=(6, 4.5), dpi=100)
    # line1, = plt.plot(actual[4], 'b', marker='o', markersize=5)
    (line1,) = plt.plot(
        actual[10:][i], "b", marker="o", markersize=5
    )
    #    line3, = plt.plot(Head2[9][i][2:242],'r', markeredgecolor='r', marker='*', markersize=3, alpha=0.3)
    (line2,) = plt.plot(
        head0[i],
        "k",
        markeredgecolor="k",
        marker="*",
        markersize=5,
    )
    #    plt.legend((line1, line3, line2),('actual','prediction1', 'prediction2'), loc='upper left')
    plt.legend(
        (line1, line2),
        ("actual", "prediction2"),
        loc="upper left",
    )
    plt.title(
        "target" + str(i + 1),
        fontdict={"fontsize": 18, "color": "black"},
    )
    plt.ylabel(r"head(m)", fontsize=14)
    plt.xlabel("time(day)", fontsize=14)
    #    plt.ylim([35.5,40])
    plt.ylim([40, 41])
    plt.savefig("target_riv_" + str(i + 1))
