# -*- coding: utf-8 -*-
import numpy as np
import flopy
import random
import pandas as pd
import flopy.utils.binaryfile as bf
import datetime

# 本code目標__________
"""
1. 在flopy中模擬單一抽水井，做為後續訓練資料
1-1. modflow基本設置(包含模擬大小、網格大小、邊界、初始水位等等)
2. 變數：抽水量/抽水型態、k、s

"""
# ________________準備抽水資料(本研究共設置9種不同抽水型態、但建議都以隨機產生為主)________________
starttime = datetime.datetime.now()
well = []
"""
#抽24補24
for i in range(1):
    count = 0    
    a = []
    well.append(a)
    for i in range(1000):
        count = count + 1
        pumping = random.randrange(50,600)   
        if i ==0:
            a.append(pumping*(-1))
        elif(count-1) %24 == 0:
            a.append(a[-1] *(-1))
        else:
            a.append(a[-1]) 

#抽48補48          
for i in range(1):
    count = 0    
    a = []
    well.append(a)
    for i in range(1000):
        count = count + 1
        pumping = random.randrange(50,800)
        if i ==0:
            a.append(pumping*(-1))
        elif(count-1) %48 == 0:
            a.append(a[-1] *(-1))
        else:
            a.append(a[-1])

#抽30停30
for i in range(1):  
    a = []
    well.append(a)
    pumping = random.randrange(20,850)
    for i in range(100):        
        for k in range(30):
            if len(a) >999:
                break
            a.append(-pumping)
        for k in range(30):
            if len(a) >999:
                break
            a.append(0)  
"""
# 抽12停12
for i in range(1):
    a = []
    well.append(a)
    pumping = random.randrange(20, 900)
    for i in range(100):
        for k in range(12):
            if len(a) > 999:
                break
            a.append(-pumping)
        for k in range(12):
            if len(a) > 999:
                break
            a.append(0)
"""
#抽24停12補12
for i in range(1):  
    a = []
    well.append(a)
    pumping = random.randrange(50,900)            
    for i in range(21):
        for k in range(24):
            a.append(-pumping)
        for k in range(12):
            a.append(0)    
        for k in range(12):
            a.append(pumping)    
            if len(a) >999:
                break

#抽48補10停15     
for i in range(1):  
    a = []
    well.append(a)
    pumping = random.randrange(50,900)
    for i in range(14):   
        for k in range(48):
            a.append(-pumping)
        for k in range(10):
            if len(a) >999:
                break         
            a.append(pumping)  
        for k in range(15):    
            if len(a) >999:
                break 
            a.append(0)         

#偏差t/4
for i in range(1):  
    a = []
    well.append(a)
    pumping = random.randrange(50,900)
    for i in range(25):   
        for k in range(12):
            if len(a) >13:
                break       
            a.append(pumping)
        for k in range(24):
            if len(a) >999:
                break         
            a.append(-pumping)  
        for k in range(24):    
            if len(a) >999:
                break 
            a.append(pumping)

#抽12停24(抽的量不同)
for i in range(1):  
    a = []
    well.append(a)    
    for i in range(100): 
        pumping = random.randrange(50,850)
        for k in range(12):
            if len(a) >999:
                break
            a.append(-pumping)
        for k in range(24):
            if len(a) >999:
                break
            a.append(0)  

#抽的時間不固定、量一樣
for i in range(1):  
    a = []
    well.append(a)    
    for i in range(100):
        pumping = random.randrange(50,900)
        for k in range(random.randrange(0,30)):
            if len(a) > 999:
                break
            a.append(-pumping)
        for k in range(random.randrange(5,80)):
            if len(a) > 999:
                break
            a.append(0)

#抽補停時間、量都不一樣
for i in range(1):  
    a = []
    well.append(a)    
    for i in range(100):
        pumping = random.randrange(50,500)
        pumping1 = random.randrange(50,500)
        for k in range(random.randrange(5,20)):            
            if len(a) > 999:
                break
            a.append(-pumping)
        for k in range(random.randrange(5,40)):
            if len(a) > 999:
                break
            a.append(0)
        for k in range(random.randrange(5,30)):           
            if len(a) > 999:
                break
            a.append(pumping1)   
"""
# well = pd.DataFrame(well)
# well.to_csv('D:/論文資料/ANN_gwater/input/4test-confined-p/pump.csv')


# _______________建立modflow基本設置_____________
# create flopy model
modelname = "1000m_mf"
mf = flopy.modflow.Modflow(modelname, exe_name="mf2005")

# model information
Lx = 4000.0
Ly = 4000.0
ztop = 30.0
zbot = 0.0
nlay = 1
nrow = 400
ncol = 400
delr = Lx / ncol
delc = Ly / nrow
delv = (ztop - zbot) / nlay
botm = np.linspace(ztop, zbot, nlay + 1)
sy = 0.1
# ss = 0.01
# 變成unconfined(confined laytyp=0)
# laytyp = 1

# basic package(constant head) 四周定水頭
ibound = np.ones((nlay, nrow, ncol), dtype=np.int32)
ibound[:, :, 0] = -1
ibound[:, :, -1] = -1
ibound[:, 0, :] = -1
ibound[:, -1, :] = -1
strt = 40 * np.ones((nlay, nrow, ncol), dtype=np.float32)

nper = 1000
perlen = np.ones(1000)
nstp = np.ones(1000)
steady = []
for i in range(1000):
    if i == 0:
        steady.append(False)
    else:
        steady.append(False)
# create flopy objects that do not change with time
dis = flopy.modflow.ModflowDis(
    mf,
    nlay,
    nrow,
    ncol,
    delr=delr,
    delc=delc,
    top=ztop,
    botm=botm[1:],
    nper=nper,
    perlen=perlen,
    nstp=nstp,
    itmuni=3,
    steady=steady,
)
bas = flopy.modflow.ModflowBas(mf, ibound=ibound, strt=strt)
pcg = flopy.modflow.ModflowPcg(mf)
# _______________建立操作迴圈 _____________________
for i in range(len(well)):
    # 抽水井設置
    stress_period_data = {}
    w = well[i]
    for k in range(len(w)):
        stress_period_data[k] = [0, nrow / 2, ncol / 2, w[k]]

    wel = flopy.modflow.ModflowWel(
        mf, stress_period_data=stress_period_data
    )

    # 基本設置
    while True:
        hk = random.uniform(0.2, 2)
        vka = hk
        ss = random.uniform(0.000005, 0.001)
        # sy = random.uniform(0.01,0.3)
        #        lpf = flopy.modflow.ModflowLpf(mf, hk=hk, vka=vka, sy=sy, ss=ss, laytyp=laytyp, ipakcb=53)
        lpf = flopy.modflow.ModflowLpf(
            mf, hk=hk, vka=vka, sy=sy, ss=ss, ipakcb=53
        )

        # output設置
        stress_period_data = {}
        for kper in range(nper):
            for kstp in range(int(nstp[kper])):
                stress_period_data[(kper, kstp)] = [
                    "save head",
                    "save drawdown",
                    "save budget",
                    "print head",
                    "print budget",
                ]
        oc = flopy.modflow.ModflowOc(
            mf,
            stress_period_data=stress_period_data,
            compact=True,
        )

        # RUNNING THE MODEL
        mf.write_input()
        success, buff = mf.run_model()
        if not success:
            raise Exception(
                "MODFLOW did not terminate normally."
            )

        # ___________________結果後處理 ___________________________
        # 取head資料
        headobj = bf.HeadFile(modelname + ".hds")
        times = headobj.get_times()
        cbb = bf.CellBudgetFile(modelname + ".cbc")

        # plot head - time
        idx = []
        idx.append((0, int(nrow / 2), int(ncol / 2)))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 5))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 10))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 15))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 20))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 25))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 30))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 35))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 40))
        idx.append((0, int(nrow / 2), int(ncol / 2) + 45))

        ts = []
        for m in range(10):
            head = headobj.get_ts(idx[m])
            ts.append(head[:, 1])
            one = ts[0]
        if min(one) < 0:
            print("false")
            break
        else:
            df = pd.DataFrame(ts)
            df.to_csv("test.csv")
            break

    # 轉置成神經可用的input
    d = pd.read_csv("test.csv")
    d = d.values
    d = np.delete(d, 0, axis=1)
    data = list(map(list, zip(*d)))
    data = pd.DataFrame(data)
    data = data.values
    data = np.insert(data, 0, w, axis=1)
    b = hk * np.ones((1000, 1))
    a = ss * np.ones((1000, 1))
    data = np.column_stack((data, b))
    data = np.column_stack((data, a))
    data = pd.DataFrame(data)
    #    data.to_csv('填上你要儲存檔案的路徑')
    data.to_csv(
        "D:/論文資料/ANN_gwater/input/4train-confined-p-40/4case"
        + str(i + 37)
        + ".csv"
    )

endtime = datetime.datetime.now()
print(endtime - starttime)
