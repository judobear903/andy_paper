# -*- coding: utf-8 -*-
import math
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error


# 線性疊加[實際值 和 (單井類神經+疊加) 比較]________________________

# 匯入點的位置
well = pd.read_csv("D:/論文資料/複雜方案/well1.csv")
well = well.values
target = pd.read_csv("D:/論文資料/複雜方案/target1.csv")
target = target.values


# 算各自距離
r = []
for i in range(len(target)):
    for j in range(len(well)):
        a = (
            math.sqrt(
                (target[i, 0] - well[j, 0]) ** 2
                + (target[i, 1] - well[j, 1]) ** 2
            ),
        )
        r.append(a)
r = np.array(r)


def split_list(init_list, list_len):
    list_of_groups = zip(*(iter(init_list),) * list_len)
    end_list = [list(i) for i in list_of_groups]
    count = len(init_list) % list_len
    (
        end_list.append(init_list[-count:])
        if count != 0
        else end_list
    )
    return end_list


# distance[0]:target1和well1~6間距離
distance = split_list(r, len(well))

# 計算疊加水位
one = np.ones((498, 1))
intial = 25 * one
zero = 0 * one
D_2 = []
for i in range(len(target)):
    D1 = []
    for j in range(len(well)):
        data = pd.read_csv(
            "D:/論文資料/ANN_gwater/workspace2/un_case5-1/m2/unc_case5_w"
            + str(j + 1)
            + "_sim2.csv"
        )
        data = data.values
        data = np.delete(data, [0], axis=1)
        # 大於450的用0
        if distance[i][j] > 450:
            d1 = zero[:, 0]
            D1.append(d1)
        # 距離/10 可以被5整除(代表是整數。直接可以用)
        elif (distance[i][j] / 10) % 5 == 0:
            c = int((distance[i][j] / 10) / 5)  # -1
            d1 = data[:, c]
            D1.append(d1)
        # 其他不是整數也沒超過450的用計算的(線性內插)
        else:
            d = int(distance[i][j] / 50)  # -1
            d1 = (
                data[:, d + 1]
                - (
                    (data[:, d + 1] - data[:, d])
                    * ((d + 1) * 50 - distance[i][j])
                )
                / 50
            )
            D1.append(d1)
    D_2.append(D1)

head2 = []
t = []
h = intial[0, 0]
for k in range(len(D_2)):
    t.append(sum(D_2[k]))
    head0 = intial[:, 0] + t
    head2.append(head0)

# 輸入實際值(3.產生的結果)
actual = pd.read_csv(
    "D:/論文資料/複雜方案/un_case3/unconfined_case3.csv"
)
actual = actual.values

xmax = []
xmin = []
for i in range(len(target)):
    xmax.append(actual[3:, i + len(well) + 1].max())
    xmin.append(actual[3:, i + len(well) + 1].min())


def normalization(data):
    _range = np.max(data) - np.min(data)
    return (data - np.min(data)) / _range


def nor(data, head):
    _min = np.min(data)
    _range = np.max(data) - np.min(data)
    return (head - _min) / _range


one = np.ones((500, 1))
intial = 25 * one
zero = 0 * one
D_21 = []
for i in range(len(target)):
    D1 = []
    for j in range(len(well)):
        # 輸入讀取檔案的路徑
        data = pd.read_csv(
            "D:/論文資料/ANN_gwater/input/un_case5-1/un_case5_real_w"
            + str(j + 1)
            + ".csv"
        )
        data = data.values
        data = np.delete(data, [0, 1, 12, 13], axis=1)
        # 大於450的用0
        if distance[i][j] > 450:
            d1 = zero[:, 0]
            D1.append(d1)
        # 距離/10 可以被5整除(代表是整數。直接可以用)
        elif (distance[i][j] / 10) % 5 == 0:
            c = int((distance[i][j] / 10) / 5)  # -1
            d1 = intial[:, 0] - data[:, c]
            D1.append(d1)
        # 其他不是整數也沒超過450的用計算的(線性內插)
        else:
            d = int(distance[i][j] / 50)  # -1
            d1 = intial[:, 0] - (
                data[:, d + 1]
                - (
                    (data[:, d + 1] - data[:, d])
                    * ((d + 1) * 50 - distance[i][j])
                )
                / 50
            )
            D1.append(d1)
    D_21.append(D1)

Head2 = []
t = []
h = intial[0, 0]
for k in range(len(D_21)):
    t.append(sum(D_21[k]))
    head0 = intial[:, 0] - t
    Head2.append(head0)

# 相關性計算
H21 = []
for i in range(len(target)):
    H21.append(
        mean_squared_error(
            actual[3:, i + len(well) + 1], Head2[9][i, 3:].T
        )
    )
h21_1 = []
for i in range(len(target)):
    h21_1.append(
        sum(
            (
                (Head2[9][i, 3:].T)
                - (np.mean(actual[2:, i + len(well) + 1]))
            )
            ** 2
        )
        / (len(Head2[0][0]))
    )
Hreal = []
for i in range(len(H21)):
    Hreal.append(H21[i] / h21_1[i])

# 畫圖(水位時序比較)
for i in range(len(target)):
    actual1 = normalization(actual[3:, i + len(well) + 1])
    plt.figure(figsize=(6, 4.5), dpi=100)
    (line1,) = plt.plot(
        actual[3:243, i + len(well) + 1],
        "b",
        marker="o",
        markersize=5,
    )
    (line3,) = plt.plot(
        Head2[9][i][2:242],
        "r",
        markeredgecolor="r",
        marker="*",
        markersize=3,
        alpha=0.3,
    )
    (line2,) = plt.plot(
        head2[9][i][2:242],
        "k",
        markeredgecolor="k",
        marker="*",
        markersize=5,
    )
    plt.legend(
        (line1, line3, line2),
        ("actual", "prediction1", "prediction2"),
        loc="upper left",
    )
    plt.title(
        "target" + str(i + 1),
        fontdict={"fontsize": 18, "color": "black"},
    )
    plt.ylabel(r"head(m)", fontsize=14)
    plt.xlabel("time(hr)", fontsize=14)
    #    plt.ylim([35.5,40])
    plt.ylim([24, 25.1])
    plt.savefig("target" + str(i))
