# -*- coding: utf-8 -*-
import numpy as np
import flopy
import random
import pandas as pd
import flopy.utils.binaryfile as bf

# 目的________做多井案例
# 1. 輸入抽水井位置(rol、col)、觀測井位置(rol、col)、參數(para.csv)、注意侷限/非侷限
# 2. 建立各抽水井抽水方案
# 3. 輸出水井位置/水位結果
# ________________________

# 抽水位置
p = pd.read_csv("well1-1.csv")
p = p.values
# 觀測井位置
t = pd.read_csv("target1-1.csv")
t = t.values

# 參數
para = pd.read_csv("para-1.csv")
para = para.values
para = np.delete(para, 0, axis=1)

# create flopy model
modelname = "10000m_mf"
mf = flopy.modflow.Modflow(modelname, exe_name="mf2005")

# model information
Lx = float(para[0])  # 10000.0
Ly = float(para[1])
ztop = 30.0
zbot = 0.0
nlay = 1
nrow = int(para[2])  # 1000
ncol = int(para[3])  # 1000
delr = Lx / ncol
delc = Ly / nrow
delv = (ztop - zbot) / nlay
botm = np.linspace(ztop, zbot, nlay + 1)
k1 = float(para[4])
hk = k1 * np.ones((nlay, nrow, ncol), dtype=np.int32)
# hk[:,:,501:1000] = para[5]


# 注意侷限/非侷限
laytyp = 1

# 定義初始水位
if laytyp == 1:
    waterlevel = 25
else:
    waterlevel = 40


vka = hk
ss = float(para[6])  # 0.000545461
ss = ss * np.ones((nlay, nrow, ncol), dtype=np.int32)
# ss[:,501:1000,:] = para[7]
sy = para[8]  # 0.01
sy = sy * np.ones((nlay, nrow, ncol), dtype=np.int32)
# sy[:,501:1000,:] = para[9]


# basic package(constant head) 四周定水頭
ibound = np.ones((nlay, nrow, ncol), dtype=np.int32)
ibound[:, :, 0] = -1
ibound[:, :, -1] = -1
ibound[:, 0, :] = -1
ibound[:, -1, :] = -1
strt = waterlevel * np.ones((nlay, nrow, ncol), dtype=np.float32)

# stress periods
nper = int(para[10])  # 500
perlen = np.ones(nper)
nstp = np.ones(nper)
steady = []
for i in range(nper):
    if i == 0:
        steady.append(False)
    else:
        steady.append(False)

# create flopy objects that do not change with time
dis = flopy.modflow.ModflowDis(
    mf,
    nlay,
    nrow,
    ncol,
    delr=delr,
    delc=delc,
    top=ztop,
    botm=botm[1:],
    nper=nper,
    perlen=perlen,
    nstp=nstp,
    itmuni=3,
    steady=steady,
)

bas = flopy.modflow.ModflowBas(mf, ibound=ibound, strt=strt)
# lpf = flopy.modflow.ModflowLpf(mf, hk=hk, vka=vka, sy=sy, ss=ss, ipakcb=53)
lpf = flopy.modflow.ModflowLpf(
    mf, hk=hk, vka=vka, sy=sy, ss=ss, laytyp=laytyp, ipakcb=53
)
pcg = flopy.modflow.ModflowPcg(mf)


############ Transient Well Package ##########
# Create the well package
# 產生新的抽水序列
def pumping_data(period):
    a = []
    for i in range(100):
        pumping = random.randrange(50, 250)
        for k in range(random.randrange(5, 15)):
            if len(a) > period - 1:
                break
            a.append(-pumping)
        for k in range(random.randrange(5, 30)):
            if len(a) > period - 1:
                break
            a.append(0)
    return a


# 輸入已知抽水量
well = pd.read_csv("D:/論文資料/複雜方案/c_case1/pump_1.csv")
well = well.values
well = np.delete(well, [0], axis=1)

l1 = []
stress_period_data = {}
for k in range(len(well[0])):
    l = []
    for j in range(len(p)):
        l1 = [0, p[j][0] / delr, p[j][1] / delc, well[j][k]]
        l.append(l1)
        stress_period_data[k] = l


wel = flopy.modflow.ModflowWel(
    mf, stress_period_data=stress_period_data
)

############ Outpur Control #################
stress_period_data = {}
for kper in range(nper):
    for kstp in range(int(nstp[kper])):
        stress_period_data[(kper, kstp)] = [
            "save head",
            "save drawdown",
            "save budget",
            "print head",
            "print budget",
        ]
oc = flopy.modflow.ModflowOc(
    mf, stress_period_data=stress_period_data, compact=True
)

############ RUNNING THE MODEL ################
# write the model input files
mf.write_input()
# run the model
success, buff = mf.run_model()
if not success:
    raise Exception("MODFLOW did not terminate normally.")

########### 結果後處理 ################
# 取head資料
headobj = bf.HeadFile(modelname + ".hds")
times = headobj.get_times()
cbb = bf.CellBudgetFile(modelname + ".cbc")

############### plot head - time ######################3
import matplotlib.pyplot as plt

# col/row
wpt = []
for i in range(len(p)):
    wpt.append(tuple(p[i]))

# col/row
tar = []
for i in range(len(t)):
    tar.append(tuple(t[i]))

# 畫水井位置圖
fig = plt.figure(figsize=(15, 15))
ax = fig.add_subplot()
pmv = flopy.plot.PlotMapView(model=mf, layer=0, ax=ax)
qm = pmv.plot_ibound()
lc = pmv.plot_grid()
# qm = pmv.plot_bc()

for i in range(len(p)):
    (l1,) = ax.plot(
        wpt[i][1],
        wpt[i][0],
        lw=0,
        marker="o",
        color="green",
        markersize=15,
        markeredgewidth=0.5,
        markeredgecolor="blue",
        zorder=9,
    )  # , label='well')
    ax.text(
        wpt[i][1] + 18,
        wpt[i][0] - 18,
        "p" + str(i + 1),
        size=30,
        zorder=2,
    )

for i in range(len(t)):
    (l2,) = ax.plot(
        tar[i][1],
        tar[i][0],
        lw=0,
        marker="o",
        color="red",
        markersize=15,
        markeredgewidth=0.5,
        markeredgecolor="black",
        zorder=9,
    )
    ax.text(
        tar[i][1] + 18,
        tar[i][0] - 18,
        "o" + str(i + 1),
        size=30,
        zorder=12,
    )
"""    
    if i ==1:
        l2,=ax.plot(tar[i][1],tar[i][0],lw=0,marker="o",color='red',
            markersize=15,markeredgewidth=0.5,
            markeredgecolor="black",zorder=9)
        ax.text(tar[i][1]+18, tar[i][0] - 18, 'o'+str(i+1), size=30, zorder=12)
    elif i ==4:
        l2,=ax.plot(tar[i][1],tar[i][0],lw=0,marker="o",color='red',
            markersize=15,markeredgewidth=0.5,
            markeredgecolor="black",zorder=9)
        ax.text(tar[i][1]+18, tar[i][0] - 18, 'o'+str(i+1), size=30, zorder=12)
    elif i ==7:
        l2,=ax.plot(tar[i][1],tar[i][0],lw=0,marker="o",color='red',
            markersize=15,markeredgewidth=0.5,
            markeredgecolor="black",zorder=9)
        ax.text(tar[i][1]+18, tar[i][0] - 18, 'o'+str(i+1), size=30, zorder=12)
    elif i ==9:
        l2,=ax.plot(tar[i][1],tar[i][0],lw=0,marker="o",color='red',
            markersize=15,markeredgewidth=0.5,
            markeredgecolor="black",zorder=9)
        ax.text(tar[i][1]+18, tar[i][0] - 18, 'o'+str(i+1), size=30, zorder=12)
    else:
        continue
"""

plt.legend(
    handles=[l1, l2],
    labels=["pumping well", "observation well"],
    loc="upper left",
)

# 輸出水位資料
"""
#row/col
idx = []
for q in range(len(p)):
    idx.append((0, int(p[q][0]/10), int(p[q][1]/10)))
for q in range(len(t)):
    idx.append((0, int(t[q][0]/10), int(t[q][1]/10)))


ts = []
for m in range(len(idx)):
    head = headobj.get_ts(idx[m])
    ts.append(head[:,1])
    if min(ts[m]) <0:
       print('false!')
       break
    else:
       df = pd.DataFrame(ts) 
       df.to_csv('test.csv')
"""
