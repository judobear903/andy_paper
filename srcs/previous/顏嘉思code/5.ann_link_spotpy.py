# -*- coding: utf-8 -*-
import spotpy
from spotpy.objectivefunctions import rmse  # , mse, mae
from ann import pred11  # , pred, pred2
import os
import pandas as pd
import numpy as np
import math
import sys

# 配合.bat檔使用


flist = [
    os.path.join(sys.argv[1], file)
    for file in os.listdir(sys.argv[1])
]
for argv in sys.argv[2:]:
    if argv.find("weight=") >= 0:
        export_weight = argv.replace("weight=", "")
    elif argv.find("weight_min=") >= 0:
        export_min_weight = argv.replace("weight_min=", "")
    elif argv.find("target_position=") >= 0:
        target_position = argv.replace("target_position=", "")
    elif argv.find("well_position=") >= 0:
        well_position = argv.replace("well_position=", "")
    elif argv.find("initialhead=") >= 0:
        initialhead = int(argv.replace("initialhead=", ""))
    elif argv.find("case=") >= 0:
        case = argv.replace("case=", "")
    elif argv.find("pump=") >= 0:
        pump = argv.replace("pump=", "")


def split_list(init_list, list_len):
    list_of_groups = zip(*(iter(init_list),) * list_len)
    end_list = [list(i) for i in list_of_groups]
    count = len(init_list) % list_len
    (
        end_list.append(init_list[-count:])
        if count != 0
        else end_list
    )
    return end_list


def superposition(target_position, well_position):
    target = pd.read_csv(target_position)
    target = target.values
    well = pd.read_csv(well_position)
    well = well.values
    # 算各自距離
    r2 = []
    for i in range(len(target)):
        for j in range(len(well)):
            a = (
                math.sqrt(
                    (target[i, 0] - well[j, 0]) ** 2
                    + (target[i, 1] - well[j, 1]) ** 2
                ),
            )
            r2.append(a)
    r2 = np.array(r2)

    distance2 = split_list(r2, len(well))

    intial = initialhead * np.ones((500, 1))

    D_1 = []
    for i in range(len(target)):
        D1 = []
        count = 0
        for fname in flist:
            data = pd.read_csv(fname)
            data = data.values
            data = np.delete(data, [0], axis=1)
            # 大於450的用450的水位
            if distance2[i][count] > 500:
                d1 = intial[:, 0] - intial[:, 0]
                D1.append(d1)
            elif 450 <= distance2[i][count] <= 500:
                d1 = data[:, -1]
                D1.append(d1)
            # 距離/10 可以被5整除(代表是整數。直接可以用)
            elif (distance2[i][count] / 10) % 5 == 0:
                c = int((distance2[i][count] / 10) / 5)
                d1 = data[:, c]
                D1.append(d1)
            # 其他不是整數也沒超過450的用計算的
            else:
                d = int(distance2[i][count] / 50)
                d1 = (
                    data[:, d + 1]
                    - (
                        (data[:, d + 1] - data[:, d])
                        * ((d + 1) * 50 - distance2[i][count])
                    )
                    / 50
                )
                D1.append(d1)
            count += 1
        D_1.append(D1)

    head1 = []
    t = []
    for k in range(len(D_1)):
        t.append(sum(D_1[k]))
        # print(t)
        head0 = intial[:, 0] + t
        head1.append(head0)

    head1 = np.array(head1)

    return head1


class spot_setup(object):
    def __init__(self):  # , fname:str):
        self.params = [
            spotpy.parameter.Uniform("k1", 0.2, 2),
            spotpy.parameter.Uniform("k2", 0.2, 2),
            spotpy.parameter.Uniform("k3", 0.2, 2),
            spotpy.parameter.Uniform("k4", 0.2, 2),
            spotpy.parameter.Uniform("k5", 0.2, 2),
            spotpy.parameter.Uniform("k6", 0.2, 2),
            spotpy.parameter.Uniform("s1", 0.00005, 0.001),
            spotpy.parameter.Uniform("s2", 0.00005, 0.001),
            spotpy.parameter.Uniform("s3", 0.00005, 0.001),
            spotpy.parameter.Uniform("s4", 0.00005, 0.001),
            spotpy.parameter.Uniform("s5", 0.00005, 0.001),
            spotpy.parameter.Uniform("s6", 0.00005, 0.001),
        ]

        # Load Observation data from file
        # 輸入權重
        wminfile = pd.read_csv(export_min_weight)
        wminfile = wminfile.values
        self.w = (np.delete(wminfile, [0], axis=1)).T

        weightfile = pd.read_csv(export_weight)
        weightfile = weightfile.values
        self.w1 = (np.delete(weightfile, [0], axis=1)).T

    def parameters(self):
        return spotpy.parameter.generate(self.params)

    def simulation(self, vector):
        # Here the model is actualy started with a unique parameter combination that it gets from spotpy for each time the model is called
        # 輸入抽水序列、初始水位
        pumpfile = pd.read_csv(pump)
        pumpfile = pumpfile.values
        pumpfile = np.delete(pumpfile, [0], axis=1)
        p = pumpfile.T

        # t=2
        p0 = np.zeros((1, p.shape[1]))
        p = np.row_stack([p0, p])  # [:-1]])
        # t=3
        # p0 = np.zeros((2,p.shape[1]))
        # p = np.row_stack([p0,p])#[:-2]])

        intial1 = initialhead * np.ones((p.shape[0], 10))

        # 準備輸入ann的input檔和輸入ann預測
        for i in range(p.shape[1]):
            # 準備參數k、s
            inpu = np.array(vector)
            k = inpu[i] * np.ones((p.shape[0], 1))
            s = inpu[i + 6] * np.ones((p.shape[0], 1))
            # t=1
            """
            inpu = np.column_stack([p[:,i],intial1])
            inpu = np.column_stack([inpu,k])
            inpu = np.column_stack([inpu,s])
            """
            # t=2
            inpu = np.column_stack([p[1:, i], intial1[1:, :]])
            inpu = np.column_stack([inpu, k[1:, :]])
            inpu = np.column_stack([inpu, s[1:, :]])
            inpu = np.column_stack([inpu, p[:-1, i]])
            inpu = np.column_stack([inpu, intial1[:-1, :]])
            """
            #t=3
            inpu = np.column_stack([k[2:,:],s[2:,:]])
            inpu = np.column_stack([inpu,p[2:,i]])
            inpu = np.column_stack([inpu,intial1[2:,:]])           
            inpu = np.column_stack([inpu,p[1:-1,i]])
            inpu = np.column_stack([inpu, intial1[1:-1,:]])
            inpu = np.column_stack([inpu,p[:-2,i]])
            inpu = np.column_stack([inpu, intial1[:-2,:]])
            """
            # 正規化+回傳至ann預測
            head = (inpu - self.w) / (self.w1 - self.w)
            h_sim = pred11(head)
            # 存成csv檔
            pd.DataFrame(h_sim).to_csv(
                "D:/論文資料/spotpy/input/c_m2_case3_1/h_sim_"
                + str(i + 1)
                + ".csv"
            )
        # 再superposition
        s_head = superposition(target_position, well_position)
        return s_head.reshape((-1))

    def evaluation(self):
        # 觀測出的水位
        target = pd.read_csv(target_position)
        target = target.values
        well = pd.read_csv(well_position)
        well = well.values

        true = pd.read_csv(case)
        true = true.values
        true = np.delete(true, [0], axis=1)
        true = true[:, len(well) : len(well) + len(target)]
        # pd.DataFrame(true.reshape((-1))).to_csv('D:/論文資料/spotpy/input/m3_1/true3.csv')
        return true.reshape((-1))

    def objectivefunction(
        self, simulation, evaluation, params=None
    ):
        RMSE = -rmse(evaluation, simulation)
        # MSE = -mse(evaluation,simulation)
        # MAE = -mae(evaluation,simulation)
        return RMSE  # MAE, MSE,


results = []
# 迭代次數設定
rep = 1000
timeout = 10  # given in second

parallel = "seq"
dbformat = "csv"

# 不同演算法
# sampler=spotpy.algorithms.mc(spot_setup(),parallel=parallel, dbname='m1_RosenMC_c_case3-3', dbformat=dbformat, sim_timeout=timeout)
# sampler.sample(rep)
# results.append(sampler.getdata())

# sampler=spotpy.algorithms.lhs(spot_setup(),parallel=parallel, dbname='m1_RosenLHS_c_case3-3', dbformat=dbformat, sim_timeout=timeout)
# sampler.sample(rep)
# results.append(sampler.getdata())

sampler = spotpy.algorithms.sa(
    spot_setup(),
    parallel=parallel,
    dbname="m2_RosenSA_c_case3-1_t9",
    dbformat=dbformat,
    sim_timeout=timeout,
)
sampler.sample(rep)
results.append(sampler.getdata())
# sys.exit()
