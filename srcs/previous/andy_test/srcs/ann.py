# -*- coding: utf-8 -*-
import datetime
import os
import sys
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error

# from sklearn.metrics import mean_absolute_percentage_error
# import TensorFlow as tf
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.models import load_model
from keras import initializers
from keras.optimizers import SGD, Adam
import tensorflow.keras.backend as k
import pickle
from matplotlib import pyplot as plt

# 本code目標____________
"""
0. 需要配合model_train.bat進行操作
1. 設定引數(讀取的檔案、需要輸出的訓練結果)
2. 資料準備(考慮不同時刻長度：t-1、t-1~t-2、t-1~t-3)
3. 訓練類神經網路
4. 用訓練好的網路進行水位洩降預測及繪圖
"""
# ____________________
starttime = datetime.datetime.now()

# 引數
weight_fname = None
export_weight_fname = None
export_model_fname = None
train_history_fname = None

flist = [
    os.path.join(sys.argv[1], file)
    for file in os.listdir(sys.argv[1])
]
for argv in sys.argv[2:]:
    if argv.find("export_param=") >= 0:
        # ANN 輸出參數
        export_weight_fname = argv.replace("param=", "")
    elif argv.find("export_model=") >= 0:
        # ANN 模式輸出
        export_model_fname = argv.replace("=", "")
    elif argv.find("model=") >= 0:
        # ANN 前期模式
        model_fname = argv.replace("model=", "")
    elif argv.find("train_history=") >= 0:
        # ANN 模式輸出
        train_history_fname = argv.replace("=", "")
    elif argv.find("epochs=") >= 0:
        # ANN 模式輸出
        epochs = int(argv.replace("epochs=", ""))
    elif argv.find("loss_history=") >= 0:
        # 模式loss history輸出
        loss_history = argv.replace("=", "")
    elif argv.find("mae_history=") >= 0:
        # 模式mae history輸出
        mae_history = argv.replace("=", "")


# data prepare
def data_preparation(fname):  # str):
    _df_data = pd.read_csv(fname)
    data = np.array(_df_data.iloc[:, 1:].values)
    """  
    #t-1
    xtrain = np.zeros((data.shape[0] - 1, data.shape[1]))
    ytrain = np.zeros((data.shape[0] - 1, 10))
    for t in range(1, data.shape[0]):
        xtrain[t - 1, :] = data[t - 1, :]
        ytrain[t - 1, :] = data[t, 1:11]
    
    intial = 25 * np.ones((data.shape[0] - 1, 10))
    ytrain = ytrain - intial
    xtrain[:,1:11] = xtrain[:,1:11] -  intial
    
    """
    window = 2
    intial = 25 * np.ones((data.shape[0] - window, 10))
    # t-1 + t-2
    xtrain = np.zeros(
        (data.shape[0] - window, 24)
    )  # (1000-window, data.shape[1](13))
    ytrain = np.zeros(
        (data.shape[0] - window, 10)
    )  # (1000-window, 10)
    for t in range(window, data.shape[0]):
        xtrain[t - window, :] = np.concatenate(
            [data[t - 1, :], data[t - 2, :11]]
        )
        ytrain[t - window, :] = data[t, 1:11]

    ytrain = ytrain - intial
    xtrain[:, 1:11] = xtrain[:, 1:11] - intial
    xtrain[:, 14:] = xtrain[:, 14:] - intial
    """ 
    window = 3
    intial = 25 * np.ones((data.shape[0] - window, 10))
    #t-1 + t-2 + t-3    
    xtrain = np.zeros((data.shape[0] - window, 35)) #(1000-window, data.shape[1](13))
    ytrain = np.zeros((data.shape[0] - window, 10))  #(1000-window, 10)
    for t in range(window, data.shape[0]):
        xtrain[t - window, :] = np.concatenate([data[t - 1, 11:], data[t - 1, :11], data[t - 2, :11],data[t - 3, :11]])
        ytrain[t - window, :] = data[t, 1:11]    
    
    ytrain = ytrain - intial
    xtrain[:,3:13] = xtrain[:,3:13] -  intial
    xtrain[:,14:24] = xtrain[:,14:24] -  intial    
    xtrain[:,25:] = xtrain[:,25:] -  intial   
    """
    return (xtrain, ytrain)  # Tuple


def post_merge_data(data_merge):
    xtrain_size_single = data_merge[0][0].shape
    ytrain_size_single = data_merge[0][1].shape
    case_size = len(data_merge)
    xtrain_merge = np.zeros(
        (
            xtrain_size_single[0] * case_size,
            xtrain_size_single[1],
        )
    )
    ytrain_merge = np.zeros(
        (
            ytrain_size_single[0] * case_size,
            ytrain_size_single[1],
        )
    )
    for i, data_tuple in enumerate(data_merge):
        v1 = i * xtrain_size_single[0]
        v2 = (i + 1) * xtrain_size_single[0]
        xtrain_merge[v1:v2, :] = data_tuple[0]
        ytrain_merge[v1:v2, :] = data_tuple[1]
    return xtrain_merge, ytrain_merge


# define lose function
def NSE(ytrain, xpred):
    return k.sum(k.square(xpred - ytrain)) / k.sum(
        k.square(
            ytrain - (k.mean(ytrain, axis=1, keepdims=True))
        )
    )


def MSE(ytrain, xpred):
    return k.mean(k.square(xpred - ytrain), axis=-1)


# design network
def netwrok(
    xtrain, ytrain, lose_func, model_weight
):  # , epochs):
    model = Sequential()
    model.add(
        Dense(
            units=64,
            input_dim=xtrain.shape[1],
            kernel_initializer=model_weight,
            activation="elu",
        )
    )  ## sigmoid'relu'linear'))
    model.add(
        Dense(
            units=32,
            input_dim=64,
            kernel_initializer=model_weight,
            activation="elu",
        )
    )
    #   model.add(Dropout(0.2))
    #   model.add(Dense(units=50, activation='tanh'))
    model.add(
        Dense(units=ytrain.shape[1], activation="linear")
    )  # , kernel_initializer= model_weight
    # model.add(Dropout(0.2))
    model.compile(
        loss=lose_func, optimizer="adam", metrics=["mae"]
    )  # ,'mape'])
    return model


def train(xtrain, ytrain, epochs):  # , xtest, ytest):
    train_history = model.fit(
        xtrain,
        ytrain,
        epochs=epochs,
        batch_size=64,
        validation_split=0.2,
        verbose=1,
        shuffle=True,
    )  # , callbacks =[earlystopping])
    scores = model.evaluate(xtrain, ytrain, batch_size=64)
    return train_history, scores


def split_list(init_list, list_len):
    list_of_groups = zip(*(iter(init_list),) * list_len)
    end_list = [list(i) for i in list_of_groups]
    count = len(init_list) % list_len
    (
        end_list.append(init_list[-count:])
        if count != 0
        else end_list
    )
    return end_list


# t-1
def pred(xtrain):
    model = load_model(
        export_model_fname, custom_objects={"NSE": NSE}
    )
    x = []
    for i in range(len(xtrain)):
        if i == 0:
            xpred = model.predict(xtrain[0:1], batch_size=64)
            y = np.row_stack((xpred))

        elif i == len(xtrain) - 1:
            xpred = model.predict(x[0:1], batch_size=64)
            y = np.row_stack((y, xpred))
            break
        else:
            xpred = model.predict(x[0:1], batch_size=64)
            y = np.row_stack((y, xpred))

        xpred = (xpred - a[1:11]) / (a1[1:11] - a[1:11])
        xpred = np.insert(xpred, 0, xtrain[i + 1, 0], axis=1)
        xpred = np.insert(xpred, 11, xtrain[i + 1, 11], axis=1)
        xpred = np.insert(xpred, 12, xtrain[i + 1, 12], axis=1)

        x = np.array(xpred)
    return y


# t-1 + t -2
def pred11(xtrain):
    model = load_model(
        export_model_fname, custom_objects={"NSE": NSE}
    )
    x = []

    for i in range(len(xtrain)):
        if i == 0:
            xpred = model.predict(xtrain[0:1], batch_size=64)
            y = np.row_stack((xpred))
            add = np.concatenate(
                [xtrain[1][11:13], xtrain[0][:11]]
            )

        elif i == len(xtrain) - 1:
            xpred = model.predict(x[0:1], batch_size=64)
            y = np.row_stack((y, xpred))
            break
        else:
            xpred = model.predict(x[0:1], batch_size=64)
            y = np.row_stack((y, xpred))
            add = np.concatenate(
                [xtrain[i + 1][11:13], x[0][:11]]
            )

        xpred = (xpred - a[1:11]) / (a1[1:11] - a[1:11])
        xpred = np.concatenate([xtrain[i + 1][0:1], xpred[0]])
        xpred = np.concatenate([xpred, add])
        x = np.array(xpred)

        x = x.reshape((1, 24))
    return y


# t-1 + t -2 + t -3
def pred2(xtrain):
    model = load_model(
        export_model_fname, custom_objects={"NSE": NSE}
    )
    x = []
    for i in range(len(xtrain)):
        if i == 0:
            xpred = model.predict(xtrain[0:1], batch_size=64)
            y = np.row_stack((xpred))
            add1 = xtrain[0][2:24]

        elif i == len(xtrain) - 1:
            xpred = model.predict(x[0:1], batch_size=64)
            y = np.row_stack((y, xpred))
            break
        else:
            xpred = model.predict(x[0:1], batch_size=64)
            y = np.row_stack((y, xpred))
            add1 = x[0][2:24]

        xpred = (xpred - a[3:13]) / (a1[3:13] - a[3:13])

        add = np.concatenate([xtrain[i + 1][0:3], xpred[0]])
        xpred = np.concatenate([add, add1])

        x = np.array(xpred)
        x = x.reshape((1, 35))
    return y


# 要儲存的檔案：誤差值的csv檔
def error(xpred, ytrain):
    mae = np.mean(np.abs((xpred - ytrain)))
    mse = mean_squared_error(ytrain, xpred)
    rmse = (mean_squared_error(ytrain, xpred)) ** 0.5
    mape = mean_absolute_percentage_error(
        ytrain + 25, xpred + 25
    )
    return mae, mse, rmse, mape  # , percentage, relative


def corr(xpred, ytrain):
    #    cc = []
    for i in range(10):
        data = pd.DataFrame(
            {"A": [xpred[:, i]], "B": [ytrain[:, i]]}
        )
        print(data.corr(method="pearson"))


# 預測結果：相關係數...圖和csv檔
def Correlation(xpred, ytrain):
    xpred = xpred.reshape((-1))
    ytrain = ytrain.reshape((-1))

    xpred_average = np.mean(xpred + 25, axis=0)
    ytrain_average = np.mean(ytrain + 25, axis=0)

    R = []
    r = np.sqrt(
        np.sum((xpred + 25 - xpred_average) ** 2)
        * (np.sum((ytrain + 25 - ytrain_average) ** 2))
    )
    rr = np.sum(
        (xpred + 25 - xpred_average)
        * (ytrain + 25 - ytrain_average)
    )
    cor = rr / r

    R.append(cor)
    """
      
    fig = plt.figure(figsize=(8, 8))
#設定axes
    xmin = ytrain.min()
    xmax = ytrain.max()+1
    ymin = xpred.min()
    ymax = xpred.max()+1    
    if xmin < ymin:
        a = xmin
    else:
        a = ymin
    if xmax > ymax:
        b = xmax
    else:
        b = ymax  
    plt.axis([a, b, a, b])
    plt.axis([-40, 40, -40, 40])
    ax = fig.add_subplot(1, 1, 1)
#設定散佈圖的顏色，大小等等
    for i in range(l):
        ax.scatter(ytrain[i,:,0], xpred[i,:,0], s=20,alpha=0.4, linewidths=2.5, c='#AAAFFF', edgecolors='black')
        ax.scatter(ytrain[i,:,1], xpred[i,:,1], s=20,alpha=0.4, linewidths=2.5, c='#AAAFFF', edgecolors='gray')
        ax.scatter(ytrain[i,:,2], xpred[i,:,2], s=20,alpha=0.4, linewidths=2.5, c='#AAAFFF', edgecolors='brown')
        ax.scatter(ytrain[i,:,3], xpred[i,:,3], s=20,alpha=0.4, linewidths=2.5, c='#AAAFFF', edgecolors='purple')
        ax.scatter(ytrain[i,:,4], xpred[i,:,4], s=20,alpha=0.4, linewidths=2.5, c='#AAAFFF', edgecolors='navy')
        ax.scatter(ytrain[i,:,5], xpred[i,:,5], s=20,alpha=0.4, linewidths=2.5, c='#AAAFFF', edgecolors='lightblue')
        ax.scatter(ytrain[i,:,6], xpred[i,:,6], s=20,alpha=0.4, linewidths=2.5, c='#AAAFFF', edgecolors='green')
        ax.scatter(ytrain[i,:,7], xpred[i,:,7], s=20,alpha=0.4, linewidths=2.5, c='#AAAFFF', edgecolors='yellow')
        ax.scatter(ytrain[i,:,8], xpred[i,:,8], s=20,alpha=0.4, linewidths=2.5, c='#AAAFFF', edgecolors='orange')
        ax.scatter(ytrain[i,:,9], xpred[i,:,9], s=20,alpha=0.4, linewidths=2.5, c='#AAAFFF', edgecolors='red')
    ax.plot((0, 1), (0, 1), transform=ax.transAxes, ls='--',c='k', label="1:1 line")
    plt.title('correlation coefficient')
    plt.xlabel('actual')
    plt.ylabel('prediction')
    """
    return R


# 預測結果：水位圖
def sim(xpred, ytrain):
    fig, axes = plt.subplots(nrows=10, ncols=1, figsize=(20, 24))
    for i in range(10):
        (line1,) = axes[i].plot(ytrain[:, i], "b", marker="o")
        (line2,) = axes[i].plot(
            xpred[:, i],
            "r",
            markeredgecolor="r",
            marker="*",
            markersize=3,
        )
        axes[i].legend(
            (line1, line2),
            ("actual", "prediction"),
            loc="upper left",
        )
        axes[i].set_title(
            "observation " + str(i),
            fontdict={"fontsize": 18, "color": "black"},
        )


# ________________________________________主程式____________________________________________________________
# 資料準備
data_merge = []
for fname in flist:
    data_merge.append(
        data_preparation(fname)
    )  # 逐一讀取, 並製作 xtrain & ytrain

xtrain, ytrain = post_merge_data(data_merge)  # 資料整合再一起

# Normalize
scaler = MinMaxScaler()
xtrain = scaler.fit_transform(xtrain)
a = scaler.data_min_
a1 = scaler.data_max_
pd.DataFrame(a).to_csv("unconfined_wmin.csv")
pd.DataFrame(a1).to_csv("unconfined_w1.csv")
xtrain, ytrain = shuffle(xtrain, ytrain)

# training
if weight_fname is not None:
    model = load_model(model_fname, custom_objects={"NSE": NSE})
else:
    model_weight = initializers.RandomNormal(
        mean=0.0, stddev=0.05, seed=None
    )
    model = netwrok(xtrain, ytrain, MSE, model_weight)

train_history, scores = train(xtrain, ytrain, epochs)

# 要儲存的網路、權重
model.save_weights(export_weight_fname)
model.save(export_model_fname)
with open(train_history_fname, "wb") as f:
    pickle.dump(train_history.history, f)

# model history
plt.plot(train_history.history["loss"])
plt.plot(train_history.history["val_loss"])
plt.title("Train History")
plt.ylabel("loss")
plt.xlabel("Epoch")
plt.yscale("log")
plt.legend(["loss", "val_loss"], loc="upper left")
plt.savefig(loss_history)
plt.show()

plt.plot(train_history.history["mae"])
plt.plot(train_history.history["val_mae"])
plt.title("Model mae")
plt.ylabel("MAE")
plt.xlabel("Epoch")
plt.yscale("log")
plt.legend(["mae", "val_mae"], loc="upper left")
plt.savefig(mae_history)
pd.DataFrame(scores).to_csv("4un2d_scores.csv")

endtime = datetime.datetime.now()
print(endtime - starttime)


# ___________________________預測____________________________
a = pd.read_csv("unconfined_wmin.csv")
a = a.values
for i in range(len(a)):
    a = np.delete(a, [i][0])

a1 = pd.read_csv("unconfined_w1.csv")
a1 = a1.values
for i in range(len(a1)):
    a1 = np.delete(a1, [i][0])


# 正規化成現在類神經要用的
xtrain = (xtrain - a) / (a1 - a)
xtrain_list = np.array(split_list(xtrain, 998))
ytrain_list = np.array(split_list(ytrain, 998))
model = load_model(
    export_model_fname, custom_objects={"NSE": NSE}
)

err = []
COR = []
# x = []
# y = []

for i in range(len(xtrain_list)):
    print("hi: ", i)
    xpred = pred11(xtrain_list[i])
    pd.DataFrame(xpred).to_csv("xpred.csv")
    ytrain = ytrain_list[i]
    sim(xpred, ytrain)
    plt.savefig("test" + str(i + 1) + ".png")

    err.append(list(error(xpred, ytrain)))
    plt.close()
    #    x.append(xpred)
    #    y.append(ytrain)
    pd.DataFrame(xpred).to_csv("xpred.csv")
    pd.DataFrame(ytrain).to_csv("ytrain.csv")
    COR.append(corr(xpred, ytrain))
    print(corr(xpred, ytrain))
pd.DataFrame(COR).to_csv("4un2dtest_Correlation.csv")
