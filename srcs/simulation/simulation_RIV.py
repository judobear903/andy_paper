# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# ## Getting Started
#
# If FloPy has been properly installed, then it can be imported as follows:
from pathlib import Path
import numpy as np
import flopy
import matplotlib.pyplot as plt
import flopy.utils.binaryfile as bf
import os
import sys

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jutility as jut


if __name__ == "__main__":
    workspace = "simulation_RIV"
    name = "tutorial02_mf"
    mf = flopy.modflow.Modflow(
        name, exe_name="mf_owhm_2_1", model_ws=workspace
    )

    Lx = 3000
    Ly = 300
    ztop = 0.0
    zbot = -50.0
    nlay = 1
    nrow = 3
    ncol = 10
    delr = Lx / ncol
    delc = Ly / nrow
    delv = (ztop - zbot) / nlay
    botm = np.linspace(ztop, zbot, nlay + 1)
    hk = 1.0
    vka = 1.0
    sy = 0.2
    ss = 1.0e-4
    laytyp = 1

    nper = 3
    perlen = [1, 100, 100]
    nstp = [1, 100, 100]
    steady = [True, False, False]

    dis = flopy.modflow.ModflowDis(
        mf,
        nlay,
        nrow,
        ncol,
        delr=delr,
        delc=delc,
        top=ztop,
        botm=botm[1:],
    )

    ibound = np.ones((nlay, nrow, ncol), dtype=np.int32)
    ibound[:, 0, 0:9] = 0
    ibound[:, 2, 0:9] = 0

    strt = np.ones((nlay, nrow, ncol), dtype=np.float32)
    strt[:, :, 0] = 0.0
    strt[:, :, -1] = 0.0
    bas = flopy.modflow.ModflowBas(mf, ibound=ibound, strt=strt)

    lrcd = {}
    lrcd[0] = [
        [0, 0, 9, 15.6, 1050.0, -4],
        [0, 1, 9, 15.6, 1050.0, -4],
        [0, 2, 9, 15.6, 1050.0, -4],
    ]
    riv = flopy.modflow.ModflowRiv(mf, stress_period_data=lrcd)

    lpf = flopy.modflow.ModflowLpf(
        mf, hk=10.0, vka=10.0, ipakcb=53
    )

    spd = {
        (0, 0): [
            "print head",
            "print budget",
            "save head",
            "save budget",
        ],
        (1, 0): [
            "print head",
            "print budget",
            "save head",
            "save budget",
        ],
        (2, 0): [
            "print head",
            "print budget",
            "save head",
            "save budget",
        ],
    }
    oc = flopy.modflow.ModflowOc(
        mf, stress_period_data=spd, compact=True
    )

    pcg = flopy.modflow.ModflowPcg(mf)

    mf.write_input()

    success, buff = mf.run_model()
    ##assert success, "MODFLOW did not terminate normally."

    hds = bf.HeadFile(Path(workspace) / f"{name}.hds")
    print(hds.get_kstpkper())
    mytimes = [1.0, 101.0, 201.0]
    for iplot, time in enumerate(mytimes):
        print("*****Processing time: ", time)
        head = hds.get_data(totim=time)
        print("*****Processing time: ", time)
        print(type(head), head.shape)
        print(head[0, :, :3])

    extent = (
        delr / 2.0,
        Lx - delr / 2.0,
        Ly - delc / 2.0,
        delc / 2.0,
    )

    sys.exit()

    fig = plt.figure(figsize=(6, 6))
    ax = fig.add_subplot(1, 1, 1, aspect="equal")
    ax.contour(
        head[0, :, :], levels=np.arange(1, 10, 1), extent=extent
    )

    hds = bf.HeadFile(Path(workspace) / f"{name}.hds")
    times = hds.get_times()
    head = hds.get_data(totim=times[-1])

    cbb = bf.CellBudgetFile(Path(workspace) / f"{name}.cbc")
    kstpkper_list = cbb.get_kstpkper()
    frf = cbb.get_data(text="FLOW RIGHT FACE", totim=times[-1])[
        0
    ]
    fff = cbb.get_data(text="FLOW FRONT FACE", totim=times[-1])[
        0
    ]
    (
        qx,
        qy,
        qz,
    ) = flopy.utils.postprocessing.get_specific_discharge(
        (frf, fff, None), mf, head
    )

    fig = plt.figure(figsize=(6, 6))
    ax = fig.add_subplot(1, 1, 1, aspect="equal")
    modelmap = flopy.plot.PlotMapView(model=mf, layer=0, ax=ax)
    qm = modelmap.plot_ibound()
    lc = modelmap.plot_grid()
    cs = modelmap.contour_array(
        head, levels=np.linspace(0, 10, 11)
    )
    quiver = modelmap.plot_vector(qx, qy)

    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(1, 1, 1, aspect="equal")
    mapview = flopy.plot.PlotMapView(model=mf)
    quadmesh = mapview.plot_ibound()
    quadmesh = mapview.plot_bc("RIV")
    linecollection = mapview.plot_grid()

    jut.save_fig(os.path.join("pics", "RIV1"))
    jut.save_fig(os.path.join("pics", "RIV2"))
