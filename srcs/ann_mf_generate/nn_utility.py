from typing import Dict, Tuple
import numpy as np
import sys
import torch


def determine_normalize_params(
    xvector: np.ndarray,
    yvector: np.ndarray,
    columns_in,
    columns_out,
) -> Dict:
    """
    依據變數名稱, 計算正規化數據
    平均值與標準差
    """

    assert xvector.shape[1] == len(
        columns_in
    ), f"{xvector.shape}/{len(columns_in)}"
    assert yvector.shape[1] == len(
        columns_out
    ), f"{yvector.shape}/{len(columns_out)}"

    normalize_params: Dict = {}
    for i, col in enumerate(columns_in):  # 篩選變數名稱
        sepline = col.split("_")
        if sepline[0] in ["drawdown", "h", "levelgap"]:
            # drawdown_E_20_0
            # 只保留前三碼
            # 只計算最後一碼為 0 者，只處理 t=0
            # drawdown_E_400_0
            assert len(sepline) == 4
            if int(sepline[-1]) == 0:
                normalize_params[col.replace("_0", "")] = (
                    np.nanmean(xvector[:, i]),
                    np.nanstd(xvector[:, i]),
                )
        else:
            # Bt, Sy, Kh, wel_qlist_t
            # 特別排除 B_t+1
            if col != "B_t+1":
                normalize_params[col] = (
                    np.nanmean(xvector[:, i]),
                    np.nanstd(xvector[:, i]),
                )
    normalize_params["columns"] = {
        "in": columns_in,
        "out": columns_out,
    }
    return normalize_params


def param_fetch(normalize_params: Dict, col: str):
    """
    依據 col 名稱, 取出對應正規化參數
    """
    sepline = col.split("_")
    params = (None, None)
    if sepline[0] in ["drawdown", "h", "levelgap"]:
        # drawdown_E_20_0
        # 只保留前三碼
        # 只計算最後一碼為 0 者
        assert len(sepline) == 4
        col2 = "{}_{}_{}".format(
            sepline[0], sepline[1], sepline[2]
        )  # 重新拆解組合字串成drawdown_E_20
        params = normalize_params[col2]
    elif col in [
        "B_t",
        "B_t+1",
    ]:  # 不管是"B_t", "B_t+1"，寫死 B_t
        params = normalize_params["B_t"]
    else:
        # Sy, Kh, wel_qlist_t
        params = normalize_params.get(col)

    assert (params[0] is not None) or (params[1] is not None)
    return params


'''
def param_fetch(normalize_params: Dict, col: str):
    """
    依據 col 名稱, 取出對應正規化參數
    """
    sepline = col.split("_")
    if sepline[0] in ["drawdown", "h", "levelgap"]:
        # drawdown_E_20_0 -> 只保留前三碼，產生 drawdown_E_20
        assert len(sepline) == 4, f"Unexpected format for column: {col}"
        col2 = "{}_{}_{}".format(sepline[0], sepline[1], sepline[2])
        if col2 not in normalize_params:
            print(f"Warning: normalization parameter for {col2} not found, using default (0,1)")
            return (0, 1)
        else:
            return normalize_params[col2]
    elif col in ["B_t", "B_t+1"]:
        if "B_t" not in normalize_params:
            print("Warning: normalization parameter for B_t not found, using default (0,1)")
            return (0, 1)
        else:
            return normalize_params["B_t"]
    else:
        params = normalize_params.get(col)
        if params is None:
            print(f"Warning: normalization parameter for {col} not found, using default (0,1)")
            return (0, 1)
        return params
'''


def normalize_process(
    xvector, yvector, normalize_params
) -> Tuple:
    """
    依據欄位逐欄進行正規化
    """

    # print("Keys in normalize_params:", normalize_params.keys())

    def normalize_core(data_vector, columns, normalize_params):
        # data_vector = np.array(data_vector)
        data_vector2 = np.zeros(data_vector.shape)
        for i, col in enumerate(columns):
            # 依據參數名稱, 取出對應的正規化參數
            try:
                params = param_fetch(normalize_params, col)
            except AssertionError:
                message = "Warning: 缺少正規化資料: {col}"
                print(message)
                # raise AssertionError(message)

            if params[1] == 0 or np.isnan(params[1]):
                divisor = 1e-6
                # print(f"Warning: Normalization parameter sigma is zero for column: {col}")
                # print(f"Warning: params[1]為 0 或 NaN  : {col}")
                # continue
            else:
                divisor = params[1]

            # Y = (X - mu) / sigma

            # data_vector2[:, i] = (data_vector[:, i] - params[0]) / params[1]
            data_vector2[:, i] = (
                data_vector[:, i] - params[0]
            ) / divisor
        return data_vector2

    xvector2 = normalize_core(
        xvector,
        normalize_params["columns"]["in"],
        normalize_params,
    )
    yvector2 = normalize_core(
        yvector,
        normalize_params["columns"]["out"],
        normalize_params,
    )
    return xvector2, yvector2


def normalize_process_inverse(
    xvector, yvector, normalize_params
) -> Tuple:
    """
    依據欄位逐欄進行 逆 正規化
    """

    def normalize_core(data_vector, columns, normalize_params):
        data_vector2 = np.zeros(data_vector.shape)
        for i, col in enumerate(columns):
            # 依據參數名稱, 取出對應的正規化參數
            try:
                params = param_fetch(normalize_params, col)
            except AssertionError:
                message = "Warning: 缺少正規化資料: {col}"
                print(message)
                # raise AssertionError(message)

            # Y * sigma + mu = X
            data_vector2[:, i] = (
                data_vector[:, i] * params[1] + params[0]
            )
        return data_vector2

    xvector2 = normalize_core(
        xvector,
        normalize_params["columns"]["in"],
        normalize_params,
    )
    yvector2 = normalize_core(
        yvector,
        normalize_params["columns"]["out"],
        normalize_params,
    )
    return xvector2, yvector2
