"""
nn model 後處理
1. load data (training, testing & validation)
2. plot training history
"""

import os
import sys
import pickle
from typing import Dict, List, Optional
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
import numpy as np
import torch
import psutil
from torch.utils.data import TensorDataset, DataLoader

# Argument Analysis
import argv_analysis
import nn_utility
import mf_ann_preprocess
import view_distance  # 觀測點距離

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import file_utility as fut
import plt_parameters
import jutility as jut
from nn_training import nn_Gwater


dtype = torch.float32  # 模型 dtype
device_name = "cuda" if torch.cuda.is_available() else "cpu"


# float64 --> float32
def data_transform(data):
    data = torch.from_numpy(data)
    data = data.to(dtype)
    data = data.to(device_name)
    return data


def random_pickup(
    data_vector: np.ndarray,
    rvalue: np.ndarray,
    crteria: float = 0.1,
) -> np.ndarray:
    """
    依據 rvalue 與門檻, 挑選符合條件的數據
    """
    return np.array(
        [
            data_vector[i]
            for i in range(data_vector.shape[0])
            if rvalue[i] <= crteria
        ]
    )


font_path = r"C:\Users\andy\AppData\Local\Microsoft\Windows\Fonts\SimHei.ttf"


def plot_45degree_comparison_post(ax, val_limit):
    prop = fm.FontProperties(fname=font_path)
    plt.rcParams["font.sans-serif"] = ["SimHei"]  # 指定預設字體
    plt.rcParams["axes.unicode_minus"] = (
        False  # 確保負號顯示正常
    )
    ax.plot(
        val_limit, val_limit, linestyle="--", color="k", lw=1.5
    )
    ax.set_title("45-degree line plot")
    ax.set_xlabel("真值", fontproperties=prop)
    ax.set_ylabel("推估值", fontproperties=prop)
    ax.legend(legend, shadow=True)
    ax.grid(True)
    return ax


def plot_45degree_comparison(
    ax,
    X,
    Y,
    # val_limit: Optional[List[None, None]],
    val_limit: Optional[List[float]],
    legend: List,
    model_name: str,
    random_pickup_criteria: Optional[float] = None,
    **plot_kwargs,
):
    """
    繪製 45 度線 比較圖
    """
    if random_pickup_criteria is not None:
        assert isinstance(random_pickup_criteria, float)
        # 隨機挑選數量
        rvalue = np.random.uniform(0, 1, X.shape[0])
        X = random_pickup(
            X, rvalue, random_pickup_criteria
        )  # true value
        Y = random_pickup(Y, rvalue, random_pickup_criteria)

    # 繪圖
    ax.plot(
        X,  # 真值
        Y,
        markerfacecolor="None",
        alpha=plot_kwargs.get("alpha", 0.5),
        marker=plot_kwargs.get("marker", "o"),
    )

    legend.append(model_name)
    if val_limit is None:
        val_limit = [
            min(
                np.min(y_training2),
                np.min(split_data["training"]["Y"].values),
            ),
            max(
                np.max(y_training2),
                np.max(split_data["training"]["Y"].values),
            ),
        ]
    else:
        val_limit = [
            min(
                np.min(y_training2),
                np.min(split_data["training"]["Y"].values),
                val_limit[0],
            ),
            max(
                np.max(y_training2),
                np.max(split_data["training"]["Y"].values),
                val_limit[1],
            ),
        ]
    return ax, legend, val_limit


if __name__ == "__main__":
    argv_params = argv_analysis.argument_analysis(sys.argv[1:])
    # 計算訓練變數名稱
    distance_list = np.array(view_distance.view_distance)
    columns_in, columns_out, var_size_in, var_size_out = (
        mf_ann_preprocess.determine_variable_names(
            argv_params, distance_list
        )
    )

    # 數據分析
    # load training, testing & validation data
    split_data_fname = os.path.join(
        argv_params["export_path"],
        argv_params["training_name"],
        "dataset.pickle",
    )
    with open(split_data_fname, "rb") as f:
        split_data = pickle.load(f)

    ##############################################################
    # 決定 normalize 參數
    normalize_params_fname = os.path.join(
        argv_params["export_path"], "normalize_params.pickle"
    )
    with open(normalize_params_fname, "rb") as f:
        normalize_params = pickle.load(f)

    ##############################################################
    # load NN models
    weight_flist = fut.search_files_in_dir(
        os.path.join(
            argv_params["export_path"],
            argv_params["dataset"],
            "weight",
        ),
        regular_flags="\.pt",
    )

    #########################################################################
    # results_dict: Dict = {}
    # for weight_fname in weight_flist:
    #     with open(weight_fname, "rb") as f:
    #         results_dict[os.path.basename(weight_fname)] = (
    #             pickle.load(f)
    #         )
    ##################################################### 2
    # 因應 results["model"] 是一個 PyTorch 模型，加載後轉移到 CPU 上
    nn_weight_dict: Dict = {}
    for weight_fname in weight_flist:
        print(f"Loading {weight_fname}")
        results = torch.load(
            weight_fname, map_location=torch.device("cpu")
        )
        print(type(results))
        nn_weight_dict[os.path.basename(weight_fname)] = results
    ############################################################################
    ############################################################################

    # 繪製 45 度線圖
    output_dir = os.path.join(
        argv_params["export_path"], "Draw_45_training"
    )
    image_name = f"45degree_fitting.png"
    plt.style.use("bmh")
    _fig, ax = plt.subplots(1, figsize=(15, 15))
    legend = []
    xlim = None

    for (
        key,
        results,
    ) in nn_weight_dict.items():  # 載入多個權重檔案為多種模型
        # model = results["model"]
        model = results
        model.to(device_name)
        print("Model", model)

        # 實質進行 normalize
        x_training_normalized, y_training_normalized = (
            nn_utility.normalize_process(
                split_data["training"]["X"].values,
                split_data["training"]["Y"].values,
                normalize_params,
            )
        )
        x = data_transform(x_training_normalized)  # 轉檔
        y = data_transform(y_training_normalized)

        ##############################################################
        # Model 計算
        y_predict = model(x)  # 預測
        # 實質進行 normalize_inverse
        x_training2, y_training2 = (
            nn_utility.normalize_process_inverse(
                # x_training_normalized,
                x.detach().cpu().numpy(),
                y_predict.detach().cpu().numpy(),
                normalize_params,
            )
        )

        ##############################################################
        ## 繪圖
        ax, legend, xlim = plot_45degree_comparison(
            ax,
            np.array(split_data["training"]["Y"].values),
            np.array(y_training2),
            xlim,
            legend,
            key,
            raindom_pickup_criteria=0.1,
        )

        # annotate
        # https://steam.oxxostudio.tw/category/python/example/matplotlib-annotate.html
        # https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.annotate.html
        """
        加入 RMSE or r2
        message = "test"
        loc = (0.05, 0.95)
        ax.annotate(
            message,
            xy=loc,
            xytext=loc,
            xycoords="axes fraction"
        )
        """

    plot_45degree_comparison_post(ax, xlim)
    font_path = r"C:\Users\andy\AppData\Local\Microsoft\Windows\Fonts\SimHei.ttf"
    jut.save_fig(
        os.path.join(
            output_dir,
            image_name,
        )
    )
