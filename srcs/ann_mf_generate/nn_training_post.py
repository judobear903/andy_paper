"""
nn training post analysis
"""

import os
import sys
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import numpy as np
from typing import Tuple, List
import torch.nn as nn
import torch

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import file_utility as fut

# import plt_parameters
import jutility as jut
import cmap_define

# Argument Analysis
import argv_analysis


class nn_Gwater(nn.Module):
    def __init__(self, **ann_setup):
        super(nn_Gwater, self).__init__()
        model_list = []
        self.layer_size = len(ann_setup["layer_node_size"])
        for i in range(self.layer_size - 1):
            lin = nn.Linear(
                ann_setup["layer_node_size"][i],
                ann_setup["layer_node_size"][i + 1],
            )

            model_list.append(lin)
            if (
                i < self.layer_size - 2
            ):  # 在最後一層之前添加ReLU激勵函數
                model_list.append(nn.ReLU())
        self.linears = nn.ModuleList(
            model_list
        )  # an iterable of modules to add

    def forward(self, x):
        for i, layer in enumerate(self.linears):
            x = layer(x)
        return x


def model_name_by_layer(layer_node_size) -> str:
    """
    因應層數建立模型名稱
        [A, B, C, D] 為模型層數與結點數
        --> A_B_C_D
    """
    # debug
    #   2025/3/6
    #   通用化權重檔名命名, 以利因應不同層數的檔名建立
    #   直接用 model_name_by_layer 函式計算
    #   並搬移到 layer_node_size 定義後
    model_name = ""
    for m, n in enumerate(layer_node_size):
        model_name += str(n)
        if m < len(layer_node_size) - 1:
            model_name += "_"
    return model_name


def model_name_by_layer2(layer_node_size) -> str:
    """
    因應層數建立模型名稱
        [A, B, C, D] 為模型層數與結點數
        --> (A, B, C, D)
    """
    # debug
    #   2025/3/6
    #   通用化權重檔名命名, 以利因應不同層數的檔名建立
    #   直接用 model_name_by_layer 函式計算
    #   並搬移到 layer_node_size 定義後
    model_name = "("
    for m, n in enumerate(layer_node_size):
        model_name += str(n)
        if m < len(layer_node_size) - 1:
            model_name += ", "
        else:
            model_name += ")"
    return model_name


def find_last_epoch_weight(pickle_dir, **kwargs) -> Tuple:
    """
    取得符合搜尋條件的權重文件
        並挑選最後一個 epoch
        回傳檔案路徑與epoch

    kwargs["filter_conditions"]
        形態為 List
        預設為 []
        內包含 str, 作為搜尋條件
    debug:
        2025/3/6
            1. 將 find_last_epoch_pickle 改為 find_last_epoch_weight
            2. 將 file_prefix 改為 filter_conditions
                原本規劃為單一字串，現改為 List of str
                原本搜尋直接在程式碼中限定 特定型態的副檔名, 則可在 List of str 呼叫設定
    """
    # 自動檢索所有符合前綴名稱的 .pickle 文件
    filter_conditions: List[str] = kwargs.get(
        "filter_conditions", []
    )
    assert isinstance(filter_conditions, list)
    weight_files = fut.search_files_in_dir(
        pickle_dir,
        log_recursive=True,
    )
    for fc in filter_conditions:
        # 以搜尋條件限制範圍
        assert isinstance(fc, str)
        weight_files = fut.filter_search_result(
            weight_files, regular_flags=fc
        )
    if len(weight_files) == 0:
        # 意味著完全無符合條件之檔案
        return None, None

    # 排序取出最後的 epoch 資訊
    df_weight = pd.DataFrame(weight_files, columns=["fname"])
    df_weight.loc[:, "epoch"] = [
        int(os.path.basename(fname).split(".")[0].split("_")[-1])
        for fname in df_weight.loc[:, "fname"].values
    ]
    df_weight = df_weight.sort_values(
        by=["epoch"], ascending=False
    )

    # 最後的 epoch 檔案
    weight_path = df_weight.loc[df_weight.index[0], "fname"]
    epoch = df_weight.loc[df_weight.index[0], "epoch"]
    return weight_path, epoch


def plot_individual_loss_curves_core(ax, pickle_dir, **kwargs):
    """
    1. 找出最後一個 epoch 的檔案名稱與 epoch
    2. load pickle
    3. plot loss & rmse curve
    """

    # 找出最後一個 epoch 的檔案名稱與 epoch
    pickle_path, _epoch = find_last_epoch_weight(
        pickle_dir, **kwargs
    )
    index = kwargs.get("index", 0)
    ax_secondary = kwargs.get("ax_secondary", None)

    if pickle_path is not None:
        # 讀取 .pickle 文件中的損失數據
        result = {}
        try:
            with open(pickle_path, "rb") as f:
                result = pickle.load(f)
        except Exception as e:
            print(
                f"讀取 .pickle 檔案失敗: {pickle_path}, 錯誤訊息: {e}"
            )
            return None, None, {}

        # 提取損失數據
        iterations = np.array(result.get("iterations", []))
        losses = np.array(result.get("losses", []))
        losses_val = np.array(result.get("losses_val", []))
        # 檢查是否有損失數據
        if losses.size == 0 or losses_val.size == 0:
            print(
                f"警告：{pickle_path} 的損失數據為空，跳過該檔案"
            )

        # 計算 RMSE 值 (由 MSE 開根號得到)
        # rmse_train = np.sqrt(losses)
        # rmse_val = np.sqrt(losses_val)

        df_loss = pd.DataFrame(
            losses,
            index=iterations,
            columns=["Training Loss"],
        )
        df_loss.loc[:, "Validation Loss"] = losses_val

        df_time_consumed = pd.DataFrame(
            [
                (
                    result["dt_history"][0] - result["dt_str"]
                ).total_seconds()
            ]
            + [
                (
                    result["dt_history"][m + 1]
                    - result["dt_history"][m]
                ).total_seconds()
                for m in range(len(result["dt_history"]) - 1)
            ],
            columns=["time_consumed"],
            index=result["save_iterations"],
        )
        df_time_consumed.loc[:, "time_consumed_each_epoch"] = (
            df_time_consumed.iloc[0, 0]
            / df_time_consumed.index[0]
        )
        df_time_consumed.loc[:, "time_consumed_accum"] = (
            df_time_consumed.iloc[0, 0] / 60
        )
        for m in range(df_time_consumed.shape[0] - 1):
            df_time_consumed.loc[
                df_time_consumed.index[m + 1],
                "time_consumed_each_epoch",
            ] = df_time_consumed.loc[
                df_time_consumed.index[m + 1], "time_consumed"
            ] / (
                df_time_consumed.index[m + 1]
                - df_time_consumed.index[m]
            )
            # df_time_consumed.loc[:, "time_consumed_accum"]
            df_time_consumed.loc[
                df_time_consumed.index[m + 1],
                "time_consumed_accum",
            ] = (
                df_time_consumed.loc[
                    df_time_consumed.index[m],
                    "time_consumed_accum",
                ]
                + df_time_consumed.iloc[m + 1, 0] / 60
            )
        # loss
        plot_params = {
            "Training Loss": [0, 0],
            "Validation Loss": [0, 1],
        }
        legends = {
            0: [],
            1: [],
        }
        for key, pp in plot_params.items():
            df_loss.loc[:, key].plot(
                ax=ax[pp[0]],
                grid=True,
                alpha=0.7,
                color=cmap_define.bmh_colors[
                    index % len(cmap_define.bmh_colors)
                ],
                linestyle=cmap_define.line_styles[
                    pp[1] % len(cmap_define.line_styles)
                ],
            )
            legends[pp[0]].append(key)
            # 取得最後一個點的座標並標記數值（保留4位小數）
            ax[pp[0]].annotate(
                f"{df_loss.loc[df_loss.index[-1], key]:.6f}",
                (
                    df_loss.index[-1],
                    df_loss.loc[df_loss.index[-1], key],
                ),
                textcoords="offset points",
                xytext=(0, 10),
                ha="center",
                color="blue",
            )
        ax[0].legend(legends[0], shadow=True)
        df_time_consumed.loc[:, "time_consumed_each_epoch"].plot(
            ax=ax[1],
            grid=True,
            color=cmap_define.bmh_colors[
                index % len(cmap_define.bmh_colors)
            ],
        )

        if index == 0:
            ax_secondary = ax[1].twinx()
        df_time_consumed.loc[:, "time_consumed_accum"].plot(
            ax=ax_secondary,
            grid=True,
            linestyle="-.",
            color=cmap_define.bmh_colors[
                index % len(cmap_define.bmh_colors)
            ],
        )

        ax[1].set_yscale("linear")
        ax[0].set_yscale("log")

        # plt.gca().xaxis.set_major_locator(MultipleLocator(100))
        for m in range(2):
            ax[m].set_xlabel("Epochs")
        ax[0].set_ylabel("Loss (MSE)")
        ax[0].set_title(f"Loss function")
        ax[1].set_ylabel(r"Time consumed ($s$)")
        ax[1].set_title(f"Time Comsumed for Each Epoch")
        if index == 0:
            ax_secondary.set_ylabel(r"Accumulated Time ($mins$)")

        if "loss_ylim" in kwargs:
            ax[0].set_ylim(kwargs["loss_ylim"])
        return pickle_path, ax_secondary, result
    return None, ax_secondary, {}


def plot_individual_loss_curves(
    pickle_dir, output_dir, **kwargs
):
    """
    自動檢索 pickle_dir 中符合 kwargs["filter_conditions"] 的文件，
    並在 output_dir 中針對每個檔案繪製損失曲線。

    如果 kwargs 設定 'image_name'
        則以此檔名輸出繪圖
        如果沒有, 則另行定義
            image_name = f"{file_prefix}_training_history.png"
    """
    os.makedirs(output_dir, exist_ok=True)
    # 繪製損失曲線
    plt.style.use("bmh")
    _fig, ax = plt.subplots(2, 1, figsize=(12, 12), sharex=True)
    pickle_fname, _ax_secondary, _result = (
        plot_individual_loss_curves_core(
            ax, pickle_dir, **kwargs
        )
    )

    if pickle_fname is not None:
        image_name = ""
        if "image_name" in kwargs:
            image_name = kwargs["image_name"]
        elif "file_prefix" in kwargs:
            image_name = "{}_training_history.png".format(
                kwargs["file_prefix"]
            )
        else:
            image_name = "training_history.png"

        plt.tight_layout()
        jut.save_fig(
            os.path.join(
                output_dir, image_name.replace(r"\.png", "")
            )
        )
    else:
        plt.close()


if __name__ == "__main__":
    argv_params = argv_analysis.argument_analysis(sys.argv[1:])

    #####################################################################
    # 找出所有模式結構
    flist = fut.search_files_in_dir(
        os.path.join(
            argv_params["export_path"],
            argv_params["dataset"],
            "weight",
        ),
        regular_flags=r"\.pickle$",
    )
    # 排除 final 結果
    flist = fut.filter_search_result(
        flist,
        regular_flags="final",
        log_reverse=True,  # 反向選取, 排除
    )
    mat = []
    for fname in flist:
        sepline = (
            os.path.basename(fname).split(".")[0].split("_")
        )
        layer_node_size = [
            int(sepline[l]) for l in range(len(sepline) - 2)
        ]
        mat.append(
            [
                layer_node_size,
                int(sepline[-2]),
                int(sepline[-1]),
            ]
        )
    df_models = pd.DataFrame(
        mat, columns=["model_nodes", "test_case", "epoch"]
    )
    model_available = []  # 找出可用的模型
    for model_node in df_models.loc[:, "model_nodes"].values:
        if not jut.check_list_in_list(
            model_available, model_node
        ):
            model_available.append(model_node)

    #####################################################################
    # 依據不同模式結構 & test case 進行比較
    # 1. 比較收斂過程
    # 2. 比較計算時間
    # 3. 比較最終收斂結果

    # 繪製不同 ts case, 相同結構下的
    results_list = []
    plt.style.use("bmh")
    _fig, ax = plt.subplots(2, 1, figsize=(12, 12), sharex=True)
    legend = []
    ax_secondary = None
    index = 0
    for model_nodes in model_available:
        for ts in range(10):
            pickle_fname, ax_secondary, result = (
                plot_individual_loss_curves_core(
                    ax,
                    os.path.join(
                        argv_params["export_path"],
                        argv_params["dataset"],
                        "weight",
                    ),  # pickle_dir
                    filter_conditions=[
                        "{}_{}".format(
                            model_name_by_layer(model_nodes), ts
                        ),
                        r"\.pickle$",
                    ],
                    index=index,
                    ax_secondary=ax_secondary,
                )
            )
            if pickle_fname is not None:
                index += 1
            if pickle_fname is not None:
                if "model_nodes" not in result:
                    result["model_nodes"] = tuple(model_nodes)
                results_list.append(result)

                message = r"${}$ / Case ${}$".format(
                    model_name_by_layer2(model_nodes),
                    ts,
                )
                if "batch_size" in result:
                    message += " / batch=${}$".format(
                        result["batch_size"]
                    )
                legend.append(message)

    ncol = [1, 1]
    if len(legend) > 3:
        ncol[0] = 2
    elif len(legend) > 6:
        ncol[0] = 3
        ncol[1] = 2
    ax[0].legend(
        [
            "{}, {}".format(lg, flag)
            for lg in legend
            for flag in ["Training", "Validation"]
        ],
        shadow=True,
        ncol=ncol[0],
    )
    ax[1].legend(
        legend, shadow=True, loc="upper left", ncol=ncol[1]
    )

    # ax[0].set_ylim([8e-4, 3e-3])
    plt.tight_layout()
    jut.save_fig(
        os.path.join(
            argv_params["export_path"],
            "pics",
            "current_check_result",
        )
    )

    plt.style.use("bmh")
    _fig, ax = plt.subplots(1, figsize=(12, 12), sharex=True)
    mat = []
    for result in results_list:
        mat.append(
            [
                result["model_nodes"],
                result["batch_size"],
                result["iterations"][-1],
                np.array(result.get("losses", []))[-1],
            ]
        )
    df_models = pd.DataFrame(
        mat,
        columns=[
            "model_nodes",
            "batch_size",
            "iteration",
            "loss",
        ],
    )
    for m, index in enumerate(df_models.index):
        loc = tuple(df_models.loc[index, ["iteration", "loss"]])
        color = cmap_define.bmh_colors[
            m % len(cmap_define.bmh_colors)
        ]
        ax.plot(
            df_models.loc[index, "iteration"],
            df_models.loc[index, "loss"],
            linestyle="none",
            marker="o",
            color=color,
        )
        ax.annotate(
            "{} / {}".format(
                df_models.loc[index, "model_nodes"],
                df_models.loc[index, "batch_size"],
            ),
            xy=loc,
            xytext=loc,
            xycoords="data",
            color=color,
        )

    ax.grid(color="k", linestyle="--")
    ax.set_title("Model Comparison")
    ax.set_ylabel("Loss Function")
    ax.set_xlabel("Epoch")
    ax.set_xlim([0, None])
    ax.set_yscale("log")
    plt.tight_layout()
    jut.save_fig(
        os.path.join(
            argv_params["export_path"],
            "pics",
            "model_loss_compare",
        )
    )
