"""
Artificial Neural Network
Training, testing and validation
1. training, testing and validation set seperation
2. NN Training + Performance demonstration 
3. NN Testing + Performance demonstration
4. NN Validation + Performance demonstration
"""

import os
import sys
import numpy as np
import pandas as pd
import torch
import pickle
from typing import Dict, Tuple, Optional
from torch.utils.data import TensorDataset, DataLoader
from tqdm import tqdm
from IOHandler import process_short_x, process_short_y
import shutil
import datetime
import math

# Argument Analysis
import argv_analysis
import view_distance
import mf_ann_preprocess
import nn_utility
import nn_training_post

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
# import plt_parameters
import jlib_logging

dtype = torch.float32


def save_model_weight(model, result, base_filename):
    """
    儲存模型權重與其他過程資訊
    """
    # 保存完整的模型檔案（包含模型架構和權重）
    torch.save(model, f"{base_filename}.pt")
    print(f"權重已保存至 {base_filename}.pt")

    with open(f"{base_filename}.pickle", "wb") as f:
        pickle.dump(result, f)
    print(f"訓練結果已保存至 {base_filename}.pickle")
    return result


def data_load_split(argv_params: Dict, **kwargs) -> Dict:
    """
    數據下載
    """
    # 預設分割參數
    split_criteria = kwargs.get(
        "split_criteria", np.array([0.8, 0.1, 0.1])
    )
    split_criteria /= np.sum(split_criteria)
    split_criteria_accum = np.array(
        [0] + list(np.cumsum(split_criteria))
    )
    split_criteria_params = {
        0: {"name": "training", "portion": split_criteria[0]},
        1: {"name": "validation", "portion": split_criteria[1]},
        2: {"name": "testing", "portion": split_criteria[2]},
    }

    # load raw data
    data_raw = {
        flag: pd.read_csv(
            "{}_{}.csv".format(
                os.path.join(
                    argv_params["export_path"],
                    argv_params["dataset"],
                ),
                flag,
            )
        ).set_index(
            ["case_index", "tlist"]
        )  # 把讀取的 DataFrame 的 case_index & tlist 列設置為索引
        for flag in ["X", "Y"]
    }
    # data split
    # 隨機挑選 case_index, 區分為 training, validation & testing
    # 獲選的 case, 完整歸屬同一資料集
    # 確認索引正確性
    case_index_list = np.unique(
        (data_raw["X"].index.get_level_values("case_index"))
    )

    case_index_list = np.sort(case_index_list)
    rvalues = np.random.uniform(0, 1, case_index_list.shape[0])
    case_belong = -np.ones(case_index_list.shape[0])

    for i in range(3):
        # 依據 case 案例分配到 training, validation & testing
        case_belong = np.where(
            np.logical_and(
                rvalues >= split_criteria_accum[i],
                rvalues < split_criteria_accum[i + 1],
            ),
            i,
            case_belong,
        )

    # 計算各資料集數量與比例
    training_count = np.sum(case_belong == 0)
    validation_count = np.sum(case_belong == 1)
    testing_count = np.sum(case_belong == 2)
    total_count = len(case_belong)

    counts = [training_count, validation_count, testing_count]
    ratios = [c / total_count for c in counts]
    split_criteria = np.array([0.8, 0.1, 0.1])  # 最初設定的比例

    # 簡潔的印出結果
    print("分配比例 (實際 vs. 設定):")
    for name, real, expected, count in zip(
        ["訓練集", "驗證集", "測試集"],
        ratios,
        split_criteria,
        counts,
    ):
        print(
            f"{name}: {real:.2%} vs. {expected:.2%}, 案例個數: {count}"
        )

    # i, split_criteria_params[i]["name"], flag
    # 0, training, X
    # 0, training, Y
    def data_reformulate(
        df_all: pd.DataFrame, case_belong_dataset: np.ndarray
    ) -> pd.DataFrame:
        """
        # df_all 為待分割的所有數據, 已在外部區分出 X & Y
        # case_belong_dataset 為所屬該資料集之 case_index
        """
        df_merge = pd.DataFrame([])
        for case_index in case_belong_dataset:
            df_selected = df_all[
                df_all.index.get_level_values("case_index")
                == case_index
            ]
            df_merge = pd.concat([df_merge, df_selected], axis=0)
        return df_merge

    split_data = {}
    for i in range(3):
        # 0, training
        # 1, validation
        # 2, testing
        dataset_name = split_criteria_params[i]["name"]
        if dataset_name not in split_data:
            split_data[dataset_name] = {}  # 初始化子字典

        for flag in ["X", "Y"]:
            split_data[split_criteria_params[i]["name"]][
                flag
            ] = data_reformulate(
                data_raw[flag],
                np.array(
                    [
                        case_index_list[j]
                        for j in range(case_index_list.shape[0])
                        if case_belong[j] == i
                    ]
                ),
            )
    # 建立字典以便查詢每個 case_index 的分配情況
    case_index_distribution = {i: None for i in case_index_list}
    for i, dataset_name in enumerate(
        ["training", "validation", "testing"]
    ):
        for flag in ["X", "Y"]:
            df_subset = split_data[dataset_name][flag]
            assigned_cases = df_subset.index.get_level_values(
                "case_index"
            ).unique()
            for case in assigned_cases:
                case_index_distribution[case] = dataset_name

    # 印出分配情況
    # print("每個 case_index 的分配情況：")
    # print(case_index_distribution)

    # # 下列版本, 採用隨機函數進行切割, 會橫跨不同 modflow 模擬案例
    # rvalues = np.random.uniform(0, 1, data_raw["X"].shape[0])
    # mask = -np.ones(data_raw["X"].shape[0])
    # split_data = {}
    # for i in range(3):
    #     print(
    #         f"{split_criteria_params[i]['name']} count:",
    #         np.sum(mask == i),
    #     )
    #     print(
    #         f"{split_criteria_params[i]['name']} expected proportion:",
    #         split_criteria_params[i]["portion"],
    #     )
    #     mask = np.where(
    #         np.logical_and(
    #             rvalues >= split_criteria_accum[i],
    #             rvalues < split_criteria_accum[i + 1],
    #         ),
    #         i,
    #         mask,
    #     )
    #     split_data[split_criteria_params[i]["name"]] = {
    #         flag: data_raw[flag][mask == i]
    #         for flag in ["X", "Y"]
    #     }

    return split_data


def compute_drawdown_average(split_data: Dict, **kwargs
                             ) -> Dict:
    """
    針對分類好的 dataset，計算各資料集 (training, validation, testing)
    中 Y (output) 數據裡所有 drawdown 欄位的平均值。
    參數:
        split_data: 包含 "training"、"validation"、"testing" 資料集的字典，
                    每個子項皆包含 "X" 與 "Y" 兩個 DataFrame。
    """
    avg_drawdown = {}
    root_logger = kwargs.get("root_logger", None)
    for subset in ["training", "validation", "testing"]:
        # 取得 output (Y) 的 DataFrame
        df_y = split_data[subset]["Y"]
        # 選取欄位名稱中包含 "drawdown"（不區分大小寫）的欄位
        drawdown_columns = [
            col
            for col in df_y.columns
            if "drawdown" in col.lower()
        ]
        if not drawdown_columns:
            if root_logger is not None:
                root_logger.debug(
                    f"{subset} set 中未找到與 drawdown 相關的欄位。"
                )
            avg_drawdown[subset] = None
        else:
            # 直接將這些欄位中的所有數值攤平後取平均
            overall_mean = (
                df_y[drawdown_columns].values.flatten().mean()
            )
            avg_drawdown[subset] = overall_mean
            if root_logger is not None:
                root_logger.debug(
                    f"{subset} set out 中 drawdown 欄位的平均值為: {overall_mean}"
                )
    return avg_drawdown


if __name__ == "__main__":
    argv_params = argv_analysis.argument_analysis(sys.argv[1:])
    program_name = "nn_training"
    root_logger = jlib_logging.logger_setup(
        program_name,
        os.path.join("logging", program_name),  # filename
        log_append=False,
    )
    root_logger.info("# Program: {}".format(program_name))
    root_logger.info("  --> {}".format(argv_params))
    num_epochs = argv_params[
        "training_iter"
    ]  # 由外部 makefile 或 batch file 傳入
    save_interval = argv_params[
        "save_iter"
    ]  # 設定保存權重的頻率
    # 計算訓練變數名稱
    distance_list = np.array(view_distance.view_distance)
    columns_in, columns_out, var_size_in, var_size_out = (
        mf_ann_preprocess.determine_variable_names(
            argv_params, distance_list
        )
    )
    # 啟動 GPU 加速
    device_name = "cpu"
    if torch.cuda.is_available():
        device_name = "cuda"
    elif torch.backends.mps.is_available():
        device_name = "mps"
    root_logger.info(
        "  --> Process device: {}".format(device_name)
    )

    ##############################################################
    # 資料集載入 & 細部切割
    root_logger.info("  --> Data load & split")
    split_data_fname = os.path.join(
        argv_params["export_path"],
        argv_params["training_name"],
        "dataset.pickle",
    )
    if not os.path.exists(split_data_fname):
        split_data = data_load_split(argv_params)
        os.makedirs(
            os.path.dirname(split_data_fname), exist_ok=True
        )
        with open(split_data_fname, "wb") as f:
            pickle.dump(split_data, f)
    else:
        # 直接讀取 pickle
        with open(split_data_fname, "rb") as f:
            split_data = pickle.load(f)

    # 呼叫函數計算平均值：
    drawdown_averages = compute_drawdown_average(split_data, root_logger=root_logger)

    """
    資料預備
    訓練集、驗證集中的特徵和標籤數據分別提取出來，轉換成 NumPy 數組格式
    """
    x_training = np.array(split_data["training"]["X"].values)
    y_training = np.array(split_data["training"]["Y"].values)
    x_validation = np.array(split_data["validation"]["X"].values)
    y_validation = np.array(split_data["validation"]["Y"].values)
    root_logger.debug(
        "  --> Training data size: {}".format(x_training.shape)
    )

    ##########################################################2/24
    x_training_short = process_short_x(x_training)
    x_validation_short = process_short_x(x_validation)
    y_training_short = process_short_y(y_training)
    y_validation_short = process_short_y(y_validation)

    root_logger.debug(
        "  --> 簡化數列:\n{}\nX training: {}\nX validation: {}\nY training: {}\nY validation: {}".format(
            x_training_short[0],
            x_training_short.shape,
            x_validation_short.shape,
            y_training_short.shape,
            y_validation_short.shape,
        )
    )
    # 假設 columns_in 與 columns_out 分別為原始的欄位名稱列表
    # 預期為 92 & 89 --> 簡化為 26 & 23
    root_logger.debug("  --> Length of Column: {} / {}".format(len(columns_in), len(columns_out)))

    # 根據 x_training 與 y_training 的實際欄位數動態產生索引
    # indices_x = list(range(0, 10)) + list(range(22, 32)) + list(range(44, 54)) + list(range(66, 76)) + list(range(x_training.shape[1] - 4, x_training.shape[1]))
    # indices_y = list(range(0, 10)) + list(range(22, 32)) + list(range(44, 54)) + list(range(66, 76)) + list(range(y_training.shape[1] - 1, y_training.shape[1]))

    indices_x = list(range(0, 10)) + list(
        range(x_training.shape[1] - 4, x_training.shape[1])
    )
    indices_y = list(range(0, 10)) + list(
        range(y_training.shape[1] - 1, y_training.shape[1])
    )
    root_logger.debug("  --> indeices: {} / {}".format(indices_x, indices_y))

    columns_in_short = columns_in[:10] + columns_in[-4:]
    columns_out_short = columns_out[:10] + columns_out[-1:]
    root_logger.debug("  --> 縮減後 columns: {} / {}".format(columns_in_short, columns_out_short))

    # 將 x_training_short 轉成 DataFrame
    df_x_training_short = pd.DataFrame(
        x_training_short, columns=columns_in_short
    )
    # 將 y_training_short 轉成 DataFrame
    df_y_training_short = pd.DataFrame(
        y_training_short, columns=columns_out_short
    )

    # 儲存成 Excel 文件
    output_excel_path = os.path.join(
        argv_params["export_path"], "training_data.xlsx"
    )
    root_logger.debug("  --> Excel export: {}".format(output_excel_path))
    with pd.ExcelWriter(output_excel_path) as writer:
        df_x_training_short.to_excel(
            writer, sheet_name="X_training_short", index=False
        )
        df_y_training_short.to_excel(
            writer, sheet_name="Y_training_short", index=False
        )

    #############################################################

    # 決定 normalize 參數 (只能有一組參數)
    # 不同方向不同區位水位的 normalize
    # X = np.concatenate((x_training, x_validation), axis=0)
    # Y = np.concatenate((y_training, y_validation), axis=0)

    X = np.concatenate(
        (x_training_short, x_validation_short), axis=0
    )
    Y = np.concatenate(
        (y_training_short, y_validation_short), axis=0
    )

    normalize_params = nn_utility.determine_normalize_params(
        X,
        Y,
        # columns_in,
        # columns_out,
        columns_in_short,
        columns_out_short,
    )

    if argv_params.get("log_unite_drawdown_normalize", False):
        # 統一水位洩降的 normalize
        data_array = []
        # for i, col in enumerate(columns_in):
        for i, col in enumerate(columns_in_short):
            sepline = col.split("_")
            if sepline[0] in ["drawdown", "h", "levelgap"]:
                data_array.append(X[:, i].reshape((-1, 1)))

        data_array2 = np.concatenate(
            tuple(data_array),
            axis=1,
        ).reshape((-1, 1))
        result = (
            np.nanmean(data_array2),
            np.nanstd(data_array2),
        )
        for key, _ in normalize_params.items():
            sepline = key.split("_")
            if sepline[0] in ["drawdown", "h", "levelgap"]:
                normalize_params[key] = result

    normalize_params_fname = os.path.join(
        argv_params["export_path"], "normalize_params.pickle"
    )
    with open(normalize_params_fname, "wb") as f:
        pickle.dump(normalize_params, f)

    # 實質進行 normalize
    x_training_normalized, y_training_normalized = (
        nn_utility.normalize_process(
            # x_training,
            # y_training,
            x_training_short,
            y_training_short,
            normalize_params,
        )
    )
    x_validation_normalized, y_validation_normalized = (
        nn_utility.normalize_process(
            # x_validation,
            # y_validation,
            x_validation_short,
            y_validation_short,
            normalize_params,
        )
    )
    # 檢查形狀
    for dd in [
        ["x_training", x_training_normalized],
        ["y_training", y_training_normalized],
        ["x_validation", x_validation_normalized],
        ["y_validation", y_validation_normalized],
    ]:
        root_logger.debug("  --> Normalied {}'s shape is {}".format(dd[0], dd[1].shape))
    # 組合輸出路徑
    export_path = argv_params["export_path"]
    x_train_csv = os.path.join(
        export_path, "x_training_normalized_all.csv"
    )
    y_train_csv = os.path.join(
        export_path, "y_training_normalized_all.csv"
    )
    x_valid_csv = os.path.join(
        export_path, "x_validation_normalized_all.csv"
    )
    y_valid_csv = os.path.join(
        export_path, "y_validation_normalized_all.csv"
    )

    # 將所有正規化數據存成 CSV
    pd.DataFrame(x_training_normalized).to_csv(
        x_train_csv, index=False
    )
    pd.DataFrame(y_training_normalized).to_csv(
        y_train_csv, index=False
    )
    pd.DataFrame(x_validation_normalized).to_csv(
        x_valid_csv, index=False
    )
    pd.DataFrame(y_validation_normalized).to_csv(
        y_valid_csv, index=False
    )

    # float64 --> float32
    def data_transform(data):
        data = torch.from_numpy(data)
        data = data.to(dtype)
        data = data.to(device_name)
        return data

    def model_create(layer_node_size, **kwargs):
        """
        建立 ANN model
        權重隨機初始化
        """
        device_name = kwargs["device"]
        # Construct our model by instantiating the class defined above
        model = nn_training_post.nn_Gwater(
            layer_node_size=layer_node_size
        )
        model = model.to(device=device_name)
        kwargs = {"input_size": (layer_node_size[0],)}
        return model

    test_size = 3  # 每次try 1遍, 只針對 final 那一組
    results = []
    all_train_losses = []
    all_val_losses = []
    weight_names = []
    # batch_size_pool = [2048]    # for Mac Studio M2 Max, 32GB
    batch_size_pool = [
        2048,
        4096,
        8192,
    ]  # for MacBook Pro M3, 36 GB
    weight_path = os.path.join(
        argv_params["export_path"],
        argv_params["dataset"],
        "weight",
    )
    os.makedirs(
        weight_path,
        exist_ok=True,
    )

    for layer_node_inner in argv_params["nn_structure"]:
        layer_node_size = (
            [x_training_short.shape[1]]
            + layer_node_inner
            + [y_training_short.shape[1]]
        )
        desired_weight_file = (
            nn_training_post.model_name_by_layer(layer_node_size)
        )
        root_logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        root_logger.debug("# Model structure: {}".format(nn_training_post.model_name_by_layer2(layer_node_size)))

        for ts in range(test_size):
            ##################################
            # 1.新增控制 epochs 次數
            # 2.新增控制 batch_size 次數
            # 3.新增放入結果之字典
            ##################################

            result = {
                "iterations": [],  # 就是epoch次數（和上面計算 iteration 次數不同）
                "losses": [],
                "losses_val": [],
                "save_iterations": [],
                "dt_history": [],
                "last_iter": 0,
                "model": None,
                "model_nodes": tuple(layer_node_size),
            }

            def check_pickle_exist(
                weight_fname: Optional[str],
            ) -> bool:
                """
                確認 pt & pickle 同時存在
                如果同時存在, return true
                """
                log_file_exists = weight_fname is not None
                if log_file_exists:
                    # .pt & .pickle 需同時存在
                    log_file_exists = log_file_exists and (
                        os.path.exists(
                            weight_fname.replace(
                                r"\.pt$", ".pickle"
                            )
                        )
                    )
                return log_file_exists

            def create_model_process(
                weight_fname: Optional[str],
                result: Dict,
            ) -> Tuple:
                log_exists = check_pickle_exist(weight_fname)
                log_new = True
                if log_exists:  # pt & pickle 同時存在
                    # find_last_epoch_weight 如果無符合條件的檔案, 則回傳 None
                    # 非 None, 則有前期權重
                    print(
                        f"找到權重文件 {weight_fname}，可以啟用重訓練。"
                    )

                    # 加載權重文件
                    model = torch.load(weight_fname)
                    print("模型權重已加載")

                    # 嘗試加載對應的 .pickle 文件
                    pickle_fname = weight_fname.replace(
                        ".pt", ".pickle"
                    )
                    print("加載訓練狀態...")
                    with open(pickle_fname, "rb") as f:
                        result = pickle.load(f)
                    print(
                        f"Loaded iterations: {result['iterations']}"
                    )
                    result["last_iter"] = max(
                        result["iterations"]
                    )  # 確保使用最新的迭代次數
                    log_new = False
                else:
                    print(
                        "沒有找到符合條件的權重文件，無法啟用重訓練。"
                    )
                    model = model_create(
                        layer_node_size, device=device_name
                    )  # 創建新模型
                    result["last_iter"] = 0
                return model, result, log_new

            log_new = True  # 是否為全新產生的權重模型
            if argv_params["training"]:
                # Construct our model by instantiating the class defined above
                # 以下定義兩種不同的狀態
                # 1. 全新的模型, 產生新的權重
                # 2. 之前中斷的計算, 依據過程結果
                weight_fname, epoch = (
                    nn_training_post.find_last_epoch_weight(
                        weight_path,  # pickle_dir
                        filter_conditions=[
                            "{}_{}".format(
                                desired_weight_file,  # layer 分層與結點數來建立模型名稱
                                ts,  # 第 ts 次的模擬結果
                            ),
                            r"\.pt$",
                        ],  # 搜尋條件
                    )
                )
                model, result, log_new = create_model_process(
                    weight_fname, result
                )
            elif argv_params["retraining"]:
                # 以 final 作為目標繼續訓練
                # debug
                #   2025/3/6
                #   以 find_last_epoch_weight 統一搜尋最新權重之功能
                weight_fname, epoch = (
                    nn_training_post.find_last_epoch_weight(
                        weight_path,  # pickle_dir
                        filter_conditions=[
                            "{}_{}".format(
                                desired_weight_file,  # layer 分層與結點數來建立模型名稱
                                "final",  # final, 前期比較多組初始猜值之結果
                            ),
                            r"\.pt$",
                        ],  # 搜尋條件
                    )
                )
                model, result, log_new = create_model_process(
                    weight_fname, result
                )

            batch_size = batch_size_pool[
                ts % len(batch_size_pool)
            ]
            if batch_size not in result:
                result["batch_size"] = batch_size
            else:
                # retraining 專用, 前期設定
                batch_size = result["batch_size"]

            # 計算 iteration 次數
            # debug, 2025, 3, 7
            #   1. 計算一次 epoch 中包含多少個 bastch
            #       變數從 num_iterations_per_epoch 改為 num_batch_per_epoch
            #   2. 改以無條件進位法來計算，簡化程式碼
            num_batch_per_epoch = int(
                math.ceil(len(x_training_short) / batch_size)
            )
            '''
            num_iterations_per_epoch = (
                len(x_training_short) // batch_size
            )
            if len(x_training_short) % batch_size != 0:
                num_iterations_per_epoch += 1
            '''
            root_logger.debug(f"  --> batch size: {batch_size} / 每個 epoch 包含 {num_batch_per_epoch} 次 batchs")

            # Construct our loss function and an Optimizer. The call to model.parameters()
            # in the Adam constructor will contain the learnable parameters (defined
            # with torch.nn.Parameter) which are members of the model.
            # define lose function
            criterion = torch.nn.MSELoss(
                reduction="mean"
            )  # MSE（Mean Squared Error）
            # optimizer = torch.optim.Adam(model.parameters(), lr=1e-4) # 搜尋演算法: 隨機梯度下降 (SGD)、Adam
            optimizer = torch.optim.Adam(
                model.parameters(),
                lr=1e-3,
                betas=(0.9, 0.999),
                eps=1e-8,
                weight_decay=1e-5,
            )
            """
            1.轉換數據為 PyTorch Tensor 使用
            2.to mps & torch.float32
            """
            x = data_transform(x_training_normalized)
            y = data_transform(y_training_normalized)
            x_val = data_transform(x_validation_normalized)
            y_val = data_transform(y_validation_normalized)

            """
            1.將已轉換好的數據打包成 PyTorch 的 TensorDataset, 並利用 DataLoader 來迭代數據集
            2.shuffle=True 打亂數據 ，訓練過程中可以幫助模型更好地泛化。
            """
            train_dataset = TensorDataset(x, y)
            train_loader = DataLoader(
                train_dataset,
                batch_size=batch_size,
                shuffle=True,
            )

            validation_dataset = TensorDataset(x_val, y_val)
            validation_loader = DataLoader(
                validation_dataset,
                batch_size=batch_size,
                shuffle=False,
            )

            print(f"Training dataset size: {len(train_dataset)}")
            print(
                f"Validation dataset size: {len(validation_dataset)}"
            )

            device = device_name
            convergence_time = None

            """
            使用 tqdm 庫顯示一個進度條，監控訓練過程。
            """
            # for t in range(result["last_iter"], result["last_iter"] + argv_params["training_iter"]):
            if "dt_str" not in result:
                # 起始計算時間
                result["dt_str"] = datetime.datetime.now()

            iter_str = 0  # 起始迭代
            iter_end = num_epochs  # 結束迭代
            if not log_new:
                # 非從頭開始者, 從之前記錄的迭代數持續進行
                iter_str = result["last_iter"]
            for epoch in tqdm(
                range(iter_str, iter_end),
                desc="Training Epochs",
            ):

                # Forward pass: Compute predicted y by passing x to the model
                train_loss = 0
                model.train(True)
                for x_batch, y_batch in train_loader:
                    x_batch = x_batch.to(device)
                    y_batch = y_batch.to(device)

                    y_pred = model(
                        x_batch
                    )  # 前向傳播：計算預測值

                    loss = criterion(
                        y_pred, y_batch
                    )  #  計算當前 mini-batch 的損失
                    optimizer.zero_grad()  # 清除前一次的梯度
                    loss.backward()  # 反向傳播：計算梯度
                    optimizer.step()  # 更新模型參數
                    train_loss += (
                        loss.item()
                    )  # 累加本 mini-batch 的損失

                valid_loss = 0
                model.eval()
                with torch.no_grad():
                    for (
                        x_batch,
                        y_batch,
                    ) in validation_loader:
                        x_batch = x_batch.to(device)
                        y_batch = y_batch.to(device)

                        y_pred_val = model(x_batch)

                        loss = criterion(
                            y_pred_val, y_batch
                        )  # 計算當前批次的損失
                        valid_loss += loss.item()

                # 記錄損失(記錄每個 epoch 的迭代次數與訓練損失&驗證損失(保證數據完整&畫圖))
                # 計算當前 epoch 平均損失，並記錄
                train_loss_avg = train_loss / len(train_loader)
                valid_loss_avg = valid_loss / len(
                    validation_loader
                )
                result["iterations"].append(epoch + 1)
                result["losses"].append(float(train_loss_avg))
                result["losses_val"].append(
                    float(valid_loss_avg)
                )

                # ======= 新增每＿＿次保存權重 =======
                if (epoch + 1) % save_interval == 0:
                    result["save_iterations"].append(epoch + 1)
                    result["dt_history"].append(
                        datetime.datetime.now()
                    )

                    # 定義保存的權重檔案名稱＆路徑
                    # debug
                    #   2025/3/6
                    #   額外增加 ts 測試次數
                    # 保存完整的模型檔案（包含模型架構和權重）
                    result = save_model_weight(
                        model,
                        result,
                        os.path.join(
                            weight_path,
                            f"{desired_weight_file}_{ts}_{epoch + 1}",
                        ),
                    )

                    # 即時繪製損失曲線圖
                    nn_training_post.plot_individual_loss_curves(
                        weight_path,  # pickle_dir
                        os.path.join(
                            argv_params["export_path"],
                            "pics",
                        ),  # output_dir
                        filter_conditions=[
                            "{}_{}".format(
                                desired_weight_file, ts
                            ),
                            r"\.pickle$",
                        ],
                        file_prefix="{}_{}".format(
                            desired_weight_file, ts
                        ),
                    )
                    result["last_iter"] = epoch + 1

            # 輸出 training result
            # .pickle 文件來保存訓練過程中的數據
            # .pt 文件保存模型的權重
            # debug
            #   統一用 save_model_weight 儲存模型結構
            result["save_iterations"].append(epoch + 1)
            result["dt_history"].append(datetime.datetime.now())
            result = save_model_weight(
                model,
                result,
                os.path.join(
                    weight_path,
                    f"{desired_weight_file}_{ts}_{epoch + 1}",
                ),
            )

        # ======== 在此處保存最終結果 ========
        # 比較不同的 ts 結果
        # 取出最佳版本的結果
        #   不同測試之最佳 loss
        mat = []
        for ts in range(test_size):
            # 找出該 ts 最後一個 epoch 的 pickle file
            pickle_fname, epoch = (
                nn_training_post.find_last_epoch_weight(
                    weight_path,  # pickle_dir
                    filter_conditions=[
                        "{}_{}".format(
                            desired_weight_file,  # layer 分層與結點數來建立模型名稱
                            str(ts),  # test size
                        ),
                        r"\.pickle$",
                    ],  # 搜尋條件
                )
            )
            with open(pickle_fname, "rb") as f:
                result = pickle.load(f)
            mat.append(
                [ts, np.array(result.get("losses", []))[-1]]
            )

        # 找出最佳方案
        df_loss = pd.DataFrame(
            mat, columns=["ts", "loss"]
        ).set_index("ts")
        df_loss = df_loss.sort_values(by=["ts"])  # 排序
        best_ts = df_loss.index[0]  # 最佳結果

        source_fname, epoch = (
            nn_training_post.find_last_epoch_weight(
                weight_path,  # pickle_dir
                filter_conditions=[
                    "{}_{}".format(
                        desired_weight_file,  # layer 分層與結點數來建立模型名稱
                        str(best_ts),  # test size
                    ),
                    r"\.pt$",
                ],  # 搜尋條件
            )
        )
        target_fname = os.path.join(
            weight_path,
            f"{desired_weight_file}_final.pt",
        )
        # 複製檔案
        shutil.copyfile(source_fname, target_fname)
        shutil.copyfile(
            source_fname.replace(r"\.pt$", ".pickle"),
            target_fname.replace(r"\.pt$", ".pickle"),
        )
        print(
            f"最終訓練結果已保存至 {target_fname.replace('.pt', '.pickle')}"
        )
