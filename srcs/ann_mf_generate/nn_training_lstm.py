"""
Artificial Neural Network
Training, testing and validation

1. training, testing and validation set seperation
2. NN Training + Performance demonstration 
3. NN Testing + Performance demonstration
4. NN Validation + Performance demonstration
"""

import os
import sys
from typing import Dict, Tuple
import numpy as np
import pandas as pd
import torch
import glob
import torch.nn as nn
import datetime
import pickle
import openpyxl
import matplotlib.ticker as ticker
import seaborn as sns
import torch.nn.functional as F
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error
from matplotlib import pyplot as plt
from torch.utils.data import TensorDataset, DataLoader
from matplotlib.ticker import MultipleLocator
from sklearn.metrics import r2_score
import gc
import psutil

from tqdm import tqdm
import matplotlib.ticker as ticker

# Argument Analysis
import argv_analysis


from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Dropout
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import LeakyReLU


from keijzer import *
import tensorflow as tf


if __name__ == "__main__":
    a = 0
    print(a)
    sys.exit()

    gpus = tf.config.experimental.list_physical_devices("GPU")
    if gpus:
        try:
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(
                    gpu, True
                )
            print(f"有 {len(gpus)} 個物理 GPU 可用")
        except RuntimeError as e:
            print(e)
    else:
        print(
            "沒有檢測到 GPU，請檢查 TensorFlow GPU 版本與 CUDA 安裝狀況"
        )

    # In[2]:

    """
    # load data
    df = pd.read_csv('house_data_processed.csv', delimiter='\t', parse_dates=['datetime'])
    df = df.set_index(['datetime'])
    magnitude = 1
    df.head()
    print(df.shape)
    """

    df_X = pd.read_csv("wel_data_1_X.csv")
    df_Y = pd.read_csv("wel_data_1_Y.csv")

    # 檢查兩個資料集的資料筆數是否一致
    if len(df_X) != len(df_Y):
        raise ValueError(
            "資料筆數不一致：df_X 有 {} 筆，但 df_Y 有 {} 筆".format(
                len(df_X), len(df_Y)
            )
        )
    else:
        print("兩個資料集筆數一致，共 {} 筆".format(len(df_X)))
    # 印出各自的形狀，預期 df_X 有 26 個欄位，df_Y 有 23 個欄位
    print("df_X 形狀:", df_X.shape)
    print("df_Y 形狀:", df_Y.shape)

    # ========== 2. 保留 case_index, tlist ==========
    # 假設 df_X 裡面包含 case_index, tlist 欄位
    case_index_series = df_X["case_index"]
    tlist_series = df_X["tlist"]

    print("\n範例顯示前 5 筆 case_index, tlist：")
    print(case_index_series.head())
    print(tlist_series.head())

    # ========== 3. 剃除不參與訓練的欄位 ==========
    # 對 X 資料：剃除 'case_index', 'tlist'
    df_X_features = df_X.drop(["case_index", "tlist"], axis=1)
    print("\n剃除後的 X 特徵欄位:")
    print(df_X_features.columns.tolist())

    # 對 Y 資料：如果 Y 中也有 'case_index', 'tlist'，則剃除
    if "case_index" in df_Y.columns or "tlist" in df_Y.columns:
        df_Y_target = df_Y.drop(
            ["case_index", "tlist"], axis=1, errors="ignore"
        )
    else:
        df_Y_target = df_Y.copy()

    print("\n剃除後的 Y 欄位:")
    print(df_Y_target.columns.tolist())

    # ========== 4. 合併特徵與目標資料 ==========
    # 假設 df_Y 仍有 23 個欄位；若最終只需要預測其中一個，後面會再挑選
    df_merged = pd.concat([df_X_features, df_Y_target], axis=1)

    print("\n合併後資料形狀:", df_merged.shape)

    # ========== 5. 檢查合併後欄位順序 ==========
    expected_columns = (
        df_X_features.columns.tolist()
        + df_Y_target.columns.tolist()
    )
    if list(df_merged.columns) != expected_columns:
        print("警告：合併後的欄位順序與預期不一致！")
    else:
        print("合併後的欄位順序正確。")

    # ========== 6. 選擇目標欄位 ==========
    # df_to_cnn_rnn_format 只支援單一目標欄位，
    # 假設這裡要用 df_Y 裡的第一個欄位作為目標
    target_col = df_Y.columns[3]
    print(f"選擇預測目標欄位: {target_col}")

    # In[3]:

    """
    # Convert datetime to categorical
    data = df.copy()
    columns_to_category = ['hour', 'dayofweek', 'season']
    data[columns_to_category] = data[columns_to_category].astype('category')
    # Do one-hot-encoding
    data = pd.get_dummies(data, columns=columns_to_category)
    data.head()
    print(data.shape)
    """

    # In[4]:

    print(f"選擇預測目標欄位: {target_col}")
    data = df_merged.copy()
    look_back = 5 * 24  # 如果每筆代表1小時，則5天為5*24
    train_size = 0.7
    num_features = df_merged.shape[1] - 1  # 49 - 1 = 48
    # 注意這裡 target_column 參數要用 target_col 而非 "gasPower"
    X_train, y_train, X_test, y_test = df_to_cnn_rnn_format(
        df=data,
        train_size=train_size,
        look_back=look_back,
        target_column=target_col,  # 這裡改為 target_col
        scale_X=True,
    )
    print(X_train.shape)
    print(y_train.shape)
    print(X_test.shape)
    print(y_test.shape)

    # In[5]:

    # Divide dataset into training data and testing data
    split_index = int(data.shape[0] * train_size)
    X_train_values = data[:split_index]
    X_test_values = data[split_index:]
    X_train_values.shape, y_train.shape
    X_test_values.shape, y_test.shape

    # Repair time difference
    datetime_difference = len(X_train_values) - len(y_train)
    X_train_values = X_train_values[datetime_difference:]
    X_train_values.shape, y_train.shape
    datetime_difference = len(X_test_values) - len(y_test)
    X_test_values = X_test_values[datetime_difference:]
    X_test_values.shape, y_test.shape

    # In[6]:
    magnitude = 1

    # Plot time series figure
    plt.figure(figsize=(20, 10))
    plt.plot(
        X_train_values.index,
        y_train,
        ".-",
        color="red",
        label="Train set",
        alpha=0.5,
    )
    plt.plot(
        X_test_values.index,
        y_test,
        ".-",
        color="blue",
        label="Test set",
        alpha=0.5,
    )
    plt.ylabel(
        r"gasPower $\cdot$ 10$^{-%s}$ [m$^3$/h]" % magnitude,
        fontsize=14,
    )
    plt.xlabel(
        "datetime [-]", fontsize=14
    )  # TODO: set x values as actual dates
    plt.xticks(fontsize=14, rotation=45)
    plt.yticks(fontsize=14)
    plt.legend(
        loc="upper left",
        borderaxespad=0,
        frameon=False,
        fontsize=14,
        markerscale=3,
    )

    # In[7]:

    def create_model(look_back, num_features):
        model = Sequential()
        model.add(
            LSTM(
                64,
                input_shape=(look_back, num_features),
                return_sequences=True,
                kernel_initializer="TruncatedNormal",
            )
        )
        model.add(BatchNormalization())
        model.add(LeakyReLU())
        model.add(Dropout(0.391))

        # 第一個 LSTM 層後續
        model.add(
            LSTM(
                128,
                kernel_initializer="TruncatedNormal",
                return_sequences=False,
            )
        )
        model.add(BatchNormalization())
        model.add(LeakyReLU())
        model.add(Dropout(0.749))

        # Dense 層
        for _ in range(5):
            model.add(
                Dense(256, kernel_initializer="TruncatedNormal")
            )
            model.add(BatchNormalization())
            model.add(LeakyReLU())
            model.add(Dropout(0.056))
        for _ in range(2):
            model.add(
                Dense(128, kernel_initializer="TruncatedNormal")
            )
            model.add(BatchNormalization())
            model.add(LeakyReLU())
            model.add(Dropout(0.727))

        model.add(
            Dense(64, kernel_initializer="TruncatedNormal")
        )
        model.add(BatchNormalization())
        model.add(LeakyReLU())
        model.add(Dropout(0.450))

        model.add(Dense(1))
        return model

    # In[8]:

    # Set hyper-parameters
    adam = Adam(learning_rate=1e-3)
    model = create_model(look_back, num_features)

    # compile(): loss is MSE, optimizer is Adam, and metric is MAPE
    model.compile(
        optimizer="adam",
        loss=["mean_squared_error"],
        metrics=["mape"],
    )

    # In[9]:

    history = model.fit(
        X_train,
        y_train,
        epochs=1000,
        batch_size=256,
        validation_split=0.2,
        verbose=1,
    )

    # In[10]:

    y_pred = model.predict(X_test)
    y_true = y_test.reshape(y_test.shape[0], 1)
    split_index = int(data.shape[0] * train_size)
    x = data[split_index:]
    datetime_difference = len(x) - len(y_true)
    x = x[datetime_difference:]
    plt.figure(figsize=(20, 10))
    plt.plot(
        x.index,
        y_true,
        ".-",
        color="red",
        label="Real values",
        alpha=0.5,
    )
    plt.plot(
        x.index,
        y_pred,
        ".-",
        color="blue",
        label="Predicted values",
        alpha=1,
    )
    plt.ylabel(
        r"gasPower $\cdot$ 10$^{-%s}$ [m$^3$/h]" % magnitude,
        fontsize=14,
    )
    plt.xlabel("datetime [-]", fontsize=14)
    plt.xticks(fontsize=14, rotation=45)
    plt.yticks(fontsize=14)
    plt.legend(
        loc="upper left",
        borderaxespad=0,
        frameon=False,
        fontsize=14,
        markerscale=3,
    )

    mse_result, mape_result = model.evaluate(X_test, y_test)
    plt.title(
        "LSTM result \n MSE = %.2f \n MAPE = %.1f [%%]"
        % (mse_result, mape_result),
        fontsize=14,
    )

    # In[ ]:
