import os
import sys
import pickle
import pandas as pd
from typing import Dict, List, Optional
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
import numpy as np
import torch
import psutil
from torch.utils.data import TensorDataset, DataLoader

# Argument Analysis
import argv_analysis
import nn_utility
import mf_ann_preprocess
import view_distance  # 觀測點距離

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import file_utility as fut
import plt_parameters
import jutility as jut
from nn_training import nn_Gwater


dtype = torch.float32  # 模型 dtype
device_name = "cuda" if torch.cuda.is_available() else "cpu"


# float64 --> float32
def data_transform(data):
    data = torch.from_numpy(data)
    data = data.to(dtype)
    data = data.to(device_name)
    return data


if __name__ == "__main__":
    argv_params = argv_analysis.argument_analysis(sys.argv[1:])

    # 計算訓練變數名稱
    distance_list = np.array(view_distance.view_distance)
    columns_in, columns_out, var_size_in, var_size_out = (
        mf_ann_preprocess.determine_variable_names(
            argv_params, distance_list
        )
    )

    # 數據分析
    # load training, testing & validation data
    split_data_fname = os.path.join(
        argv_params["export_path"],
        argv_params["training_name"],
        "dataset.pickle",
    )
    with open(split_data_fname, "rb") as f:
        split_data = pickle.load(f)
    # 決定 normalize 參數
    normalize_params = nn_utility.determine_normalize_params(
        split_data["training"]["X"].values,
        split_data["training"]["Y"].values,
        columns_in,
        columns_out,
    )

    # load NN models
    weight_flist = fut.search_files_in_dir(
        os.path.join(
            argv_params["export_path"],
            argv_params["dataset"],
            "weight",
        ),
        regular_flags="\.pickle",
    )
    results_dict: Dict = {}
    for weight_fname in weight_flist:
        with open(weight_fname, "rb") as f:
            results_dict[os.path.basename(weight_fname)] = (
                pickle.load(f)
            )
    for key, results in results_dict.items():
        print(f"Content of {key}:")
        for k, v in results.items():
            print(f"{k}: {v}\n")
    ############################################################################
    ############################################################################
    ############################################################################

    specific_key_name = (
        "92_280_280_89_0.pickle"  # 請替換為您想要的key名稱
    )

    # for (
    #     key,
    #     results,
    # ) in results_dict.items():  # 載入多個權重檔案為多種模型
    #     model = results["model"]
    for (
        key,
        results,
    ) in results_dict.items():  # 載入多個權重檔案為多種模型
        if key == specific_key_name:  # 只針對特定key名稱繪圖
            model = results["model"]

            # 實質進行 normalize
            x_training_normalized, y_training_normalized = (
                nn_utility.normalize_process(
                    split_data["training"]["X"].values,
                    split_data["training"]["Y"].values,
                    normalize_params,
                )
            )
            x = data_transform(x_training_normalized)  # 轉檔
            y = data_transform(y_training_normalized)

            y_predict = model(x)  # 預測

            # 實質進行 normalize_inverse
            x_training2, y_training2 = (
                nn_utility.normalize_process_inverse(
                    # x_training_normalized,
                    x.detach().cpu().numpy(),
                    y_predict.detach().cpu().numpy(),
                    normalize_params,
                )
            )

            print("Shape of y_training2:", y_training2.shape)
            print(
                "First few entries in y_training2:",
                y_training2[:1],
            )

            df = pd.DataFrame(y_training2)

            # 將 DataFrame 寫入 Excel 文件
            output_file = r"D:\code\andy_paper\workspace\mf_wel_unconfined\y_training2_output.xlsx"
            df.to_excel(output_file, index=False)

            print(
                f"y_training2 has been exported to {output_file}"
            )
