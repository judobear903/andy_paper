"""
1. load mf pickle file 所有的 mf 案例
2. load 對應的 ANN model weight
    給予初始條件 & mf 案例的抽水量
    進行 ANN 模擬
3. 比較兩者之差異
"""

import os
import sys
import pickle
import re
import random
import traceback
import glob
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
import numpy as np
import pandas as pd
import torch
import torch.nn.functional as F
import torch.nn as nn
import datetime
import gc
import psutil
import matplotlib.pyplot as plt
from typing import Dict, List, Optional
from scipy.interpolate import CubicSpline
from torch.utils.data import TensorDataset, DataLoader
from sklearn.metrics import mean_squared_error
from nn_training import nn_Gwater
from matplotlib.ticker import MultipleLocator
from PIL import Image
from IOHandler import process_short_x

# Argument Analysis
import argv_analysis
import nn_utility
import nn_utility2
import view_distance  # 觀測點距離
import mf_ann_preprocess

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import parallel_framework
import file_utility as fut
import plt_parameters
import jutility as jut

log_parallel = True
four_axes = ["E", "W", "N", "S"]
dtype = torch.float32  # 模型 dtype
device = torch.device(
    "cuda" if torch.cuda.is_available() else "cpu"
)
font_path = r"C:\Users\andy\AppData\Local\Microsoft\Windows\Fonts\SimHei.ttf"
dt3 = datetime.datetime.now()


# 使用 clean_memory 在預測或繪圖後釋放內存
def clean_memory():
    gc.collect()
    if torch.cuda.is_available():
        torch.cuda.empty_cache()


def print_memory_usage(step_name=""):
    process = psutil.Process(os.getpid())  # 獲取當前進程
    mem_info = process.memory_info()
    print(
        f"[{step_name}] 記憶體使用量: {mem_info.rss / 1024 / 1024:.2f} MB"
    )  # 以 MB 顯示


# float64 --> float32
def data_transform(data):
    data = torch.from_numpy(data)
    data = data.to(dtype)
    data = data.to(device)
    return data


def random_pickup(
    data_vector: np.ndarray,
    rvalue: np.ndarray,
    criteria: float = 0.1,
) -> np.ndarray:
    """
    依據 rvalue 與門檻, 挑選符合條件的數據
    """
    return np.array(
        [
            data_vector[i]
            for i in range(data_vector.shape[0])
            if rvalue[i] <= criteria
        ]
    )


def compute_rmse(y_true, y_pred):
    """計算 RMSE"""
    return np.sqrt(np.mean((y_true - y_pred) ** 2))


def plot_45degree_comparison_post(ax, val_limit, legend):
    prop = fm.FontProperties(fname=font_path)
    plt.rcParams["font.sans-serif"] = ["SimHei"]  # 指定預設字體
    plt.rcParams["axes.unicode_minus"] = (
        False  # 確保負號顯示正常
    )
    ax.plot(
        val_limit, val_limit, linestyle="--", color="k", lw=1.5
    )
    ax.set_title("45 Degree Line Comparison")
    ax.set_xlabel("True Value", fontproperties=prop)
    ax.set_ylabel("Estimated Value", fontproperties=prop)
    ax.legend(legend, shadow=True)
    ax.grid(True)

    # 自動調整軸的範圍
    x_min, x_max = ax.get_xlim()
    y_min, y_max = ax.get_ylim()
    ax.set_xlim([max(-20, x_min), min(50, x_max)])
    ax.set_ylim([max(-20, y_min), min(50, y_max)])
    # # 手動調整 X 和 Y 軸的範圍
    # ax.set_xlim([-20, 50])
    # ax.set_ylim([-20, 50])
    return ax


def plot_45degree_comparison(
    ax,
    X,
    Y,
    # val_limit: Optional[List[None, None]],
    val_limit: Optional[List[float]],
    legend: List,
    model_name: str,
    random_pickup_criteria: Optional[float] = None,
    case_index=None,  # 添加 case_index 參數
    argv_params=None,  # 添加 argv_params 參數
    **plot_kwargs,
):
    """
    繪製 45 度線 比較圖
    """
    # 打印 random_pickup_criteria 的值
    print(f"random_pickup_criteria: {random_pickup_criteria}")
    if argv_params is None:
        raise ValueError(
            "argv_params is None. Please ensure it is passed correctly."
        )
    # # 确保只处理指定的 case_index
    # if argv_params["case_index"] != case_index:
    #     return ax, legend, val_limit  # 跳过其他案例

    if random_pickup_criteria is not None:
        assert isinstance(random_pickup_criteria, float)
        # 隨機挑選數量
        rvalue = np.random.uniform(0, 1, X.shape[0])
        X = random_pickup(
            X, rvalue, random_pickup_criteria
        )  # true value
        Y = random_pickup(Y, rvalue, random_pickup_criteria)

    # 繪圖
    ax.plot(
        X,  # 真值
        Y,
        markerfacecolor="None",
        alpha=plot_kwargs.get("alpha", 0.5),
        marker=plot_kwargs.get("marker", "o"),
        color=plot_kwargs.get("color", "red"),
    )

    legend.append(model_name)
    if val_limit is None:
        val_limit = [
            min(np.min(X), np.min(Y)),
            max(np.max(X), np.max(Y)),
        ]
    else:
        val_limit = [
            min(np.min(X), np.min(Y), val_limit[0]),
            max(np.max(X), np.max(Y), val_limit[1]),
        ]
    return ax, legend, val_limit

    # # 45度線比較圖
    # fig, ax = plt.subplots(figsize=(10, 10))
    # # 設定 X 和 Y 軸的範圍
    # x_min, x_max = -10, 20  # 根據需求調整範圍
    # y_min, y_max = -10, 20  # 根據需求調整範圍
    # ax.set_xlim([x_min, x_max])
    # ax.set_ylim([y_min, y_max])

    # # 設定 X 和 Y 軸的刻度間距
    # x_major_locator = MultipleLocator(1)  # X 軸每 2 單位一個刻度
    # y_major_locator = MultipleLocator(1)  # Y 軸每 2 單位一個刻度
    # ax.xaxis.set_major_locator(x_major_locator)
    # ax.yaxis.set_major_locator(y_major_locator)

    # val_limit = [
    #     min(
    #         np.min(model_results["y"]),
    #         np.min(list(model_results.values())[0]),
    #     ),
    #     max(
    #         np.max(model_results["y"]),
    #         np.max(list(model_results.values())[0]),
    #     ),
    # ]
    # legend = []
    # for key, elem in model_results.items():
    #     # if key not in ["tlist", "y"] and key.endswith("_direct_Red"):
    #     if key not in ["tlist", "y"] and key.startswith(
    #         desired_weight_file
    #     ):  # 只選擇指定的權重檔
    #         weight_fname = desired_weight_file
    #         # _Direct結果進行處理
    #         if key.endswith("_direct"):
    #             weight_name_no_ext = os.path.splitext(
    #                 os.path.basename(weight_fname)
    #             )[0]
    #             model_name = f"Direct ({weight_name_no_ext})"
    #             ax, legend, val_limit = plot_45degree_comparison(
    #                 ax,
    #                 model_results["y"].flatten(),  # MF真實值
    #                 elem.flatten(),  # ANN_Direct預測值
    #                 val_limit,
    #                 legend,
    #                 alpha=1,
    #                 model_name=model_name,
    #                 color="red",
    #                 random_pickup_criteria=0.5,  # 關閉random_pickup
    #                 case_index=case_index,  # 確保 case_index 傳遞進來
    #                 argv_params=argv_params,  # 確保 argv_params 傳遞
    #             )
    #         # _Recursive 結果進行處理
    #         if key.endswith("_recursive"):
    #             weight_name_no_ext = os.path.splitext(
    #                 os.path.basename(weight_fname)
    #             )[0]
    #             model_name = f"Recursive({weight_name_no_ext})"
    #             ax, legend, val_limit = plot_45degree_comparison(
    #                 ax,
    #                 model_results["y"].flatten(),  # MF真實值
    #                 elem.flatten(),  # ANN_Recursive預測值
    #                 val_limit,
    #                 legend,
    #                 alpha=0.8,
    #                 model_name=model_name,
    #                 color="blue",
    #                 random_pickup_criteria=0.5,  # 關閉random_pickup
    #                 case_index=case_index,  # 傳入 case_index
    #                 argv_params=argv_params  # 傳入 argv_params
    #             )

    # ax = plot_45degree_comparison_post(ax, val_limit, legend)
    # plt.tight_layout()
    # jut.save_fig(
    #     os.path.join(
    #         argv_params["export_path"],
    #         argv_params["export_fname"],
    #         "compare",
    #         "compare_case_{}_45degree".format(case_index),
    #     )
    # )
    # 在繪圖之前檢查記憶體
    # print_memory_usage("繪圖前")
    # 檢查記憶體使用
    # print_memory_usage("繪圖後")
    # plt.close(_fig)  # 釋放 _fig 的記憶體
    # clean_memory()
    # 檢查記憶體釋放後的情況
    # print_memory_usage("釋放記憶體後")

    # for tindex, t in enumerate(model_results["tlist"]):
    #     plt.style.use("bmh")
    #     _fig, axs = plt.subplots(5, 5, figsize=(20, 20))
    #     for i, distance in enumerate(view_distance.view_distance):
    #         ax = plt_parameters.get_subax(axs, i)
    #         ax.set_title(r"Distance ${}$ m".format(distance))
    #         legend = []
    #         for key, j in {"E": 0, "W": 1, "N": 2, "S": 3}.items():
    #             col_index2 = (
    #                 j * len(view_distance.view_distance) + i
    #             )
    #             for model_key, model_kwargs in model_results.items():
    #                 log_choiced = False
    #                 if model_key != "y":
    #                     log_choiced = model_key.find(desired_weight_file) >= 0
    #                 if log_choiced:
    #                     ax.plot(
    #                         model_results["y"][tindex, col_index2],
    #                         model_kwargs[tindex, col_index2],
    #                         linestyle="none",
    #                         marker=markers[key],
    #                         label="{} / {}".format(model_key, key),  # 添加 label
    #                         **{
    #                             key00: elem00
    #                             for key00, elem00 in model_params[model_key].items()
    #                             if key00 in ["color", "alpha"]
    #                         }
    #                     )
    #         ax.legend(loc='upper right',  shadow=True, ncol=1, fontsize='xx-small')  # 調整 fontsize
    #         #ax.get_legend().remove()
    #     plt.tight_layout()
    #     jut.save_fig(
    #         os.path.join(
    #             argv_params["export_path"],
    #             argv_params["export_fname"],
    #             "compare",
    #             "compare_case_{}_step{}".format(case_index, tindex),
    #         )
    #     )


def model_compare_plot(
    model_results: Dict, argv_params, case_info: Dict
):
    """
    產生案例時序圖，顯示 Recursive、Direct 預測以及 MODFLOW 真值
    """
    chosen_direction = argv_params.get(
        "chosen_direction"
    )  # 確認是否選擇了方向
    directions = {
        "E": "East",
        "W": "West",
        "N": "North",
        "S": "South",
    }
    if chosen_direction:
        if chosen_direction not in directions:
            raise ValueError(
                f"無效的方向: {chosen_direction}，請使用 ['E', 'W', 'N', 'S'] 中的一個"
            )
        print(f"只繪製方向: {chosen_direction}")
    else:
        print("未選擇方向，繪製所有方向")

    plt.style.use("bmh")
    col_list = [
        0,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
    ]  # 11, 12, 13 ,14, 15#, 16, 17, 18, 19, 20, 21] #選擇觀測點
    # directions = {"E": "東", "W": "西", "N": "北", "S": "南"}
    directions = {
        "E": "East",
        "W": "West",
        "N": "North",
        "S": "South",
    }
    markers = {"E": "o", "W": "s", "N": "P", "S": "1"}

    # 設定權重檔名稱
    desired_weight_file = "14_100_100_11_100.pt"

    model_params = {
        "MODFLOW": {
            "color": "black",
            "linestyle": "solid",
            "alpha": 0.5,
        },
        "Direct": {
            "color": "green",
            "linestyle": "solid",
            "alpha": 0.2,
        },
        "Recursive": {
            "color": "blue",
            "linestyle": "solid",
            "alpha": 0.6,
        },
        "Window_Recursive": {
            "color": "orange",
            "linestyle": "dashed",
            "alpha": 0.3,
        },  # 新增的線條樣式1/15
    }

    # 合併所有案例
    all_cases = []
    for flag in ["training", "validation", "testing"]:
        # all_cases.extend(model_results[flag].items())
        all_cases.extend(
            [
                (flag, case_index, case_data)
                for case_index, case_data in model_results[
                    flag
                ].items()
            ]
        )

    # 開始處理所有案例
    # for case_index, case_data in all_cases:
    #     print(f"aProcessing Case Index: {case_index}")
    for flag, case_index, case_data in all_cases:
        print(f"aProcessing {flag} - Case Index: {case_index}")

        direct_key = f"{desired_weight_file}_direct"
        recursive_key = f"{desired_weight_file}_recursive"
        window_recursive_key = (
            f"{desired_weight_file}_window_recursive"  # 1/15
        )

        # 從 case_info 中獲取 bg_type
        case_details = case_info.get(case_index, {})
        bg_type = case_details.get(
            "bg_type", "Unknown"
        )  # 預設值為 "Unknown"

        # 創建圖表
        _fig, axs = plt.subplots(
            len(col_list), 1, figsize=(20, 5 * len(col_list))
        )  # 每個子圖代表一個觀測點距離

        # 逐距離繪圖
        for i, col_index in enumerate(col_list):
            ax = axs[i]

            # 繪製每種方式的數據   1/15
            for mode, mode_key in [
                ("MODFLOW", "y"),
                ("Direct", direct_key),
                ("Recursive", recursive_key),
                # ("Window_Recursive", window_recursive_key),
            ]:
                # for mode, mode_key in [("Direct", direct_key), ("Window_Recursive", window_recursive_key)]:
                if (
                    mode_key not in case_data
                    and mode != "MODFLOW"
                ):
                    continue  # 如果該預測方式沒有結果則跳過

                # 遍歷所有方向（E/W/N/S）
                for (
                    direction_key,
                    direction_name,
                ) in directions.items():
                    log_choiced = (
                        not chosen_direction
                        or direction_key == chosen_direction
                    )
                    if not log_choiced:
                        continue

                    # 原本計算
                    col_index2 = (
                        list(directions.keys()).index(
                            direction_key
                        )
                        * len(view_distance.view_distance)
                        + col_index
                    )

                    # 如果是 Direct 模式，使用滾動修正：將 col_index2 取餘數
                    if mode in ["Direct", "Recursive"]:
                        num_cols = case_data[mode_key].shape[1]
                        col_index2 = col_index2 % num_cols

                    # 確保 x 和 y 的長度一致
                    x_values = case_data["tlist"][
                        : min(
                            len(case_data["tlist"]),
                            case_data[mode_key].shape[0],
                        )
                    ]
                    y_values = case_data[mode_key][
                        :, col_index2
                    ][
                        : len(x_values)
                    ]  # 1/15修改

                    ax.plot(
                        case_data["tlist"],
                        case_data[mode_key][:, col_index2],
                        # x_values,
                        # y_values,
                        marker=markers[direction_key],
                        label=f"{mode} - {direction_name} (Dist: {view_distance.view_distance[col_index]} m({flag}))",
                        **model_params[mode],
                        # label=f"{mode} - {direction_name} (Dist: {view_distance.view_distance[col_index] if mode=='MODFLOW' else 'predicted'}, {flag})",
                        # **model_params[mode],
                    )

            # 設置子圖屬性
            ax.set_title(
                f"Case Index: {case_index} / Distance: {view_distance.view_distance[col_index]} m({flag})m (bg_type: {bg_type})"
            )
            ax.set_ylabel(r"Drawdown $m$")
            ax.legend(
                loc="upper right",
                shadow=True,
                ncol=2,
                fontsize="small",
            )  # 1/15
            x_major_locator = MultipleLocator(
                5
            )  # 每5個單位顯示一次刻度
            ax.xaxis.set_major_locator(x_major_locator)

        # 添加圖表標題與存檔
        _fig.text(
            0.01,
            0.99,
            f"Dataset: {flag}\nweight_name: {desired_weight_file}\nbg_type: {bg_type}",
            ha="left",
            va="top",
            fontsize=25,
        )
        plt.tight_layout()
        jut.save_fig(
            os.path.join(
                argv_params["export_path"],
                argv_params["export_fname"],
                "compare",
                f"compare_case_{case_index}_{flag}",
            )
        )
        plt.close(_fig)
        del _fig
        gc.collect()
        clean_memory()


def nn_prediction_recursive(model, X):
    """
    model: nn 模型參數
    X
    """
    Y_recursive = []
    x_current = torch.tensor(
        X[0, :].reshape((1, -1)), dtype=dtype
    )  # 初始時刻
    for t in range(X.shape[0]):
        y = model(x_current)

        Y_recursive.append(y.detach().numpy())  # 紀錄時間序列
        # 直接用 y 作為下一步的輸入
        x_current = torch.cat(
            [
                y.detach(),
                torch.from_numpy(X[t, -3:])
                .reshape((1, -1))
                .to(dtype),
            ],
            dim=1,
        )  # 後3個特徵拼接(Kh、Sy、Qt)
    return np.array(Y_recursive).squeeze()


# def nn_prediction_recursive(model, X, forcing_ratio=0.5):
#     """
#     使用教師強制 (Teacher Forcing)：
#     - `forcing_ratio`：以 `forcing_ratio` 的機率，使用真實值替代預測值來遞迴輸入。
#     """
#     Y_recursive = []
#     x_current = torch.tensor(X[0, :].reshape(1, -1), dtype=dtype)  # 確保是 (1, num_features)

#     for t in range(X.shape[0]):
#         y = model(x_current)  # 預測下一步

#         Y_recursive.append(y.detach().numpy())

#         if np.random.rand() < forcing_ratio and t + 1 < X.shape[0]:
#             x_next = X[t + 1, :-3]  # 使用真實值
#         else:
#             x_next = y.detach().numpy()  # 使用預測值

#         # 確保 `x_next` 是 (1, 89)
#         x_next = x_next.reshape(1, -1)

#         # 確保 `Kh, Sy, Qt` 是 (1, 3)
#         kh_sy_qt = X[t, -3:].reshape(1, -1)

#         # 拼接輸入
#         x_current = torch.cat([torch.tensor(x_next, dtype=dtype), torch.tensor(kh_sy_qt, dtype=dtype)], dim=1)

#     return np.array(Y_recursive).squeeze()


def nn_prediction_recursive_v2(model, x_normalized):
    """
    使用遞回方式進行預測，但每次的輸入取自 x_normalized 的對應時刻。
    - model: 神經網路模型
    - x_normalized: 正規化後的輸入矩陣 (形狀: [時間步數, 特徵數量])
    """
    Y_recursive_v2 = []  # 儲存所有時間步的預測結果

    # 遍歷每一個時間步
    for t in range(x_normalized.shape[0]):
        # 取出對應時刻的輸入特徵
        x_current = torch.tensor(
            x_normalized[t, :].reshape((1, -1)), dtype=dtype
        )  # 轉換為 PyTorch 張量並設置形狀
        # 使用模型進行預測
        y = model(x_current)
        # 紀錄當前時刻的預測結果
        Y_recursive_v2.append(y.detach().numpy())

    # 將結果轉換為 Numpy 陣列並壓縮
    Y_recursive_v2 = np.array(Y_recursive_v2).squeeze()

    # 將結果轉換為 Numpy 陣列並壓縮
    return np.array(Y_recursive_v2).squeeze()


def nn_prediction_recursive_window(model, X):
    """
    Window Recursive 預測方法：
    - 每次預測兩步：t+1 和 t+2。
    - 記錄 y_1 和 y_2 的預測結果，但下一輪仍然使用真實值作為輸入。
    """
    Y_recursive = []  # 儲存所有預測結果
    x_current = torch.tensor(
        X[0].reshape((1, -1)), dtype=dtype
    )  # 初始輸入（第 0 時刻的真實值）

    for t in range(X.shape[0] - 1):
        # 第一次預測：用 t 時刻的真實值 預測 t+1
        y_next_1 = model(x_current)
        Y_recursive.append(
            y_next_1.detach().numpy()
        )  # 紀錄 t+1 預測值

        # 第二次預測：用 y_next_1 拼接 t+1 的 Kh、Sy、Qt 預測 t+2
        kh_sy_qt = torch.tensor(
            X[t + 1, -3:], dtype=dtype
        ).reshape((1, -1))
        y_next_2_input = torch.cat(
            [y_next_1, kh_sy_qt], dim=1
        )  # 拼接預測值和特徵
        y_next_2 = model(y_next_2_input)
        Y_recursive.append(
            y_next_2.detach().numpy()
        )  # 紀錄 t+2 預測值
        # 更新 x_current，使用 t+1 時刻的真實值作為下一輪的輸入
        x_current = torch.tensor(
            X[t + 1].reshape((1, -1)), dtype=dtype
        )

    return np.array(Y_recursive).squeeze()


def nn_prediction_recursive_lstm(model, x_normalized):
    """
    使用遞迴方式進行 LSTM 預測，並在每次預測後拼接 Kh、Sy、Qt 三個特徵。
    - model: LSTM 模型
    - x_normalized: 正規化後的輸入矩陣 (形狀: [時間步數, 特徵數量])
    """
    # 確保模型在正確的設備上
    model.to(device)
    model.eval()  # 切換到評估模式

    Y_recursive = []

    # 初始輸入，轉換為 [1, 1, 92]
    x_current = torch.tensor(
        x_normalized[0].reshape(1, 1, -1), dtype=torch.float32
    ).to(device)

    for t in range(x_normalized.shape[0]):
        # 使用 LSTM 進行預測
        with torch.no_grad():  # 停用梯度計算
            y, _ = model.lstm(
                x_current
            )  # LSTM 的輸出 y 形狀是 [1, 1, hidden_size]
            y_pred = model.fc(
                y[:, -1, :]
            )  # 取最後一個時間步的輸出，形狀為 [1, 89]

        Y_recursive.append(y_pred.detach().cpu().numpy())

        # 如果還有下一個時間步，拼接 Kh、Sy、Qt 三個特徵，更新輸入
        if t + 1 < x_normalized.shape[0]:
            kh_sy_qt = (
                torch.tensor(
                    x_normalized[t + 1, -3:], dtype=torch.float32
                )
                .to(device)
                .reshape(1, 1, -1)
            )  # 調整為 [1, 1, 3]

            # 拼接後的形狀應為 [1, 1, 92]
            next_input = torch.cat(
                [y_pred.unsqueeze(1), kh_sy_qt], dim=2
            )

            # 更新 x_current
            x_current = next_input

    return np.array(Y_recursive).squeeze()


def compare_first_and_last_five_inputs(x_normalized, model):
    """
    比較 nn_prediction_recursive_v2 和批量處理的第一個輸入與後五個輸入是否一致。
    """
    dtype = torch.float32  # 假設資料類型

    # **1. 逐步遞回的方式獲取輸入數據**
    recursive_inputs = []
    for t in range(x_normalized.shape[0]):
        x_current = torch.tensor(
            x_normalized[t, :].reshape((1, -1)),
            dtype=torch.float32,
        )
        recursive_inputs.append(x_current.detach().cpu().numpy())

    # 提取逐步處理的第一個輸入與後五個輸入
    recursive_first_input = recursive_inputs[0]
    recursive_last_five_inputs = recursive_inputs[-5:]

    # **2. 批量處理的方式獲取輸入數據**
    batch_first_input = x_normalized[0, :]  # 第一個輸入
    batch_last_five_inputs = x_normalized[-5:, :]  # 最後五個輸入

    # **3. 進行比對**
    print("第一個輸入是否相同:")
    print(
        np.array_equal(recursive_first_input, batch_first_input)
    )
    print("後五個輸入是否相同:")
    print(
        all(
            np.array_equal(
                recursive_last_five_inputs[i],
                batch_last_five_inputs[i],
            )
            for i in range(5)
        )
    )

    # **4. 如果想查看差異，可以打印具體內容**
    if not np.array_equal(
        recursive_first_input, batch_first_input
    ):
        print("第一個輸入的差異:")
        print("逐步處理的第一個輸入:", recursive_first_input)
        print("批量處理的第一個輸入:", batch_first_input)

    for i in range(5):
        if not np.array_equal(
            recursive_last_five_inputs[i],
            batch_last_five_inputs[i],
        ):
            print(f"第 {-5+i+1} 個輸入的差異:")
            print(
                "逐步處理的輸入:", recursive_last_five_inputs[i]
            )
            print("批量處理的輸入:", batch_last_five_inputs[i])


def compute_result_rmse(argv_params, model_results):

    # 設定結果保存路徑
    base_path = argv_params["export_path"]
    top_cases_path = os.path.join(
        base_path, "Top3_Cases_By_RMSE.xlsx"
    )  # 新增的保存路徑

    # 保存整體 RMSE、獲取距離列表
    results = {"training": [], "validation": [], "testing": []}
    distances = np.array(view_distance.view_distance)

    # 所有模型名稱
    model_list = []
    # 遍歷 training 和 validation
    for flag in ["training", "validation", "testing"]:
        for case_index, case_data in model_results[flag].items():
            for key in case_data.keys():
                if key not in [
                    "tlist",
                    "y",
                ]:  # 過濾非模型名稱的鍵
                    model_list.append(
                        key.replace("_recursive", "").replace(
                            "_direct", ""
                        )
                    )
        model_list = list(set(model_list))
    print(f"去重後的模型名稱：{model_list}")

    results = {}
    for model_name in model_list:
        # 初始化模型結構
        results[model_name] = {}
        for flag in ["training", "validation", "testing"]:
            first_key = list(model_results[flag].keys())[0]
            # y_all1 = np.empty((0, model_results[flag][first_key]["y"].shape[1]))  # 真實值
            # y_all2 = {model_type: np.empty((0,model_results[flag][first_key]["y"].shape[1]))
            #             for model_type in ["recursive", "direct"] }  # 預測值

            y_all1 = np.empty((0, 11))
            y_all2 = {
                model_type: np.empty((0, 11))
                for model_type in ["recursive", "direct"]
            }

            for case_index, case_data in model_results[
                flag
            ].items():
                print(
                    case_index,
                    case_data["y"].shape,
                    case_data[key].shape,
                )

                # y_all1 = np.concatenate((y_all1, case_data["y"]))
                y_all1 = np.concatenate(
                    (
                        y_all1,
                        np.concatenate(
                            (
                                case_data["y"][:, :10],
                                case_data["y"][:, -1:],
                            ),
                            axis=1,
                        ),
                    )
                )

                for model_type in ["recursive", "direct"]:
                    key = "{}_{}".format(model_name, model_type)
                    # if key not in case_data:
                    #     print(f"警告：案例 {case_index} 中缺少 {key} 的預測結果，跳過此項計算")

                    # if key in case_data:
                    #     y_all2[model_type] = np.concatenate(
                    #         (y_all2[model_type], case_data[key])
                    #     )
                    if key not in case_data:
                        print(
                            f"警告：案例 {case_index} 中缺少 {key} 的預測結果，跳過此項計算"
                        )
                        continue
                    # 取出預測結果
                    pred = case_data[key]
                    # 如果預測結果的列數是 23，則切片成 (n,11)；否則假設已經是 11 了
                    if pred.shape[1] == 23:
                        pred = np.concatenate(
                            (pred[:, :10], pred[:, -1:]), axis=1
                        )
                    y_all2[model_type] = np.concatenate(
                        (y_all2[model_type], pred)
                    )

                    # 將真實值轉換為 (n,11)（前 10 個欄位和最後一個欄位）
                    y_true_conv = np.concatenate(
                        (
                            case_data["y"][:, :10],
                            case_data["y"][:, -1:],
                        ),
                        axis=1,
                    )
                    # 將預測值如果還是 23 個特徵也進行同樣的轉換
                    if case_data[key].shape[1] == 23:
                        y_pred_conv = np.concatenate(
                            (
                                case_data[key][:, :10],
                                case_data[key][:, -1:],
                            ),
                            axis=1,
                        )
                    else:
                        y_pred_conv = case_data[key]

                    results[model_name][
                        (flag, model_type, case_index)
                    ] = {
                        "case_rmse": compute_rmse(
                            y_true_conv, y_pred_conv
                        ),
                    }

                    # results[model_name][(flag, model_type, case_index)] = {
                    #     "case_rmse": compute_rmse(
                    #         case_data["y"], case_data[key]),
                    # }

                results[model_name][flag] = {}
                # 在所有案例數據拼接完成後，計算整體 RMSE
                for model_type in ["recursive", "direct"]:
                    if (
                        y_all2[model_type].shape[0] > 0
                    ):  # 確保有拼接數據
                        overall_rmse = compute_rmse(
                            y_all1, y_all2[model_type]
                        )

                        if model_name not in results:
                            results[model_name] = {}

                        if (flag, model_type) not in results[
                            model_name
                        ]:
                            results[model_name][
                                (flag, model_type)
                            ] = {}

                        results[model_name][
                            (flag, model_type, "overall_rmse")
                        ] = overall_rmse

                        # 新增：計算全案例的 MAPE
                        epsilon = 1e-100  # 或其他合適的極小值
                        overall_mape = np.mean(
                            np.abs(
                                (y_all1 - y_all2[model_type])
                                / (y_all1 + epsilon)
                            )
                        )
                        results[model_name][
                            (flag, model_type, "overall_mape")
                        ] = overall_mape

                    else:
                        print(
                            f"No data for {model_name} {flag} {model_type}"
                        )

                    ######################### 計算距離層次 RMSE
                    # mat = []
                    # for i, distance in enumerate(distances):
                    #     y1 = np.array([])
                    #     y2 = np.array([])
                    #     for j in range(4):  # 拼接四個方向
                    #         y1 = np.concatenate((y1,case_data["y"][:,i+ j * distances.shape[0]]),axis=0)
                    #         y2 = np.concatenate((y2,case_data[key][:,i+ j * distances.shape[0]]),axis=0)

                    # 將真實值轉換成 reduced 版本
                    y_true_reduced = np.concatenate(
                        (
                            case_data["y"][:, :10],
                            case_data["y"][:, -1:],
                        ),
                        axis=1,
                    )
                    # 將預測值也做相同處理（如果需要）
                    if case_data[key].shape[1] == 23:
                        y_pred_reduced = np.concatenate(
                            (
                                case_data[key][:, :10],
                                case_data[key][:, -1:],
                            ),
                            axis=1,
                        )
                    else:
                        y_pred_reduced = case_data[key]

                    # 計算每一個特徵（共 11 個）的 RMSE
                    mat = []
                    for i in range(11):
                        rmse_col = compute_rmse(
                            y_true_reduced[:, i],
                            y_pred_reduced[:, i],
                        )
                        mat.append([i, rmse_col])

                        # mat.append([distance, compute_rmse(y1, y2)])
                    results[model_name][
                        (flag, model_type, case_index)
                    ]["rmse_profile"] = pd.DataFrame(
                        mat, columns=["distance", "rmse"]
                    )
                results[model_name][(flag, model_type)][
                    "rmse_all_case"
                ] = compute_rmse(y_all1, y_all2[model_type])

    # 設定輸出路徑
    output_excel_path_case_rmse = os.path.join(
        argv_params["export_path"],
        "case_rmse_results_各案例.xlsx",
    )
    output_excel_path_all_case_rmse = os.path.join(
        argv_params["export_path"],
        "all_case_rmse_results_全案例.xlsx",
    )
    output_excel_path_rmse_profile = os.path.join(
        argv_params["export_path"],
        "rmse_profile_results_距離.xlsx",
    )
    """
    # 提取 case_rmse
    case_rmse_data = []

    for model_name, model_data in results.items():
        print(f"Model Name: {model_name}")
        for key, metrics in model_data.items():
            if isinstance(key, tuple) and len(key) == 3:
                flag, model_type, case_index = key
                if (isinstance(metrics, dict) and "case_rmse" in metrics):
                    case_rmse_data.append(
                        {
                            "Model Name": model_name,
                            "Data Split": flag,
                            "Model Type": model_type,
                            "Case Index": case_index,
                            "Case RMSE": metrics["case_rmse"],
                        }
                    )
    df_case_rmse = pd.DataFrame(case_rmse_data)
    df_case_rmse.to_excel(output_excel_path_case_rmse, index=False)
    print("\nCase RMSE DataFrame:")
    print(df_case_rmse)

    # 提取全案例 MAPE 結果
    all_case_mape_data = []
    added_keys = set()  # 用來跟蹤已處理的鍵 (model_name, key)
    for model_name, model_data in results.items():
        for key, metrics in model_data.items():
            if (isinstance(key, tuple) and len(key) == 3 and 
                key[2] == "overall_mape" and (model_name, key[:2]) not in added_keys):
                flag, model_type, _ = key
                if isinstance(metrics, (float, int, np.float64)):
                    all_case_mape_data.append({
                        "Model Name": model_name,
                        "Data Split": flag,
                        "Model Type": model_type,
                        "MAPE All Case": metrics
                    })
                    added_keys.add((model_name, key[:2]))
    df_all_case_mape = pd.DataFrame(all_case_mape_data)
    output_excel_path_all_case_mape = os.path.join(argv_params["export_path"], "all_case_mape_results.xlsx")
    df_all_case_mape.to_excel(output_excel_path_all_case_mape, index=False)
    print("\nAll Case MAPE DataFrame:")
    print(df_all_case_mape)

    # **新功能：挑選 RMSE 最大的案例**
    top_cases_data = []
    for model_name, model_data in results.items():
        for key, metrics in model_data.items():
            if isinstance(key, tuple) and len(key) == 3:
                flag, model_type, case_index = key
                if (
                    isinstance(metrics, dict)
                    and "case_rmse" in metrics
                ):
                    top_cases_data.append(
                        {
                            "Model Name": model_name,
                            "Flag": flag,
                            "Model Type": model_type,
                            "Case Index": case_index,
                            "BG Type": case_info[case_index][
                                "bg_type"
                            ],
                            "Case RMSE": metrics["case_rmse"],
                        }
                    )

    # 挑選每組最大的 3 個案例
    df_top_cases = pd.DataFrame(top_cases_data)

    # 提取數字進行排序
    df_top_cases["Model Number"] = (
        df_top_cases["Model Name"]
        .str.extract(r"(\d+)\.pt$")
        .astype(int)
    )

    # 排序 RMSE 並挑選 Top 3
    top3_cases = (
        df_top_cases.sort_values(
            ["Model Number", "Case RMSE"],
            ascending=[True, False],
        )
        .groupby(["Model Name", "Flag", "Model Type"])
        .head(3)
    )

    # 保存結果到 Excel
    output_excel_path_top3_cases = os.path.join(argv_params["export_path"], "top3_rmse_cases_sorted.xlsx")
    top3_cases.to_excel(output_excel_path_top3_cases, index=False)

    print(f"\nTop 3 RMSE Cases saved to: {output_excel_path_top3_cases}")

    # 提取 rmse_all_case
    all_case_rmse_data = []
    added_keys = set()  # 用來跟蹤已處理的鍵 (model_name, key)

    for model_name, model_data in results.items():
        for key, metrics in model_data.items():
            # 處理符合 (flag, model_type) 的鍵
            if (
                isinstance(key, tuple)
                and len(key) == 2
                and (model_name, key) not in added_keys
            ):
                flag, model_type = key
                if (
                    isinstance(metrics, dict)
                    and "rmse_all_case" in metrics
                ):
                    all_case_rmse_data.append(
                        {
                            "Model Name": model_name,
                            "Data Split": flag,
                            "Model Type": model_type,
                            "RMSE All Case": metrics[
                                "rmse_all_case"
                            ],
                        }
                    )
                    added_keys.add((model_name, key))  # 記錄已處理的鍵

            # 處理符合 (flag, model_type, 'overall_rmse') 的鍵
            elif (
                isinstance(key, tuple)
                and len(key) == 3
                and key[2] == "overall_rmse"
                and (model_name, key[:2]) not in added_keys
            ):
                flag, model_type, _ = key
                if isinstance(metrics, (float, int, np.float64)):
                    all_case_rmse_data.append(
                        {
                            "Model Name": model_name,
                            "Data Split": flag,
                            "Model Type": model_type,
                            "RMSE All Case": metrics,
                        }
                    )
                    added_keys.add(
                        (model_name, key[:2])
                    )  # 記錄已處理的鍵
    df_all_case_rmse = pd.DataFrame(all_case_rmse_data)
    df_all_case_rmse.to_excel(output_excel_path_all_case_rmse, index=False)
    print("\nAll Case RMSE DataFrame:")
    print(df_all_case_rmse)

    # 提取 rmse_profile
    rmse_profile_data = []
    added_keys = set()  # 跟蹤已處理的鍵 (model_name, key)

    for model_name, model_data in results.items():
        for key, metrics in model_data.items():
            if (
                isinstance(key, tuple)
                and len(key) == 3
                and (model_name, key) not in added_keys
            ):
                flag, model_type, case_index = key
                if (
                    isinstance(metrics, dict)
                    and "rmse_profile" in metrics
                ):
                    profile_df = metrics["rmse_profile"]
                    for _, row in profile_df.iterrows():
                        rmse_profile_data.append(
                            {
                                "Model Name": model_name,
                                "Data Split": flag,
                                "Model Type": model_type,
                                "Case Index": case_index,
                                "Distance": row["distance"],
                                "RMSE": row["rmse"],
                            }
                        )
                    added_keys.add(
                        (model_name, key)
                    )  # 記錄已處理的鍵

    # 將結果轉為 DataFrame
    df_rmse_profile = pd.DataFrame(rmse_profile_data)
    df_rmse_profile.to_excel(
        output_excel_path_rmse_profile, index=False
    )
    print("\nRMSE Profile DataFrame:")
    print(df_rmse_profile)
    """
    print(
        f"Case RMSE results saved to: {output_excel_path_case_rmse}"
    )
    print(
        f"All Case RMSE results saved to: {output_excel_path_all_case_rmse}"
    )
    print(
        f"RMSE Profile results saved to: {output_excel_path_rmse_profile}"
    )

    #############################################################################
    # 使用畫分群圖
    set_type_list = [
        "training",
        "training",
        "validation",
        "validation",
    ]
    prediction_type_list = [
        "recursive",
        "direct",
        "recursive",
        "direct",
    ]
    # plot_all_rmse_by_bg_type_combinations(
    #     df=df_case_rmse,
    #     base_output_dir=argv_params["export_path"],
    #     new_folder="avg_rmse_plots",
    #     set_type_list=set_type_list,
    #     prediction_type_list=prediction_type_list,
    #     case_info=case_info,
    # )
    #############################################################################
    # # 我想比較不同模型的 RMSE
    # #   要區分 training ^ validation
    # mat = []
    # for model_name, elem in results.items():
    #     for key, elem2 in elem.items():
    #         (
    #             flag,
    #             model_type,
    #             case_index
    #         ) = key
    #         mat.append(
    #             [
    #                 model_name,
    #                 flag,
    #                 model_type,
    #                 case_index,
    #                 results[model_name][(flag, model_type, case_index)]["rmse_all_cases"]
    #             ]
    #         )
    # df = pd.DataFrame(mat, columns=[......])    # flag
    # df2 = df[df["flag"] == "training"]


def plot_all_rmse_by_bg_type_combinations(
    df,
    base_output_dir,
    new_folder,
    set_type_list,
    prediction_type_list,
    case_info,
):
    """
    繪製分群長條圖，按每個權重檔案單獨繪製，並分類存入不同資料夾。
    同時將每個權重檔案的 4 張主體圖合併，最終所有權重檔案的主體圖合併成一張大圖。
    """
    from PIL import Image
    import os
    import matplotlib.pyplot as plt

    # 創建主資料夾
    output_dir = os.path.join(base_output_dir, new_folder)
    os.makedirs(output_dir, exist_ok=True)

    # 從 case_info 提取 bg_type 映射
    bg_type_mapping = {
        case_index: case_data["bg_type"]
        for case_index, case_data in case_info.items()
    }

    # 添加 bg_type 列
    df["bg_type"] = df["Case Index"].map(bg_type_mapping)

    # 確認是否有未匹配的 bg_type
    if df["bg_type"].isnull().any():
        print(
            "有未匹配的 bg_type，請檢查 case_info 是否包含所有 Case Index"
        )
        print(df[df["bg_type"].isnull()])
        return  # 停止繪圖，避免繪製錯誤的數據

    all_image_paths = []  # 儲存所有權重檔案的 4 張圖路徑

    # 按權重檔案名稱排序（假設名稱中包含數值作為標識）
    model_names = sorted(
        df["Model Name"].unique(),
        key=lambda x: int(x.split("_")[-1].replace(".pt", "")),
    )

    for model_name in model_names:
        print(f"Processing weight file: {model_name}")
        model_image_paths = []  # 每個權重檔案的 4 張圖路徑

        # 創建每個權重檔案的資料夾
        model_output_dir = os.path.join(
            output_dir, model_name.replace(".pt", "")
        )
        os.makedirs(model_output_dir, exist_ok=True)

        for set_type, prediction_type in zip(
            set_type_list, prediction_type_list
        ):
            # 過濾數據
            filtered_df = df[
                (df["Data Split"] == set_type)
                & (df["Model Type"] == prediction_type)
                & (df["Model Name"] == model_name)
            ]

            # 按 bg_type 分組計算平均 RMSE 和案例數量
            avg_rmse_by_bg = filtered_df.groupby("bg_type")[
                "Case RMSE"
            ].mean()
            case_count_by_bg = (
                filtered_df["bg_type"]
                .value_counts()
                .sort_index()
            )

            # 找出平均 RMSE 最低的 bg_type
            min_rmse_idx = avg_rmse_by_bg.idxmin()

            # 繪製圖表
            plt.figure(figsize=(8, 6))
            bars = avg_rmse_by_bg.plot(
                kind="bar",
                color=[
                    "red" if idx == min_rmse_idx else "skyblue"
                    for idx in avg_rmse_by_bg.index
                ],
                edgecolor="black",
            )

            # 標註數值和案例數量
            for idx, bar in enumerate(bars.patches):
                avg_rmse = avg_rmse_by_bg.iloc[idx]
                case_count = (
                    case_count_by_bg.iloc[idx]
                    if idx in case_count_by_bg.index
                    else 0
                )
                bar_height = bar.get_height()
                plt.text(
                    bar.get_x() + bar.get_width() / 2,
                    bar_height,
                    f"{avg_rmse:.3f}\n({int(case_count)} cases)",
                    ha="center",
                    va="bottom",
                    fontsize=10,
                )

            # 設置圖表標題與軸標籤
            plt.title(
                f"Average RMSE by bg_type ({set_type}, {prediction_type}, {model_name})"
            )
            plt.xlabel("bg_type")
            plt.ylabel("Average RMSE")
            plt.xticks(rotation=0)  # X 軸文字保持水平
            plt.tight_layout()

            # 單獨保存圖片到對應的資料夾
            single_output_path = os.path.join(
                model_output_dir,
                f"{set_type}_{prediction_type}.png",
            )
            plt.savefig(single_output_path)
            plt.close()
            print(f"單張圖已保存到: {single_output_path}")
            model_image_paths.append(single_output_path)

        # 將當前權重檔案的 4 張圖加入總列表
        all_image_paths.extend(model_image_paths)

    # 整合圖片功能
    def combine_images(image_paths, output_path, num_cols=4):
        """
        合併多張圖片成一張大圖。
        """
        # 打開所有圖片
        images = [
            Image.open(img_path) for img_path in image_paths
        ]

        # 獲取圖片大小（假設所有圖片大小相同）
        img_width, img_height = images[0].size

        # 計算行數
        num_rows = (len(images) + num_cols - 1) // num_cols

        # 創建空白大圖
        combined_width = img_width * num_cols
        combined_height = img_height * num_rows
        combined_image = Image.new(
            "RGB", (combined_width, combined_height)
        )

        # 將圖片貼到大圖上
        for idx, img in enumerate(images):
            row_idx = idx // num_cols
            col_idx = idx % num_cols
            x_offset = col_idx * img_width
            y_offset = row_idx * img_height
            combined_image.paste(img, (x_offset, y_offset))

        # 保存大圖
        combined_image.save(output_path)
        print(f"合併完成，保存至: {output_path}")

    # 合併所有圖片到一張大圖
    combined_output_path = os.path.join(
        output_dir, "combined_all_weights.png"
    )
    combine_images(
        all_image_paths, combined_output_path, num_cols=4
    )


def compute_overall_rmse_by_model(model_results, model_names):
    """
    計算每個模型在所有案例（跨資料集）的整體 RMSE。
    model_results: 包含所有案例預測結果的字典
    model_names: 一個列表，包含所有模型檔案的名稱（例如："14_100_100_11_100.pt"）

    回傳一個字典，其結構為：
      overall_rmse[flag][model_name] = RMSE 值
    """
    overall_rmse = {
        flag: {}
        for flag in ["training", "validation", "testing"]
    }

    # 針對每一種資料集
    for flag in ["training", "validation", "testing"]:
        for model_name in model_names:
            # 以 direct 為例，若要用 recursive 只要把 key 改成 f"{model_name}_recursive"
            key = f"{model_name}_recursive"

            y_true_all = []
            y_pred_all = []

            # 對該資料集所有案例累積預測結果
            for case_index, case_data in model_results[
                flag
            ].items():
                if key in case_data:
                    y_true = case_data["y"]
                    y_pred = case_data[key]
                    y_true_all.append(y_true)
                    y_pred_all.append(y_pred)

            # 如果有數據，就計算 RMSE
            if y_true_all:
                y_true_all = np.concatenate(y_true_all, axis=0)
                y_pred_all = np.concatenate(y_pred_all, axis=0)

                # 如果真實值的欄位數為 23而預測值為 11，則取前 10 個欄位和最後 1 個欄位
                if (
                    y_true_all.shape[1] == 23
                    and y_pred_all.shape[1] == 11
                ):
                    y_true_all = np.concatenate(
                        (y_true_all[:, :10], y_true_all[:, -1:]),
                        axis=1,
                    )

                overall_rmse_val = compute_rmse(
                    y_true_all, y_pred_all
                )
                overall_rmse[flag][model_name] = overall_rmse_val
            else:
                overall_rmse[flag][model_name] = None
    return overall_rmse


def output_overall_rmse_to_excel(
    overall_rmse, output_excel_path
):
    """
    將計算出的 overall_rmse 結果輸出成 Excel 文件。
    overall_rmse: 字典結構 {flag: {model_name: RMSE}}
    output_excel_path: 輸出 Excel 文件路徑
    """
    data = []
    for flag in overall_rmse:
        for model_name, rmse in overall_rmse[flag].items():
            data.append(
                {
                    "Data Split": flag,
                    "Model Name": model_name,
                    "Overall RMSE": rmse,
                }
            )
    df = pd.DataFrame(data)
    df.to_excel(output_excel_path, index=False)
    print(f"Overall RMSE results saved to: {output_excel_path}")


def compute_training_case_rmse_details_recursive(
    model_results, case_index=2, model_names=None
):
    """
    提取 training set 中特定案例（例如 case2）的整體 RMSE 以及每個距離（或特徵）的 RMSE 值，
    以 recursive 預測結果為例。

    model_results: 包含所有案例預測結果的字典，應該有 "training" 資料集
    case_index: 指定案例索引，預設 "case2"
    model_names: 可選，若指定只處理部分模型（例如直接從 nn_weight_dict.keys() 過濾），否則處理所有模型

    回傳格式為字典：
    {
       model_name: {
           "overall_rmse": 整體 RMSE,
           "rmse_profile": DataFrame,  # 兩欄："distance"（或特徵編號）與 "rmse"
       },
       ...
    }
    """
    details = {}
    training_results = model_results.get("training", {})
    if case_index not in training_results:
        print(f"找不到 training set 中案例 {case_index}")
        return details

    case_data = training_results[case_index]

    # 如果沒有指定模型名稱，則處理所有模型
    if model_names is None:
        # 假設 nn_weight_dict 已經定義
        model_names = list(nn_weight_dict.keys())

    for model_name in model_names:
        # 以 recursive 預測為例
        key = f"{model_name}_recursive"
        if key not in case_data:
            print(f"案例 {case_index} 中沒有 {key} 預測結果")
            continue

        # 整體 RMSE：直接比較整個真實值與預測值
        y_true = case_data["y"]
        y_pred = case_data[key]
        # 若真實值原始欄位數為 23，而預測值只有 11，則取真實值的前 10 與最後 1 個欄位
        if y_true.shape[1] == 23 and y_pred.shape[1] == 11:
            y_true_conv = np.concatenate(
                (y_true[:, :10], y_true[:, -1:]), axis=1
            )
        else:
            y_true_conv = y_true
        overall_rmse_val = compute_rmse(y_true_conv, y_pred)

        # 針對每個欄位（或距離）計算 RMSE
        rmse_profile = []
        for i in range(y_true_conv.shape[1]):
            rmse_i = compute_rmse(
                y_true_conv[:, i], y_pred[:, i]
            )
            rmse_profile.append([i, rmse_i])
        rmse_profile_df = pd.DataFrame(
            rmse_profile, columns=["distance", "rmse"]
        )

        details[model_name] = {
            "overall_rmse": overall_rmse_val,
            "rmse_profile": rmse_profile_df,
        }
    return details


def output_training_case_details_to_excel(
    details, output_excel_path
):
    """
    將指定 training 案例（例如 case2）的整體 RMSE 與每個距離點的 rmse profile 輸出到 Excel 文件。
    details: 由 compute_training_case_rmse_details_recursive 得到的結果字典
    output_excel_path: 輸出 Excel 文件路徑
    """
    writer = pd.ExcelWriter(
        output_excel_path, engine="xlsxwriter"
    )

    overall_data = []
    for model_name, info in details.items():
        overall_data.append(
            {
                "Model Name": model_name,
                "Overall RMSE": info["overall_rmse"],
            }
        )
        # 每個模型的 rmse profile 分別存放在一個 sheet 中（sheet 名稱最多 31 個字元）
        sheet_name = model_name[:31]
        info["rmse_profile"].to_excel(
            writer, sheet_name=sheet_name, index=False
        )

    overall_df = pd.DataFrame(overall_data)
    overall_df.to_excel(
        writer, sheet_name="Overall RMSE", index=False
    )
    writer.close()
    print(f"Training case details saved to: {output_excel_path}")


def extract_final_losses(pickle_dir, output_dir):
    """
    自動檢索 pickle_dir 中的 .pickle 文件
    並在 output_dir 中輸出每個文件的最後損失值，按照檔名中的最後數字排序
    """
    os.makedirs(output_dir, exist_ok=True)
    pickle_files = [
        f
        for f in os.listdir(pickle_dir)
        if f.endswith(".pickle")
    ]

    # 根據檔名的最後數字排序
    def extract_number(file_name):
        match = re.search(r"_(\d+)\.pickle$", file_name)
        return int(match.group(1)) if match else 0

    sorted_files = sorted(pickle_files, key=extract_number)

    final_losses = []

    # 依據每個保存的檔案提取最後損失值
    for file in sorted_files:
        pickle_path = os.path.join(pickle_dir, file)
        print(f"正在讀取: {pickle_path}")
        try:
            with open(pickle_path, "rb") as f:
                result = pickle.load(f)
        except Exception as e:
            print(
                f"讀取 .pickle 檔案失敗: {pickle_path}, 錯誤訊息: {e}"
            )
            continue
        # 提取最後的損失值
        losses = np.array(result.get("losses", []))
        losses_val = np.array(result.get("losses_val", []))

        if losses.size > 0 and losses_val.size > 0:
            final_loss = {
                "file_name": file,
                "final_training_loss": losses[-1],
                "final_validation_loss": losses_val[-1],
            }
            final_losses.append(final_loss)
        else:
            print(
                f"警告：{pickle_path} 的損失數據為空，跳過該檔案"
            )

    output_csv_path = os.path.join(
        output_dir, "final_losses.csv"
    )
    df = pd.DataFrame(final_losses)
    df.to_csv(output_csv_path, index=False)
    print(f"最終損失值已保存到: {output_csv_path}")


def plot_case2_training_recursive_rmse(
    model_results: Dict,
    argv_params,
    case_info: Dict,
    training_case_details: Dict,
    case_index: int = 2,
):
    """
    只繪製 training set 中指定案例 (case_index=2) 的圖，
    每個模型 (權重檔) 各畫一張圖 (只畫 recursive 和 direct 結果)，
    在每個子圖右側標示該距離點的 RMSE (recursive 部分)，
    輸出圖檔名稱後面加上權重檔案最後面的數字 (epoch)。

    參數:
      model_results:         預測結果字典
      argv_params:           命令列參數字典
      case_info:             含有案例資訊 (bg_type 等)
      training_case_details: 由 compute_training_case_rmse_details_recursive() 得到的結果，
                             格式：
                             {
                               "14_100_100_11_3500.pt": {
                                   "overall_rmse": ...,
                                   "rmse_profile": DataFrame(columns=["distance", "rmse"])
                               },
                               "92_100_100_89_1000.pt": {...},
                               ...
                             }
      case_index:            預設為 2，只畫此案例 (整數型別)
    """
    # 確認 training set 中是否有該案例
    training_data = model_results["training"]
    if case_index not in training_data:
        print(f"找不到 training set 中案例 {case_index}")
        return

    # 取出該案例資料
    case_data = training_data[case_index]
    y_true_modflow = case_data["y"]  # MODFLOW 真值
    tlist = case_data["tlist"]

    # 取得案例 bg_type
    case_details = case_info.get(case_index, {})
    bg_type = case_details.get("bg_type", "Unknown")

    # 你想繪製的距離索引 (對應 view_distance.view_distance)
    col_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    # 遍歷每個模型（即每個權重檔），為每個模型分別繪製一張圖
    for model_name, detail_info in training_case_details.items():
        # 解析權重檔名的最後數字 (epoch)
        base_name = os.path.splitext(model_name)[
            0
        ]  # 例如 "14_100_100_11_3500"
        epoch_str = base_name.split("_")[-1]  # 例如 "3500"

        # 檢查該案例中是否有此模型的 recursive 結果
        rec_key = f"{model_name}_recursive"
        if rec_key not in case_data:
            print(
                f"案例 {case_index} 沒有 {rec_key} 預測結果，跳過。"
            )
            continue

        # 同時檢查 Direct 結果
        direct_key = f"{model_name}_direct"
        has_direct = direct_key in case_data

        y_pred_recursive = case_data[
            rec_key
        ]  # (time_step, n_features)
        if has_direct:
            y_pred_direct = case_data[direct_key]

        # 準備繪圖：每個模型獨立一張圖
        fig, axs = plt.subplots(
            len(col_list), 1, figsize=(20, 5 * len(col_list))
        )

        for i, col_idx in enumerate(col_list):
            ax = axs[i]
            # 畫 MODFLOW 真值 (黑線，圓點)
            ax.plot(
                tlist,
                y_true_modflow[:, col_idx],
                label="MODFLOW",
                color="black",
                linestyle="solid",
                marker="o",
                markersize=4,
                markerfacecolor="none",
                alpha=0.9,
            )
            # 畫 Recursive 預測 (藍線，圓點)
            ax.plot(
                tlist,
                y_pred_recursive[:, col_idx],
                label=f"{model_name} (Recursive)",
                color="blue",
                linestyle="solid",
                marker="o",
                markersize=4,
                markerfacecolor="none",
                alpha=0.7,
            )
            # 若 Direct 結果存在，畫 Direct 預測 (綠線，圓點)
            if has_direct:
                ax.plot(
                    tlist,
                    y_pred_direct[:, col_idx],
                    label=f"{model_name} (Direct)",
                    color="green",
                    linestyle="solid",
                    marker="o",
                    markersize=4,
                    markerfacecolor="none",
                    alpha=0.7,
                )

            # 標記 Recursive 的 RMSE 值 (直接從 detail_info 取)
            rmse_profile_df = detail_info["rmse_profile"]
            row = rmse_profile_df.loc[
                rmse_profile_df["distance"] == col_idx
            ]
            if not row.empty:
                rmse_val = row["rmse"].values[0]
                ax.text(
                    0.98,
                    0.95,
                    f"Rec_RMSE: {rmse_val:.3f}",
                    transform=ax.transAxes,
                    ha="right",
                    va="top",
                    color="blue",
                    fontsize=20,
                )

            ax.set_title(
                f"Case {case_index}, Dist: {view_distance.view_distance[col_idx]} m, bg_type: {bg_type}",
                fontsize=20,
            )
            ax.set_ylabel("Drawdown (m)", fontsize=20)
            ax.legend(
                loc="upper left",
                bbox_to_anchor=(
                    0,
                    1.02,
                ),  # 調整這個座標讓圖例稍微高於子圖
                ncol=3,  # 同一橫列的圖例數量，可依實際項目數調整
                fancybox=True,
                shadow=True,
                fontsize=15,
            )
            from matplotlib.ticker import MultipleLocator

            ax.xaxis.set_major_locator(MultipleLocator(5))

        fig.text(
            0.01,
            0.99,
            f"Training set - Case {case_index}\n(bg_type: {bg_type})",
            ha="left",
            va="top",
            fontsize=20,
        )
        plt.tight_layout()

        # 生成檔名：加入 epoch 字串，檔名格式: compare_case_{case_index}_training_Recursive_{epoch}.png
        save_fname = f"compare_case_{case_index}_training_Recursive_{epoch_str}.png"
        save_path = os.path.join(
            argv_params["export_path"],
            argv_params["export_fname"],
            "compare",
            save_fname,
        )
        jut.save_fig(save_path)
        plt.close(fig)
        gc.collect()
        clean_memory()

        print(f"圖已儲存到: {save_path}")


if __name__ == "__main__":
    # 載入案例基本資訊
    # argv_params = argv_analysis.argument_analysis(sys.argv[1:])
    # mf_pickle_fname = os.path.join(
    #     argv_params["export_path"],
    #     argv_params["pickle_fname"],
    # )
    # with open(mf_pickle_fname, "rb") as f:
    #     case_info = pickle.load(f)  # case_info 是一個字典或其他數據結構,將 case_info 傳遞給其他函數

    argv_params = argv_analysis.argument_analysis(sys.argv[1:])
    filtered_pickle_fname = os.path.join(
        argv_params["export_path"], "filtered_bg_type0.pickle"
    )
    with open(filtered_pickle_fname, "rb") as f:
        case_info = pickle.load(f)
    # 檢查所有案例的 bg_type 是否都為 0
    bg_types = {
        case.get("bg_type") for case in case_info.values()
    }
    print("case_info 中出現的 bg_type:", bg_types)
    if bg_types == {0}:
        print("所有案例的 bg_type 都是 0")
    else:
        print("有些案例的 bg_type 不是 0")

    # 事先讀取最終損失值
    extract_final_losses(
        pickle_dir=os.path.join(
            argv_params["export_path"],
            argv_params["dataset"],
            "weight",
        ),
        output_dir=os.path.join(argv_params["export_path"]),
    )

    argv_params = argv_analysis.argument_analysis(sys.argv[1:])
    split_data_fname = os.path.join(
        argv_params["export_path"],
        argv_params["training_name"],
        "dataset.pickle",
    )
    # 直接讀取 pickle
    with open(split_data_fname, "rb") as f:
        split_data = pickle.load(f)

    # 組合切割後的數據
    dataset = {}
    for flag in ["training", "validation", "testing"]:
        # 檢查 split_data 中的大小
        dataset[flag] = {
            "in": split_data[flag]["X"].sort_index(),
            "out": split_data[flag]["Y"].sort_index(),
        }

    # 決定 normalize 參數
    normalize_params_fname = os.path.join(
        argv_params["export_path"], "normalize_params.pickle"
    )
    with open(normalize_params_fname, "rb") as f:
        normalize_params = pickle.load(f)

    ##############################################################
    # load NN models
    weight_flist = fut.search_files_in_dir(
        os.path.join(
            argv_params["export_path"],
            argv_params["dataset"],
            "weight",
        ),
        regular_flags="\.pt",
    )
    ######################################################
    # 因應 results["model"] 是一個 PyTorch 模型，加載後轉移到 CPU 上
    nn_weight_dict: Dict = {}
    for weight_fname in weight_flist:
        print(
            f"Loading model: {weight_fname}"
        )  # 印出正在載入的模型檔案

        results = torch.load(
            weight_fname, map_location=torch.device("cpu")
        )
        nn_weight_dict[os.path.basename(weight_fname)] = results
    ######################################################

    # 後處理輸出
    # 1. 逐案例輸出模擬與檢定結果
    # 2. 跨案例, 輸出該案例之統計資訊
    #    比如：整體之 RMSE, 不同距離的 RMSE, 不同距離的 error
    model_results = {}
    for flag in ["training", "validation", "testing"]:
        model_results[flag] = {}
        df_in = dataset[flag]["in"]
        df_out = dataset[flag]["out"]
        case_list = list(
            set(df_in.index.get_level_values("case_index"))
        )

        # 設定只處理前__個案例
        max_cases_to_process = 3
        case_count = 0  # 計數器

        for case_index in case_list:
            if case_count >= max_cases_to_process and flag in [
                "training",
                "validation",
                "testing",
            ]:
                break  # 超過__個案例後停止處理

            print(
                f"Processing {flag} - Case Index: {case_index}"
            )
            case_count += 1  # 每處理一個案例，計數器加1

            # 逐案例進行計算
            df_in2 = df_in[
                df_in.index.get_level_values("case_index")
                == case_index
            ]
            df_out2 = df_out[
                df_out.index.get_level_values("case_index")
                == case_index
            ]

            # Normalize process
            x_normalized, y_normalized = (
                nn_utility.normalize_process(
                    df_in2.values,
                    df_out2.values,
                    normalize_params,
                )
            )

            # MODFLOW 模型的輸出結果
            model_results[flag][case_index] = {
                "tlist": df_in2.index.get_level_values("tlist"),
                "y": np.array(df_out2.values),  # MODFLOW
            }
            y_true_modflow = model_results[flag][case_index]["y"]

            #######################################################################
            # 載入多個權重檔案為多種模型
            # 預測部分
            for key, model in nn_weight_dict.items():
                #######################################################################
                # 1.遞迴預測方式
                # 針對當前案例的 x_cut 做模型預測
                # x_case = model_results[flag][case_index]["x_cut"]  # 取出截取後的X

                # y_predicted_recursive_np = (nn_prediction_recursive(model, x_normalized))
                y_predicted_recursive_np = (
                    nn_prediction_recursive(
                        model, process_short_x(x_normalized)
                    )
                )  # 陳文哥測試
                x_normalized_short = process_short_x(
                    x_normalized
                )
                x_training2, y_training2_recursive = (
                    nn_utility.normalize_process_inverse(
                        # x_normalized,
                        x_normalized_short,  # 陳文哥測試
                        y_predicted_recursive_np,
                        normalize_params,
                    )
                )

                # 紀錄 result
                model_results[flag][case_index][
                    f"{key}_recursive"
                ] = y_training2_recursive

                # # 2.類似drict_遞迴預測方式
                # y_predicted_recursive_np = nn_prediction_recursive_v2(model, x_normalized)
                # x_training2, y_training2_recursive = (
                #     nn_utility.normalize_process_inverse(
                #         x_normalized,
                #         y_predicted_recursive_np,
                #         normalize_params,
                #     )
                # )
                # #紀錄 result
                # model_results[flag][case_index][f"{key}_recursive"] = y_training2_recursive

                # # 3.window_遞迴預測方式
                # y_predicted_window_recursive_np = (nn_prediction_recursive_window(model, x_normalized))
                # x_training2, y_training2_window_recursive = (
                #     nn_utility.normalize_process_inverse(
                #         x_normalized,
                #         y_predicted_window_recursive_np,
                #         normalize_params,
                #     )
                # )
                # # 紀錄 result
                # model_results[flag][case_index][f"{key}_window_recursive"] = y_training2_window_recursive

                #######################################################################################################
                # 4.非遞迴方式，直接輸入整個數據

                x_normalized_short = process_short_x(
                    x_normalized
                )  # 陳文哥測試
                # y_predict = model(torch.tensor(x_normalized, dtype=dtype))
                y_predict = model(
                    torch.tensor(x_normalized_short, dtype=dtype)
                )  # 陳文哥測試

                x_training2, y_training2_direct = (
                    nn_utility.normalize_process_inverse(
                        # x_normalized,
                        x_normalized_short,
                        y_predict.detach().cpu().numpy(),
                        normalize_params,
                    )
                )
                # 紀錄 result
                model_results[flag][case_index][
                    f"{key}_direct"
                ] = y_training2_direct

    # model_compare_plot(model_results, argv_params, case_info)

    training_case_details = (
        compute_training_case_rmse_details_recursive(
            model_results, case_index=2
        )
    )
    output_excel_path_case2 = os.path.join(
        argv_params["export_path"],
        "training_case2_recursive_rmse_details.xlsx",
    )
    output_training_case_details_to_excel(
        training_case_details, output_excel_path_case2
    )

    # 假設 training_case_details 已由 compute_training_case_rmse_details_recursive() 得到，且案例索引我們取整數 2
    training_case_details = (
        compute_training_case_rmse_details_recursive(
            model_results, case_index=2
        )
    )
    # 當 show_rmse_annotation 為 True 且 target_case=2 時，就只繪製 training set 中案例2的圖，並在各子圖上標記 RMSE 值
    plot_case2_training_recursive_rmse(
        model_results,
        argv_params,
        case_info,
        training_case_details,
        case_index=2,
    )

    print(
        "Training set case indices:",
        model_results["training"].keys(),
    )
    model_names = list(nn_weight_dict.keys())
    overall_rmse_results = compute_overall_rmse_by_model(
        model_results, model_names
    )
    output_excel_path = os.path.join(
        argv_params["export_path"], "overall_rmse_results.xlsx"
    )
    output_overall_rmse_to_excel(
        overall_rmse_results, output_excel_path
    )

    # # 假設 `x_normalized` 是一個 NumPy 矩陣
    # # model 是已經訓練好的模型
    # compare_first_and_last_five_inputs(x_normalized, model)

    clean_memory()
dt4 = datetime.datetime.now()
print(dt3 - dt4)
