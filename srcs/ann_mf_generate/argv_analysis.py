"""
解析引數 
"""

import os
from typing import List, Dict

# 在 jlib
# time_phrase.time_phrase_multi 來解析時間符號


def argument_analysis(argvs: List) -> Dict:
    """
    解析 Argument
    """
    assert isinstance(argvs, list)
    argv_params = {}
    for argv in argvs:
        for flag in [
            "EXPORT_PATH",
            "PICKLE_FNAME",
            "EXPORT_FNAME",
            "log_unconfined",
            "PICKLE_FNAME",
            "TIMETRACKING",
            "ANN_SETUP",
            "DIST_OUTSIDE",
            "DIST_INSIDE",
            "DATASET",
            "TRAINING_NAME",
            "retraining",
            "training",
            "testing",
            "validation",
            "HIDDEN_LAYER_SIZE",
            "preanalysis",
            "TRAINING_ITER",
            "SAVE_ITER",
            "CASE_INDEX",
            "LOG_UNITE_DRAWDOWN_NORMALIZE",
            "CHOSEN_DIRECTION",
            "NN_STRUCTURE",
        ]:
            if argv.find(flag) >= 0:
                argv_params[flag.lower()] = (
                    argv.replace(flag, "")
                    .replace("=", "")
                    .strip()
                )
                if flag in [
                    "TIMETRACKING",
                    "TRAINING_ITER",
                    "SAVE_ITER",
                    "CASE_INDEX",
                ]:
                    argv_params[flag.lower()] = int(
                        argv_params[flag.lower()]
                    )

    for flag in [
        "log_unconfined",
        "retraining",
        "training",
        "testing",
        "validation",
        "preanalysis",
        "LOG_UNITE_DRAWDOWN_NORMALIZE",
    ]:
        # 邏輯變數
        # argv_params[flag] = flag in argv_params
        argv_params[flag.lower()] = any(
            [flag in arg for arg in argvs]
        )
    # 判斷邏輯變數
    # for flag in [.....]:
    #       argv_params[flag.lower()] = flag.lower() in argv_params
    # 判斷時間符號
    # for flag in [....]
    #       if argv_params[flag] != "all":
    #           時間資訊
    #           argv_params[flag.lower()] = time_phrase.time_phrase_multi("2024-1-1")

    for flag in ["dist_outside", "dist_inside"]:
        # 符點數
        if flag in argv_params:
            argv_params[flag] = float(argv_params[flag])
    for flag in ["hidden_layer_size"]:
        # 整數
        if flag in argv_params:
            argv_params[flag] = [
                int(elem)
                for elem in argv_params[flag].split(",")
            ]

    if "ann_setup" in argv_params:
        assert os.path.exists(argv_params["ann_setup"])
        lines = []
        with open(argv_params["ann_setup"], "r") as f:
            lines = f.readlines()

        argv_params["ann_setup"] = {
            "input_vars": lines[0].rstrip().split(","),
            "output_vars": lines[1].rstrip().split(","),
            "hidden_layer": int(lines[2].rstrip()),
        }
    if "nn_structure" in argv_params:
        # 讀取神經網路的結構組合
        lines = []
        with open(argv_params["nn_structure"], "r") as f:
            lines = f.readlines()

        argv_params["nn_structure"] = []
        for line in lines:
            sepline = line.rstrip().split()
            argv_params["nn_structure"].append(
                [int(elem) for elem in sepline]
            )

    return argv_params
