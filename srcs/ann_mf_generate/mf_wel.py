"""
模擬抽水井對於地下水系統之影響

地下水部分
地表至地下 50 m
模擬範圍 寬 7000 m
        長 7000 m
網格尺寸 20 m x 20 m
地下水深 地表下, 5-30 m 意味著 unconfined
    背景流場型態
        0, 水平
        1, 東西向
        2, 南北向
        3. 加入隨機 CHD 擾亂背景流場
    採用隨機擾動決定水深
    以 linspace 建立初始條件
    
含水層   K  0.5 m/day ~ 50 m/day
        Sy 0.05 ~ 0.03
        Ss 1e-6 ~ 1e-3

抽水量 -5000 ~ 5000 CMD
抽水型態
    0, 固定抽水量 (或注水)
    1, 定頻, 抽水or住水 與 停抽, 抽水量or住水量 固定
    2, 定頻, 抽水與注水, 抽水量與注水量固定, 且守恆
    3, 定頻, 抽水與注水, 抽水量與注水量固定, 且不守恆
    4, 定頻, 每天啟動固定時間抽水, 當天抽水量固定, 但隔天抽水量隨機擾動, 其他時間停抽
    5, 不定頻, 每天啟動固定時間隨機抽水or注水, 當天抽水量or住水量固定, 但隔天抽水量or住水量隨機擾動, 其他時間停抽
"""

import os
import sys
import numpy as np
import pandas as pd
import flopy
import math
from typing import Tuple, List, Dict, Union
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from multiprocessing import current_process
from cpuinfo import get_cpu_info
import pickle
import datetime

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jutility as jut
import plt_parameters  # noqa: F401
import jlib_logging
import parallel_framework
import file_utility as fut

sys.path.append(
    os.path.join("..", "..", "mf_process", "srcs", "mf_utility")
)
# import mf_utility
import mf_execute
import mf_utility

import argv_analysis
import view_distance

log_parallel = True
case_size = 2000
mf_params = {
    "DIS": {
        "dis_range": [7000, 7000, 50],
        "cell_size": [20, 20],
        "tlist": np.arange(0, 24 * 10, 1),  # 24 小時, 10 天
    },
    "BAS": {
        "IC": [5, 30],
        "bg_type": [
            0,
            1,
            2,
            3,
        ],  # 背景流場型態, 0 為水平, 1 & 2 為東西向與南北向, 3 為系統邊界隨機擾動 CHD
    },
    "LPF": {
        "Kh": [0.5, 50],
        "Sy": [0.05, 0.3],
        "Ss": [1e-6, 1e-3],
    },
    "WEL": {
        "spd_range": [-20000, 20000],  # 正負 5000 CMD
        "wel_type": [0, 1, 2, 3, 4, 5],
        "wel_period_range": [
            3,
            18,
        ],  # 抽水延時, 小時, 每天啟動時機, 採用整數
    },
}


def variable_generate(
    r: Union[int, float, np.ndarray],
    vrange: Tuple,
    loglog: bool = False,
) -> float:
    assert isinstance(vrange, tuple)
    assert len(vrange) == 2
    vari = (vrange[1] - vrange[0]) * r + vrange[0]

    if loglog:
        # log scale
        vrange2 = np.log10(vrange)
        vari = np.power(
            10, (vrange2[1] - vrange2[0]) * r + vrange2[0]
        )
    return vari


def ann_mf_wel(*arg, **kwargs):
    """
    呼叫 MF
    kwargs = {
        "dis_size": (nlay, nrow, ncol),
        "cell_length": (delr, delc),
        "zparams": (ztop, zbot, np.linspace(ztop, zbot, nlay + 1)),
        "ibound": ibound,
        "base_ws": base_ws,
        "log_parallel": log_parallel,
        "process_size": process_size,
        "tlist": mf_params["DIS"]["tlist"],
        "wel_period_range": mf_params["WEL"]["wel_period_range"],
        "root_logger": root_logger,
    }
    """
    root_logger = kwargs.get("root_logger", None)
    # (
    #     index,
    #     IC1,
    #     IC2,
    #     Kh,
    #     Sy,
    #     Ss,
    #     qlist,
    #     bg_type,
    #     wel_type,
    # ) = arg
    index = arg[0]
    IC1 = arg[1]
    IC2 = arg[2]
    Kh = arg[3]
    Sy = arg[4]
    Ss = arg[5]
    qlist = list(arg[6:-2])
    bg_type = arg[-2]
    wel_type = arg[-1]
    (nlay, nrow, ncol) = kwargs["dis_size"]
    (delr, delc) = kwargs["cell_length"]
    (ztop, _zbot, botm) = kwargs["zparams"]
    tlist = kwargs["tlist"]
    base_ws = kwargs["base_ws"]
    log_unconfined = kwargs["log_unconfined"]

    index2 = index % kwargs["process_size"]
    if kwargs["log_parallel"]:
        # 如果為平行
        p = current_process()
        try:
            index2 = p._identity[0] - 1
        except IndexError:
            pass

    message_index = "(case_id={}".format(index)
    if kwargs["log_parallel"]:
        message_index += " / parallel_id={}".format(
            current_process()._identity[0]
        )
    message_index += ")"
    if root_logger is not None:
        root_logger.debug("MF case: {}".format(message_index))

    workspace = os.path.join(base_ws, str(index2))
    name = "tutorial01_mf"
    mf = flopy.modflow.Modflow(
        name, exe_name="mf2005", model_ws=workspace
    )

    # MF 單位採用 m/hr
    # DIS
    steady = [True] + [False for _ in range(tlist.shape[0] - 1)]
    _dis = flopy.modflow.ModflowDis(
        mf,
        nlay,
        nrow,
        ncol,
        delr=delr,
        delc=delc,
        top=ztop,
        botm=botm[1:],
        nper=tlist.shape[0],
        perlen=1,  # 1 hr
        steady=steady,
    )

    # BAS
    assert isinstance(bg_type, int)
    assert bg_type in [0, 1, 2, 3]
    op = 1
    if log_unconfined:
        op = -1
    strt = op * IC1 * np.ones((nlay, nrow, ncol))  # 初始水位

    # 0 & 3 為水平向
    if bg_type == 1:
        # 東西向
        strt_line = np.linspace(op * IC1, op * IC2, ncol)
        for i in range(ncol):
            strt[:, :, i] = strt_line[i]
    elif bg_type == 2:
        # 南北向
        strt_line = np.linspace(op * IC1, op * IC2, nrow)
        for j in range(nrow):
            strt[:, j, :] = strt_line[j]
    result_single = {
        "bg_type": bg_type,
        "Kh": Kh,
        "Sy": Sy,
        "Ss": Ss,
    }
    if "bg_type" == 0:
        result_single["IC"] = op * IC1
        assert isinstance(result_single["IC"], (int, float))
    else:
        # 背景流場, 傾斜
        result_single["IC"] = [op * IC1, op * IC2]
        assert isinstance(result_single["IC"], (list, tuple))
    _bas = flopy.modflow.ModflowBas(
        mf,
        ibound=ibound,
        strt=strt,
        hnoflo=-999.0,
        stoper=0.001,
    )
    if bg_type == 3:
        # 以 CHD 擾動背景流場
        loc_index = [
            [int(nrow * jratio), int(ncol * iratio)]
            for iratio in np.arange(0.1, 1, 0.2)
            for jratio in np.arange(0.1, 1, 0.2)
            if np.sum(
                np.power(
                    np.array([iratio, jratio])
                    - np.array([0.5, 0.5]),
                    2,
                )
            )
            >= 0.16
        ]
        chd_values = variable_generate(
            np.random.uniform(0, 1, len(loc_index)),
            tuple(mf_params["BAS"]["IC"]),
        )
        spd_sub = []
        for i, loc in enumerate(loc_index):
            spd_sub.append(
                [
                    0,
                    loc[0],
                    loc[1],
                    -chd_values[i],
                    -chd_values[i],
                ]
            )
        stress_period_data = {
            t: spd_sub for t in range(tlist.shape[0])
        }
        flopy.modflow.ModflowChd(
            mf, stress_period_data=stress_period_data
        )

    # LPF
    kwargs_lpf = {
        "hk": Kh,
        "vka": 1.0,
        "sy": Sy,
        "ss": Ss,
        "laytyp": 1,
        "ipakcb": 53,
    }
    _lpf = flopy.modflow.ModflowLpf(mf, **kwargs_lpf)
    _pcg = flopy.modflow.ModflowPcg(mf)
    """
    #####################################################################
    # WEL
    # 抽水型態
    # 0, 固定抽水量 (或注水)
    # 1, 定頻, 抽水與停抽, 抽水量固定
    # 2, 定頻, 抽水與注水, 抽水量與注水量固定, 且守恆
    # 3, 定頻, 抽水與注水, 抽水量與注水量固定, 且不守恆
    # 4, 定頻, 每天啟動固定時間抽水和注水, 當天抽水量和住水量固定, 但隔天抽水量和住水量隨機擾動, 其他時間停抽
    # 5, 不定頻, 每天啟動固定時間隨機抽水和注水, 當天抽水量和住水量固定, 但隔天抽水量和住水量隨機擾動, 其他時間停抽
    # kwargs["wel_spd_range"] 抽水量範圍
    # kwargs["wel_period_range"] 抽水延時, 小時, 整數
    assert isinstance(wel_type, int)
    assert wel_type in [0, 1, 2, 3, 4, 5]

    # for 0, 1 & 2
    rsize = 1
    if wel_type in [3]:
        rsize = 2
    elif wel_type in [4, 5]:
        # 每天都擾動一次
        rsize = int(kwargs["tlist"].shape[0] / 24)  # 天數

    # 原始抽水範圍單位為 CMD, 需要轉換為 CMH
    # / 24
    r = np.random.uniform(0, 1, rsize)
    q_val = (
        variable_generate(r, tuple(kwargs["wel_spd_range"])) / 24
    )

    q_period = None  # 0, 固定抽水, 無停抽
    if wel_type in [1, 2, 3, 4]:
        q_period = int(
            variable_generate(
                np.random.uniform(0, 1, 1)[0],
                tuple(kwargs["wel_period_range"]),
            )
        )
    elif wel_type in [5]:
        r = np.random.uniform(0, 1, rsize)
        q_period = np.array(
            [
                int(
                    variable_generate(
                        r[i], tuple(kwargs["wel_period_range"])
                    )
                )
                for i in range(rsize)
            ]
        )

    spd = {}
    wel_loc = [
        0,
        int(nrow / 2),
        int(ncol / 2),
    ]
    qlist = np.array([])
    if wel_type == 0:
        # 恆定抽水 / 恆定注水
        assigned_q_val = q_val[0]
        # debug, 第一天不抽水, 第二天開始持續抽水
        # spd[24] 代表第二天的第一個時刻
        # 不設定 spd[25], 意味著延續 spd[24]
        spd[24] = [wel_loc + [q_val[0]]]
        qlist = np.ones(kwargs["tlist"].shape[0]) * q_val[0]
        qlist[:24] = 0
    elif wel_type in [1, 2, 3, 4]:
        # 定頻
        qlist = [0 for _ in range(24)]
        for t in kwargs["tlist"]:
            t = int(t)
            d = int((t - t % 24) / 24)
            if t >= 24:
                # 第一天不抽水, 第二天才開始抽水
                if t % 24 < q_period:
                    # 抽水延時之間
                    assigned_q_val = q_val[0]
                    if wel_type == 3:
                        # 強制為抽水
                        assigned_q_val = -np.absolute(
                            assigned_q_val
                        )

                    if wel_type == 4:
                        assigned_q_val = q_val[d]

                    spd[t] = [wel_loc + [assigned_q_val]]
                    qlist.append(assigned_q_val)
                else:
                    # 其他時間
                    if wel_type in [1, 4]:
                        # 停抽
                        spd[t] = [wel_loc + [0]]
                        qlist.append(0)
                    elif wel_type in [2]:
                        # 抽補守恆
                        spd[t] = [
                            wel_loc
                            + [
                                -(q_val[0] * q_period)
                                / (24 - q_period)
                            ]
                        ]
                        qlist.append(
                            -(q_val[0] * q_period)
                            / (24 - q_period)
                        )
                    elif wel_type in [3]:
                        # 不守恆
                        spd[t] = [
                            wel_loc + [np.absolute(q_val[1])]
                        ]
                        qlist.append(np.absolute(q_val[1]))
        qlist = np.array(qlist)
    elif wel_type in [5]:
        # 每天變頻
        qlist = [0 for _ in range(24)]
        for t in kwargs["tlist"]:
            t = int(t)
            d = int((t - t % 24) / 24)
            if t >= 24:
                # 第一天不抽水, 第二天才開始抽水
                if t % 24 < q_period[d]:
                    # 抽水延時之間
                    spd[t] = [wel_loc + [q_val[d]]]
                    qlist.append(q_val[d])
                else:
                    # 其他時間
                    spd[t] = [wel_loc + [0]]
                    qlist.append(0)
        qlist = np.array(qlist)
    assert tlist.shape[0] == qlist.shape[0]
    
    _wel = flopy.modflow.ModflowWel(mf, stress_period_data=spd)
    result_single["wel_type"] = wel_type
    result_single["wel_qlist"] = qlist  # 抽水量
    #####################################################################1/16
    """
    #####################################################################1/16
    # 從 df_variable 中讀取 wel_pattern
    wel_pattern = list(qlist)
    # 根據 wel_pattern 構造 spd
    wel_loc = [
        0,
        int(nrow / 2),
        int(ncol / 2),
    ]
    spd = {
        t: [wel_loc + [wel_pattern[t]]]
        for t in range(len(wel_pattern))
    }

    # 初始化 ModflowWel
    _wel = flopy.modflow.ModflowWel(mf, stress_period_data=spd)

    # 保存結果
    result_single["wel_type"] = wel_type
    result_single["wel_qlist"] = wel_pattern  # 抽水量
    #####################################################################1/16

    stress_period_data = {}
    for kper in range(tlist.shape[0]):
        for kstp in range(1):
            stress_period_data[(kper, kstp)] = [
                "save head",
                "save drawdown",
                "save budget",
                "print head",
                "print budget",
            ]
    flopy.modflow.ModflowOc(
        mf,
        stress_period_data=stress_period_data,
        extension=["oc", "hds", "cbc"],
        unitnumber=[14, 51, 53],
        compact=True,
    )
    # Write the model input files
    mf.write_input()

    # MF running
    mf_execute.run_modflow_multiple_ways(
        mf,
        modelws=workspace,
        exec="mf_owhm_2_1",
        nam_fname=name,
    )

    # Post Analysis
    hds_fname = os.path.join(workspace, "{}.hds".format(name))
    strt: List = mf_utility.read_hds(
        hds_fname, "ALL", mask_val=-999.0
    )

    # 模擬時刻數 == strt 產出時刻數
    assert len(strt) == tlist.shape[0], "{} / {}".format(
        len(strt), tlist.shape[0]
    )

    view_distance_index = [
        int(vd / mf_params["DIS"]["cell_size"][0])
        for vd in view_distance.view_distance
    ]
    direct_params = {
        "E": (1, 0),  # (Yindex, Xindex)
        "W": (-1, 0),
        "N": (0, -1),
        "S": (0, 1),
    }
    result_single["strt"] = {}
    for key, direct_elem in direct_params.items():
        mat = []
        for t, strt_elem in enumerate(strt):
            for j in range(len(view_distance.view_distance)):
                mat.append(
                    [
                        t,
                        view_distance.view_distance[j],
                        strt_elem[
                            wel_loc[0],
                            wel_loc[1]
                            + direct_elem[0]
                            * view_distance_index[j],
                            wel_loc[2]
                            + direct_elem[1]
                            * view_distance_index[j],
                        ],
                    ]
                )
        df_strt = pd.DataFrame(
            mat, columns=["time", "distance", "h"]
        ).set_index(["time", "distance"])
        result_single["strt"][key] = df_strt

    with open(
        os.path.join(kwargs["base_ws"], "mf_case.csv"), "a"
    ) as f:
        f.write(
            "{}, {}, {}\n".format(
                datetime.datetime.now(),
                index,
                index2,
                # current_process()._identity[0],
            )
        )
    if index % kwargs["process_size"] == 0:
        # 只在 pid == 0 處理
        with open(
            os.path.join(
                kwargs["base_ws"], "mf_case_size_count.csv"
            ),
            "a",
        ) as f2:
            num_lines = sum(
                1
                for _ in open(
                    os.path.join(
                        kwargs["base_ws"], "mf_case.csv"
                    )
                )
            )
            f2.write(
                "{},{}\n".format(
                    datetime.datetime.now(),
                    num_lines - 1,
                )
            )

    # for key, elem in result_single.items():
    #    print (" --> {}, {}".format(key, elem))
    fut.remove_file(workspace)
    return result_single


def ann_mf_wel_shell(flag: List):
    """
    呼叫 MF 計算河流對地下水系統的影響程度與距離
    """
    return ann_mf_wel(*tuple(flag[:-1]), **flag[-1])


def generate_wel_pattern(
    wel_type, tlist, wel_spd_range, period_range
):  # 1/16
    assert isinstance(wel_type, int)
    assert wel_type in [0, 1, 2, 3, 4, 5]

    wel_pattern = []
    q_period = None

    # for 0, 1 & 2
    rsize = 1
    if wel_type in [3]:
        rsize = 2
    elif wel_type in [4, 5]:
        # 每天都擾動一次
        rsize = int(len(tlist) / 24)  # 天數

    # 原始抽水範圍單位為 CMD, 需要轉換為 CMH
    # / 24
    r = np.random.uniform(0, 1, rsize)
    q_val = variable_generate(r, tuple(wel_spd_range)) / 24

    if wel_type in [1, 2, 3, 4]:
        q_period = int(
            variable_generate(
                np.random.uniform(0, 1, 1)[0],
                tuple(period_range),
            )
        )
    elif wel_type in [5]:
        r = np.random.uniform(0, 1, rsize)
        q_period = np.array(
            [
                int(variable_generate(r[i], tuple(period_range)))
                for i in range(rsize)
            ]
        )

    qlist = np.array([])
    if wel_type == 0:
        # 恆定抽水 / 恆定注水
        assigned_q_val = q_val[0]
        qlist = np.ones(len(tlist)) * q_val[0]
        qlist[:24] = 0
    elif wel_type in [1, 2, 3, 4]:
        # 定頻
        qlist = [0 for _ in range(24)]
        for t in tlist:
            t = int(t)
            d = int((t - t % 24) / 24)
            if t >= 24:
                if t % 24 < q_period:
                    assigned_q_val = q_val[0]
                    if wel_type == 3:
                        assigned_q_val = -np.absolute(
                            assigned_q_val
                        )
                    if wel_type == 4:
                        assigned_q_val = q_val[d]
                    qlist.append(assigned_q_val)
                else:
                    if wel_type in [1, 4]:
                        qlist.append(0)
                    elif wel_type == 2:
                        qlist.append(
                            -(q_val[0] * q_period)
                            / (24 - q_period)
                        )
                    elif wel_type == 3:
                        qlist.append(np.absolute(q_val[1]))
        qlist = np.array(qlist)
    elif wel_type in [5]:
        # 每天變頻
        qlist = [0 for _ in range(24)]
        for t in tlist:
            t = int(t)
            d = int((t - t % 24) / 24)
            if t >= 24:
                if t % 24 < q_period[d]:
                    qlist.append(q_val[d])
                else:
                    qlist.append(0)
        qlist = np.array(qlist)

    assert len(tlist) == len(qlist)
    return qlist


def plot_case_size_count(**kwargs):
    """
    繪製 case size 資訊
    """
    df_case_size_count = pd.read_csv(
        os.path.join(
            kwargs["base_ws"], "mf_case_size_count.csv"
        ),
        header=None,
    )
    df_case_size_count.columns = ["datetime", "csize"]
    df_case_size_count = df_case_size_count.set_index("datetime")
    df_case_size_count.index = pd.to_datetime(
        df_case_size_count.index
    )
    plt.style.use("bmh")
    _fig, axs = plt.subplots(1, figsize=(12, 9), sharex=True)

    df_case_size_count["csize"].plot(ax=axs, grid=True)
    # axs[0].set_xlabel("時間")
    axs.set_ylabel("完成案例數")
    axs.set_title("MF 完成度")

    # df_case_size_count["pratio"].plot(ax=axs, grid=True, secondary_y=True)
    axs.set_xlabel("時間")
    # ax2 = axs.twinx()
    # ax2.set_ylabel(r"完成比例 $\%$")
    axs.set_xticklabels(
        axs.get_xticklabels(), rotation=45, ha="right"
    )
    myFmt = mdates.DateFormatter("%H:%M:%S")
    axs.xaxis.set_major_formatter(myFmt)
    jut.save_fig(
        os.path.join(kwargs["base_ws"], "mf_case_size_count")
    )


if __name__ == "__main__":
    program_name = "ann_mf_wel"
    root_logger = jlib_logging.logger_setup(
        program_name,
        filename=os.path.join(
            "logging",
            program_name,
        ),
        log_append=False,
    )
    argv_params = argv_analysis.argument_analysis(sys.argv[1:])
    # print (argv_params)
    # sys.exit()
    for flag in [
        "export_path",
        "log_unconfined",
        "pickle_fname",
    ]:
        mf_params[flag] = argv_params[flag]

    # 隨機產生
    # 變因：地下水深, 背景流場, Kh, Sy, 抽水量
    df_variable_fname = os.path.join(
        mf_params["export_path"], "df_variable.csv"
    )

    mat = []
    if not os.path.exists(df_variable_fname):
        for i in range(case_size):
            r = np.random.uniform(0, 1, 6)
            ########################################################################
            wel_type = int(
                variable_generate(
                    r[5],
                    (0, len(mf_params["WEL"]["wel_type"]) - 1),
                )
            )  # 1/16
            wel_pattern = generate_wel_pattern(
                wel_type,
                mf_params["DIS"]["tlist"],
                mf_params["WEL"]["spd_range"],
                mf_params["WEL"]["wel_period_range"],
            )  # 1/16
            ##########################################################################
            mat_sub = [
                variable_generate(
                    r[0], tuple(mf_params["BAS"]["IC"])
                ),  # 地下水深
                variable_generate(
                    r[1], tuple(mf_params["BAS"]["IC"])
                ),  # 地下水深
                variable_generate(
                    r[2],
                    tuple(
                        mf_params["LPF"]["Kh"],
                    ),
                    loglog=True,
                ),
                variable_generate(
                    r[3], tuple(mf_params["LPF"]["Sy"])
                ),
                variable_generate(
                    r[4],
                    tuple(mf_params["LPF"]["Ss"]),
                    loglog=True,
                ),
                wel_type,  # 抽水型態1/16
            ] + list(
                wel_pattern
            )  # wel_pattern  # 抽水模式1/16
            mat.append(mat_sub)

        # tlist 的長度作為抽水模式步1/16
        pattern_steps = len(mf_params["DIS"]["tlist"])

        df_variable = pd.DataFrame(
            mat,
            columns=[
                "IC1",  # 如為水平, 則只採用 IC1
                "IC2",  # 如為南北向, 或是東西向, 才啟用 IC2
                "Kh",
                "Sy",
                "Ss",
                "wel_type",  # 1/16
            ]
            + [f"step{i}" for i in range(pattern_steps)],  # 1/16
        )
        try:
            df_variable.to_csv(df_variable_fname)
        except OSError:
            os.makedirs(os.path.dirname(df_variable_fname))
            df_variable.to_csv(df_variable_fname)
    else:
        df_variable = pd.read_csv(df_variable_fname)
        df_variable = df_variable.set_index("Unnamed: 0")
    print(df_variable)

    # 平行數量
    cpu_item = get_cpu_info()
    processor_ratio = 0.3
    process_size = max(
        int(float(cpu_item["count"]) * processor_ratio),
        1,
    )

    def mf_predefine(mf_params: Dict) -> Tuple:
        """
        預先定義 mf 相關資訊
        """
        base_ws = mf_params["export_path"]
        Lx = mf_params["DIS"]["dis_range"][0]
        Ly = mf_params["DIS"]["dis_range"][1]
        nrow = int(
            math.floor(Ly / mf_params["DIS"]["cell_size"][1])
        )
        ncol = int(
            math.floor(Lx / mf_params["DIS"]["cell_size"][0])
        )
        nlay = 1
        delr = mf_params["DIS"]["cell_size"][0]
        delc = mf_params["DIS"]["cell_size"][1]
        ztop = 0.0
        zbot = -mf_params["DIS"]["dis_range"][2]

        # 四面環繞 Const. Head
        ibound = np.ones((nlay, nrow, ncol), dtype=np.int32)
        ibound[:, 0, :] = -1
        ibound[:, -1, :] = -1
        ibound[:, :, -1] = -1
        ibound[:, :, 0] = -1
        return (
            base_ws,
            Lx,
            Ly,
            nrow,
            ncol,
            nlay,
            delr,
            delc,
            ztop,
            zbot,
            ibound,
        )

    (
        base_ws,
        Lx,
        Ly,
        nrow,
        ncol,
        nlay,
        delr,
        delc,
        ztop,
        zbot,
        ibound,
    ) = mf_predefine(mf_params)
    kwargs = {
        "dis_size": (nlay, nrow, ncol),
        "cell_length": (delr, delc),
        "zparams": (
            ztop,
            zbot,
            np.linspace(ztop, zbot, nlay + 1),
        ),
        "ibound": ibound,
        "base_ws": base_ws,
        "log_parallel": log_parallel,
        "log_unconfined": mf_params["log_unconfined"],
        "process_size": process_size,
        "tlist": mf_params["DIS"]["tlist"],
        "wel_spd_range": mf_params["WEL"]["spd_range"],
        "wel_period_range": mf_params["WEL"]["wel_period_range"],
        "root_logger": root_logger,
    }

    os.makedirs(base_ws, exist_ok=True)
    # 紀錄所有case執行進度
    if not os.path.exists(os.path.join(base_ws, "mf_case.csv")):
        root_logger.debug("Create workspace folder")
        with open(
            os.path.join(base_ws, "mf_case.csv"), "w"
        ) as f:
            f.write("time, index, index2\n")

    # mf result
    # 1. create a empty result dict
    # or
    # 2. load pervious results from an exist pickle file
    pickle_fname = os.path.join(
        base_ws, mf_params["pickle_fname"]
    )
    results = {}
    if os.path.exists(pickle_fname):
        with open(pickle_fname, "rb") as f:
            results = pickle.load(f)
    try:
        fut.remove_file(
            os.path.join(
                kwargs["base_ws"], "mf_case_size_count.csv"
            )
        )
    except FileNotFoundError:
        pass

    # 把所有的案例的參數紀錄
    # IC1, IC2, ...., wel_type, bg_type....
    root_logger.debug("MF Simulation")
    flags = []
    index2 = 0
    count = 0
    while count < case_size:
        for bg_type in mf_params["BAS"]["bg_type"]:
            for wel_type in mf_params["WEL"]["wel_type"]:
                try:
                    index = df_variable.index[count]
                    if (
                        index2 not in results
                    ):  # 用來排除前次已經計算過的案例
                        flags.append(
                            [
                                index2,
                            ]
                            + list(
                                df_variable.loc[index, :].values
                            )
                            + [
                                bg_type,
                                wel_type,
                                kwargs,
                            ]
                        )
                        index2 += 1
                except IndexError:
                    pass
                count += 1

    with open(
        os.path.join(
            kwargs["base_ws"], "mf_case_size_count.csv"
        ),
        "w",
    ) as f:
        f.write("{},{}\n".format(datetime.datetime.now(), 0))
        if os.path.exists(
            os.path.join(kwargs["base_ws"], "mf_case.csv")
        ):
            num_lines = sum(
                1
                for _ in open(
                    os.path.join(
                        kwargs["base_ws"], "mf_case.csv"
                    )
                )
            )
            f.write(
                "{},{}\n".format(
                    datetime.datetime.now(),
                    num_lines - 1,
                )
            )
    plot_case_size_count(**kwargs)

    loop_size_outter = process_size * 8
    loop_count = 0
    for i in range(0, len(flags), loop_size_outter):
        # 分段進行平行運算
        flags2 = []
        for j in range(i, min(i + loop_size_outter, len(flags))):
            if flags[j][0] not in results:
                flags2.append(flags[j])
        if len(flags2) > 0:
            message = (
                "MF loop: {}. from {} to {} / size={}".format(
                    loop_count,
                    i,
                    min(i + loop_size_outter, len(flags)),
                    loop_size_outter,
                )
            )
            root_logger.info(message)
            mat = parallel_framework.parallel_process_framework(
                ann_mf_wel_shell,
                flags2,
                log_parallel=log_parallel,
                processor_ratio=processor_ratio,
                root_logger=root_logger,
            )

            ###############################################################
            # 完成 flags2 的計算
            # 繪製 完成案例的成果
            plot_case_size_count(**kwargs)

            # 輸出 pickle dump
            if os.path.exists(pickle_fname):
                with open(pickle_fname, "rb") as f:
                    results = pickle.load(f)

            for i, elem in enumerate(flags2):
                if flags2[i][0] not in results:
                    try:
                        results[flags2[i][0]] = mat[i]
                    except IndexError as e:
                        raise IndexError(
                            "{} / {}".format(i, len(mat))
                        ) from e
            root_logger.debug(
                "  --> Export pickle: {}".format(len(results))
            )
            with open(pickle_fname, "wb") as f:
                pickle.dump(results, f)
            loop_count += 1
