def process_short_x(x):
    """
    從 x 中選取以下欄位：
      - 第 0~9 欄 (共 10 欄)
      - 第 22~31 欄 (共 10 欄)
      - 第 44~53 欄 (共 10 欄)
      - 第 66~75 欄 (共 10 欄)
      - 最後 4 欄
    最終得到一個新矩陣，總共 44 欄。
    """
    indices = list(range(0, 10)) + list(
        range(x.shape[1] - 4, x.shape[1])
    )
    # list(range(22, 32)) + \
    # list(range(44, 54)) + \
    # list(range(66, 76)) + \

    new_x = x[:, indices]
    return new_x


def process_short_y(x):
    """
    從 x 中選取以下欄位：
      - 第 0~9 欄 (共 10 欄)
      - 第 22~31 欄 (共 10 欄)
      - 第 44~53 欄 (共 10 欄)
      - 第 66~75 欄 (共 10 欄)
      - 最後 1 欄
    最終得到一個新矩陣。
    """
    indices = list(range(0, 10)) + list(
        range(x.shape[1] - 1, x.shape[1])
    )
    # list(range(22, 32)) + \
    # list(range(44, 54)) + \
    # list(range(66, 76)) + \

    new_x = x[:, indices]
    return new_x
