import matplotlib.pyplot as plt
from math import cos, sin, atan
import numpy as np
import sys
import os
from typing import List, Tuple

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jutility as jut

import argv_analysis

"""
Visualization of  Neural Netork Architecture
"""


class Neuron:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def draw(self, neuron_radius, **kwargs):
        loc = (self.x, self.y)
        circle = plt.Circle(
            loc,
            radius=neuron_radius,
            fill=False,
            **{
                key: elem
                for key, elem in kwargs.items()
                if key in ["linewidth", "color"]
            },
        )
        ax = plt.gca()

        ax.add_patch(circle)
        if "labels" in kwargs:
            ratio = 0.45
            loc2 = (
                self.x - neuron_radius * ratio,
                self.y - neuron_radius * ratio,
            )
            ax.annotate(
                kwargs["labels"],
                xy=loc2,
                xytext=loc2,
                xycoords="data",
                # color="k",
                # fontsize=12,
            )


class Layer:
    def __init__(
        self,
        network,
        number_of_neurons,
        number_of_neurons_in_widest_layer,
        neuron_radius,
    ):
        self.vertical_distance_between_layers = 6
        self.horizontal_distance_between_neurons = 2
        self.neuron_radius = neuron_radius  # 節點半徑
        self.number_of_neurons_in_widest_layer = (
            number_of_neurons_in_widest_layer
        )
        self.previous_layer = self.__get_previous_layer(network)
        self.y = self.__calculate_layer_y_position()
        self.neurons = self.__intialise_neurons(
            number_of_neurons
        )

    def __intialise_neurons(self, number_of_neurons):
        neurons = []
        x = self.__calculate_left_margin_so_layer_is_centered(
            number_of_neurons
        )
        for iteration in np.arange(number_of_neurons):
            neuron = Neuron(x, self.y)
            neurons.append(neuron)
            x += self.horizontal_distance_between_neurons
        return neurons

    def __calculate_left_margin_so_layer_is_centered(
        self, number_of_neurons
    ):
        return (
            self.horizontal_distance_between_neurons
            * (
                self.number_of_neurons_in_widest_layer
                - number_of_neurons
            )
            / 2
        )

    def __calculate_layer_y_position(self):
        if self.previous_layer:
            return (
                self.previous_layer.y
                + self.vertical_distance_between_layers
            )
        else:
            return 0

    def __get_previous_layer(self, network):
        if len(network.layers) > 0:
            return network.layers[-1]
        else:
            return None

    def __line_between_two_neurons(self, neuron1, neuron2):
        angle = atan(
            (neuron2.x - neuron1.x)
            / float(neuron2.y - neuron1.y)
        )
        x_adjustment = self.neuron_radius * sin(angle)
        y_adjustment = self.neuron_radius * cos(angle)
        """
        line = plt.Line2D(
            (neuron1.x - x_adjustment, neuron2.x + x_adjustment),
            (neuron1.y - y_adjustment, neuron2.y + y_adjustment),
            color="k",
            linewidth=0.5,
            alpha=0.6,
        )        
        plt.gca().add_line(line)
        """
        ax = plt.gca()
        ratio = 0.95
        ax.annotate(
            "",
            xy=(
                neuron1.x - x_adjustment * ratio,
                neuron1.y - y_adjustment * ratio,
            ),
            xytext=(
                neuron2.x + x_adjustment * ratio,
                neuron2.y + y_adjustment * ratio,
            ),
            xycoords="data",
            arrowprops=dict(
                arrowstyle="->",
                color="k",
                lw=0.5,
                ls="-",
                alpha=0.6,
            ),
        )

    def draw(self, layerType=0, **kwargs):
        kwargs2 = {
            key: elem
            for key, elem in kwargs.items()
            # if key in ["linewidth", "color"]
        }
        for i, neuron in enumerate(self.neurons):
            if "labels" in kwargs:
                kwargs2["labels"] = kwargs["labels"][i]
            neuron.draw(self.neuron_radius, **kwargs2)
            if self.previous_layer:
                for (
                    previous_layer_neuron
                ) in self.previous_layer.neurons:
                    self.__line_between_two_neurons(
                        neuron, previous_layer_neuron
                    )
        # write Text
        x_text = (
            self.number_of_neurons_in_widest_layer
            * self.horizontal_distance_between_neurons
        )
        # 右側文字
        if layerType == 0:
            plt.text(x_text, self.y, "Input Layer", fontsize=12)
        elif layerType == -1:
            plt.text(x_text, self.y, "Output Layer", fontsize=12)
        else:
            plt.text(
                x_text,
                self.y,
                "Hidden Layer " + str(layerType),
                fontsize=12,
            )


class NeuralNetwork:
    def __init__(
        self, number_of_neurons_in_widest_layer: int, **kwargs
    ):
        self.number_of_neurons_in_widest_layer = (
            number_of_neurons_in_widest_layer
        )
        self.layers = []
        self.layertype = 0
        self.neurons_labels = kwargs

    def add_layer(self, number_of_neurons, neuron_radius):
        layer = Layer(
            self,
            number_of_neurons,
            self.number_of_neurons_in_widest_layer,
            neuron_radius,
        )
        self.layers.append(layer)

    def draw(self, **kwargs):
        plt.figure(
            **{
                key: elem
                for key, elem in kwargs.items()
                if key in ["figsize"]
            }
        )
        for i in range(len(self.layers)):
            layer = self.layers[i]
            if i == len(self.layers) - 1:
                i = -1

            kwargs2 = {}
            if (i == 0) and (
                "input_labels" in self.neurons_labels
            ):
                # input layer
                kwargs2 = {
                    "labels": self.neurons_labels[
                        "input_labels"
                    ],
                    "linewidth": 2.5,
                    "color": "darkblue",
                    "neuron_type": "input",
                }
            elif (i in [len(self.layers) - 1, -1]) and (
                "output_labels" in self.neurons_labels
            ):
                # output layer
                kwargs2 = {
                    "labels": self.neurons_labels[
                        "output_labels"
                    ],
                    "linewidth": 2.5,
                    "color": "firebrick",
                    "neuron_type": "output",
                }
            layer.draw(i, **kwargs2)
        plt.axis("scaled")
        plt.axis("off")
        if "fig_title" in kwargs:
            plt.title(
                kwargs.get(
                    "fig_title", "Neural Network architecture"
                ),
                fontsize=15,
            )

        plt.tight_layout()
        if "fig_fname" in kwargs:
            jut.save_fig(kwargs["fig_fname"])
        else:
            plt.show()


class DrawNN:
    def __init__(self, neural_network, **kwargs):
        self.neural_network = neural_network
        self.labels = {
            key: kwargs[key]
            for key in ["input_labels", "output_labels"]
            if key in kwargs
        }
        print(self.labels)

    def draw(self, **kwargs):
        widest_layer = max(self.neural_network)
        network = NeuralNetwork(widest_layer, **self.labels)
        for l, layer in enumerate(self.neural_network):
            if (l == 0) or (l == len(self.neural_network) - 1):
                network.add_layer(layer, 0.6)  # IO, 節點放大
            else:
                network.add_layer(
                    layer, 0.5
                )  # 隱藏層, radius 縮小
        network.draw(**kwargs)


variable_label_params = {
    "Kh": r"$K_{h}$",
    "T": r"$T$",
    "Sy": r"$S_{y}$",
    "S": r"$S$",
    "wel_qlist": r"$Q_{t}$",
    "B": "B",  # 含水層厚度
    "h": "h",
    "drawdown": "d",
    "levelgap": "\Delta h",
}


def calc_variables(var_define: List, tstr) -> Tuple:
    """
    定義變數數量與符號
    """
    var_count = 0
    var_labels = []
    for var in var_define:
        if var in ["h", "drawdown", "levelgap"]:
            var_labels += [
                r"$\vec{"
                + variable_label_params[var]
                + "}_{"
                + tstr
                + ",E}$",
                r"$\vec{"
                + variable_label_params[var]
                + "}_{"
                + tstr
                + ",W}$",
                r"$\vec{"
                + variable_label_params[var]
                + "}_{"
                + tstr
                + ",N}$",
                r"$\vec{"
                + variable_label_params[var]
                + "}_{"
                + tstr
                + ",S}$",
            ]
        elif var in ["B"]:
            var_labels += [
                r"$B_{" + tstr + "}$",
            ]
        elif var in ["wel_qlist"]:
            var_labels += [
                r"$Q_{t}$",
            ]
        else:
            var_labels += [
                variable_label_params[var],
            ]

    var_count = len(var_labels)
    return var_count, var_labels


if __name__ == "__main__":
    argv_params = argv_analysis.argument_analysis(sys.argv[1:])
    print(argv_params)

    var_size_in, var_labels_in = calc_variables(
        argv_params["ann_setup"]["input_vars"], "t"
    )
    var_size_out, var_labels_out = calc_variables(
        argv_params["ann_setup"]["output_vars"], "t+1"
    )
    print(var_labels_out)

    network = DrawNN(
        [var_size_in]
        + argv_params["hidden_layer_size"]
        + [var_size_out],
        input_labels=var_labels_in,
        output_labels=var_labels_out,
    )
    network.draw(
        fig_fname=os.path.join(
            argv_params["export_path"],
            argv_params["export_fname"],
        ),
        figsize=(12, 9),
        fig_title="NN Architecture for Unconfined Aquifer",
    )
