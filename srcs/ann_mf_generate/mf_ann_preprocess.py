"""
1. pickle load mf_wel simulation results
2. 因應訓練架構, 輸出 csv 格式的訓練結果

功能增加

"""

import os
import sys
import pickle
import pandas as pd
import numpy as np
from typing import List, Dict, Tuple, Union
from scipy.interpolate import CubicSpline

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import parallel_framework

import argv_analysis
import view_distance  # 觀測點距離

log_parallel = True

four_axes = ["E", "W", "N", "S"]


def load_mf_data(argv_params) -> Dict:
    """
    Data loading from pickle file
    """
    with open(
        os.path.join(
            argv_params["export_path"],
            argv_params["pickle_fname"],
        ),
        "rb",
    ) as f:
        data_dict = pickle.load(f)
    return data_dict


if __name__ == "__main__":
    argv_params = argv_analysis.argument_analysis(sys.argv[1:])

    # 檢查 argv_params 是否有 chosen_direction，如果沒有設定，則 chosen_direction 為 None
    chosen_direction = argv_params.get("chosen_direction")
    # 檢查方向
    if chosen_direction == "E":
        print("只處理東邊方向的觀測點數據")
    elif chosen_direction == "N":
        print("只處理北邊方向的觀測點數據")
    elif chosen_direction == "W":
        print("只處理西邊方向的觀測點數據")
    elif chosen_direction == "S":
        print("只處理南邊方向的觀測點數據")
    else:
        print("處理所有方向的觀測點數據")

    print(argv_params)


def grep_dist_h(
    strt_dict: Dict,
    tlist,
    dist: Union[float, int],
    chosen_direction=None,
) -> np.ndarray:
    """
    return 擷取特定距離下，四軸方向，隨時間而變的水位 np.ndarray
    """
    mat = []
    x = np.array(view_distance.view_distance)
    directions = (
        [chosen_direction] if chosen_direction else four_axes
    )  # 如果有 chosen_direction 就只選擇該方向
    for t in tlist:
        mat2 = []
        # for dirc in four_axes:  # 四軸, ["E", "W", "N", "S"]
        for (
            dirc
        ) in (
            directions
        ):  # 如果有 chosen_direction 就只選擇該方向
            df = strt_dict[dirc][
                strt_dict[dirc].index.get_level_values("time")
                == t
            ]
            df = df.sort_index()

            y = np.array(df.iloc[:, 0].values)
            try:
                cs = CubicSpline(x, y)
            except ValueError:
                if x.shape[0] > y.shape[0]:
                    x = x[: y.shape[0]]
                try:
                    cs = CubicSpline(x, y)
                except ValueError as e:
                    raise ValueError(
                        "{}, {}: {} / {} / {}".format(
                            t,
                            dirc,
                            x.shape,
                            y.shape,
                            df.shape,
                        )
                    ) from e

            mat2.append(
                cs(dist)
            )  # 以 Cubic Spline 計算 dist 距離的水位
        mat.append(mat2)  # 計算四個方向的水位
    return np.array(mat)


def inter_dist_h(
    *arg, chosen_direction=None, **_kwargs
) -> np.ndarray:
    """
    計算環繞 WEL 或 CHD 一定距離的平均水位值
    return 特定距離下隨時間而變的水位 np.ndarray
    """
    h_around = grep_dist_h(
        *arg, chosen_direction=chosen_direction
    )
    results = h_around.mean(axis=1)
    return results


def determine_variable_names(
    argv_params, distance_list
) -> Tuple:
    columns_in = []
    columns_out = []
    var_size_in = 0
    var_size_out = 0

    # 檢查 argv_params 是否有 chosen_direction，如果沒有設定，則 chosen_direction 為 None
    chosen_direction = argv_params.get("chosen_direction")
    directions = (
        [chosen_direction] if chosen_direction else four_axes
    )

    for var_name in argv_params["ann_setup"]["input_vars"]:
        if var_name in ["drawdown", "h", "levlegap"]:
            # for direc in four_axes:
            for direc in directions:
                for distance in distance_list:
                    for tt_shift in range(
                        argv_params["timetracking"], 0, -1
                    ):
                        columns_in.append(
                            "{}_{}_{}_{}".format(
                                var_name,
                                direc,
                                int(distance),
                                -(tt_shift - 1),
                            )
                        )
                        var_size_in += 1
        elif var_name in ["B", "wel_qlist"]:
            columns_in.append("{}_t".format(var_name))
            var_size_in += 1
        else:
            # K, Sy, T, S
            columns_in.append(var_name)
            var_size_in += 1

    for var_name in argv_params["ann_setup"]["output_vars"]:
        if var_name in ["drawdown", "h", "levlegap"]:
            # for direc in four_axes:
            for direc in directions:
                for distance in distance_list:
                    columns_out.append(
                        "{}_{}_{}_{}".format(
                            var_name,
                            direc,
                            int(distance),
                            1,
                        )
                    )
                    var_size_out += 1
        elif var_name in ["B"]:
            columns_out.append("{}_t+1".format(var_name))
            var_size_out += 1
    return columns_in, columns_out, var_size_in, var_size_out


def dictdata2df_core(flag: List) -> Tuple:
    """
    從 mf 模擬案例,
    """
    (case_index, _mf_size, elem, argv_params) = tuple(flag)
    ann_setup = argv_params["ann_setup"]
    chosen_direction = argv_params.get("chosen_direction")
    # p = current_process()
    # index2 = p._identity[0] - 1
    # if index % 100 == 0:
    #    # 平行處理, 只輸出 pid == 0 者
    #    print("# {}/{}".format(index, mf_size))

    tlist = np.sort(
        np.array(
            list(
                set(
                    elem["strt"]["E"].index.get_level_values(
                        "time"
                    )
                )
            )
        )
    )
    distance_list = np.array(view_distance.view_distance)
    df = elem["strt"]["E"]
    df = df[
        df.index.get_level_values("distance") == distance_list[0]
    ]

    # 依據 ANN setup 內容, 定義變數名稱
    columns_in, columns_out, var_size_in, var_size_out = (
        determine_variable_names(argv_params, distance_list)
    )
    print("columns_in:", columns_in)
    print("columns_out:", columns_out)
    print("columns_in_len:", len(columns_in))
    print("columns_out_len:", len(columns_out))

    def filter_multiindex(
        df_multiindex: pd.DataFrame,
        index_name: str,
        var: Union[float, int],
    ) -> pd.DataFrame:
        """
        輸入 multiindex 的 DataFrame
        給予篩選條件
            1. 給予挑選欄位 index_name 與 數值, 進行篩選
            2. 刪除挑選完的 index
            3. 針對殘餘 index 排序
        """
        df = df_multiindex[
            df_multiindex.index.get_level_values(index_name)
            == var
        ]
        df.index = df.index.droplevel(index_name)
        df = df.sort_index()
        return df

    def levelbased_variable(var_name: str) -> Tuple:
        """
        回傳
            是否為水位類型的變數, bool
            第幾種, int
                0: h
                1: drawdown
                2: levelgap
        """
        for i, flag in enumerate(["h", "drawdown", "levelgap"]):
            if var_name.find(flag) >= 0:
                return True, i
        return False, -1

    def determine_mf_data(
        elem: Dict,
        columns: List,
        mat: np.ndarray,
        chosen_direction=None,
    ) -> np.ndarray:
        directions = (
            [chosen_direction] if chosen_direction else four_axes
        )
        """
        資料擷取 (input & output 共用函數)
        1. Kh, Sy, T, S 非時間相關參數
        2. 時間相關參數
            wel_qlist
            B
            h, levelgap, drawdown
        """
        count_index = -1
        for var_name in columns:
            count_index += 1
            if var_name in ["Kh", "Sy", "T", "S"]:
                # K, Sy, T & S 等與時間無關的參數
                try:
                    mat_in[:, count_index] = (
                        np.ones(mat_in.shape[0]) * elem[var_name]
                    )
                except KeyError:
                    # 處理 T, S
                    B = 50
                    if var_name in ["T"]:
                        mat_in[:, count_index] = (
                            np.ones(mat_in.shape[0]) * elem["Kh"]
                        ) * B
                    elif var_name in ["S"]:
                        mat_in[:, count_index] = (
                            np.ones(mat_in.shape[0]) * elem["Ss"]
                        ) * B
            else:
                # 時間性參數
                # wel_qlist, B, h, drawdown levelgap
                ############################################
                # h, drawdown, levelgap
                # 0, h 代表水位
                # 1, drawdown 代表洩降, 為 h - 無限遠處水位
                # 2, levelgap 代表水位差, 為輸入一基準水位, h - 基準水位
                var_check_result = levelbased_variable(var_name)

                sub_mat = np.array([])
                if var_check_result[0]:
                    # 地下水位類型的變數 h, drawdown, levelgap
                    direc_name = var_name.split("_")[1]
                    distance = int(var_name.split("_")[2])
                    tt_shift = int(var_name.split("_")[3])
                    df = filter_multiindex(
                        elem["strt"][direc_name],
                        "distance",
                        distance,
                    )

                    if var_check_result[1] == 0:
                        # h
                        sub_mat = df.loc[
                            tt_shift + 1 :, "h"
                        ].values

                    elif var_check_result[1] == 1:
                        # drawdown
                        # 遠處水位
                        df2 = filter_multiindex(
                            elem["strt"][direc_name],
                            "distance",
                            distance_list[-1],
                        )
                        sub_mat = (
                            df.loc[:, "h"] - df2.loc[:, "h"]
                        ).values[tt_shift + 1 :]

                elif var_name.find("B") >= 0:
                    # B_t
                    tt_shift = 0
                    if var_name.find("t+1") >= 0:
                        # B_t+1
                        tt_shift = 1

                    # 含水層厚度
                    # 近程水位 - 含水層底部 (模擬預設為 -50 m)
                    h_inside = inter_dist_h(
                        elem["strt"],
                        tlist,
                        argv_params["dist_inside"],
                        var_name=var_name,
                    )
                    aquifer_thickness = h_inside + 50
                    sub_mat = aquifer_thickness[tt_shift + 1 :]

                elif var_name.find("wel_qlist") >= 0:
                    sub_mat = elem["wel_qlist"][
                        argv_params["timetracking"] - 1 :
                    ]

                try:
                    mat[:, count_index] = sub_mat[: mat.shape[0]]

                except ValueError as e:
                    raise ValueError(
                        "{}: {} / {} / {}".format(
                            var_name,
                            mat.shape,
                            sub_mat.shape,
                            tt_shift,
                        )
                    ) from e
        return mat

    mat_in = np.zeros(
        (
            tlist.shape[0]
            - argv_params["timetracking"]
            - 1,  # 改成減1, 排除每個案例, 第一個時刻, steady state
            var_size_in,
        )
    )
    mat_in = determine_mf_data(elem, columns_in, mat_in)

    mat_out = np.zeros(
        (
            tlist.shape[0]
            - argv_params["timetracking"]
            - 1,  # 改成減1, 排除每個案例, 第一個時刻, steady state
            var_size_out,
        )
    )
    mat_out = determine_mf_data(elem, columns_out, mat_out)

    # print("mat_out:", mat_out)

    df_in = pd.DataFrame(mat_in, columns=columns_in)
    df_out = pd.DataFrame(mat_out, columns=columns_out)
    # 欄位名稱依序為 case_index, tlist 與各項參數
    df_in.loc[:, "case_index"] = case_index
    df_out.loc[:, "case_index"] = case_index
    df_in.loc[:, "tlist"] = tlist[
        : -(argv_params["timetracking"] + 1)
    ]
    df_out.loc[:, "tlist"] = tlist[
        : -(argv_params["timetracking"] + 1)
    ]
    df_in = df_in.set_index(["case_index", "tlist"])
    df_out = df_out.set_index(["case_index", "tlist"])

    # print(columns_in, columns_out)
    # a = np.array(df_in.loc[:, ["B_t"]].values - df_out.loc[:, ["B_t+1"]].values)
    # print (a)
    # b = np.array(
    #    df_in.loc[:, ["drawdown_E_40_0"]].values
    #    - df_out.loc[:, ["drawdown_E_40_1"]].values
    # )
    # print (a)
    # print(b)
    # sys.exit()
    return df_in, df_out


def dictdata2df(
    mf_data_dict: Dict, argv_params: Dict
) -> pd.DataFrame:
    # mf_keys = list(set(mf_data_dict.keys()))
    mf_keys = sorted(mf_data_dict.keys())  ###浩安0303改

    flags = []

    # for index, key in enumerate(mf_keys):
    for key in mf_keys:  ###浩安0303改
        flags.append(
            [
                # index,
                key,  ###浩安0303改
                len(mf_keys),
                mf_data_dict[key],
                # argv_params["ann_setup"],
                argv_params,
            ]
        )

    # 平行處理
    mat = parallel_framework.parallel_process_framework(
        dictdata2df_core,
        flags,
        log_parallel=log_parallel,
        processor_ratio=0.45,
    )

    df_in_all = pd.DataFrame([])
    df_out_all = pd.DataFrame([])
    for elem in mat:
        # 循序把不同案例的數據串成一個大的DataFrame
        df_in_all = pd.concat([df_in_all, elem[0]], axis=0)
        df_out_all = pd.concat([df_out_all, elem[1]], axis=0)

    ##################################################################
    # 在串接後對 DataFrame 依據 "case_index" 進行排序
    df_in_all.sort_index(level="case_index", inplace=True)
    df_out_all.sort_index(level="case_index", inplace=True)
    ##################################################################浩安0303改

    return df_in_all, df_out_all


if __name__ == "__main__":
    # load data from pickle
    X_fname = "{}_X.csv".format(
        os.path.join(
            argv_params["export_path"],
            argv_params["export_fname"],
        )
    )
    Y_fname = "{}_Y.csv".format(
        os.path.join(
            argv_params["export_path"],
            argv_params["export_fname"],
        )
    )

    if (not os.path.exists(X_fname)) or (
        not os.path.exists(Y_fname)
    ):
        mf_data_dict = load_mf_data(argv_params)

        ################################################################################################################
        # 篩選出 bg_type 為 0 的案例 0225
        mf_data_dict = {
            key: case
            for key, case in mf_data_dict.items()
            if case.get("bg_type") == 0
        }

        # 檢查是否全部案例都符合 bg_type 為 0
        if all(
            case.get("bg_type") == 0
            for case in mf_data_dict.values()
        ):
            print("所有案例皆為 bg_type == 0")
        else:
            print("有案例不符合 bg_type == 0")

            # 這裡存成新的 pickle 檔
        filtered_pickle_path = os.path.join(
            argv_params["export_path"],
            "filtered_bg_type0.pickle",
        )
        with open(filtered_pickle_path, "wb") as f:
            pickle.dump(mf_data_dict, f)
        print(f"已將過濾後的案例存成 {filtered_pickle_path}")
        ################################################################################################################

        # from data_dict to dataframe
        df_in, df_out = dictdata2df(mf_data_dict, argv_params)
        print("data size:", df_in.shape, df_out.shape)

        # export to csv
        df_in.to_csv(X_fname)
        df_out.to_csv(Y_fname)
