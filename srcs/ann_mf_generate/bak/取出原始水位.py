# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 17:12:31 2024

@author: andy
"""

import os
import sys
import numpy as np
from scipy import stats
import pandas as pd
import random
import math
import matplotlib.pyplot as plt
import pickle

fname = "mf_wel_confined.pickle"
data = {}
with open(fname, "rb") as f:
    data = pickle.load(f)

# for case_number, case_data in data.items():
#   print(f"Case {case_number} - Kh value: {case_data['Kh']}")

print(data.keys())
print(data[0].keys())

print(data[0]["wel_type"])
print(data[0]["wel_qlist"])
print(data[0]["IC"])

df = data[968]["strt"]["E"]
df1 = pd.DataFrame(df)
df_unstacked = df.unstack(level="time")
print(df_unstacked)
print(df1)
print("----------------")

with pd.ExcelWriter("output.xlsx") as writer:
    df1.to_excel(writer, sheet_name="Sheet1")
    df_unstacked.to_excel(writer, sheet_name="Sheet2")

folder_name = "pumping_plots"
os.makedirs(folder_name, exist_ok=True)
