# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 11:02:01 2024

@author: andy
"""

import pandas as pd
import matplotlib.pyplot as plt

# 讀取Excel文件的第二個工作表
file_path = r"D:\andy_test\小教學code\pickle\output.xlsx"
sheet_name = 1  # 第二個工作表，索引從0開始

# 使用pandas讀取Excel文件
df = pd.read_excel(file_path, sheet_name=sheet_name)

# 將第一行作為列標題
df.columns = df.iloc[0]
df = df.drop([0, 1]).reset_index(drop=True)

# 獲取時間數據
time = df.columns[1:].astype(float)

# 繪製每個distance的數據
plt.figure(figsize=(25, 8))

for i in range(len(df)):
    distance = df.iloc[i, 0]  # 獲取distance值
    values = df.iloc[i, 1:].astype(float)  # 獲取對應的數值
    plt.plot(time, values, label=f"Distance {distance} m")

plt.xlabel("Time")
plt.ylabel("Water level")
plt.title("Pumping type 5")
plt.legend()
plt.grid(True)

# 保存圖表為PNG文件
output_path = "Pumping type 5.png"
plt.savefig(output_path)
plt.show()
