"""
分析 ANN 訓練案例之關聯性
"""

import os
import sys
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jutility as jut
import plt_parameters


if __name__ == "__main__":
    """
    data_dict = {}
    with open(sys.argv[1], "rb") as f:
        data_dict = pickle.load(f)
    print(data_dict.keys())

    output_dir = "mf_wel_unconfined_V4"
    os.makedirs(output_dir, exist_ok=True)



    mat = []
    case_ids = []

    # 篩選 wel_type = 0, 1, 2, 3 的案例
    selected_wel_types = [0, 1, 2, 3]
    wel_type_count = {wt: 0 for wt in selected_wel_types}  # 記錄每種 wel_type 的數量

    #for key, elem in data_dict.items():
    for case_id, (key, elem) in enumerate(data_dict.items()):
        #if (elem["wel_type"] == 0) and (elem["bg_type"] == 0):
        #if elem["wel_type"] in range(6) and elem["bg_type"] in range(4): ########讀取每個分組
        if elem["wel_type"] in selected_wel_types and elem["bg_type"] in range(4):  # 篩選條件
            wel_type_count[elem["wel_type"]] += 1  # 統計每種 wel_type 的數量

            mat_sub = []
            for dirc in ["S"]:
                df = elem["strt"][dirc]
                df = df[df.index.get_level_values("time") == 239]
                df = df.sort_index()
                for r in [20, 40, 60, 80, 100]:
                    df2 = df[
                        df.index.get_level_values("distance")
                        == r
                    ]
                    mat_sub.append(df2.iloc[0, 0])
            mat.append(
                [case_id] +
                [
                    elem[flag]
                    for flag in ["Kh", "Sy", "Ss"]
                ] + [elem["IC"][0], elem["wel_qlist"][26]] + mat_sub
                + [elem["bg_type"], elem["wel_type"]]
            )

    df = pd.DataFrame(mat, columns=["Case_ID", "Kh", "Sy", "Ss", "IC", "Q", "h20", "h40", "h60", "h80", "h100","bg_type","wel_type"])
    for r in [20, 40, 60, 80, 100]:
        df["d"+str(r)] = df["IC"] - df["h"+str(r)]

    # 輸出每種 wel_type 的數量
    print("\n每種 wel_type 的案例數量：")
    for wel_type, count in wel_type_count.items():
        print(f"wel_type={wel_type}: {count} 組案例")

    # 計算每組 wel_type 中抽水量值（Q）的重複性
    print("\n每種 wel_type 的抽水量值重複檢查：")
    grouped = df.groupby("wel_type")
    for wel_type, group in grouped:
        # 針對 wel_type=2 和 wel_type=3，只提取每個案例中 Q 為負值的數據
        if wel_type in [2, 3]:
            negative_q_group = group[group["Q"] < 0]  # 提取 Q < 0 的數據

            q_values = list(negative_q_group["Q"])  # 取出 Q 為負值的數據
        else:
            q_values = list(group["Q"])  # 其他 wel_type，直接取全部 Q 值

        unique_q_values = list(set(q_values))  # 去重後的 Q 值
        print(f"wel_type={wel_type}:")
        print(f"  總案例數: {len(q_values)}")
        print(f"  唯一 Q 值數量: {len(unique_q_values)}")
        if len(q_values) > len(unique_q_values):
            print("  有重複的抽水量值")
        else:
            print("  沒有重複的抽水量值")



    # 加入相關性分組分析
    ########################################################################################
    # grouped = df.groupby(["bg_type", "wel_type"])
    # for (bg_type, wel_type), group in grouped:
    #     count = group.shape[0]  # 計算每組的個數
    #     print(f"分析組合: bg_type={bg_type}, wel_type={wel_type}, 個數={count}")
    #     print(group[["Kh", "Sy", "Q", "d20"]].describe())

    #     # 計算相關性矩陣
    #     corr_matrix = group[["Kh", "Sy", "Q", "d20"]].corr()
    #     print("參數間的相關性矩陣：")
    #     print(corr_matrix)

    #     # 繪製熱圖
    #     plt.figure(figsize=(8, 6))
    #     plt.title(f"Correlation Heatmap: bg_type={bg_type}, wel_type={wel_type}")
    #     sns.heatmap(corr_matrix, annot=True, cmap="coolwarm", fmt=".2f")
    #     heatmap_name = f"correlation_bg{bg_type}_wel{wel_type}.png"
    #     plt.savefig(os.path.join(output_dir, heatmap_name))
    #     plt.clf()
    #############################################################################################
    ax = plt.figure(figsize=(12, 9)).add_subplot(projection='3d')
    ax.scatter(
        df.loc[:, "Kh"],
        df.loc[:, "Sy"],
        df.loc[:, "d20"],
    )
    ax.set_xlabel(r"$K_h$")
    ax.set_ylabel(r"$S_y$")
    ax.set_zlabel(r"Drawdown $m$")
    jut.save_fig(os.path.join(output_dir, "data_corr02"))

    #############################################################################################
    # 深入分析抽水量（Q）
    # 抽水量的統計描述

    # q_stats = df['Q'].describe()
    # print("抽水量（Q）的統計描述：")
    # print(q_stats)

    """
    # 讀取 CSV 文件
    csv_file_path = (
        "df_variable.csv"  # 請更換為實際 CSV 檔案路徑
    )
    if os.path.exists(csv_file_path):
        csv_data = pd.read_csv(csv_file_path)
        print("成功讀取 csv 文件！")

        # 篩選 wel_type 是 0, 1, 2, 3 的案例
        selected_wel_types = [0, 1, 2, 3]
        if "wel_type" in csv_data.columns:
            filtered_data = csv_data[
                csv_data["wel_type"].isin(selected_wel_types)
            ]

            # 計算每種 wel_type 的案例數量
            wel_type_count = (
                filtered_data["wel_type"]
                .value_counts()
                .sort_index()
            )
            print("\n每種 wel_type 的案例數量：")
            for wel_type, count in wel_type_count.items():
                print(f"wel_type={wel_type}: {count} 組案例")

            # 找出 step 欄位
            step_columns = [
                col
                for col in csv_data.columns
                if col.startswith("step")
            ]

            if step_columns:
                grouped = filtered_data.groupby("wel_type")
                print("\n每種 wel_type 的抽水量值檢查：")
                for wel_type, group in grouped:
                    # 提取所有 step 欄位的數據，排除 Q = 0 的值
                    q_values = group[
                        step_columns
                    ].values.flatten()  # 將所有 step 欄位數據展平成一維數組
                    q_values = q_values[
                        q_values != 0
                    ]  # 排除 0 的值

                    if wel_type in [0, 1]:
                        # 針對 type=0,1 的情況，提取唯一的非零值
                        unique_q_values = list(set(q_values))
                    elif wel_type in [2, 3]:
                        # 針對 type=2,3 的情況，提取唯一的負值
                        negative_q_values = [
                            q for q in q_values if q < 0
                        ]
                        unique_q_values = list(
                            set(negative_q_values)
                        )
                    else:
                        continue

                    # 比較案例數量與唯一值數量
                    case_count = group.shape[0]
                    unique_count = len(unique_q_values)
                    print(f"wel_type={wel_type}:")
                    print(f"  總案例數量: {case_count}")
                    print(f"  唯一 Q 值數量: {unique_count}")
                    if unique_count == case_count:
                        print("  唯一 Q 值數量與案例數量一致")
                    else:
                        print("  唯一 Q 值數量與案例數量不一致")
            else:
                print(
                    "找不到以 'step' 開頭的欄位，請檢查 CSV 文件。"
                )
