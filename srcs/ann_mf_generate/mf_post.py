# -*- coding: utf-8 -*-
"""
1. data load from pickle file 
    include MF simulation result
2. plot pumping data and groundwater table
"""

import os
import sys
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
import argv_analysis
import matplotlib.ticker as ticker
from matplotlib.ticker import FuncFormatter
import numpy as np


# Set font properties to support minus sign
plt.rcParams["font.sans-serif"] = ["SimHei"]  # 用黑體顯示中文
plt.rcParams["axes.unicode_minus"] = False  # 確保負號顯示正常

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jutility as jut


# 自定義格式化函數，顯示精確的Y軸數值
def precise_format(x, pos):
    return "{:.1f}".format(x)


if __name__ == "__main__":
    argv_params = argv_analysis.argument_analysis(sys.argv[1:])
    print(argv_params)

    pickle_fname = os.path.join(
        argv_params["export_path"],
        argv_params["pickle_fname"],
    )
    results = {}
    with open(pickle_fname, "rb") as f:
        results = pickle.load(f)
    for key, elem in results.items():

        if key < 999:
            _fig, ax = plt.subplots(
                12, 1, figsize=(12, 15), sharex=True
            )
            tlist = list(
                set(
                    elem["strt"]["E"].index.get_level_values(
                        "time"
                    )
                )
            )
            df_q = pd.DataFrame(
                elem["wel_qlist"], columns=["Q"], index=tlist
            )

            df_q.plot.bar(ax=ax[0], grid=True)
            ax[0].set_title("Pumpage")
            ax[0].set_ylabel(r"抽水量 $CMH$")
            # 設置第一個子圖的X軸刻度
            ax[0].xaxis.set_major_locator(
                ticker.MultipleLocator(5)
            )
            # 設置Y軸顯示更精確的數值
            ax[0].yaxis.set_major_formatter(
                FuncFormatter(precise_format)
            )
            # 設置 Y 軸刻度為具體的抽水量數值範圍
            y_min = df_q["Q"].min()  # 抽水量的最小值
            y_max = df_q["Q"].max()  # 抽水量的最大值
            ax[0].set_ylim([y_min, y_max])  # 設置Y軸範圍
            yticks = np.linspace(
                y_min, y_max, num=5
            )  # 生成Y軸刻度
            ax[0].set_yticks(yticks)  # 設置具體的Y軸刻度
            ax[0].set_yticklabels(
                ["{:.1f}".format(y) for y in yticks]
            )  # 顯示具體數值

            # 20, 40, 60, 80, 300, 900, 1600, 2000,
            distance_param = {
                1: 20,
                2: 40,
                3: 60,
                4: 80,
                5: 220,
                6: 300,
                7: 400,
                8: 700,
                9: 1000,
                10: 1600,
                11: 2000,
            }
            for ax_index, distance in distance_param.items():
                legend = []
                for dir in ["E", "W", "S", "N"]:
                    df2 = elem["strt"][dir][
                        elem["strt"][dir].index.get_level_values(
                            "distance"
                        )
                        == distance
                    ]
                    df2.index = df2.index.droplevel("distance")
                    df2 = df2.sort_index()
                    df2.plot(
                        ax=ax[ax_index],
                        grid=True,
                        linestyle="--",
                        alpha=0.6,
                    )
                    legend.append(dir)
                ax[ax_index].set_title(
                    r"Distance$={}$".format(distance)
                )
                ax[ax_index].legend(legend, shadow=True)
                ax[ax_index].set_ylabel(r"水位 $m$")
                # 設置X軸刻度間隔，每5個時刻顯示一次
                ax[ax_index].xaxis.set_major_locator(
                    ticker.MultipleLocator(5)
                )

            # 強制所有子圖顯示X軸刻度標籤
            for a in ax:
                a.tick_params(labelbottom=True)

            ax[0].set_xlim([None, 240])
            jut.save_fig(
                os.path.join(
                    argv_params["export_path"],
                    "mf_pics_verybig抽水",
                    "mf_result_{}".format(key),
                )
            )

            # 根據 bg_type 和 wel_type 建立資料夾
            bg_type = elem.get("bg_type", "unknown")
            wel_type = elem.get("wel_type", "unknown")

            # 建立對應的資料夾
            group_dir = os.path.join(
                argv_params["export_path"],
                "mf_pics_verybig抽水",
                f"bg_type_{bg_type}_wel_type_{wel_type}",
            )
            os.makedirs(group_dir, exist_ok=True)

            # 構建圖片檔名
            save_path = os.path.join(
                group_dir, f"mf_result_{key}.png"
            )

            # 儲存圖片
            plt.tight_layout()  # 調整佈局，避免裁切
            plt.savefig(save_path)  # 儲存圖片
            print(f"圖片已保存至 {save_path}")

            # 清除畫布，準備繪製下一張圖片
            plt.close(_fig)
