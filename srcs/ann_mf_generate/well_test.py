import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime

from sklearn.metrics import mean_squared_error

# Argument Analysis
import argv_analysis
import view_distance  # 觀測點距離


sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
# import parallel_framework
import plt_parameters
import well_function.well_subroutine as ws
import jutility as jut


if __name__ == "__main__":
    argv_params = argv_analysis.argument_analysis(sys.argv[1:])
    print(argv_params)
    dt_str = datetime.datetime(2024, 12, 1, 0, 0, 0, 0)
    # 提取時間和抽水量
    mat = []
    for t in range(238):
        q = 0
        if t >= 24:
            q = -197
        mat.append([dt_str + datetime.timedelta(hours=t), q])

    # mat = []
    # for t in range(238):
    #     # 計算是第幾天（從 0 開始）
    #     day_index = t // 24
    #     # 計算當天的第幾個小時
    #     hour_in_day = t % 24

    #     # 預設值
    #     q = 0

    #     if day_index == 0:
    #         # 第 0 天（小時 0～23）全部 0，不做任何改動
    #         q = 0
    #     else:
    #         # 從第 1 天開始，採用 15 小時抽水、9 小時不抽水的模式
    #         if hour_in_day < 15:
    #             q = 189.2536
    #         else:
    #             q = 0

    #     mat.append([
    #         dt_str + datetime.timedelta(hours=t),
    #         q
    #     ])

    Qchange = pd.DataFrame(mat, columns=["tlist", "Q"])
    Qchange = Qchange.set_index("tlist")
    S = 0.26
    T = 153.4

    # 日期範圍與頻率設定
    str_dt = Qchange.index.min()  # 起始日期
    end_dt = Qchange.index.max()  # 結束日期
    freq = "H"  # 設定頻率，例如小時

    results = {}
    # 使用 calc_drawdown_vari 計算洩降量
    drawdown_df = ws.calc_drawdown_vari(
        str_dt=str_dt.strftime("%Y-%m-%d %H:%M:%S"),
        end_dt=end_dt.strftime("%Y-%m-%d %H:%M:%S"),
        freq=freq,
        Qchange=Qchange["Q"].values,
        T=T,
        S=S,
        r=np.array(view_distance.view_distance),
    )
    for i, r in enumerate(view_distance.view_distance):
        col = "drawdown"
        if i != 0:
            col = "drawdown{}".format(i + 1)
        drawdown_df = drawdown_df.rename(
            columns={col: "drawdown_{}".format(int(r))}
        )
    print(drawdown_df)

    csv_fname1 = "wel_data_1_X.csv"
    df = pd.read_csv(csv_fname1)
    df = df[df["case_index"] == 0]
    df = df.set_index("tlist")
    df = df.sort_index()
    df.index = [
        dt_str + datetime.timedelta(hours=t)
        for t in range(df.shape[0])
    ]
    df = df.drop(columns=["case_index"])

    csv_fname2 = "moldflow.csv"
    df2 = pd.read_csv(csv_fname2)
    df2 = df2.sort_values("Time")
    df2.index = [
        dt_str + datetime.timedelta(hours=t) for t in df2["Time"]
    ]
    df2 = df2.sort_index()

    for i, r in enumerate(view_distance.view_distance):
        _fig, ax = plt.subplots(1, figsize=(12, 9))
        # 1. 畫 Theis 解
        drawdown_df.loc[:, "drawdown_{}".format(int(r))].plot(
            ax=ax, grid=True
        )

        # 2. 畫第一個 CSV (wel_data_1_X.csv) 的 E、W、N、S 四條線
        for flag in ["E", "W", "N", "S"]:
            df.loc[:, "drawdown_{}_{}_0".format(flag, r)].plot(
                ax=ax, grid=True, linestyle="--"
            )

        # 3. 繪製 moldflow.csv (df2) 的資料 (假設欄位叫做 "Head")
        if r in [
            20,
            40,
            60,
            80,
            100,
            140,
            180,
            220,
            260,
            300,
            400,
            500,
        ]:
            df2[f"Head_{int(r)}"].plot(
                ax=ax,
                grid=True,
                linestyle="-.",
                color="magenta",
                label="Moldflow HEAD",
            )

        ax.legend(
            [
                "Well Function",
                "MF E",
                "MF W",
                "MF N",
                "MF S",
                "Moldflow HEAD",
            ]
        )
        ax.set_title(r"$r={}\; m$".format(r))
        jut.save_fig(
            os.path.join(
                argv_params["export_path"],
                "drawdown_{}".format(r),
            )
        )
