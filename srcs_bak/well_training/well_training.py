# -*- coding: utf-8 -*-
"""
<抽水模組數值試驗>(拘限/非拘限)
變數: 抽水量、K值、S值
範圍: 2010m*2010m
單位網格: 10m*10m
初始水位: 隨機
厚度：    隨機
模擬時刻: hr小時
抽水井位置: 左下
定水頭: 上,右邊界
No-flow: 下,左邊界
觀測點: 每個50m 觀測一次
K: [0.1~100](m/day)
S: [0.0015~0.03](m/day)
Ss:[S/delv](1/m)
Sy:(1/m)
抽水量:[-3000,0](m/day)
抽水型態: type0 & type1
模擬次數: 5
"""
import os
import sys
import numpy as np
from scipy import stats
import pandas as pd
import flopy
import random
import math
import matplotlib.pyplot as plt
import seaborn as sns
from typing import Tuple, List
import matplotlib.pyplot as plt
from multiprocessing import Pool, current_process
from cpuinfo import get_cpu_info
import pickle
import datetime
import seaborn as sns
import plotly.express as px


sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jutility as jut

# import plt_parameters  noqa: F401
import jlib_logging
import flopy.utils.binaryfile as bf
import parallel_framework

sys.path.append(
    os.path.join("..", "..", "mf_process", "srcs", "mf_utility")
)
import mf_execute
import mf_utility


def mf_create_shell(flag):
    return mf_create(*flag[0], **flag[1])


def mf_create(*arg, **kwargs):
    """
    產生 MF 案例
    """

    root_logger = kwargs.get("root_logger", None)

    (
        nper,
        nlay,
        nrow,
        ncol,
    ) = kwargs[
        "dis_params"
    ][0]

    (
        delr,
        delc,
    ) = kwargs[
        "dis_params"
    ][1]

    (
        ztop,
        _,  # 外面設定為 None, zbot 由 df_variable 定義
    ) = kwargs["dis_params"][2]

    base_ws = kwargs["base_ws"]
    ibound = kwargs["ibound"]
    name = kwargs["mf_name"]
    df_variable = kwargs["df_variable"]
    case_name = kwargs["case_name"]

    (index,) = arg

    case_name = "{}_pt{}".format(index, kwargs["pumping_type"])
    zbot = (
        ztop - df_variable.loc[index, "b"]
    )  # 由含水層厚度, 計算底層高程
    #########################################################################
    #########################################################################
    #########################################################################
    for i, var in enumerate(
        [ztop, zbot]
    ):  # enumerate 可以產出是第i個變量出問題
        # 斷言
        # 必須滿足條件
        # 程式可分為 debug mode & release mode
        try:
            assert isinstance(var, (float, np.float64))
        except AssertionError as e:
            message = (
                "Variable type not matched: {}. {} / {}".format(
                    i, type(var), var
                )
            )
            # debug, warning, error
            root_logger.error(message, exc_info=True)
            raise TypeError(message) from e
    #########################################################################
    #########################################################################
    #########################################################################

    workspace = os.path.join(base_ws, case_name)
    if root_logger is not None:
        root_logger.debug(
            "case name: {}, workspace: {}".format(
                case_name, workspace
            )
        )

    mf = flopy.modflow.Modflow(
        name, exe_name="mf2005", model_ws=workspace
    )

    # MF 單位採用 m/hr
    # DIS
    nstp = np.ones(nper)
    perlen = np.ones(nper)
    delv = (ztop - zbot) / nlay
    botm = np.linspace(ztop, zbot, nlay + 1)

    _dis = flopy.modflow.ModflowDis(
        mf,
        nlay,
        nrow,
        ncol,
        delr=delr,
        delc=delc,
        top=ztop,
        botm=botm[1:],
        nper=nper,
        perlen=perlen,  # 1 hr
        nstp=nstp,
        steady=False,
    )

    strt = df_variable.loc[index, "IC"]  # 初始水位
    # BAS
    _bas = flopy.modflow.ModflowBas(
        mf,
        ibound=ibound,  # 邊界
        strt=strt,  # 初始水位
        hnoflo=-999.0,  # 非活動區域
        stoper=0.001,  # 收斂標準值
    )

    # LPF
    hk = df_variable.loc[index, "K"]
    ss = df_variable.loc[index, "S"] / delv
    _lpf = flopy.modflow.ModflowLpf(
        mf,
        hk=hk,
        vka=hk,
        sy=df_variable.loc[index, "Sy"],
        ss=ss,
        laytyp=1,  # convertible
        ipakcb=53,
    )
    pcg = flopy.modflow.ModflowPcg(mf)

    # WEL
    well = np.array([])
    if kwargs["pumping_type"] in [0, 1, 2, 3, 4, 5, 6]:
        # 0: 定量抽水, 結束為止
        well = np.ones(nper) * df_variable.loc[index, "P"]

        if kwargs["pumping_type"] == 1:
            # 1: step function, 延時隨機
            for t in range(
                df_variable.loc[index, "Ptime"], nper
            ):
                well[t] = 0

        if kwargs["pumping_type"] == 2:
            # 2: 隨機定量, 抽2hr停2hr 再繼續
            for t in range(nper):
                if (t // 2) % 2 == 0:  # 每2小時切換一次抽水狀態
                    well[t] = df_variable.loc[index, "P"]
                else:
                    well[t] = 0

        if kwargs["pumping_type"] == 3:
            # 3: 隨機定量, 抽30hr停30hr 再繼續
            for t in range(nper):
                if (
                    t // 30
                ) % 2 == 0:  # 每30小時切換一次抽水狀態
                    well[t] = df_variable.loc[index, "P"]
                else:
                    well[t] = 0

        if kwargs["pumping_type"] == 4:
            # 4: 隨機定量, 抽6hr停6hr 再繼續
            for t in range(nper):
                if (t // 6) % 2 == 0:  # 每6小時切換一次抽水狀態
                    well[t] = df_variable.loc[index, "P"]
                else:
                    well[t] = 0

        if kwargs["pumping_type"] == 5:
            # 5: 隨機定量, 抽48hr停48hr 再繼續
            for t in range(nper):
                if (
                    t // 48
                ) % 2 == 0:  # 每48小時切換一次抽水狀態
                    well[t] = df_variable.loc[index, "P"]
                else:
                    well[t] = 0

        if kwargs["pumping_type"] == 6:
            for t in range(nper):
                pumping_time = random.randint(1, 101)
                if (
                    t % pumping_time == 0
                ):  # 每隔 pumping_time 小時抽水
                    well[t] = df_variable.loc[index, "P"]
                else:
                    well[t] = 0

    stress_period_data = {}

    for t in range(nper):
        # 抽水井設置
        # stress_period_data[t] = [0, nrow / 201 + 198 , ncol / 201 , well[t]]
        stress_period_data[t] = [0, nrow - 2, 1, well[t]]
    _wel = flopy.modflow.ModflowWel(
        mf, stress_period_data=stress_period_data
    )

    stress_period_data = {}
    for kper in range(nper):
        for kstp in range(int(nstp[kper])):
            stress_period_data[(kper, kstp)] = [
                "save head",
                "save drawdown",
                "save budget",
                "print head",
                "print budget",
            ]
    _oc = flopy.modflow.ModflowOc(
        mf,
        stress_period_data=stress_period_data,
        compact=True,
    )
    # Write the model input files
    if root_logger is not None:
        root_logger.debug(
            "Write MF model to {}".format(
                os.path.join(workspace, "{}.nam".format(name))
            )
        )
    mf.write_input()
    # MF running
    if root_logger is not None:
        root_logger.debug("Execute MF model")

    success = mf_execute.run_modflow_multiple_ways(
        mf,
        modelws=workspace,
        exec="mf_owhm_2_1",
        nam_fname=name,
    )
    if not success:
        if root_logger is not None:
            root_logger.warning("Model run failed.")

    # 取head資料
    hds_fname = os.path.join(workspace, "{}.hds".format(name))
    strt: List = mf_utility.read_hds(
        hds_fname, "ALL", mask_val=-999.0
    )
    # plot head - time
    # 要擷取的觀測井位置 index
    idxs = [
        (0, strt[0].shape[1] - 2, i)  # 讀取倒數第二行的觀測head
        for i in range(strt[0].shape[2])
    ]
    tindex = np.arange(len(strt))
    mat = []
    for t in tindex:
        mat_sub = [
            strt[t][
                idx[0], idx[1], idx[2]
            ]  # idx[0], idx[1], idx[2]
            for idx in idxs
        ]
        mat.append(mat_sub)

    df_strt = pd.DataFrame(
        mat, columns=[idx[2] for idx in idxs], index=tindex
    )

    # df 處理 -1e30 干掉網格的問題
    for col in df_strt.columns:
        df_strt.loc[:, col] = np.where(
            df_strt.loc[:, col].values < -1e29,
            np.NaN,
            df_strt.loc[:, col].values,
        )
    print(df_strt)

    if kwargs.get("log_plot", False):
        # 繪製 mf 輸出結果
        mf_plot_head(df_strt, case_name, workspace)
    return df_strt


def mf_plot_head(
    df_strt: pd.DataFrame, case_name: str, workspace
):
    """
    繪製地下水位模擬資訊
    1. 繪製每個時刻的水位, 繪製網格
        抽水井位置編號為 1
        每隔100公尺輸出1, 11 .21,....
    """
    for index, row in df_variable.iterrows():
        IC_value = row["IC"]
    # df_strt2 = df_strt - IC_value
    df_strt2 = df_strt

    plt.style.use("bmh")
    _fig, ax = plt.subplots(1, 2, figsize=(15, 15), sharey=True)
    legend = []
    # ax[0]
    # 繪製觀測點隨時間變化
    for index in range(
        1, np.max(df_strt2.columns) + 1, 10
    ):  # 每隔100公尺輸出1, 11 .21,....
        line_label = "抽水井"
        if index != 1:  # 當index不等於1執行以下
            line_label = r"${}\; m$".format(
                int((index - 1) * 10)
            )  # 10m 是網格大小，用來變成實際距離
        df_strt2.loc[:, index].plot(ax=ax[0], label=line_label)
        legend.append(line_label)
    ax[0].legend(legend, loc="lower right", shadow=True)
    ax[0].set_title("觀測點隨時間變化")
    ax[0].set_ylabel(r"洩降 $m$")
    ax[0].set_xlabel(r"時間 $hrs$")

    """
    1.取出類神經水位資料
    2.儲存 CSV 檔案到 Data 資料夾中
    3.轉置成類神經可用input, 行:模擬時間/列:觀測點
    """
    data_folder = os.path.join(base_ws, "Data_head")
    os.makedirs(data_folder, exist_ok=True)

    output_training_values = []
    for index in range(
        1, np.max(df_strt2.columns.tolist()) + 1, 10
    ):
        water_head = df_strt2.loc[:, index]
        output_training_values.append(water_head)

    df = pd.DataFrame(output_training_values)
    df = df.values
    data = list(map(list, zip(*df)))  # 轉置
    data = np.insert(data, 0, 69, axis=1)  # 新增第零行為抽水量

    data = pd.DataFrame(data)
    csv_path = os.path.join(
        data_folder, "{}_Well_training.csv".format(case_name)
    )
    data.to_csv(csv_path)

    data = pd.read_csv(
        os.path.join(
            data_folder, "{}_Well_training.csv".format(case_name)
        )
    )

    A = []  # 加入個案的K、Sy值到個案.csv檔案中最後2行
    for i in df_variable.index:
        b = df_variable.loc[i, "K"] * np.ones(
            (mf_params["DIS"]["nper"], 1)
        )
        A.append(b)

    for index, b in enumerate(A):
        data = np.column_stack((data, b))

    # print(df_variable.index)
    # print(df_variable.loc[:, "Sy"])

    # 計算每個觀測網格對抽水網格的距離
    # 繪製各時間的水位剖面
    loc_list = np.array(
        [
            int((index - 1) * 10)
            for index in range(1, np.max(df_strt2.columns) + 1)
        ]
    )

    legend2 = []
    for t in range(
        0, len(df_strt2.index), 100
    ):  # 從時間 0 開始，每隔 10 小時紀錄一次
        ax[1].plot(
            loc_list,
            np.array(
                df_strt2.loc[
                    df_strt2.index[t], df_strt.columns[1:]
                ].values
            ),
        )

        legend2.append(r"Time = ${}\; hr$".format(t))
    ax[1].legend(legend2, shadow=True)
    ax[1].set_title("各時刻之水位剖面")
    ax[1].set_xlabel(r"與抽水網格距離 $m$")
    plt.tight_layout()
    jut.save_fig(
        os.path.join(
            workspace, "{}_head_curve".format(case_name)
        )
    )


if __name__ == "__main__":
    program_name = "well_training"
    root_logger = jlib_logging.logger_setup(
        program_name,
        filename=os.path.join(
            "logging",
            program_name,
        ),
        log_append=False,
    )
    starttime = datetime.datetime.now()

    ########################################################
    ########################################################
    ########################################################
    mf_params = {
        "DIS": {
            "dis_range": [2010, 2010],  # 研究區域範圍
            "cell_size": [10, 10],  # 單一網格尺寸
            "nper": 1000,  # 時刻數
        },
        "BAS": {
            "IC": [-20, 30],  # 初始水位
        },
        "LPF": {
            "K": [0.1, 100],  # m/day
            "S": [0.0015, 0.03],  # m/day
            "Sy": [0.05, 0.3],  # m/day
            "b": [20, 50],  # 厚度 m
        },
        "WEL": [-3000, 0],  # m^3/day, CMD
        "others": {
            "test_size": 100,
        },
    }
    ########################################################
    ########################################################
    ########################################################

    # .info, .debug, .warning, .error, .critical
    root_logger.debug("mf_params: {}".format(mf_params))

    # 隨機產生
    def variable_generate(
        r, vrange: Tuple, loglog: bool = False
    ) -> float:

        assert isinstance(vrange, tuple)
        assert len(vrange) == 2
        vari = (vrange[1] - vrange[0]) * r + vrange[0]

        if loglog:
            # log scale
            vrange2 = np.log10(vrange)
            vari = np.power(
                10, (vrange2[1] - vrange2[0]) * r + vrange2[0]
            )
        return vari

    mat = []
    for i in range(mf_params["others"]["test_size"]):
        r = np.random.uniform(0, 1, 7)
        mat_sub = [
            variable_generate(
                r[0], tuple(mf_params["LPF"]["K"]), loglog=True
            ),
            variable_generate(
                r[1], tuple(mf_params["LPF"]["S"]), loglog=True
            ),
            variable_generate(
                r[2], tuple(mf_params["LPF"]["Sy"])
            ),
            variable_generate(r[3], tuple(mf_params["WEL"])),
            int(
                variable_generate(
                    r[4], (1, mf_params["DIS"]["nper"])
                )
            ),
            variable_generate(
                r[5], tuple(mf_params["LPF"]["b"])
            ),
            variable_generate(
                r[6], tuple(mf_params["BAS"]["IC"])
            ),
        ]
        mat.append(mat_sub)

    df_variable = pd.DataFrame(
        mat,
        columns=[
            "K",
            "S",
            "Sy",
            "P",  # 抽水量
            "Ptime",  # 抽水延時
            "b",  # 厚度
            "IC",  # 初始水位
        ],
    )

    df_variable.loc[:, "T"] = (
        df_variable.loc[:, "K"] * df_variable.loc[:, "b"]
    )
    root_logger.debug(df_variable)
    # df_variable.loc[:, "T"] = (
    #    df_variable.loc[:, "K"] * df_variable.loc[:, "b"]
    # )

    """
    1.判斷 Modflow 為 confined or unconfined   
    
    """
    ztop = 0
    confined = 0
    unconfined = 0
    for index, row in df_variable.iterrows():
        IC_value = row["IC"]
        if IC_value >= ztop:
            result = "confined"
            confined += 1
        else:
            result = "unconfined"
            unconfined += 1
        root_logger.debug(
            f"Case {index}: IC={IC_value}, ztop={ztop}, 地層類型：{result}"
        )

    """
    1.繪製散點圖矩陣
    2.使用變量 K、S、Sy、P、Ptime、b、IC

    """
    fig, ax = plt.subplots(1, figsize=(12, 9))
    sns.set_context("talk", font_scale=1.3)
    sns.color_palette("Spectral", as_cmap=True)
    df2 = df_variable
    df_variable["confined"] = confined
    df_variable["unconfined"] = unconfined
    sns.pairplot(
        df_variable.loc[
            :,
            [
                "K",
                "S",
                "Sy",
                "P",
                "Ptime",
                "b",
                "IC",
                "T",
                "confined",
                "unconfined",
            ],
        ],
        diag_kind="kde",
        vars=["K", "S", "Sy", "P", "b", "IC"],
        corner=True,
    )
    output_folder = r"pics"
    os.makedirs(output_folder, exist_ok=True)
    output_file = os.path.join(output_folder, "analyze.png")
    plt.savefig(output_file)

    base_ws = "well_training"
    name = "andy01_mf"
    Lx = mf_params["DIS"]["dis_range"][0]
    Ly = mf_params["DIS"]["dis_range"][1]
    nrow = int(math.floor(Ly / mf_params["DIS"]["cell_size"][1]))
    ncol = int(math.floor(Lx / mf_params["DIS"]["cell_size"][0]))
    nlay = 1
    nper = mf_params["DIS"]["nper"]
    root_logger.debug((nlay, nrow, ncol))
    # sys.exit()
    delr = mf_params["DIS"]["cell_size"][0]
    delc = mf_params["DIS"]["cell_size"][1]
    ztop = 0.0
    ibound = np.ones((nlay, nrow, ncol), dtype=np.int32)
    ibound[:, 0, :] = -1  # 上邊界側 Const. Head
    ibound[:, :, 0] = 0  # 左邊界側 No.flow
    ibound[:, :, -1] = -1  # 右邊界側 Const. Head
    ibound[:, -1, :] = 0  # 下邊界側 No.flow

    kwargs = {
        "base_ws": base_ws,
        "dis_params": [
            (nper, nlay, nrow, ncol),
            (delr, delc),
            (
                ztop,
                None,
            ),  # 將 zbot 改為 None, 由後續傳入 df_variable.loc[:, "b"] 定義 zbot
        ],
        "ibound": ibound,
        "mf_name": name,
        "df_variable": df_variable,
        "root_logger": root_logger,
        "log_plot": True,
    }

    flags = []
    for index in df_variable.index:
        arg = (index,)
        flags.append(
            [
                arg,
                {
                    "pumping_type": 0,
                    "log_plot": True,
                    "case_name": "{}_pt0".format(index),
                    **kwargs,
                },
            ]
        )

        flags.append(
            [
                arg,
                {
                    "pumping_type": 1,
                    "log_plot": True,
                    "case_name": "{}_pt1".format(index),
                    **kwargs,
                },
            ]
        )

        flags.append(
            [
                arg,
                {
                    "pumping_type": 2,
                    "log_plot": True,
                    "case_name": "{}_pt2".format(index),
                    **kwargs,
                },
            ]
        )

        flags.append(
            [
                arg,
                {
                    "pumping_type": 3,
                    "log_plot": True,
                    "case_name": "{}_pt3".format(index),
                    **kwargs,
                },
            ]
        )

        flags.append(
            [
                arg,
                {
                    "pumping_type": 4,
                    "log_plot": True,
                    "case_name": "{}_pt4".format(index),
                    **kwargs,
                },
            ]
        )

        flags.append(
            [
                arg,
                {
                    "pumping_type": 5,
                    "log_plot": True,
                    "case_name": "{}_pt5".format(index),
                    **kwargs,
                },
            ]
        )

        flags.append(
            [
                arg,
                {
                    "pumping_type": 6,
                    "log_plot": True,
                    "case_name": "{}_pt6".format(index),
                    **kwargs,
                },
            ]
        )

    mat = parallel_framework.parallel_process_framework(
        mf_create_shell,  # 函式
        flags,
        log_parallel=True,
        # log_debug=True,
        root_logger=root_logger,
    )
    result = {
        elem[1]["case_name"]: mat[i]
        for i, elem in enumerate(flags)
    }
    result["df_variable"] = df_variable
    print(result.keys())

    # https://clay-atlas.com/blog/2020/03/28/使用-pickle-模組保存-python-資料/
    # export pickle
    pickle_fname = "test.pickle"
    with open(pickle_fname, "wb") as f:
        pickle.dump(result, f)

    # load
    # with open(pickle_fname, "rb") as f:
    #    result2 = pickle.load(f)

    """
    index = [0]
    b = df_variable.loc[index[0], "K"] * np.ones((10,1))
    data = np.column_stack((data,b))
    """
