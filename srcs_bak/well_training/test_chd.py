# -*- coding: utf-8 -*-
"""
<定水頭模組數值試驗>(拘限/非拘限)
變數: 變動水頭量、K值、S值
範圍:4010m*4010m
單位網格:10m*10m
模擬時刻:10小時
抽水井位置:中心點
觀測點:中心定水頭位置每10m一個

T:30-1440(m/day)
S:0.0015-0.03
Ss:0.00005-0.001(1/m)
時變定水頭:0-3000(m/day)
時變型態

"""
import os
import sys
import numpy as np
from scipy import stats
import pandas as pd
import flopy
import random
import math
import matplotlib.pyplot as plt
from typing import Tuple, List
import matplotlib.pyplot as plt
from multiprocessing import Pool, current_process

# from cpuinfo import get_cpu_info
import pickle
import datetime
import seaborn as sns

sys.path.append(os.path.join("..", "..", "jlib", "srcs"))
import jutility as jut

# import plt_parameters  # noqa: F401
import jlib_logging
import flopy.utils.binaryfile as bf

sys.path.append(
    os.path.join("..", "..", "mf_process", "srcs", "mf_utility")
)
# import mf_utility
import mf_execute
import mf_utility


if __name__ == "__main__":
    program_name = "test_chd"
    root_logger = jlib_logging.logger_setup(
        program_name,
        filename=os.path.join(
            "logging",
            program_name,
        ),
        log_append=False,
    )
    starttime = datetime.datetime.now()

    mf_params = {
        "DIS": {
            "dis_range": [4010, 4010, 40],  # 研究區域範圍
            "cell_size": [10, 10],  # 單一網格尺寸
        },
        "BAS": {
            "IC": [40],
        },
        "LPF": {
            "T": [30, 1440],
            "S": [0.0015, 0.03],
            "Sy": [0.05, 0.3],
        },
    }
    # .info, .debug, .warning, .error, .critical
    root_logger.debug("mf_params: {}".format(mf_params))

    # 隨機產生
    # 變因：地下水深, Kh, Sy, 底泥 Kh, 流量, 河寬
    def variable_generate(
        r, vrange: Tuple, loglog: bool = False
    ) -> float:
        assert isinstance(vrange, tuple)
        assert len(vrange) == 2
        vari = (vrange[1] - vrange[0]) * r + vrange[0]

        if loglog:
            # log scale
            vrange2 = np.log10(vrange)
            vari = np.power(
                10, (vrange2[1] - vrange2[0]) * r + vrange2[0]
            )
        return vari

    mat = []
    for i in range(10):
        r = np.random.uniform(0, 1, 3)
        mat_sub = [
            variable_generate(
                r[0], tuple(mf_params["LPF"]["T"])
            ),
            variable_generate(
                r[1], tuple(mf_params["LPF"]["S"])
            ),
            variable_generate(
                r[2], tuple(mf_params["LPF"]["Sy"])
            ),
        ]
        mat.append(mat_sub)
    df_variable = pd.DataFrame(
        mat,
        columns=[
            "T",
            "S",
            "Sy",
        ],
    )
    root_logger.debug(df_variable)

    base_ws = "chdtest"
    Lx = mf_params["DIS"]["dis_range"][0]
    Ly = mf_params["DIS"]["dis_range"][1]
    nrow = int(math.floor(Ly / mf_params["DIS"]["cell_size"][1]))
    ncol = int(math.floor(Lx / mf_params["DIS"]["cell_size"][0]))
    nlay = 1
    root_logger.debug((nlay, nrow, ncol))
    # sys.exit()
    delr = mf_params["DIS"]["cell_size"][0]
    delc = mf_params["DIS"]["cell_size"][1]
    ztop = 0.0
    zbot = -mf_params["DIS"]["dis_range"][2]
    ibound = np.ones((nlay, nrow, ncol), dtype=np.int32)
    ibound[:, :, 0] = -1  # 左邊界側 No.flow
    ibound[:, -1, :] = -1  # 下邊界側 No.flow
    ibound[:, :, -1] = -1  # 右邊界側 Const. Head
    ibound[:, 0, :] = -1  # 上邊界側 Const. Head

    for index in df_variable.index:
        workspace = os.path.join(base_ws, str(index))
        name = "andy01_mf"
        mf = flopy.modflow.Modflow(
            name, exe_name="mf2005", model_ws=workspace
        )

        # MF 單位採用 m/hr
        # DIS
        nper = 10
        nstp = np.ones(nper)
        perlen = np.ones(nper)
        delv = (ztop - zbot) / nlay
        botm = np.linspace(ztop, zbot, nlay + 1)
        dis = flopy.modflow.ModflowDis(
            mf,
            nlay,
            nrow,
            ncol,
            delr=delr,
            delc=delc,
            top=ztop,
            botm=botm[1:],
            nper=nper,
            perlen=perlen,  # 1 hr
            nstp=nstp,
            steady=False,
        )

        # BAS
        strt = -mf_params["BAS"]["IC"][0] * np.ones(
            (nlay, nrow, ncol)
        )  # 初始水位
        bas = flopy.modflow.ModflowBas(
            mf,
            ibound=ibound,  # 邊界
            strt=strt,  # 初始水位
            hnoflo=-999.0,  # 非活動區域
            stoper=0.001,  # 收斂標準值
        )

        # LPF
        hk = df_variable.loc[index, "T"] / delv
        ss = df_variable.loc[index, "S"] / delv
        lpf = flopy.modflow.ModflowLpf(
            mf,
            hk=hk,
            vka=hk,
            sy=df_variable.loc[index, "Sy"],
            ss=ss,
            laytyp=0,
            ipakcb=53,
        )
        pcg = flopy.modflow.ModflowPcg(mf)
        """
        # WEL
        well = []
        
        #抽24補24
        for i in range(1):
            count = 0    
            a = []
            well.append(a)
            for i in range(1000):
                count = count + 1
                pumping = random.randrange(50,600)   
                if i ==0:
                    a.append(pumping*(-1))
                elif(count-1) %24 == 0:
                    a.append(a[-1] *(-1))
                else:
                    a.append(a[-1]) 
        
        
        #抽24停12補12(量一樣)
        for i in range(1):  
            a = []
            well.append(a)
            pumping = random.randrange(50,900)            
            for i in range(21):
                for k in range(24):
                    a.append(-pumping)
                for k in range(12):
                    a.append(0)    
            for k in range(12):
                    a.append(pumping)    
                    if len(a) >999:
                        break
                    a.append(0)
        
        #抽的時間不固定、量一樣
        for i in range(1):  
            a = []
            well.append(a)    
            for i in range(100):
                pumping = random.randrange(50,900)
                for k in range(random.randrange(0,30)):
                    if len(a) > 999:
                        break
                    a.append(-pumping)
                for k in range(random.randrange(5,80)):
                    if len(a) > 999:
                        break
                    a.append(0)

        well = pd.DataFrame(well)            
        well.to_csv('pump_1.csv')

        for i in range(len(well)): 
        #抽水井設置            
            stress_period_data ={}
            w = well[i]
            for k in range(len(w)):
                stress_period_data[k] = [0, nrow / 201 + 198 , ncol / 201 , w[k]]
        
        wel = flopy.modflow.ModflowWel(mf, stress_period_data=stress_period_data)
        """
        # 時變CHD設置

        stress_period_data = {}
        for kper in range(nper):
            for kstp in range(int(nstp[kper])):
                stress_period_data[(kper, kstp)] = [
                    "save head",
                    "save drawdown",
                    "save budget",
                    "print head",
                    "print budget",
                ]
        oc = flopy.modflow.ModflowOc(
            mf,
            stress_period_data=stress_period_data,
            # extension=["oc", "hds", "cbc"],
            # unitnumber=[14, 51, 53],
            compact=True,
        )
        # Write the model input files
        print(
            "Write MF model",
            os.path.join(workspace, "{}.nam".format(name)),
        )
        mf.write_input()
        # MF running
        print("Execute MF model")
        success = mf_execute.run_modflow_multiple_ways(
            mf,
            modelws=workspace,
            exec="mf_owhm_2_1",
            nam_fname=name,
        )
        if not success:
            print("Model run failed.")

        # 取head資料
        # Post Analysis
        """
        hds_fname = os.path.join(
            workspace, "{}.hds".format(name)
        )
        """
        """
        #plot head - time    
        idx = []
        idx.append((0, int(nrow / 201 + 199), int(ncol / 201)))
        idx.append((0, int(nrow / 201 + 199), int(ncol / 201)+10))
        idx.append((0, int(nrow / 201 + 199), int(ncol / 201)+20))
        idx.append((0, int(nrow / 201 + 199), int(ncol / 201)+30))
        idx.append((0, int(nrow / 201 + 199), int(ncol / 201)+40))
        idx.append((0, int(nrow / 201 + 199), int(ncol / 201)+50))
        idx.append((0, int(nrow / 201 + 199), int(ncol / 201)+60))
        idx.append((0, int(nrow / 201 + 199), int(ncol / 201)+70))
        idx.append((0, int(nrow / 201 + 199), int(ncol / 201)+80))
        idx.append((0, int(nrow / 201 + 199), int(ncol / 201)+90))
        
        ts = []
        for m in range(10):
            head = hds_fname(idx[m])
            ts.append(head[:,1])
            one = ts[0]
        if min(one) < 0:
            print('false')
            break
        else:    
            df = pd.DataFrame(ts) 
            df.to_csv('test.csv')            
            break

        #cbc_fname = os.path.join(
        #    workspace, "{}.cbc".format(name)
        #)
        """
        """
        strt: List = mf_utility.read_hds(
            hds_fname, "ALL", mask_val=-999.0
        )
        #plot head - time    
        # 要擷取的觀測井位置 index
        idxs = [
            (0, strt[0].shape[1] - 2, i)
            for i in range(strt[0].shape[2])
        ]
        tindex = np.arange(len(strt))
        mat = []
        for t in tindex:
            mat_sub = [
                strt[t][idx[0], idx[1], idx[2]]   # idx[0], idx[1], idx[2]
                for idx in idxs
            ]
            mat.append(mat_sub)
        df_strt = pd.DataFrame(
            mat, 
            columns=[
                str(idx[2])
                for idx in idxs
            ], 
            index=tindex
        )
        print(mat)
        """

        # 繪製網格
        fig = plt.figure(figsize=(30, 30))
        ax = fig.add_subplot(1, 1, 1, aspect="equal")
        mapview = flopy.plot.PlotMapView(model=mf)
        linecollection = mapview.plot_grid()
        t = ax.set_title("Wel Model Grid")
        jut.save_fig(os.path.join("pics", "CHD", "All_grid"))

        # 繪製ibound
        fig = plt.figure(figsize=(30, 30))
        ax = fig.add_subplot(1, 1, 1, aspect="equal")
        # ml.modelgrid.set_coord_info(angrot=-14)
        mapview = flopy.plot.PlotMapView(model=mf)
        quadmesh = mapview.plot_ibound()
        linecollection = mapview.plot_grid()
        t = ax.set_title("Wel Model Grid")
        jut.save_fig(os.path.join("pics", "CHD", "ibound"))

        # 繪製邊界條件
        bc_color_dict = {
            "default": "black",
            "WEL": "red",
            "DRN": "yellow",
            "RIV": "green",
            "GHB": "cyan",
            "CHD": "navy",
        }

        fig = plt.figure(figsize=(30, 30))
        ax = fig.add_subplot(1, 1, 1, aspect="equal")
        mapview = flopy.plot.PlotMapView(model=mf)
        quadmesh = mapview.plot_ibound()
        # quadmesh = mapview.plot_bc("RIV")
        # quadmesh = mapview.plot_bc("WEL")
        linecollection = mapview.plot_grid()
        t = ax.set_title("Wel Model Grid")
        jut.save_fig(os.path.join("pics", "CHD", "all"))

        """
        cbc_list: List = mf_utility.read_cbc(
            cbc_fname,
            "ALL",
            [
                "FLOW RIGHT FACE",
                "FLOW FRONT FACE",
                None,
            ],
        )
        """
        """
        print(type(strt))
        print(len(strt))
        print(type(strt[0]))
        print(strt[0].shape)

        plt.style.use("bmh")
        fig, ax = plt.subplots(1, figsize=(12, 9), sharex=True)
        legend = []
        for t in range(len(strt)):
            if t < 10:
                ax.plot(np.arange(strt[t].shape[2]), strt[t][0, 0, :], label=str(t))
                legend.append(str(t))
        ax.legend(legend, shadow=True)
        plt.tight_layout()
        jut.save_fig("strt")


        """
        """
        print(type(cbc_list))
        print(len(cbc_list))
        print(type(cbc_list[0]))
        print(cbc_list[0][0].shape, cbc_list[0][1].shape)
        """

        sys.exit()
