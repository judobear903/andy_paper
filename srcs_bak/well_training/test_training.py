"""
ANN 的流程
1. data I/O and preprocessing
2. ANN 的模型結構定義
    幾層, 幾個節點, 輸入輸出節點為何？
    Y = f(X)
    X: ht, K, S, Qt
    Y: ht+1
    如何以程式碼實質定義上述概念？
3. ANN training
4. ANN performance & postprocessing
    計算指標
    畫圖


訓練, 驗證, 預測

"""

# -*- coding: utf-8 -*-
import datetime
import os
import sys
import numpy as np
import pandas as pd
from typing import List
from sklearn.preprocessing import MinMaxScaler
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error

# import TensorFlow as tf
from keras.models import Sequential
from keras.layers import Dense, Dropout, LeakyReLU  # , arctanh
from keras.models import load_model
from keras import initializers
from keras.optimizers import SGD, Adam
import tensorflow.keras.backend as k
import pickle
from matplotlib import pyplot as plt


starttime = datetime.datetime.now()

# 引數
weight_fname = None  # 權重
export_weight_fname = None  #
export_model_fname = None  # 訓練好的model匯出
train_history_fname = None  # 訓練過程

# 讀取想要的資料夾內容
# argv[0]是引數是bat.裡面想要讀取的code
# argv[1]是引數是bat.裡面想要讀取放訓練資料夾的位置(資料夾裡是很多的.csv檔案)
# argv[2]是引數是bat.裡面
flist = [
    os.path.join(sys.argv[1], file)
    for file in os.listdir(sys.argv[1])
]

for argv in sys.argv[2:]:
    if argv.find("export_param=") >= 0:
        # ANN 輸出參數
        export_weight_fname = argv.replace(
            "param=", ""
        )  # 會輸出檔名(去.bat裡面看)
    elif argv.find("export_model=") >= 0:
        # ANN 模式輸出
        export_model_fname = argv.replace("=", "")

    elif argv.find("model=") >= 0:
        # ANN 前期模式讀取
        model_fname = argv.replace("model=", "")
    elif argv.find("model_weight=") >= 0:
        # ANN 前期權重讀取
        weight_fname = argv.replace("model_weight=", "")

    elif argv.find("model=") >= 0:
        # ANN 前期模式
        model_fname = argv.replace("model=", "")
    elif argv.find("train_history=") >= 0:  # 做幾次??
        # ANN 模式輸出
        train_history_fname = argv.replace("=", "")
    elif argv.find("epochs=") >= 0:
        # ANN 模式輸出
        epochs = int(argv.replace("epochs=", ""))
    elif argv.find("loss_history=") >= 0:
        # 模式loss history輸出
        loss_history = argv.replace("=", "")
    elif argv.find("mae_history=") >= 0:
        # 模式mae history輸出
        mae_history = argv.replace("=", "")

times = 1


# Design Network
"""
1.model.add(Dense....): 隱藏層
2.units=32:隱藏層神經元數量
3.activation='relu': 轉換函數

"""


def netwrok(
    xtrain, ytrain, lose_func, model_weight
):  # , epochs):
    model = Sequential()  # 寫死
    model.add(
        Dense(
            units=64,
            input_dim=xtrain.shape[1],
            kernel_initializer=model_weight,
            activation="relu",
        )
    )  ## sigmoid'relu'linear'tanh))
    model.add(
        Dense(
            units=64,
            input_dim=64,
            kernel_initializer=model_weight,
            activation="relu",
        )
    )
    #   model.add(Dropout(0.2)) #丟棄率(dropout rate)為 20% ,防止過擬合
    #   model.add(Dense(units=50, activation='tanh'))
    model.add(
        Dense(units=ytrain.shape[1], activation="linear")
    )  # , kernel_initializer= model_weight
    # model.add(Dropout(0.2))
    # ['mae'] 表示使用平均絕對誤差（MAE）作為評估模型性能的指標
    model.compile(
        loss=lose_func, optimizer="adam", metrics=["mae"]
    )  # ,'mape'])
    return model


# define lose function
def NSE(ytrain, xpred):
    return k.sum(k.square(xpred - ytrain)) / k.sum(
        k.square(
            ytrain - (k.mean(ytrain, axis=1, keepdims=True))
        )
    )


def MSE(ytrain, xpred):
    return k.mean(k.square(xpred - ytrain), axis=-1)

    # training
    if weight_fname is not None:
        model = load_model(
            model_fname, custom_objects={"NSE": NSE}
        )

    else:  # 變量可以被用作神經網絡模型的初始化權重
        model_weight = initializers.RandomNormal(
            mean=0.0, stddev=0.05, seed=None
        )
        model = netwrok(xtrain, ytrain, MSE, model_weight)

    train_history, scores = train(xtrain, ytrain, epochs)
